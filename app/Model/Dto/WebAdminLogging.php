<?php
/**
 * WebAdminLogging
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 10.04.2020
 * Time: 09:25
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

use App\Model\Enum\Logging\Action;
use Carbon\Carbon;

class WebAdminLogging
{

    /**
     * @var int|null
     */
    private $id;

    /**
     * @var int
     */
    private $accountId = 0;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $action;

    /**
     * @var Carbon
     */
    private $dateTime;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebAdminLogging
     */
    public function setId(?int $id): WebAdminLogging
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getAccountId(): int
    {
        return $this->accountId;
    }

    /**
     * @param int $accountId
     * @return WebAdminLogging
     */
    public function setAccountId(int $accountId): WebAdminLogging
    {
        $this->accountId = $accountId;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return WebAdminLogging
     */
    public function setName(string $name): WebAdminLogging
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return WebAdminLogging
     */
    public function setAction(string $action): WebAdminLogging
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getDateTime(): Carbon
    {
        return $this->dateTime;
    }

    /**
     * @param Carbon $dateTime
     * @return WebAdminLogging
     */
    public function setDateTime(Carbon $dateTime): WebAdminLogging
    {
        $this->dateTime = $dateTime;
        return $this;
    }
}
