<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 25.07.2020
 * Time: 16:48
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-balance-scale-right"></i>
    Rechte Managment
@endsection
@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col"></th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($groups as $group)
            <tr>
                <th scope="row">{{ $group->getId() }}</th>
                <td>{{ $group->getName() }}</td>
                <td>
                    <a href="{{ route('admin.permission.edit', $group->getId()) }}" class="btn btn-primary">
                        Bearbeiten
                    </a>
                </td>
                <td>
                    <form action="{{ route('admin.permission.delete', $group->getId()) }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE"/>
                        <button class="btn btn-danger" type="submit">Löschen</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <h2> Erstellen </h2>
    <form action="{{ route('admin.permission.create') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name">
        </div>
        <button type="submit" class="btn btn-dark">Erstellen</button>
    </form>
@endsection
