<?php
/**
 * WebAdminItemList
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 11.05.2020
 * Time: 22:03
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

use Carbon\Carbon;

class WebAdminItemList
{
    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var int
     */
    private $characterId = 0;

    /**
     * @var string
     */
    private $storage = '';

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var int
     */
    private $quantity = 0;

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @var null|Carbon
     */
    private $updatedAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebAdminItemList
     */
    public function setId(?int $id): WebAdminItemList
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCharacterId(): int
    {
        return $this->characterId;
    }

    /**
     * @param int $characterId
     * @return WebAdminItemList
     */
    public function setCharacterId(int $characterId): WebAdminItemList
    {
        $this->characterId = $characterId;
        return $this;
    }

    /**
     * @return string
     */
    public function getStorage(): string
    {
        return $this->storage;
    }

    /**
     * @param string $storage
     * @return WebAdminItemList
     */
    public function setStorage(string $storage): WebAdminItemList
    {
        $this->storage = $storage;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return WebAdminItemList
     */
    public function setName(string $name): WebAdminItemList
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return WebAdminItemList
     */
    public function setQuantity(int $quantity): WebAdminItemList
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return WebAdminItemList
     */
    public function setCreatedAt(?Carbon $createdAt): WebAdminItemList
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon|null $updatedAt
     * @return WebAdminItemList
     */
    public function setUpdatedAt(?Carbon $updatedAt): WebAdminItemList
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
