<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 20.11.2019
 * Time: 20:20
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('admin.layout.app')
@section('title')
    <i class="fab fa-wpforms"></i>
    Eintrag erstellen
@endsection
@section('content')
    <form action="{{ route('admin.supportForm.refund.store') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="userName">Name IC</label>
            <input type="text" class="form-control" id="userName" name="userName">
        </div>
        <div class="form-group">
            <label for="itemType">Gegenstands Typ</label>
            <input type="text" class="form-control" id="itemType" name="itemType">
        </div>
        <div class="form-group">
            <label for="amount">Betrag</label>
            <input type="number" class="form-control" id="amount" name="amount">
        </div>
        <div class="form-group">
            <label for="description">Grobe Beschreibung für den Ersatz</label>
            <textarea class="form-control" id="description" name="description"></textarea>
        </div>
        <br/>
        <button type="submit" class="btn btn-dark">Erstellen</button>
    </form>
@endsection
