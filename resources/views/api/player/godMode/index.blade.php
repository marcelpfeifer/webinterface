<?php
/**
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 13.04.2020
 * Time: 19:43
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('api.layout.app')
@section('title')
    <i class="fas fa-bible"></i>
    Aktiviert/Deaktiviert Godmode für einen Spieler
@endsection
@section('content')
    <form method="post" action="{{ route('api.player.godMode.save') }}">
        {{ csrf_field() }}

        @include('api.includes.select', [
         'name' => 'pid',
         'label' => 'Spieler',
         'entries' => $players,
         'currentSelect' => $currentPlayer,
       ])

        <div class="form-check">
            <input class="form-check-input" type="checkbox" id="active" name="active">
            <label class="form-check-label" for="active">
                Aktivieren?
            </label>
        </div>

        <button type="submit" class="btn btn-danger">Bestätigen</button>
    </form>
    <br/>
@endsection
