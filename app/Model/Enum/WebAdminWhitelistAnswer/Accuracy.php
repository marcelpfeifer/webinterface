<?php
/**
 * Accuracy
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 13.11.2021
 * Time: 15:17
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Enum\WebAdminWhitelistAnswer;

use App\Libraries\Enum\AEnum;

class Accuracy extends AEnum
{

    /**
     * @var string
     */
    const RIGHT = 'RIGHT';

    /**
     * @var string
     */
    const UNSURE = 'UNSURE';

    /**
     * @var string
     */
    const WRONG = 'WRONG';
}
