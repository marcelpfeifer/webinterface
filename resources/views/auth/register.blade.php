@extends('layouts.app')
@section('title')
    Registrieren
@endsection
@section('content')
    <form method="POST" action="{{ route('register') }}">
        @csrf
        <h2>Daten</h2>
        <div class="form-group row">
            <label for="username" class="col-md-4 col-form-label text-md-right">
                Benutzername
                <i class="fas fa-user"></i>
            </label>
            <div class="col-md-6">
                <input id="username" type="text"
                       class="form-control @error('username') is-invalid @enderror" name="username"
                       value="{{ old('username') }}" required>
                @error('username')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
        </div>
        <div class="form-group row">
            <label for="password" class="col-md-4 col-form-label text-md-right">
                Passwort
                <i class="fas fa-lock"></i>
            </label>
            <div class="col-md-6">
                <input id="password" type="password"
                       class="form-control @error('password') is-invalid @enderror" name="password"
                       required autocomplete="new-password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
        </div>
        <div class="form-group row">
            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">
                Bestätige Passwort
                <i class="fas fa-lock"></i>
            </label>

            <div class="col-md-6">
                <input id="password-confirm" type="password" class="form-control"
                       name="password_confirmation" required autocomplete="new-password">
            </div>
        </div>
        <div class="form-group row">
            <label for="code" class="col-md-4 col-form-label text-md-right">
                Registrierungscode
                <i class="fas fa-qrcode"></i>
            </label>
            <div class="col-md-6">
                <input id="code" type="text"
                       class="form-control @error('code') is-invalid @enderror" name="code"
                       value="{{ old('code') }}" required>
                @error('code')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
        </div>
        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-secondary">
                    Registrieren
                </button>
            </div>
        </div>
    </form>
@endsection
