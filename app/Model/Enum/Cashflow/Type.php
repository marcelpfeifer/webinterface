<?php
/**
 * Type
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 14.08.2020
 * Time: 17:15
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Enum\Cashflow;

class Type
{
    /**
     * @var string
     */
    const CASH = "CASH";

    /**
     * @var string
     */
    const BANK = "BANK";

    /**
     * @var string
     */
    const BITCOIN = "BITCOIN";

    /**
     * @var string
     */
    const BITCOIN_ILLEGAL = "BITCOIN_ILLEGAL";

    /**
     * @var string
     */
    const BLACKMONEY = "BLACKMONEY";
}
