<?php
/**
 * GiveMoneyController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 12.04.2020
 * Time: 12:28
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Api\Player;

use App\Http\Controllers\Api\AController;
use App\Http\Controllers\Api\IController;
use App\Http\Request\Api\Player\GiveMoney\Index;
use App\Http\Request\Api\Player\GiveMoney\Save;
use App\Http\Request\Dto\ADto;
use App\Libraries\Api\Player\GiveMoney;
use App\Libraries\Api\Player\Online;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class GiveMoneyController  extends AController implements IController
{
    /**
     * @param Request $request
     * @return View
     */
    public function index(Request $request): View
    {
        $validator = new Index($request);
        return $this->processIndex($validator, 'api.player.giveMoney.index');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function save(Request $request): RedirectResponse
    {
        $validator = new Save($request);
        return $this->processSave($validator, 'api.player.giveMoney.index');
    }

    /**
     * @param \App\Http\Request\Dto\Api\Player\GiveMoney\Index $requestDto
     * @return array
     */
    public function getIndexData(ADto $requestDto): array
    {
        return [
            'players'       => Online::getPlayers(),
            'currentPlayer' => $requestDto->getCurrentPlayer(),
        ];
    }

    /**
     * @param \App\Http\Request\Dto\Api\Player\GiveMoney\Save $requestDto
     * @return bool
     */
    public function execApiCall(ADto $requestDto): bool
    {
        return GiveMoney::call($requestDto->getPlayerId(), $requestDto->getAmount());
    }

    /**
     * @param \App\Http\Request\Dto\Api\Player\GiveMoney\Save $requestDto
     * @return array
     */
    public function getRedirectData(ADto $requestDto): array
    {
        return [
            'currentPlayer' => $requestDto->getPlayerId()
        ];
    }
}
