<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertPermissionData4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_CASH_FLOW => 'Spieler CashFlow ansehen',

        ];

        foreach ($data as $id => $name) {
            $dto = (new \App\Model\Dto\WebPermission())
                ->setId($id)
                ->setName($name);
            (new \App\Model\WebPermission())->insertEntry($dto);

            $dto = (new \App\Model\Dto\WebPermissionToWebPermission())
                ->setWebPermissionId($id)
                ->setWebPermissionGroupId(1);
            (new \App\Model\WebPermissionToWebPermission())->insertEntry($dto);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
