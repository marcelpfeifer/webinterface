<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 18.09.2020
 * Time: 17:46
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-bug"></i>
    Reportübersicht
@endsection
@section('content')
    <form action="{{ route('admin.report.index') }}" method="GET">
        <div class="form-row">
            <div class="form-group col-8">
                <input type="text" class="form-control" id="search"  name="search" placeholder="Suche" value="{{ $search }}">
            </div>
            <div class="form-group col-2">
                <button type="submit" class="btn btn-dark">Suchen</button>
            </div>
        </div>
    </form>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Spielername</th>
            <th scope="col">Spieler Position</th>
            <th scope="col">Uhrzeit</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($reports as $report)
            <tr>
                <th scope="row">{{ $report->getReport()->getId() }}</th>
                <td>{{  $report->getCharacter() ? $report->getCharacter()->getName() :  $report->getReport()->getGuid()}}</td>
                <td>{{ $report->getReport()->getPosX() }},{{ $report->getReport()->getPosY() }}, {{ $report->getReport()->getPosZ() }}</td>
                <td>{{ $report->getReport()->getCreatedAt()->format('Y-m-d H:i:s') }}</td>
                <td>
                    <a href="{{ route('admin.report.show', $report->getReport()->getId()) }}" class="btn btn-primary">
                        Anzeigen
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
