<?php
/**
 * Status
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 11.08.2021
 * Time: 16:17
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Api\Server\System;


use App\Libraries\Api\AClient;
use App\Model\WebConfig;

class Status extends AClient
{
    /**
     * @var string
     */
    const URL = 'system/status';

    /**
     * @return object|null
     */
    public static function call()
    {
        $response = self::sendRequest(
            'GET',
            WebConfig::getEntryByKey(\App\Model\Enum\WebConfig::API_SYSTEM_URL)->getValue() . self::URL,
            [],
            false
        );
        if ($response && $response->getStatusCode() == 200) {
            $content = $response->getBody()->getContents();
            return json_decode($content);
        }
        return null;
    }
}
