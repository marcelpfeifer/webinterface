<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 20.11.2019
 * Time: 20:56
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@php
    /** @var $entry \App\Model\Dto\WebAdminRefund */
    /** @var $user \App\Model\Dto\Account */
@endphp
@extends('admin.layout.app')
@section('title')
    <i class="fab fa-wpforms"></i>
    Eintrag {{ $entry->getId() }}
    <a class="btn btn-dark btn-sm float-right"
       href="{{ route('admin.supportForm.refund.index') }}"
       role="button">
        Zurück
    </a>
@endsection
@section('content')
    <dl class="row">
        <dt class="col-3">Datum</dt>
        <dd class="col-9">{{ $entry->getCreatedAt()->format('Y-m-d H:i:s') }}</dd>

        <dt class="col-3">Name IC</dt>
        <dd class="col-9">{{ $entry->getUserName() }}</dd>

        <dt class="col-3">Gegenstands Typ</dt>
        <dd class="col-9">{{ $entry->getItemType() }}</dd>

        <dt class="col-3">Betrag</dt>
        <dd class="col-9">{{ $entry->getAmount() }}</dd>

        <dt class="col-3">Bearbeitet von...</dt>
        <dd class="col-9">{{ $user->getUsername() }}</dd>

        <dt class="col-3">Grobe Beschreibung für den Ersatz</dt>
        <dd class="col-9">{{ $entry->getDescription() }}</dd>
    </dl>
@endsection
