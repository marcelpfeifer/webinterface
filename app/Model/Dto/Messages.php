<?php
/**
 * Messages
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 01.11.2019
 * Time: 14:16
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

use Carbon\Carbon;

class Messages
{
    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var string
     */
    private $numberFrom = '';

    /**
     * @var string
     */
    private $numberTo = '';

    /**
     * @var string
     */
    private $message = '';

    /**
     * @var string
     */
    private $date = '';

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Messages
     */
    public function setId(?int $id): Messages
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumberFrom(): string
    {
        return $this->numberFrom;
    }

    /**
     * @param string $numberFrom
     * @return Messages
     */
    public function setNumberFrom(string $numberFrom): Messages
    {
        $this->numberFrom = $numberFrom;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumberTo(): string
    {
        return $this->numberTo;
    }

    /**
     * @param string $numberTo
     * @return Messages
     */
    public function setNumberTo(string $numberTo): Messages
    {
        $this->numberTo = $numberTo;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return Messages
     */
    public function setMessage(string $message): Messages
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     * @return Messages
     */
    public function setDate(string $date): Messages
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return Messages
     */
    public function setCreatedAt(?Carbon $createdAt): Messages
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}
