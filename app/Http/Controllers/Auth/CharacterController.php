<?php
/**
 * CharacterController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 18.04.2020
 * Time: 10:56
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Auth;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Model\Dto\Character;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CharacterController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        if (!($account = Auth::user())) {
            $messages[] = (new Message())
                ->setMessage("Bitte anmelden")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::NOTICE);
            return redirect()->route('login')->with('messages', $messages);
        }
        $character = \App\Model\Character::find($account->id);
        return view('auth.character', [
            'dto' => $character ? \App\Model\Character::entryAsDto($character) : null
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function save(Request $request)
    {
        if (!($account = Auth::user())) {
            $messages[] = (new Message())
                ->setMessage("Bitte anmelden")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::NOTICE);
            return redirect()->route('login')->with('messages', $messages);
        }

        $character = \App\Model\Character::find($account->id);
        if (!($character)) {
            $messages[] = (new Message())
                ->setMessage("Character konnte nicht gefunden werden")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
            return redirect()->route('auth.dashboard')->with('messages', $messages);
        }

        $dto = \App\Model\Character::entryAsDto($character);

        $validator = $this->validateRequest($request, $dto);
        if ($validator->fails()) {
            $messages = [];
            foreach ($validator->errors()->all() as $error) {
                $messages[] = (new Message())
                    ->setMessage($error)
                    ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
            }
            return redirect()->route('auth.character.index')->with('messages',
                $messages)->withInput()->withErrors($validator->errors());
        }

        $dto->setServiceNumber($request->input('serviceNumber', ''))
            ->setServiceName($request->input('serviceName', ''));
        (new \App\Model\Character())->updateEntry($dto);

        $messages[] = (new Message())
            ->setMessage("Wurde aktualisiert")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('auth.character.index')->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param Character $dto
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validateRequest(Request $request, Character $dto)
    {
        $validator = Validator::make(
            $request->input(),
            [
                'serviceNumber' => ['integer', 'min:1', 'max:99',],
                'serviceName'   => ['string','nullable'],
            ],
            [
                'serviceNumber.integer' => 'Die Service Nummer darf nur aus Zahlen bestehen',
                'serviceNumber.min'     => 'Die Service Nummer darf nicht kleiner als 1 sein',
                'serviceNumber.max'     => 'Die Service Nummer darf nicht größer als 99 sein',
                'serviceName.string'    => 'Der Service Name muss ein Text sein',
            ]
        );

        $validator->after(function ($validator) use ($request, $dto) {
            if ($request->input('serviceName') && $this->isServiceNameUnique($request,$dto)) {
                $validator->errors()->add('serviceName', 'Den Service Name gibt es schon');
            }
            if ($request->input('serviceNumber') && $this->isServiceNumberUnique($request, $dto)) {
                $validator->errors()->add('serviceNumber', 'Die Service Nummer gibt es schon.');
            }
        });

        return $validator;
    }

    /**
     * @param Request $request
     * @param Character $dto
     * @return bool
     */
    private function isServiceNumberUnique(Request $request, Character $dto)
    {
        $db = new \App\Model\Character();
        $serviceNumber = $request->input('serviceNumber');
        if ($dto->getServiceNumber() == $serviceNumber) {
            return false;
        }
        return $db->doesServiceNumberExits($dto, $serviceNumber);
    }

    /**
     * @param Request $request
     * @param Character $dto
     * @return bool
     */
    private function isServiceNameUnique(Request $request, Character $dto)
    {
        $db = new \App\Model\Character();
        $serviceName = $request->input('serviceName');
        if ($dto->getServiceName() == $serviceName) {
            return false;
        }
        return $db->doesServiceNameExits($dto, $serviceName);
    }
}
