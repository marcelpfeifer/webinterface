<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'webNews',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('webNewsCategoryId')->nullable();
                $table->foreign('webNewsCategoryId')->references('id')->on('webNewsCategory')->onDelete('set null');
                $table->string('title');
                $table->string('image');
                $table->text('shortDescription');
                $table->longText('description');
                $table->string('author');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webNews');
    }
}
