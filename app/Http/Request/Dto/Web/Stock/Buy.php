<?php
/**
 * Buy
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 02.09.2021
 * Time: 14:17
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Web\Stock;


use App\Http\Request\Dto\ADto;
use App\Model\Dto\Character;
use App\Model\Dto\WebStock;

class Buy extends ADto
{

    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var WebStock
     */
    private $webStock;

    /**
     * @var Character
     */
    private $character;

    /**
     * @var int
     */
    private $amount = 0;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Buy
     */
    public function setId(int $id): Buy
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return WebStock
     */
    public function getWebStock(): WebStock
    {
        return $this->webStock;
    }

    /**
     * @param WebStock $webStock
     * @return Buy
     */
    public function setWebStock(WebStock $webStock): Buy
    {
        $this->webStock = $webStock;
        return $this;
    }

    /**
     * @return Character
     */
    public function getCharacter(): Character
    {
        return $this->character;
    }

    /**
     * @param Character $character
     * @return Buy
     */
    public function setCharacter(Character $character): Buy
    {
        $this->character = $character;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     * @return Buy
     */
    public function setAmount(int $amount): Buy
    {
        $this->amount = $amount;
        return $this;
    }
}
