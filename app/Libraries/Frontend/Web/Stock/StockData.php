<?php
/**
 * StockData
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 29.08.2021
 * Time: 18:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Web\Stock;


use App\Model\WebStockChange;
use Illuminate\Database\Eloquent\Model;

class StockData
{

    /**
     * @param int $webStockId
     * @return array[]
     */
    public static function getViewData(int $webStockId): array
    {
        $labels = [];
        $changes = [];
        $entries = (new WebStockChange())->getEntries($webStockId);
        foreach ($entries as $entry) {
            $labels[] = self::getLabel($entry);
            $changes[] = $entry->getPrice() ;
        }
        return [
            'labels'  => $labels,
            'changes' => $changes,
        ];
    }

    /**
     * @var array
     */
    private static $selectedFormats = [];

    /**
     * @param \App\Model\Dto\WebStockChange $dto
     * @return string
     */
    private static function getLabel(\App\Model\Dto\WebStockChange $dto): string
    {
        $carbon = $dto->getCreatedAt()
            ->setTimezone('Europe/Berlin');
        $currentDay = $carbon->format('Y-m-d');
        if (in_array($currentDay, self::$selectedFormats)) {
            return $carbon->format('H:i');
        }

        self::$selectedFormats[] = $currentDay;
        return $carbon->format('Y-m-d H:i');
    }
}
