<?php
/**
 * ItemStore
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 19.09.2020
 * Time: 09:20
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

use App\Libraries\Logging\ILogging;
use App\Libraries\Logging\Traits\Variables;

class ItemStore implements ILogging
{
    use Variables;

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var string
     */
    private $item = '';

    /**
     * @var int
     */
    private $price = 0;

    /**
     * @var int
     */
    private $stock = 0;

    /**
     * @var string
     */
    private $shop = '';

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return ItemStore
     */
    public function setId(?int $id): ItemStore
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getItem(): string
    {
        return $this->item;
    }

    /**
     * @param string $item
     * @return ItemStore
     */
    public function setItem(string $item): ItemStore
    {
        $this->item = $item;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @param int $price
     * @return ItemStore
     */
    public function setPrice(int $price): ItemStore
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     * @return ItemStore
     */
    public function setStock(int $stock): ItemStore
    {
        $this->stock = $stock;
        return $this;
    }

    /**
     * @return string
     */
    public function getShop(): string
    {
        return $this->shop;
    }

    /**
     * @param string $shop
     * @return ItemStore
     */
    public function setShop(string $shop): ItemStore
    {
        $this->shop = $shop;
        return $this;
    }
}
