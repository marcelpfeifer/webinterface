<?php
/**
 * Status
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 11.08.2021
 * Time: 14:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Api\Server;


use App\Libraries\Api\AClient;
use App\Model\WebConfig;

class Status extends AClient
{
    /**
     * @var string
     */
    const URL = '{type}/status';

    /**
     * @param string $type
     * @return bool
     */
    public static function call(string $type): bool
    {
        $response = self::sendRequest(
            'GET',
            self::getUrl($type),
            []
        );

        if ($response && $response->getStatusCode() == 200) {
            $content = $response->getBody()->getContents();
            $status = json_decode($content);
            return $status == 'true';
        }
        return false;
    }

    /**
     * @param string $type
     * @return array|string|string[]
     */
    private static function getUrl(string $type)
    {
        $url = WebConfig::getEntryByKey(\App\Model\Enum\WebConfig::API_SYSTEM_URL)->getValue() . self::URL;
        return str_replace('{type}', $type, $url);
    }
}
