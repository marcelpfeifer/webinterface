<?php
/**
 * TechnicalFaq.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 04.09.2019
 * Time: 21:21
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\SupportForm;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Model\Account;
use App\Model\WebAdminTechnicalFaq;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TechnicalFaqController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_SUPPORT_FORM_TECHNICAL_FAQ);
    }

    /**
     * @return View
     */
    public function index(): View
    {
        $entries = WebAdminTechnicalFaq::all();

        $result = [];
        foreach ($entries as $entry) {
            $dto = WebAdminTechnicalFaq::entryAsDto($entry);
            $user = Account::entryAsDto($entry->account);
            $result[] = [
                'dto' => $dto,
                'user' => $user,
            ];
        }

        return view('admin.supportForm.technicalFaq.index', [
            "entries" => $result
        ]);
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return view('admin.supportForm.technicalFaq.create');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $dto = (new \App\Model\Dto\WebAdminTechnicalFaq())
            ->setExplanation($request->input('explanation'))
            ->setErrorCode($request->input('errorCode'))
            ->setTroubleShooting($request->input('troubleShooting'))
            ->setTroubleShooting2($request->input('troubleShooting2'))
            ->setFixed((bool)$request->input('fixed'))
            ->setUserId(Auth::user()->{Account::COLUMN_ID});

        (new WebAdminTechnicalFaq())->insertEntry($dto);
        Logging::insert(Name::SUPPORT_FORM_FAQ, Auth::id(), $dto);

        $messages[] = (new Message())
            ->setMessage("Eintrag wurde erstellt")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.supportForm.technicalFaq.index')->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return View
     */
    public function show(Request $request, int $id): View
    {
        $entry = WebAdminTechnicalFaq::find($id);
        $dto = WebAdminTechnicalFaq::entryAsDto($entry);
        $user = Account::entryAsDto($entry->account);

        return view('admin.supportForm.technicalFaq.show', [
            'entry' => $dto,
            'user'  => $user
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function solve(Request $request, int $id): RedirectResponse
    {
        $dto = WebAdminTechnicalFaq::entryAsDto(WebAdminTechnicalFaq::find($id));
        $oldDto = clone $dto;
        $dto->setFixed(true);
        (new WebAdminTechnicalFaq())->updateEntry($dto);
        Logging::update(Name::SUPPORT_FORM_FAQ, Auth::id(), $oldDto, $dto);

        $messages[] = (new Message())
            ->setMessage("Eintrag wurde auf gelöst gesetzt")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.supportForm.technicalFaq.index')->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(Request $request, int $id): RedirectResponse
    {
        $entry = WebAdminTechnicalFaq::find($id);
        $dto = WebAdminTechnicalFaq::entryAsDto($entry);

        (new WebAdminTechnicalFaq())->deleteEntry($dto);
        Logging::delete(Name::SUPPORT_FORM_FAQ, Auth::id(), $dto);

        $messages[] = (new Message())
            ->setMessage("Eintrag wurde gelöscht")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.supportForm.technicalFaq.index')->with('messages', $messages);
    }
}
