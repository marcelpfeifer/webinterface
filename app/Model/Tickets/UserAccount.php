<?php
/**
 * UserAccount
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 27.08.2020
 * Time: 19:30
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Tickets;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserAccount extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_USER_ID = 'user_id';

    /**
     * @var string
     */
    const COLUMN_STATUS = 'status';

    /**
     * @var string
     */
    const COLUMN_USERNAME = 'username';

    /**
     * @var string
     */
    const COLUMN_PASSWORD = 'passwd';

    /**
     * @var string
     */
    const COLUMN_REGISTERED = 'registered';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'ost_user_account';

    /**
     * @var string
     */
    protected $connection = 'mysqlTickets';

    /**
     * @param Dto\UserAccount $dto
     */
    public function insertEntry(Dto\UserAccount $dto)
    {
        $this->insert(
            [
                self::COLUMN_ID         => $dto->getId(),
                self::COLUMN_USER_ID    => $dto->getUserId(),
                self::COLUMN_STATUS     => $dto->getStatus(),
                self::COLUMN_USERNAME   => $dto->getUsername(),
                self::COLUMN_PASSWORD   => $dto->getPassword(),
                self::COLUMN_REGISTERED => Carbon::now(),
            ]
        );
    }

    public function truncateTable()
    {
        self::truncate();
    }
}
