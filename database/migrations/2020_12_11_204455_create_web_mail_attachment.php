<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebMailAttachment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webMailAttachment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('webMailId')->nullable();
            $table->foreign('webMailId')->references('id')->on('webMail')->onDelete('cascade');
            $table->string('name');
            $table->string('path');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webMailAttachment');
    }
}
