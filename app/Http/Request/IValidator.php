<?php
/**
 * IValidator
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 16.09.2021
 * Time: 18:55
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request;


interface IValidator
{

    /**
     * @return array
     */
    public function getRules(): array;

    /**
     * @return array
     */
    public function getMessages(): array;
}
