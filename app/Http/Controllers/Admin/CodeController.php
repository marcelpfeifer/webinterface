<?php
/**
 * CodeController.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 24.08.2019
 * Time: 15:25
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Http\Model\RandomString;
use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Model\WebRegistrationCodes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class CodeController extends Controller
{

    /**
     * @return View
     */
    public function index(): View
    {
        $codes = WebRegistrationCodes::all();
        return view('admin.code.index', [
            'codes' => $codes
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|View
     */
    public function generate()
    {
        $db = new WebRegistrationCodes();
        $code = '';
        while (true) {
            $code = RandomString::generate();
            if ($db->getEntryByCode($code)) {
                continue;
            }
            $dto = (new \App\Model\Dto\WebRegistrationCodes())
                ->setCode($code);
            $db->insertEntry($dto);
            Logging::insert(Name::REGISTRATION_CODE, Auth::id(), $dto);
            break;
        }

        $messages[] = (new Message())
            ->setMessage("Es wurde folgender Code generiert: {$code}")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->back()->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|View
     */
    public function delete(Request $request, int $id)
    {
        $dto = (new \App\Model\Dto\WebRegistrationCodes())->setId($id);
        (new WebRegistrationCodes())->deleteEntry($dto);
        Logging::delete(Name::REGISTRATION_CODE, Auth::id(), $dto);

        $messages[] = (new Message())
            ->setMessage("Der Code wurde gelöscht")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->back()->with('messages', $messages);
    }
}
