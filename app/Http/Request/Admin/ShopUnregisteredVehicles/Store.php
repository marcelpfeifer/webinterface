<?php
/**
 * Store
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 12.10.2021
 * Time: 15:29
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Admin\ShopUnregisteredVehicles;

use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;
use App\Libraries\Common\Enum\Db\Length;

class Store extends AValidator implements IValidator
{
    /**
     * @return string[]
     */
    public function getRules(): array
    {
        return [
            'model'       => [
                'required',
                'string',
                'max:' . Length::VARCHAR,
            ],
            'stock'       => [
                'required',
                'int',
                'max:' . Length::INTEGER,
            ],
            'posX'        => [
                'required',
                'string',
                'max:' . Length::VARCHAR,
            ],
            'posY'        => [
                'required',
                'string',
                'max:' . Length::VARCHAR,
            ],
            'posZ'        => [
                'required',
                'string',
                'max:' . Length::VARCHAR,
            ],
            'rotX'        => [
                'required',
                'string',
                'max:' . Length::VARCHAR,
            ],
            'rotY'        => [
                'required',
                'string',
                'max:' . Length::VARCHAR,
            ],
            'rotZ'        => [
                'required',
                'string',
                'max:' . Length::VARCHAR,
            ],
            'mapId'       => [
                'required',
                'int',
                'max:' . Length::INTEGER,
            ],
            'npcPosX'     => [
                'required',
                'string',
                'max:' . Length::VARCHAR,
            ],
            'npcPosY'     => [
                'required',
                'string',
                'max:' . Length::VARCHAR,
            ],
            'npcPosZ'     => [
                'required',
                'string',
                'max:' . Length::VARCHAR,
            ],
            'npcRotation' => [
                'required',
                'string',
                'max:' . Length::VARCHAR,
            ],
            'npcModel'    => [
                'required',
                'string',
                'max:' . Length::VARCHAR,
            ],
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Admin\ShopUnregisteredVehicles\Store())
            ->setModel($this->request->input('model'))
            ->setStock($this->request->input('stock'))
            ->setPositionX($this->request->input('posX'))
            ->setPositionY($this->request->input('posY'))
            ->setPositionZ($this->request->input('posZ'))
            ->setRotationX($this->request->input('rotX'))
            ->setRotationY($this->request->input('rotY'))
            ->setRotationZ($this->request->input('rotZ'))
            ->setMapId($this->request->input('mapId'))
            ->setNpcPositionX($this->request->input('npcPosX'))
            ->setNpcPositionY($this->request->input('npcPosY'))
            ->setNpcPositionZ($this->request->input('npcPosZ'))
            ->setNpcRotation($this->request->input('npcRotation'))
            ->setNpcModel($this->request->input('npcModel'));
    }
}
