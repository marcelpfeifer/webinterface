<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.10.2021
 * Time: 14:08
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Api\Server\Server;

use App\Http\Request\Dto\ADto;

class Save extends ADto
{

    /**
     * @var string
     */
    private $action = '';

    /**
     * @var string
     */
    private $type = '';

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return Save
     */
    public function setAction(string $action): Save
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Save
     */
    public function setType(string $type): Save
    {
        $this->type = $type;
        return $this;
    }
}
