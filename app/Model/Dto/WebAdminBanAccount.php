<?php
/**
 * WebAdminBanAccount
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 05.09.2020
 * Time: 14:16
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;


use App\Libraries\Logging\ILogging;
use App\Libraries\Logging\Traits\Variables;
use Carbon\Carbon;

class WebAdminBanAccount implements ILogging
{
    use Variables;

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var int
     */
    private $accountId = 0;

    /**
     * @var int
     */
    private $points = 0;

    /**
     * @var string
     */
    private $reason = '';

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @var null|Carbon
     */
    private $updatedAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebAdminBanAccount
     */
    public function setId(?int $id): WebAdminBanAccount
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getAccountId(): int
    {
        return $this->accountId;
    }

    /**
     * @param int $accountId
     * @return WebAdminBanAccount
     */
    public function setAccountId(int $accountId): WebAdminBanAccount
    {
        $this->accountId = $accountId;
        return $this;
    }

    /**
     * @return int
     */
    public function getPoints(): int
    {
        return $this->points;
    }

    /**
     * @param int $points
     * @return WebAdminBanAccount
     */
    public function setPoints(int $points): WebAdminBanAccount
    {
        $this->points = $points;
        return $this;
    }

    /**
     * @return string
     */
    public function getReason(): string
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     * @return WebAdminBanAccount
     */
    public function setReason(string $reason): WebAdminBanAccount
    {
        $this->reason = $reason;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return WebAdminBanAccount
     */
    public function setCreatedAt(?Carbon $createdAt): WebAdminBanAccount
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon|null $updatedAt
     * @return WebAdminBanAccount
     */
    public function setUpdatedAt(?Carbon $updatedAt): WebAdminBanAccount
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
