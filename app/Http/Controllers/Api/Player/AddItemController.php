<?php
/**
 * AddItemController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 11.04.2020
 * Time: 13:58
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Api\Player;

use App\Http\Controllers\Api\AController;
use App\Http\Controllers\Api\IController;
use App\Http\Request\Api\Player\AddItem\Index;
use App\Http\Request\Api\Player\AddItem\Save;
use App\Http\Request\Dto\ADto;
use App\Libraries\Api\Player\AddItem;
use App\Libraries\Api\Player\Online;
use App\Libraries\Api\Server\GetItems;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AddItemController extends AController implements IController
{

    /**
     * @param Request $request
     * @return View
     */
    public function index(Request $request): View
    {
        $validator = new Index($request);
        return $this->processIndex($validator, 'api.player.addItem.index');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function save(Request $request): RedirectResponse
    {
        $validator = new Save($request);
        return $this->processSave($validator, 'api.player.addItem.index');
    }

    /**
     * @param \App\Http\Request\Dto\Api\Player\AddItem\Index $requestDto
     * @return array
     */
    public function getIndexData(ADto $requestDto): array
    {
        return [
            'players'       => Online::getPlayers(),
            'currentPlayer' => $requestDto->getCurrentPlayer(),
            'items'         => GetItems::get(),
        ];
    }

    /**
     * @param \App\Http\Request\Dto\Api\Player\AddItem\Save $requestDto
     * @return bool
     */
    public function execApiCall(ADto $requestDto): bool
    {
        return AddItem::call($requestDto->getPlayerId(), $requestDto->getItem(), $requestDto->getAmount());
    }

    /**
     * @param \App\Http\Request\Dto\Api\Player\AddItem\Save $requestDto
     * @return array
     */
    public function getRedirectData(ADto $requestDto): array
    {
        return [
            'currentPlayer' => $requestDto->getPlayerId()
        ];
    }
}
