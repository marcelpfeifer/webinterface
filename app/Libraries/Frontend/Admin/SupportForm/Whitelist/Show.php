<?php
/**
 * Show
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 13.11.2021
 * Time: 15:46
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Admin\SupportForm\Whitelist;

use App\Model\Account;
use App\Model\WebAdminWhitelist;
use App\Model\WebAdminWhitelistAnswer;
use App\Model\WebAdminWhitelistQuestion;
use Illuminate\Support\Facades\Log;

class Show
{
    /**
     * @param int $id
     * @return \App\Libraries\Frontend\Dto\Admin\SupportForm\Whitelist\Show
     */
    public static function getViewData(int $id): \App\Libraries\Frontend\Dto\Admin\SupportForm\Whitelist\Show
    {
        $entry = WebAdminWhitelist::find($id);
        $questions = [];
        foreach (WebAdminWhitelistQuestion::all() as $question) {
            $questions[] = WebAdminWhitelistQuestion::entryAsDto($question);
        }

        $answers = [];
        foreach ($entry->answers as $answer) {
            $dto = WebAdminWhitelistAnswer::entryAsDto($answer);
            $answers[$dto->getWebAdminWhitelistQuestionId()] = $dto;
        }
        $dto = WebAdminWhitelist::entryAsDto($entry);
        $user = Account::entryAsDto($entry->account);

        return (new \App\Libraries\Frontend\Dto\Admin\SupportForm\Whitelist\Show())
            ->setQuestions($questions)
            ->setAnswers($answers)
            ->setUser($user)
            ->setEntry($dto);
    }
}
