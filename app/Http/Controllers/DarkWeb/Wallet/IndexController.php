<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 15.08.2020
 * Time: 10:38
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\DarkWeb\Wallet;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Http\Request\DarkWeb\Wallet\Exchange;
use App\Http\Request\DarkWeb\Wallet\Transfer;
use App\Libraries\Frontend\DarkWeb\Wallet;
use App\Model\Character;
use App\Model\Transactions;
use Illuminate\Http\Request;

class IndexController extends Controller
{

    /**
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, string $hash)
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('darkWeb.index', $hash);
        }
        $characterDto = Character::entryAsDto($character);
        return view(
            'darkWeb.wallet.index',
            [
                'hash'      => $hash,
                'character' => $characterDto,
            ]
        );
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(Request $request, string $hash)
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('darkWeb.index', $hash);
        }
        $characterDto = Character::entryAsDto($character);
        $worked = Wallet::create($characterDto);
        if ($worked) {
            $messages[] = (new Message())
                ->setMessage("Wurde erstellt!")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        } else {
            $messages[] = (new Message())
                ->setMessage("Konnte nicht erstellt werden!")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
        }

        return redirect()->route('darkWeb.wallet.index', $hash)->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function transactions(Request $request, string $hash)
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('darkWeb.index', $hash);
        }
        $characterDto = Character::entryAsDto($character);

        return view(
            'darkWeb.wallet.transactions',
            [
                'hash'         => $hash,
                'character'    => $characterDto,
                'transactions' => (new Transactions())->getTransactionsBitcoinIllegalByGuid($characterDto->getId())
            ]
        );
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function transfer(Request $request, string $hash)
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('darkWeb.index', $hash);
        }
        $characterDto = Character::entryAsDto($character);
        return view(
            'darkWeb.wallet.transfer',
            [
                'hash'      => $hash,
                'character' => $characterDto,
            ]
        );
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\Http\RedirectResponse
     */
    public function transferPost(Request $request, string $hash)
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('darkWeb.index', $hash);
        }
        $characterDto = Character::entryAsDto($character);

        // Validiere die Anfrage
        $validator = (new Transfer($request, $characterDto->getId()));
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            $messages = [];
            foreach ($validatedData->errors()->all() as $error) {
                $messages[] = (new Message())
                    ->setMessage($error)
                    ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
            }
            return redirect()->route('darkWeb.wallet.transfer', $hash)
                ->with('messages', $messages)
                ->withInput()
                ->withErrors($validatedData->errors());
        }

        $worked = Wallet::transfer($characterDto, $validator->toDto());
        if ($worked) {
            $messages[] = (new Message())
                ->setMessage("Wurde transferiert")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        } else {
            $messages[] = (new Message())
                ->setMessage("Konnte nicht transferiert werden!")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
        }

        return redirect()->route('darkWeb.wallet.transfer', $hash)->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function exchange(Request $request, string $hash)
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('darkWeb.index', $hash);
        }
        $characterDto = Character::entryAsDto($character);
        return view(
            'darkWeb.wallet.exchange',
            [
                'hash'      => $hash,
                'character' => $characterDto,
            ]
        );
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\Http\RedirectResponse
     */
    public function exchangePost(Request $request, string $hash)
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('darkWeb.index', $hash);
        }
        $characterDto = Character::entryAsDto($character);

        // Validiere die Anfrage
        $validator = (new Exchange($request, $characterDto->getId()));
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            $messages = [];
            foreach ($validatedData->errors()->all() as $error) {
                $messages[] = (new Message())
                    ->setMessage($error)
                    ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
            }
            return redirect()->route('darkWeb.wallet.exchange', $hash)
                ->with('messages', $messages)
                ->withInput()
                ->withErrors($validatedData->errors());
        }

        $worked = Wallet::exchange($characterDto, $validator->toDto());
        if ($worked) {
            $messages[] = (new Message())
                ->setMessage("Wurde umgewandelt")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        } else {
            $messages[] = (new Message())
                ->setMessage("Konnte nicht umgewandelt werden!")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
        }

        return redirect()->route('darkWeb.wallet.exchange', $hash)->with('messages', $messages);
    }
}
