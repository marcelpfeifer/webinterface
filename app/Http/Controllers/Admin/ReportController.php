<?php
/**
 * ReportController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 18.09.2020
 * Time: 17:46
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Model\Report;
use App\Model\ReportValues;
use App\Model\WebBugReport;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReportController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $search = $request->input('search', '');
        $reports = (new Report())->getReports($search);
        return view(
            'admin.report.index',
            [
                'reports' => $reports,
                'search'  => $search,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function show(Request $request, int $id)
    {
        $reportEntry = Report::find($id);
        if (!$reportEntry) {
            return view('404', []);
        }
        $report = Report::entryAsDto($reportEntry);
        $reportValues = (new ReportValues())->getEntriesByCaseId($id);

        return view(
            'admin.report.show',
            [
                'report'       => $report,
                'reportValues' => $reportValues,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function delete(Request $request, int $id): RedirectResponse
    {
        $entry = Report::find($id);
        if (!($entry)) {
            return redirect()->route('admin.report.index');
        }

        $dto = Report::entryAsDto($entry);
        (new Report())->deleteEntry($dto);

        Logging::delete(Name::REPORT, Auth::id(), $dto);

        $messages[] = (new Message())
            ->setMessage("Der Eintrag wurde gelöscht")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.report.index')->with('messages', $messages);
    }
}
