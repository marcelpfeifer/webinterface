<?php
/**
 * VehicleShopController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 29.04.2020
 * Time: 16:33
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Http\Request\Admin\ShopUnregisteredVehicles\Store;
use App\Http\Request\Admin\ShopUnregisteredVehicles\Update;
use App\Libraries\Api\Server\GetVehicles;
use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Model\ShopUnregisteredVehicles;
use App\Model\VehicleStore;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

/**
 * Class VehicleShopUnregisteredController
 * @package App\Http\Controllers\Admin
 */
class VehicleShopUnregisteredController extends Controller
{
    public function __construct()
    {
        $this->middleware(
            'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_VEHICLE_SHOP
        );
    }

    /**
     * @return\Illuminate\View\View
     */
    public function index(): View
    {
        $results = [];
        foreach (ShopUnregisteredVehicles::all() as $vehicle) {
            $results[] = ShopUnregisteredVehicles::entryAsDto($vehicle);
        }
        $vehicles = GetVehicles::get();
        return view(
            'admin.vehicleShopUnregistered.index',
            [
                'results'  => $results,
                'vehicles' => $vehicles,
            ]
        );
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create(): View
    {
        $vehicles = GetVehicles::get();
        return view(
            'admin.vehicleShopUnregistered.create',
            [
                'vehicles' => $vehicles,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function show(Request $request, int $id): View
    {
        $vehicles = GetVehicles::get();
        $entry = ShopUnregisteredVehicles::find($id);
        $dto = ShopUnregisteredVehicles::entryAsDto($entry);

        return view(
            'admin.vehicleShopUnregistered.show',
            [
                'dto'      => $dto,
                'vehicles' => $vehicles,
            ]
        );
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        // Validiere die Anfrage
        $validator = (new Store($request));
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            return redirect()->route('admin.vehicleShopUnregistered.create')
                ->with('messages', $validator->getErrorsAsMessageArray($validatedData))
                ->withInput()
                ->withErrors($validatedData->errors());
        }

        /**
         * @var $requestDto \App\Http\Request\Dto\Admin\ShopUnregisteredVehicles\Store
         */
        $requestDto = $validator->toDto();

        $saved = \App\Libraries\Frontend\Admin\VehicleShopUnregistered\Store::save($requestDto);

        $messages = [];
        if ($saved) {
            $messages[] = (new Message())
                ->setMessage("Eintrag wurde erstellt")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        } else {
            $messages[] = (new Message())
                ->setMessage("Eintrag konnte nicht erstellt werden")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
        }
        return redirect()->route('admin.vehicleShopUnregistered.index')->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id):RedirectResponse
    {
        // Validiere die Anfrage
        $validator = (new Update($request, $id));
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            return redirect()->route('admin.vehicleShopUnregistered.show', $id)
                ->with('messages', $validator->getErrorsAsMessageArray($validatedData))
                ->withInput()
                ->withErrors($validatedData->errors());
        }

        /**
         * @var $requestDto \App\Http\Request\Dto\Admin\ShopUnregisteredVehicles\Update
         */
        $requestDto = $validator->toDto();

        $saved = \App\Libraries\Frontend\Admin\VehicleShopUnregistered\Update::save($requestDto);

        $messages = [];
        if ($saved) {
            $messages[] = (new Message())
                ->setMessage("Eintrag wurde aktualisiert")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        } else {
            $messages[] = (new Message())
                ->setMessage("Eintrag konnte nicht aktualisiert werden")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
        }
        return redirect()->route('admin.vehicleShopUnregistered.index')->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(Request $request, int $id): RedirectResponse
    {
        $entry = ShopUnregisteredVehicles::find($id);
        $dto = ShopUnregisteredVehicles::entryAsDto($entry);
        (new ShopUnregisteredVehicles())->deleteEntry($dto);
        Logging::delete(Name::VEHICLE_STORE_UNREGISTERED, Auth::id(), $dto);
        $messages[] = (new Message())
            ->setMessage("Eintrag wurde gelöscht")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.vehicleShopUnregistered.index')->with('messages', $messages);
    }
}
