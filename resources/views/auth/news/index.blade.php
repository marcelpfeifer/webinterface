<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 12.08.2021
 * Time: 12:49
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

/**
 * @var $address \App\Model\Dto\WebMailAddress\WithWebMailAddressToAccount
 */
?>
@extends('layouts.app')
@section('title')
    <i class="fas fa-newspaper"></i>
    News
    @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_NEWS_EDIT)
    <a href="{{ route('auth.news.create') }}" class="btn btn-secondary float-right">
        Neu
    </a>
    @endpermission
    @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_NEWS_CATEGORY)
    <a href="{{ route('auth.news.category.index') }}" class="btn mr-1 float-right btn-secondary">
        Zu den Kategorien
    </a>
    @endpermission
@endsection
@section('content')
    <table class="table table-striped mt-2">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col">Kurzbeschreibung</th>
            @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_NEWS_EDIT)
            <th scope="col"></th>
            @endpermission
            @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_NEWS_EDIT_DELETE)
            <th scope="col"></th>
            @endpermission
        </tr>
        </thead>
        <tbody>
        @foreach($news as $entry)
            <tr>
                <td>{{ $entry->getId() }}</td>
                <td>{{ $entry->getTitle() }}</td>
                <td>{{ $entry->getShortDescription() }}</td>
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_NEWS_EDIT)
                <td>

                    <a href="{{ route('auth.news.edit', $entry->getId()) }}"
                       class="text-primary">
                        Bearbeiten
                    </a>
                </td>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_NEWS_EDIT_DELETE)
                <td>
                    <form action="{{ route('auth.news.delete', $entry->getId()) }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE"/>
                        <button class="btn" type="submit"><i class="text-danger fas fa-trash"></i>
                        </button>
                    </form>
                </td>
                @endpermission
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
