<?php
/**
 * CallHistory
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 24.11.2020
 * Time: 15:17
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;


use Carbon\Carbon;

class CallHistory
{

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var string
     */
    private $numberFrom = '';

    /**
     * @var string
     */
    private $numberTo = '';

    /**
     * @var string
     */
    private $date = '';

    /**
     * @var bool
     */
    private $suppressed = false;

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return CallHistory
     */
    public function setId(?int $id): CallHistory
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumberFrom(): string
    {
        return $this->numberFrom;
    }

    /**
     * @param string $numberFrom
     * @return CallHistory
     */
    public function setNumberFrom(string $numberFrom): CallHistory
    {
        $this->numberFrom = $numberFrom;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumberTo(): string
    {
        return $this->numberTo;
    }

    /**
     * @param string $numberTo
     * @return CallHistory
     */
    public function setNumberTo(string $numberTo): CallHistory
    {
        $this->numberTo = $numberTo;
        return $this;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     * @return CallHistory
     */
    public function setDate(string $date): CallHistory
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSuppressed(): bool
    {
        return $this->suppressed;
    }

    /**
     * @param bool $suppressed
     * @return CallHistory
     */
    public function setSuppressed(bool $suppressed): CallHistory
    {
        $this->suppressed = $suppressed;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return CallHistory
     */
    public function setCreatedAt(?Carbon $createdAt): CallHistory
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}
