<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 26.07.2020
 * Time: 14:57
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('darkWeb.layout.app')
@section('url')
    {{  md5('https://eternityV.de/weirdImages') }}
@endsection
@section('content')

    <div class="row imageWeird">
        @foreach($images as $image)
            <div class="col-12">
                <img class="img-fluid m-2" src="{{$image}}">
            </div>
        @endforeach
    </div>
@endsection
