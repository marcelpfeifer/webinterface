<?php
/**
 * Customization
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.01.2021
 * Time: 15:47
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Vehicle;


use App\Model\Dto\Vehicle;

class Customization
{
    /**
     * @var int
     */
    public const ENGINE = 11;

    /**
     * @var int
     */
    public const BRAKES = 12;

    /**
     * @var int
     */
    public const HORNS = 14;

    /**
     * @var int
     */
    public const SUSPENSION = 15;

    /**
     * @var int
     */
    public const ARMOR = 16;

    /**
     * @var int
     */
    public const TURBO = 18;

    /**
     * @var int
     */
    public const CUSTOM_TIRE_SMOKE = 20;

    /**
     * @var int
     */
    public const XENON = 22;

    /**
     * @param Vehicle $vehicle
     * @return Dto\Customization
     */
    public static function fromJson(Vehicle $vehicle): Dto\Customization
    {
        $json = json_decode($vehicle->getCustomization());
        return (new Dto\Customization())
            ->setEngine($json->{self::ENGINE} ?? 0)
            ->setBrakes($json->{self::BRAKES} ?? 0)
            ->setHorns($json->{self::HORNS} ?? 0)
            ->setSuspension($json->{self::SUSPENSION} ?? 0)
            ->setArmor($json->{self::ARMOR} ?? 0)
            ->setTurbo($json->{self::TURBO} ?? 0)
            ->setCustomTireSmoke($json->{self::CUSTOM_TIRE_SMOKE} ?? 0)
            ->setXenon($json->{self::XENON} ?? 0);
    }

    /**
     * @param Dto\Customization $customization
     * @param Vehicle $vehicle
     * @return string
     */
    public static function toJson(Dto\Customization $customization, Vehicle $vehicle): string
    {
        $json = json_decode($vehicle->getCustomization());
        $json->{self::ENGINE} = $customization->getEngine();
        $json->{self::BRAKES} = $customization->getBrakes();
        $json->{self::HORNS} = $customization->getHorns();
        $json->{self::SUSPENSION} = $customization->getSuspension();
        $json->{self::ARMOR} = $customization->getArmor();
        $json->{self::TURBO} = $customization->getTurbo();
        $json->{self::CUSTOM_TIRE_SMOKE} = $customization->getCustomTireSmoke();
        $json->{self::XENON} = $customization->getXenon();
        return json_encode($json);
    }
}
