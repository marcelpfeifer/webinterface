<?php
/**
 * OrderBy
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 16.09.2021
 * Time: 18:48
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Enum\Hunt\Leaderboard;


use App\Libraries\Enum\AEnum;

class OrderBy extends AEnum
{

    /**
     * @var string
     */
    const ANIMAL_COUNT = 'animalCount';

    /**
     * @var string
     */
    const TOTAL_WEIGHT = 'totalWeight';

    /**
     * @var string
     */
    const DISTANCE = 'distance';
}
