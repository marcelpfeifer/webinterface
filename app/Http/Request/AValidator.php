<?php
/**
 * AValidator
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 07.08.2020
 * Time: 21:58
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request;

use App\Dto\Message;
use App\Http\Request\Dto\ADto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

abstract class AValidator implements IValidator
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var null|int
     */
    protected $id = null;

    /**
     * AValidator constructor.
     * @param Request $request
     * @param int|null $id
     */
    public function __construct(Request $request, int $id = null)
    {
        $this->request = $request;
        $this->id = $id;
    }

    /**
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validate(): \Illuminate\Contracts\Validation\Validator
    {
        return $this->getValidator();
    }

    /**
     * @param \Illuminate\Contracts\Validation\Validator $validator
     * @return Message[]
     */
    public function getErrorsAsMessageArray(\Illuminate\Contracts\Validation\Validator $validator): array
    {
        $messages = [];
        foreach ($validator->errors()->all() as $error) {
            $messages[] = (new Message())
                ->setMessage($error)
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
        }
        return $messages;
    }

    /**
     * @return \Illuminate\Contracts\Validation\Validator
     */
    private function getValidator(): \Illuminate\Contracts\Validation\Validator
    {
        return Validator::make(
            $this->request->all(),
            $this->getRules(),
            $this->getMessages()
        );
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return new ADto();
    }
}
