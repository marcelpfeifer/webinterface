<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebAdminItemListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webAdminItemList', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('characterId')->index();
            $table->enum('storage', [
                'PERSON',
                'HOUSE',
                'VEHICLE'
            ]);
            $table->string('name')->index();
            $table->integer('quantity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webAdminItemList');
    }
}
