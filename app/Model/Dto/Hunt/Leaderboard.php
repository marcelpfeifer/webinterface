<?php
/**
 * Leaderboard
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 16.09.2021
 * Time: 18:39
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto\Hunt;


class Leaderboard
{

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var int
     */
    private $animalCount = 0;

    /**
     * @var int
     */
    private $totalWeight = 0;

    /**
     * @var int
     */
    private $distance = 0;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Leaderboard
     */
    public function setName(string $name): Leaderboard
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getAnimalCount(): int
    {
        return $this->animalCount;
    }

    /**
     * @param int $animalCount
     * @return Leaderboard
     */
    public function setAnimalCount(int $animalCount): Leaderboard
    {
        $this->animalCount = $animalCount;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalWeight(): int
    {
        return $this->totalWeight;
    }

    /**
     * @param int $totalWeight
     * @return Leaderboard
     */
    public function setTotalWeight(int $totalWeight): Leaderboard
    {
        $this->totalWeight = $totalWeight;
        return $this;
    }

    /**
     * @return int
     */
    public function getDistance(): int
    {
        return $this->distance;
    }

    /**
     * @param int $distance
     * @return Leaderboard
     */
    public function setDistance(int $distance): Leaderboard
    {
        $this->distance = $distance;
        return $this;
    }
}
