<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.01.2021
 * Time: 19:13
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_GROUP)
<!-- Gruppen zuordnen -->
<h4>Gruppe + Rang zuweisen</h4>
<div class="form-group">
    <label for="group">Gruppe</label>
    <select class="form-control" id="group" name="group">
        <option value="0" {{ ($character->getGroup() == 0) ? 'selected' : '' }}>
            Keine Gruppe
        </option>
        @foreach($groups as $group)
            <option
                value="{{ $group->id }}" {{ ($character->getGroup() == $group->id) ? 'selected' : '' }}>
                {{ $group->name }}
            </option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="groupRank">Rang</label>
    <select class="form-control" id="groupRank" name="groupRank">
        @for($i = 0; $i < 5; $i++)
            <option value="{{ $i }}" {{ ($character->getGroupRank() == $i) ? 'selected' : '' }}>
                {{ $i }}
            </option>
        @endfor
    </select>
</div>
@endpermission
