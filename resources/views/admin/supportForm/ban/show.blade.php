@php
    /** @var $entry \App\Model\Dto\WebAdminBan */
    /** @var $user \App\Model\Dto\Account */
@endphp
@extends('admin.layout.app')
@section('title')
    <i class="fab fa-wpforms"></i>
    Eintrag {{ $entry->getId() }}
    <a class="btn btn-dark btn-sm float-right"
       href="{{ route('admin.supportForm.ban.index') }}"
       role="button">
        Zurück
    </a>
@endsection
@section('content')
    <dl class="row">
        <dt class="col-3">Name IC</dt>
        <dd class="col-9">{{ $entry->getUserName() }}</dd>

        <dt class="col-3">IP-Adresse</dt>
        <dd class="col-9">{{ $entry->getIpAddress() }}</dd>

        <dt class="col-3">SocialClub Name</dt>
        <dd class="col-9">{{ $entry->getSocialClubName() }}</dd>

        <dt class="col-3">Fallgrund</dt>
        <dd class="col-9">{{ $entry->getReason() }}</dd>

        <dt class="col-3">Banndauer</dt>
        <dd class="col-9">{{ $entry->getLength() }}</dd>

        <dt class="col-3">Datum</dt>
        <dd class="col-9">{{ $entry->getCreatedAt()->format('Y-m-d H:i:s') }}</dd>

        <dt class="col-3">Ausgesprochen von...</dt>
        <dd class="col-9">{{ $user->getUsername() }}</dd>
    </dl>
@endsection
