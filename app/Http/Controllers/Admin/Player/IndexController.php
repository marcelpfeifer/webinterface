<?php
/**
 * PlayerController.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 22.08.2019
 * Time: 21:18
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\Player;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Http\Model\Admin\Player;
use App\Http\Request\Admin\Player\Index\Save;
use App\Http\Request\AValidator;
use App\Libraries\Api\Player\Kick;
use App\Libraries\Api\Player\SetDimension;
use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Model\Account;
use App\Model\Business;
use App\Model\Character;
use App\Model\Enum\Locker\Type;
use App\Model\Groups;
use App\Model\Locker;
use App\Model\Prison;
use App\Model\Vehicle;
use App\Model\VehicleKeys;
use App\Model\WebPermissionGroup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;

class IndexController extends Controller
{
    /**
     * @param Request $request
     * @return View
     */
    public function index(Request $request): View
    {
        $search = $request->input('search', '');

        $characters = (new Character())->getCharacters($search);

        return view(
            'admin.player.index',
            [
                "characters" => $characters,
                "search"     => $search,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $userId
     * @return View
     */
    public function edit(Request $request, int $userId): View
    {
        $account = Account::find($userId);
        $characterDto = null;
        if ($account) {
            $character = Character::find($account->id);
            $characterDto = Character::entryAsDto($character);
        }
        $groups = Groups::all();
        $permissionGroups = WebPermissionGroup::all();
        $businesses = Business::all();
        return view(
            'admin.player.edit',
            [
                'account'          => $account,
                'character'        => $characterDto,
                'groups'           => $groups,
                'permissionGroups' => $permissionGroups,
                'businesses'       => $businesses
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $userId
     * @return RedirectResponse
     */
    public function save(Request $request, int $userId): RedirectResponse
    {
        $validator = (new Save($request));
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            return redirect()->route('admin.player.edit', $userId)
                ->with('messages', $validator->getErrorsAsMessageArray($validatedData))
                ->withInput()
                ->withErrors($validatedData->errors());
        }

        /**
         * @var $requestDto \App\Http\Request\Dto\Admin\Player\Index\Save
         */
        $requestDto = $validator->toDto();

        $saved = (new Player())->save($requestDto, $userId);

        $messages[] = (new Message())
            ->setMessage("Änderungen wurden gespeichert")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->back()->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $userId
     * @return RedirectResponse
     */
    public function releaseFromPrison(Request $request, int $userId): RedirectResponse
    {
        $prison = Prison::find($userId);
        if (!($prison)) {
            $messages[] = (new Message())
                ->setMessage("Spieler war nicht im Gefängnis!")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::NOTICE);
            return redirect()->back()->with('messages', $messages);
        }

        $dto = Prison::entryAsDto($prison);
        $oldDto = clone $dto;
        $dto->setMin(0);
        (new Prison())->updateEntry($dto);
        Logging::update(Name::CHARACTER, Auth::id(), $oldDto, $dto);

        $messages[] = (new Message())
            ->setMessage("Spieler wurde erfolgreich aus dem Gefängnis geholt")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->back()->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $userId
     * @return RedirectResponse
     */
    public function resetDimension(Request $request, int $userId): RedirectResponse
    {
        $user = Character::find($userId);
        if (!($user)) {
            return redirect()->back();
        }

        // Anfrage in der Datenbank aktualisieren
        $dto = Character::entryAsDto($user);
        $oldDto = clone $dto;
        $dto->setDimension(0);
        (new Character())->updateEntry($dto);

        // Anfrage auf dem Live Server machen
        SetDimension::call($userId, 0);

        // Schreibe den Log Eintrag
        Logging::update(Name::CHARACTER, Auth::id(), $oldDto, $dto);

        $messages[] = (new Message())
            ->setMessage("Spieler Dimension wurde zurückgesetzt")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->back()->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $userId
     * @return RedirectResponse
     */
    public function deleteFactionLocker(Request $request, int $userId): RedirectResponse
    {
        $user = Character::find($userId);
        if (!($user)) {
            return redirect()->back();
        }

        (new Locker())->deleteEntryByGuidAndType($userId, Type::NORMAL);

        $messages[] = (new Message())
            ->setMessage("Spieler Locker wurde zurückgesetzt")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->back()->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $userId
     * @return RedirectResponse
     */
    public function deleteFactionLockerWeapon(Request $request, int $userId): RedirectResponse
    {
        $user = Character::find($userId);
        if (!($user)) {
            return redirect()->back();
        }

        (new Locker())->deleteEntryByGuidAndType($userId, Type::WEAPON);

        $messages[] = (new Message())
            ->setMessage("Spieler Waffen Locker wurde zurückgesetzt")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->back()->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $userId
     * @return RedirectResponse
     */
    public function inventoryReset(Request $request, int $userId): RedirectResponse
    {
        $user = Character::find($userId);
        if (!($user)) {
            return redirect()->back();
        }

        Kick::call($userId, 'Inventar wird zurückgesetzt');
        sleep(5);

        $dto = Character::entryAsDto($user);
        $dto->setInventory(
            '[{"inventory":[null,null],"maxWeight":2},{"label":"Shirt","quantity":1,"props":{"female":[{"id":11,"value":2,"texture":2},{"id":8,"value":2,"texture":0},{"id":3,"value":2,"texture":0}],"male":[{"id":11,"value":16,"texture":0},{"id":8,"value":16,"texture":0},{"id":3,"value":0,"texture":0}]},"slot":1,"weight":0.5,"maxWeight":6,"inventory":[null,null],"stackable":false,"hash":"cc3bd063cd8875165b5343560186ab2bc8ec1f5701a3475c47f54efc771b3812"},{"label":"Hose","quantity":1,"props":{"female":[{"id":4,"value":0,"texture":2}],"male":[{"id":4,"value":0,"texture":0}]},"slot":2,"weight":0.5,"maxWeight":12,"inventory":[null,null,null,null],"stackable":false,"hash":"a44c308ae9a1ebde4b791612b684fcab6ccb79214e417f5c34064c09c0def424"},null,null,null,null,{"label":"Schuhe","quantity":1,"props":{"female":[{"id":6,"value":3,"texture":0}],"male":[{"id":6,"value":1,"texture":0}]},"slot":7,"weight":0.5,"stackable":false,"hash":"7f58b77680d3703da15152529332fcb4cd3610e1601cdd6b9ff1e3cef114e8cd"},null,null,null,null,null,null]'
        );
        $messages[] = (new Message())
            ->setMessage("Spieler Inventar wurde zurückgesetzt")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->back()->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $userId
     * @return RedirectResponse
     */
    public function playerResetPosition(Request $request, int $userId): RedirectResponse
    {
        $user = Character::find($userId);
        if (!($user)) {
            return redirect()->back();
        }

        $dto = Character::entryAsDto($user);
        $oldDto = clone $dto;
        $dto->setLastPosition(
            '{"x":-219.07252502441406,"y":-1048.997802734375,"z":30.13916015625,"rot":0,"dim":0}'
        )->setHouseId(0);

        (new Character())->updateEntry($dto);
        Logging::update(Name::CHARACTER, Auth::id(), $oldDto, $dto);

        $messages[] = (new Message())
            ->setMessage("Spieler Position wurde zurückgesetzt")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->back()->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $userId
     * @return RedirectResponse
     */
    public function playerReset(Request $request, int $userId): RedirectResponse
    {
        $user = Character::find($userId);
        $dto = Character::entryAsDto($user);
        (new Character())->deleteEntry($dto);

        $vehicleDb = new Vehicle();
        foreach ($vehicleDb->getVehiclesByCharacterId($userId) as $vehicle) {
            (new VehicleKeys())->deleteEntryByCarId($vehicle->getId());
        }
        (new Vehicle())->deleteEntryByGuid($userId);
        $dto->setFace('');
        $dto->setInventory('');
        Logging::delete(Name::CHARACTER, Auth::id(), $dto);

        $messages[] = (new Message())
            ->setMessage("Spieler wurde zurückgesetzt")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.player.index')->with('messages', $messages);
    }
}
