<?php
/**
 * Buy
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 02.09.2021
 * Time: 14:17
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Web\Stock;


use App\Http\Request\Dto\ADto;
use App\Model\Dto\Character;
use App\Model\Dto\WebStockCharacter;

class Sell extends ADto
{

    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var WebStockCharacter
     */
    private $webStockCharacter;

    /**
     * @var Character
     */
    private $character;

    /**
     * @var int
     */
    private $amount = 0;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Sell
     */
    public function setId(int $id): Sell
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return WebStockCharacter
     */
    public function getWebStockCharacter(): WebStockCharacter
    {
        return $this->webStockCharacter;
    }

    /**
     * @param WebStockCharacter $webStockCharacter
     * @return Sell
     */
    public function setWebStockCharacter(WebStockCharacter $webStockCharacter): Sell
    {
        $this->webStockCharacter = $webStockCharacter;
        return $this;
    }

    /**
     * @return Character
     */
    public function getCharacter(): Character
    {
        return $this->character;
    }

    /**
     * @param Character $character
     * @return Sell
     */
    public function setCharacter(Character $character): Sell
    {
        $this->character = $character;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     * @return Sell
     */
    public function setAmount(int $amount): Sell
    {
        $this->amount = $amount;
        return $this;
    }
}
