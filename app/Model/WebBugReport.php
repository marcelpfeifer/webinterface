<?php
/**
 * WebBugReport
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 03.08.2021
 * Time: 11:00
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class WebBugReport extends Model
{
    /**
     * @var string
     */
    public const COLUMN_ID = 'id';

    /**
     * @var string
     */
    public const COLUMN_CHARACTER_ID = 'characterId';

    /**
     * @var string
     */
    public const COLUMN_TITLE = 'title';

    /**
     * @var string
     */
    public const COLUMN_DESCRIPTION = 'description';

    /**
     * @var string
     */
    public const COLUMN_EVIDENCE = 'evidence';


    /**
     * @var string
     */
    public const COLUMN_POSITION = 'position';

    /**
     * @var string
     */
    public const COLUMN_APPROVED = 'approved';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webBugReport';

    /**
     * @return HasOne
     */
    public function character(): HasOne
    {
        return $this->hasOne(Character::class, Character::COLUMN_ID, self::COLUMN_CHARACTER_ID);
    }

    /**
     * @return Dto\WebBugReport[]
     */
    public function getUnapprovedEntries(): array
    {
        $result = [];
        $entries = $this
            ->where(self::COLUMN_APPROVED, '=', false)
            ->get();

        foreach ($entries as $entry) {
            $result[] = self::entryAsDto($entry);
        }
        return $result;
    }

    /**
     * @param Dto\WebBugReport $dto
     */
    public function insertEntry(Dto\WebBugReport $dto)
    {
        $this->insert(
            [
                self::COLUMN_ID           => $dto->getId(),
                self::COLUMN_CHARACTER_ID => $dto->getCharacterId(),
                self::COLUMN_TITLE        => $dto->getTitle(),
                self::COLUMN_DESCRIPTION  => $dto->getDescription(),
                self::COLUMN_EVIDENCE     => $dto->getEvidence(),
                self::COLUMN_POSITION     => $dto->getPosition(),
                self::COLUMN_APPROVED     => $dto->isApproved(),
                self::CREATED_AT          => Carbon::now(),
                self::UPDATED_AT          => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebBugReport $dto
     */
    public function updateEntry(Dto\WebBugReport $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_ID           => $dto->getId(),
                self::COLUMN_CHARACTER_ID => $dto->getCharacterId(),
                self::COLUMN_TITLE        => $dto->getTitle(),
                self::COLUMN_DESCRIPTION  => $dto->getDescription(),
                self::COLUMN_EVIDENCE     => $dto->getEvidence(),
                self::COLUMN_POSITION     => $dto->getPosition(),
                self::COLUMN_APPROVED     => $dto->isApproved(),
                self::UPDATED_AT          => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebBugReport $dto
     */
    public function deleteEntry(\App\Model\Dto\WebBugReport $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebBugReport $entry
     * @return Dto\WebBugReport
     */
    public static function entryAsDto(WebBugReport $entry): Dto\WebBugReport
    {
        return (new Dto\WebBugReport())
            ->setId($entry->{self::COLUMN_ID})
            ->setCharacterId($entry->{self::COLUMN_CHARACTER_ID})
            ->setTitle($entry->{self::COLUMN_TITLE})
            ->setDescription($entry->{self::COLUMN_DESCRIPTION})
            ->setEvidence($entry->{self::COLUMN_EVIDENCE})
            ->setPosition($entry->{self::COLUMN_POSITION})
            ->setApproved($entry->{self::COLUMN_APPROVED})
            ->setCreatedAt($entry->{self::CREATED_AT})
            ->setUpdatedAt($entry->{self::UPDATED_AT});
    }
}
