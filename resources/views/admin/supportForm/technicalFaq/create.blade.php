<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 20.11.2019
 * Time: 20:20
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('admin.layout.app')
@section('title')
    <i class="fab fa-wpforms"></i>
    Eintrag erstellen
@endsection
@section('content')
    <form action="{{ route('admin.supportForm.technicalFaq.store') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="explanation">Fehlermeldung</label>
            <input type="text" class="form-control" id="explanation" name="explanation">
        </div>
        <div class="form-group">
            <label for="errorCode">Fehlercode</label>
            <input type="text" class="form-control" id="errorCode" name="errorCode">
        </div>
        <div class="form-group">
            <label for="troubleShooting">Fehlerbehebung</label>
            <textarea class="form-control" id="troubleShooting" name="troubleShooting"> </textarea>
        </div>
        <div class="form-group">
            <label for="troubleShooting2">Fehlerbehebung 2</label>
            <textarea class="form-control" id="troubleShooting2" name="troubleShooting2"> </textarea>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" name="fixed" id="fixed">
            <label class="form-check-label" for="fixed">
                Gelöst
            </label>
        </div>
        <br/>
        <button type="submit" class="btn btn-dark">Erstellen</button>
    </form>
@endsection
