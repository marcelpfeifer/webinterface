<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.10.2021
 * Time: 14:08
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Api\Server\Server;


use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\Enum\Api\Server\Server\Action;
use App\Http\Request\IValidator;
use App\Libraries\Api\Server\Enum\Type;
use Illuminate\Validation\Rule;

class Save extends AValidator implements IValidator
{
    /**
     * @return string[]
     */
    public function getRules(): array
    {
        return [
            'type'   => [
                'required',
                'string',
                Rule::in(Type::getConstants())
            ],
            'action' => [
                'required',
                'string',
                Rule::in(Action::getConstants())
            ],
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Api\Server\Server\Save())
            ->setType($this->request->input('type'))
            ->setAction($this->request->input('action'));
    }
}
