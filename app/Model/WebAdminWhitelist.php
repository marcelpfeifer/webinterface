<?php
/**
 * WebAdminWhitelist
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 28.04.2020
 * Time: 17:07
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class WebAdminBan
 * @package App\Model
 */
class WebAdminWhitelist extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_USER_ID = 'userId';

    /**
     * @var string
     */
    const COLUMN_STATUS = 'status';

    /**
     * @var string
     */
    const COLUMN_RETRY_DATE = 'retryDate';

    /**
     * @var string
     */
    const COLUMN_USER_NAME = 'userName';

    /**
     * @var string
     */
    const COLUMN_MESSAGE = 'message';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webAdminWhitelist';

    /**
     * @var array
     */
    protected $dates = [
        self::COLUMN_RETRY_DATE,
        self::CREATED_AT,
        self::UPDATED_AT,
    ];

    /**
     * @return BelongsTo
     */
    public function account(): BelongsTo
    {
        return $this->belongsTo(Account::class, self::COLUMN_USER_ID, Account::COLUMN_ID);
    }

    /**
     * @return HasMany
     */
    public function answers(): HasMany
    {
        return $this->hasMany(WebAdminWhitelistAnswer::class, WebAdminWhitelistAnswer::COLUMN_WEB_ADMIN_WHITELIST_ID, WebAdminWhitelistAnswer::COLUMN_ID);
    }

    /**
     * @param Dto\WebAdminWhitelist $dto
     * @return int
     */
    public function insertEntry(Dto\WebAdminWhitelist $dto): int
    {
        return $this->insertGetId(
            [

                self::COLUMN_USER_ID    => $dto->getUserId(),
                self::COLUMN_USER_NAME  => $dto->getUserName(),
                self::COLUMN_STATUS     => $dto->getStatus(),
                self::COLUMN_RETRY_DATE => $dto->getRetryDate(),
                self::COLUMN_MESSAGE    => $dto->getMessage(),
                self::CREATED_AT        => Carbon::now(),
                self::UPDATED_AT        => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebAdminWhitelist $dto
     * @return bool
     */
    public function updateEntry(Dto\WebAdminWhitelist $dto): bool
    {
        return $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_ID         => $dto->getId(),
                self::COLUMN_USER_ID    => $dto->getUserId(),
                self::COLUMN_USER_NAME  => $dto->getUserName(),
                self::COLUMN_RETRY_DATE => $dto->getRetryDate(),
                self::COLUMN_STATUS     => $dto->getStatus(),
                self::COLUMN_MESSAGE    => $dto->getMessage(),
                self::UPDATED_AT        => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebAdminWhitelist $dto
     */
    public function deleteEntry(\App\Model\Dto\WebAdminWhitelist $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebAdminWhitelist $entry
     * @return Dto\WebAdminWhitelist
     */
    public static function entryAsDto(WebAdminWhitelist $entry): Dto\WebAdminWhitelist
    {
        return (new Dto\WebAdminWhitelist())
            ->setId($entry->{self::COLUMN_ID})
            ->setUserId($entry->{self::COLUMN_USER_ID})
            ->setUserName($entry->{self::COLUMN_USER_NAME})
            ->setStatus($entry->{self::COLUMN_STATUS})
            ->setRetryDate($entry->{self::COLUMN_RETRY_DATE})
            ->setMessage($entry->{self::COLUMN_MESSAGE})
            ->setCreatedAt($entry->{self::CREATED_AT})
            ->setUpdatedAt($entry->{self::UPDATED_AT});
    }
}
