<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.08.2021
 * Time: 16:22
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\BugReport;


use App\Dto\Message;
use App\Http\Request\BugReport\Index\Save;
use App\Libraries\Api\Player\CloseApp;
use App\Libraries\Api\Player\Position;
use App\Model\Character;
use App\Model\Dto\WebBugReport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class IndexController
{
    /**
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\View\View
     */
    public function index(Request $request, string $hash): \Illuminate\View\View
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return view('404');
        }

        return view(
            'bugReport.index',
            [
                'hash' => $hash,
            ]
        );
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request, string $hash): \Illuminate\Http\RedirectResponse
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('bugReport.index', $hash);
        }
        // Validiere die Anfrage
        $validator = (new Save($request));
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            return redirect()->route('bugReport.index', $hash)
                ->with('messages', $validator->getErrorsAsMessageArray($validatedData))
                ->withInput()
                ->withErrors($validatedData->errors());
        }

        /**
         * @var $requestDto \App\Http\Request\Dto\BugReport\Index\Save
         */
        $requestDto = $validator->toDto();

        $characterDto = Character::entryAsDto($character);
        $dto = (new WebBugReport())
            ->setTitle($requestDto->getTitle())
            ->setDescription($requestDto->getDescription())
            ->setPosition(Position::call($characterDto->getId()))
            ->setEvidence($requestDto->getEvidence())
            ->setApproved(false)
            ->setCharacterId($characterDto->getId());
        (new \App\Model\WebBugReport())->insertEntry($dto);

        try {
            CloseApp::call($characterDto->getId());
        } catch (\Throwable $exception) {
            Log::error($exception->getMessage());
        }

        $messages[] = (new Message())
            ->setMessage("Wurde gespeichert!")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('bugReport.index', $hash)->with('messages', $messages);
    }

}
