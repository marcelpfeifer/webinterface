<?php
/**
 * WebMarketplaceBuy
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 02.05.2020
 * Time: 17:56
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;


use Carbon\Carbon;

class WebMarketplaceBuy
{

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var int
     */
    private $characterId = 0;

    /**
     * @var int|null
     */
    private $categoryId = null;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $imageUrl = '';

    /**
     * @var string
     */
    private $message = '';

    /**
     * @var Carbon
     */
    private $createAt;

    /**
     * @var Carbon
     */
    private $updatedAt;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebMarketplaceBuy
     */
    public function setId(?int $id): WebMarketplaceBuy
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCharacterId(): int
    {
        return $this->characterId;
    }

    /**
     * @param int $characterId
     * @return WebMarketplaceBuy
     */
    public function setCharacterId(int $characterId): WebMarketplaceBuy
    {
        $this->characterId = $characterId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCategoryId(): ?int
    {
        return $this->categoryId;
    }

    /**
     * @param int|null $categoryId
     * @return WebMarketplaceBuy
     */
    public function setCategoryId(?int $categoryId): WebMarketplaceBuy
    {
        $this->categoryId = $categoryId;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return WebMarketplaceBuy
     */
    public function setName(string $name): WebMarketplaceBuy
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getImageUrl(): string
    {
        return $this->imageUrl;
    }

    /**
     * @param string $imageUrl
     * @return WebMarketplaceBuy
     */
    public function setImageUrl(string $imageUrl): WebMarketplaceBuy
    {
        $this->imageUrl = $imageUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return WebMarketplaceBuy
     */
    public function setMessage(string $message): WebMarketplaceBuy
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getCreateAt(): Carbon
    {
        return $this->createAt;
    }

    /**
     * @param Carbon $createAt
     * @return WebMarketplaceBuy
     */
    public function setCreateAt(Carbon $createAt): WebMarketplaceBuy
    {
        $this->createAt = $createAt;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getUpdatedAt(): Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon $updatedAt
     * @return WebMarketplaceBuy
     */
    public function setUpdatedAt(Carbon $updatedAt): WebMarketplaceBuy
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
