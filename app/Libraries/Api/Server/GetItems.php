<?php
/**
 * GetItems
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 11.04.2020
 * Time: 13:57
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Api\Server;

use App\Libraries\Api\AClient;
use App\Model\Character;
use App\Model\WebConfig;

class GetItems extends AClient
{

    /**
     * @var string
     */
    const URL = 'server/getItems';

    /**
     * @return array
     */
    public static function get(): array
    {
        $result = [];
        $response = self::sendRequest('GET', WebConfig::getEntryByKey('api_url')->getValue() . self::URL, []);
        if ($response && $response->getStatusCode() == 200) {
            $content = $response->getBody()->getContents();
            $entries = json_decode($content);
            foreach ($entries as $id => $name) {
                $result[$id] = $name;
            }
        } else {
            $result = [
                'No_Server_Response' => 'No Server Response',
                'Test'               => 'Test',
            ];
        }
        return $result;
    }
}
