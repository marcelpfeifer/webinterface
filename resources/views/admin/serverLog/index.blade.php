<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 24.09.2021
 * Time: 17:23
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>

@extends('admin.layout.app')
@section('title')
    Server Log
@endsection
@section('content')
    <div class="container border">
        <div class="m-2 p-2 font-weight-bold border-bottom">
            <form action="{{ route('admin.serverLog.index') }}" method="GET" id="serverLog" class="form-inline">
                <div class="mr-2">
                    Server Log
                </div>
                <div class="form-check form-check-inline" onclick="document.getElementById('serverLog').submit();">
                    <input class="form-check-input" type="checkbox" name="filter[]" id="warning"
                           value="{{ \App\Libraries\Frontend\Enum\Admin\ServerLog\Type::WARNING }}"
                        {{ (in_array(\App\Libraries\Frontend\Enum\Admin\ServerLog\Type::WARNING, $filter)) ? 'checked' : '' }}>
                    <label class="form-check-label" for="warning">Warnung</label>
                </div>
                <div class="form-check form-check-inline" onclick="document.getElementById('serverLog').submit();">
                    <input class="form-check-input" type="checkbox" id="error" name="filter[]"
                           value="{{ \App\Libraries\Frontend\Enum\Admin\ServerLog\Type::ERROR }}"
                        {{ (in_array(\App\Libraries\Frontend\Enum\Admin\ServerLog\Type::ERROR, $filter)) ? 'checked' : '' }}>
                    <label class="form-check-label" for="error">Error</label>
                </div>
                <a class="float-right" href="{{ route('admin.serverLog.reload') }}">
                    <i class="fas fa-redo"></i>
                </a>
            </form>
        </div>
        <div class="overflow-auto vh-100">
            @foreach($viewData->getEntries() as $entry)
                <span
                    class="{{ $entry->getType() === \App\Libraries\Frontend\Enum\Admin\ServerLog\Type::ERROR ? 'bg-danger text-white': '' }}
                    {{ $entry->getType() === \App\Libraries\Frontend\Enum\Admin\ServerLog\Type::WARNING ? 'bg-warning': '' }}">
                {{ $entry->getLine() }}
            </span>
                <br/>
            @endforeach
        </div>
    </div>
@endsection
