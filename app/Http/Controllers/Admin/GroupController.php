<?php
/**
 * GroupController.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 22.08.2019
 * Time: 21:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Model\Character;
use App\Model\Groups;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

/**
 * Class GroupController
 * @package App\Http\Controllers\Admin
 */
class GroupController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function index(): View
    {
        $groups = Groups::all();
        return view('admin.group.index', [
            "groups" => $groups
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|View
     */
    public function show(Request $request, int $id)
    {
        $group = Groups::find($id);
        if (!($group)) {
            $messages[] = (new Message())
                ->setMessage("Die Gruppe wurde nicht gefunden")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::NOTICE);
            return redirect()->route('admin.group.index')->with('messages', $messages);
        }
        $group = Groups::entryAsDto($group);
        $characters = (new Character())->getAllUsersOfGroupByGroupId($id);

        return view('admin.group.show', [
            "group"      => $group,
            "characters" => $characters,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|View
     */
    public function create(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name'  => 'required|string',
                'type'  => 'required|int',
                'money' => 'required|int',
            ],
            [
                'name.required'  => 'Das Feld `Name` darf nicht leer sein',
                'name.string'    => 'Das Feld `Name` darf nur Zeichen enthalten',
                'type.required'  => 'Das Feld `Typ` darf nicht leer sein',
                'name.int'       => 'Das Feld `Typ` darf nur Zahlenwerte enthalten',
                'money.required' => 'Das Geld `Typ` darf nicht leer sein',
                'money.int'      => 'Das Feld `Geld` darf nur Zahlenwerte enthalten',
            ]
        );
        if ($validator->fails()) {
            $messages = [];
            foreach ($validator->errors()->all() as $error) {
                $messages[] = (new Message())
                    ->setMessage($error)
                    ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
            }
            return redirect()->back()->with('messages', $messages)->withInput();
        }


        $dto = (new \App\Model\Dto\Groups())
            ->setName($request->input('name'))
            ->setType($request->input('type'))
            ->setMoney($request->input('money'))
            ->setMessages('Nachricht des Tages');

        Logging::insert(Name::GROUP, Auth::id(), $dto);
        (new Groups())->insertEntry($dto);

        $messages[] = (new Message())
            ->setMessage("Die Gruppe erfolgreich angelegt")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->back()->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|View
     */
    public function delete(Request $request, int $id)
    {
        $group = Groups::find($id);
        if (!($group)) {
            return redirect()->route('groupIndex');
        }
        $dto = Groups::entryAsDto($group);

        // Setze bei allen Benutzern die die Gruppe haben den Rank und die Gruppe auf 0
        (new Character())->deleteGroupRank($dto);
        // Lösche die Gruppe
        (new Groups())->deleteEntry($dto);

        Logging::delete(Name::GROUP, Auth::id(), $dto);

        $messages[] = (new Message())
            ->setMessage("Die Gruppe wurde gelöscht")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->back()->with('messages', $messages);
    }
}
