<?php
/**
 * Entry
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 24.09.2021
 * Time: 17:28
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Dto\Admin\ServerLog\Index;


class Entry
{

    /**
     * @var string
     */
    private $line = '';

    /**
     * @var string
     */
    private $type = '';

    /**
     * @return string
     */
    public function getLine(): string
    {
        return $this->line;
    }

    /**
     * @param string $line
     * @return Entry
     */
    public function setLine(string $line): Entry
    {
        $this->line = $line;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Entry
     */
    public function setType(string $type): Entry
    {
        $this->type = $type;
        return $this;
    }
}
