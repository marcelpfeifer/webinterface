<?php
/**
 * PermissionController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 25.07.2020
 * Time: 13:45
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin;

use App\Dto\Message;
use App\Model\WebPermission;
use App\Model\WebPermissionGroup;
use App\Model\WebPermissionToWebPermission;
use Illuminate\Http\Request;
use Illuminate\View\View;

class PermissionController extends \App\Http\Controllers\Controller
{

    /**
     * @return View
     */
    public function index(): View
    {
        $groups = [];
        foreach (WebPermissionGroup::all() as $group) {
            $groups[] = WebPermissionGroup::entryAsDto($group);
        }

        return view(
            'admin.permission.index',
            [
                'groups' => $groups
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $groupId
     * @return View
     */
    public function edit(Request $request, int $groupId): View
    {
        $groupEntry = WebPermissionGroup::find($groupId);
        if (!$groupEntry) {
            return view('404', []);
        }

        $permissions = [];
        foreach (WebPermission::all() as $permission) {
            $permissions[] = WebPermission::entryAsDto($permission);
        }

        return view(
            'admin.permission.edit',
            [
                'group'            => WebPermissionGroup::entryAsDto($groupEntry),
                'groupPermissions' => (new WebPermissionToWebPermission())->getWebPermissionsIdsByPermissionGroupId(
                    $groupId
                ),
                'permissions'      => $permissions,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $groupId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(Request $request)
    {
        $dto = (new \App\Model\Dto\WebPermissionGroup())
            ->setName($request->input('name'));
        (new WebPermissionGroup())->insertEntry($dto);

        $messages[] = (new Message())
            ->setMessage("Erfolgreich gespeichert!")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);

        return redirect()->route('admin.permission.index')->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $groupId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request, int $groupId)
    {
        (new WebPermissionToWebPermission())->deleteEntry(
            (new \App\Model\Dto\WebPermissionToWebPermission())
                ->setWebPermissionGroupId($groupId)
        );

        foreach (WebPermission::all() as $permission) {
            $permissionDto = WebPermission::entryAsDto($permission);

            $isPermissionSet = $request->input($permissionDto->getId(), null);
            if ($isPermissionSet) {
                $dto = (new \App\Model\Dto\WebPermissionToWebPermission())
                    ->setWebPermissionGroupId($groupId)
                    ->setWebPermissionId($permissionDto->getId());
                (new WebPermissionToWebPermission())->insertEntry($dto);
            }
        }

        $messages[] = (new Message())
            ->setMessage("Erfolgreich gespeichert!")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.permission.edit', $groupId)->with('messages', $messages);
    }


    /**
     * @param Request $request
     * @param int $groupId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request, int $groupId)
    {
        (new WebPermissionToWebPermission())->deleteEntry(
            (new \App\Model\Dto\WebPermissionToWebPermission())
                ->setWebPermissionGroupId($groupId)
        );

        (new WebPermissionGroup())->deleteEntry(
            (new \App\Model\Dto\WebPermissionGroup())
                ->setId($groupId)
        );

        $messages[] = (new Message())
            ->setMessage("Erfolgreich gelöscht!")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.permission.edit', $groupId)->with('messages', $messages);
    }
}
