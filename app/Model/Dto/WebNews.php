<?php
/**
 * WebNews
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.08.2021
 * Time: 11:53
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

use App\Libraries\Logging\ILogging;
use App\Libraries\Logging\Traits\Variables;
use Carbon\Carbon;

class WebNews implements ILogging
{
    use Variables;

    /**
     * @var int|null
     */
    private $id = null;

    /**
     * @var int|null
     */
    private $webNewsCategoryId = null;

    /**
     * @var string
     */
    private $title = '';

    /**
     * @var string
     */
    private $image = '';

    /**
     * @var string
     */
    private $shortDescription = '';

    /**
     * @var string
     */
    private $description = '';

    /**
     * @var string
     */
    private $author = '';

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @var null|Carbon
     */
    private $updatedAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebNews
     */
    public function setId(?int $id): WebNews
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getWebNewsCategoryId(): ?int
    {
        return $this->webNewsCategoryId;
    }

    /**
     * @param int|null $webNewsCategoryId
     * @return WebNews
     */
    public function setWebNewsCategoryId(?int $webNewsCategoryId): WebNews
    {
        $this->webNewsCategoryId = $webNewsCategoryId;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return WebNews
     */
    public function setTitle(string $title): WebNews
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return WebNews
     */
    public function setImage(string $image): WebNews
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return string
     */
    public function getShortDescription(): string
    {
        return $this->shortDescription;
    }

    /**
     * @param string $shortDescription
     * @return WebNews
     */
    public function setShortDescription(string $shortDescription): WebNews
    {
        $this->shortDescription = $shortDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return WebNews
     */
    public function setDescription(string $description): WebNews
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * @param string $author
     * @return WebNews
     */
    public function setAuthor(string $author): WebNews
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return WebNews
     */
    public function setCreatedAt(?Carbon $createdAt): WebNews
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon|null $updatedAt
     * @return WebNews
     */
    public function setUpdatedAt(?Carbon $updatedAt): WebNews
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
