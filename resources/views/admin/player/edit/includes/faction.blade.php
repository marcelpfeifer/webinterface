<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.01.2021
 * Time: 19:12
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_FACTION)
<h4>Fraktionszuordnung</h4>
<div class="form-group">
    <label for="medicRank">Medic Rang</label>
    <select class="form-control" id="medicRank" name="medicRank">
        @for($i = 0; $i < 9; $i++)
            <option value="{{ $i }}" {{ ($character->getMedRank() == $i) ? 'selected' : '' }}>
                {{ $i }}
            </option>
        @endfor
    </select>
</div>
<div class="form-group">
    <label for="fibRank">FIB Rang</label>
    <select class="form-control" id="fibRank" name="fibRank">
        @for($i = 0; $i < 9; $i++)
            <option value="{{ $i }}" {{ ($character->getFibRank() == $i) ? 'selected' : '' }}>
                {{ $i }}
            </option>
        @endfor
    </select>
</div>
<div class="form-group">
    <label for="bwfiRank">Behörde für Wirtschaft, Finanzen und Infrastruktur Rang</label>
    <select class="form-control" id="bwfiRank" name="bwfiRank">
        @for($i = 0; $i < 9; $i++)
            <option value="{{ $i }}" {{ ($character->getBwfiRank() == $i) ? 'selected' : '' }}>
                {{ $i }}
            </option>
        @endfor
    </select>
</div>
<div class="form-group">
    <label for="copRank">Cop Rang</label>
    <select class="form-control" id="copRank" name="copRank">
        @for($i = 0; $i < 9; $i++)
            <option value="{{ $i }}" {{ ($character->getCopRank() == $i) ? 'selected' : '' }}>
                {{ $i }}
            </option>
        @endfor
    </select>
</div>
<div class="form-group">
    <label for="dojRank">DOJ Rang</label>
    <select class="form-control" id="dojRank" name="dojRank">
        @for($i = 0; $i < 9; $i++)
            <option value="{{ $i }}" {{ ($character->getDojRank() == $i) ? 'selected' : '' }}>
                {{ $i }}
            </option>
        @endfor
    </select>
</div>
<div class="form-group">
    <label for="lawyerRank">Juristen Rang</label>
    <select class="form-control" id="lawyerRank" name="lawyerRank">
        @for($i = 0; $i < 9; $i++)
            <option value="{{ $i }}" {{ ($character->getLawyerRank() == $i) ? 'selected' : '' }}>
                {{ $i }}
            </option>
        @endfor
    </select>
</div>

<div class="form-group">
    <label for="taxiRank">Taxi Rang</label>
    <select class="form-control" id="taxiRank" name="taxiRank">
        @for($i = 0; $i < 9; $i++)
            <option value="{{ $i }}" {{ ($character->getTaxiRank() == $i) ? 'selected' : '' }}>
                {{ $i }}
            </option>
        @endfor
    </select>
</div>

<div class="form-group">
    <label for="onDuty">Ist im Dienst?</label>
    <select class="form-control" id="onDuty" name="onDuty">
        <option value="0" {{ ($character->getIsDuty()== 0) ? 'selected' : '' }}>Nein</option>
        <option value="1" {{ ($character->getIsDuty() == 1) ? 'selected' : '' }}>Ja</option>
    </select>
</div>
<br/>

@endpermission
