<?php
/**
 * UserAccount
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 27.08.2020
 * Time: 19:30
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Tickets\Dto;


class UserAccount
{
    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var int
     */
    private $userId = 0;

    /**
     * @var int
     */
    private $status = 1;

    /**
     * @var string
     */
    private $username = '';

    /**
     * @var string
     */
    private $password = '';

    /**
     * @var string
     */
    private $registered = '';

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return UserAccount
     */
    public function setId(int $id): UserAccount
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return UserAccount
     */
    public function setUserId(int $userId): UserAccount
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return UserAccount
     */
    public function setStatus(int $status): UserAccount
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return UserAccount
     */
    public function setUsername(string $username): UserAccount
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return UserAccount
     */
    public function setPassword(string $password): UserAccount
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegistered(): string
    {
        return $this->registered;
    }

    /**
     * @param string $registered
     * @return UserAccount
     */
    public function setRegistered(string $registered): UserAccount
    {
        $this->registered = $registered;
        return $this;
    }
}
