require('bootstrap');

$(document).ready(function () {
    $('select').each(function () {
        $(this).select2({
            theme: 'bootstrap4',
        });
    });
});
