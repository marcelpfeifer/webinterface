<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 05.01.2021
 * Time: 03:05
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-clipboard"></i>
    API Log-Einträge
@endsection
@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Account ID</th>
            <th scope="col">Url</th>
            <th scope="col">Options</th>
            <th scope="col">Datum</th>
        </tr>
        </thead>
        <tbody>
        @foreach($entries as $entry)
            <tr>
                <th scope="row">{{ $entry->{\App\Model\WebAdminLoggingApi::COLUMN_ID} }}</th>
                <td>{{ $entry->{\App\Model\WebAdminLoggingApi::COLUMN_ACCOUNT_ID} }}</td>
                <td>{{ $entry->{\App\Model\WebAdminLoggingApi::COLUMN_URL} }}</td>
                <td>{{ $entry->{\App\Model\WebAdminLoggingApi::COLUMN_OPTIONS} }}</td>
                <td>{{ $entry->{\App\Model\WebAdminLoggingApi::COLUMN_DATE_TIME}->format('Y-m-d H:i:s') }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="col-12">
            {{ $entries->links() }}
        </div>
    </div>
@endsection
