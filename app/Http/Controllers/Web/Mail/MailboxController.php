<?php
/**
 * MailboxController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 12.08.2021
 * Time: 11:54
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Web\Mail;


use App\Dto\Message;
use App\Http\Controllers\HashController;
use App\Http\Request\Auth\Mail\Mailbox\AddSave;
use App\Http\Request\Auth\Mail\Mailbox\Save;
use App\Model\Character;
use App\Model\WebMailAddress;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class MailboxController extends HashController
{

    /**
     * @param string $hash
     * @return View
     */
    public function index(string $hash): View
    {
        $characterDto = $this->getCharacterByHash($hash);
        if (!($characterDto)) {
            return view('404');
        }
        $addresses = (new WebMailAddress())->getEntriesForAccount($characterDto->getId());
        return view(
            'web.mailbox.index',
            [
                'addresses' => $addresses,
                'hash'      => $hash,
            ]
        );
    }

    /**
     * @param string $hash
     * @return View
     */
    public function add(string $hash): View
    {
        $characterDto = $this->getCharacterByHash($hash);
        if (!($characterDto)) {
            return view('404');
        }
        return view(
            'web.mailbox.add',
            [
                'hash' => $hash,
            ]
        );
    }

    /**
     * @param string $hash
     * @return View
     */
    public function create(string $hash): View
    {
        $characterDto = $this->getCharacterByHash($hash);
        if (!($characterDto)) {
            return view('404');
        }
        return view(
            'web.mailbox.create',
            [
                'hash' => $hash,
            ]
        );
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return RedirectResponse
     */
    public function save(Request $request, string $hash): RedirectResponse
    {
        $characterDto = $this->getCharacterByHash($hash);
        if (!($characterDto)) {
            return redirect()->route('web.mailbox.index', $hash);
        }

        // Validiere die Anfrage
        $validator = (new Save($request));
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            return redirect()->route('web.mailbox.create', $hash)
                ->with('messages', $validator->getErrorsAsMessageArray($validatedData))
                ->withInput()
                ->withErrors($validatedData->errors());
        }

        /**
         * @var $requestDto \App\Http\Request\Dto\Auth\Mail\Mailbox\Save
         */
        $requestDto = $validator->toDto();

        \App\Http\Model\Auth\Mail\Mailbox\Save::save($requestDto, $characterDto->getId());

        $messages[] = (new Message())
            ->setMessage("Email Account wurde erfolgreich angelegt")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('web.mailbox.index', $hash)->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return RedirectResponse
     */
    public function addSave(Request $request, string $hash): RedirectResponse
    {
        $characterDto = $this->getCharacterByHash($hash);
        if (!($characterDto)) {
            return redirect()->route('web.mailbox.index', $hash);
        }

        // Validiere die Anfrage
        $validator = (new AddSave($request));
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            return redirect()->route('web.mailbox.add', $hash)
                ->with('messages', $validator->getErrorsAsMessageArray($validatedData))
                ->withInput()
                ->withErrors($validatedData->errors());
        }

        /**
         * @var $requestDto \App\Http\Request\Dto\Auth\Mail\Mailbox\AddSave
         */
        $requestDto = $validator->toDto();

        $worked = \App\Http\Model\Auth\Mail\Mailbox\AddSave::save($requestDto, $characterDto->getId());

        if ($worked) {
            $messages[] = (new Message())
                ->setMessage("Email Account wurde erfolgreich angelegt")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        } else {
            $messages[] = (new Message())
                ->setMessage("Email Account konnte nicht hinzugefügt werden")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
        }
        return redirect()->route('web.mailbox.index', $hash)->with('messages', $messages);
    }
}
