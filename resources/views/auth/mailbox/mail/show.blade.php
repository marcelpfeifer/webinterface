<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 23.11.2020
 * Time: 10:29
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

/**
 * @var $mail \App\Model\Dto\WebMail
 */
?>
@extends('layouts.app')
@section('title')
    <i class="fas fa-envelope"></i>
    @if($dto)
        Postfach: {{ $dto->getEmail() }}
    @endif
    <a class="btn mr-2 btn-secondary btn-sm float-right"
       href="{{ route('auth.mailbox.mail.share', [$webMailAddressId, $mail->getId()]) }}"
       role="button"
    >
        <i class="fas fa-share"></i> Weiterleiten
    </a>
    <a class="btn mr-2 btn-secondary btn-sm float-right"
       href="{{ route('auth.mailbox.mail.reply', [$webMailAddressId, $mail->getId()]) }}"
       role="button"
    >
        <i class="fas fa-reply"></i> Antworten
    </a>
    <a class="btn mr-2 btn-secondary btn-sm float-right"
       href="{{ route('auth.mailbox.mail.index', $webMailAddressId) }}"
       role="button"
    >
        <i class="fas fa-backward"></i> Zurück
    </a>
@endsection
@section('content')
    <div class="row">
        <div class="col-2">
            <div class="row">
                <a href="{{ route('auth.mailbox.mail.index', $webMailAddressId) }}"
                   class="col-12 border p-2 bg-primary text-white">
                    Eingang
                </a>
                <a href="{{ route('auth.mailbox.mail.outgoing', $webMailAddressId) }}" class="col-12 border p-2">
                    Ausgang
                </a>
            </div>
        </div>
        <div class="col-10">
            @if(count($attachments))
                <div class="row">
                    @foreach($attachments as $attachment)
                        <div class="ml-2 p-1">
                            <button type="button" class="btn btn-outline-dark" data-toggle="modal"
                                    data-target="#attachment{{ $attachment->getId() }}">
                                {{  strtoupper(pathinfo($attachment->getName(), PATHINFO_EXTENSION)) }}
                                | {{ $attachment->getName() }}
                            </button>
                        </div>
                    @endforeach
                </div>
                <div class="border m-1"></div>
            @endif
            <div class="row m-1 p-2">
                <div class="col-4 font-weight-bold"> Von:</div>
                <div class="col-8">
                    {{ $mail->getSender() }}
                </div>
            </div>
            <div class="row m-1 p-2">
                <div class="col-4 font-weight-bold"> An:</div>
                <div class="col-8">
                    {{ $mail->getReceiver() }}
                </div>
            </div>
            <div class="row m-1 p-2">
                <div class="col-4 font-weight-bold"> Betreff:</div>
                <div class="col-8 ">
                    <u> {{ $mail->getTitle() }}</u>
                </div>
            </div>
            <div class="row m-1 p-2">
                <div class="col-12 border rounded m-3">{!! $mail->getMessage() !!}</div>
            </div>
            @foreach($attachments as $attachment)
                <div class="modal m-1 fade" id="attachment{{ $attachment->getId() }}" tabindex="-1" role="dialog"
                     aria-hidden="true">
                    <div class="modal-dialog modal-xl" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">{{ $attachment->getName() }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <embed width="100%" height="500px" src="{{ asset($attachment->getPath()) }}">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
