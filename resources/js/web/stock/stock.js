class Stock {

    /**
     * @public
     */
    index() {
        var graphs = document.getElementsByClassName("stock");
        for (let i = 0; i < graphs.length; i++) {
            let graph = graphs[i];
            let id = graph.id;
            let url = graph.dataset.route;
            let name = graph.dataset.name;
            axios.get(url, {
                params: {},
            }).then(response => {
                let data = response.data;
                this.graph(id, name, data.labels, data.changes);
            });
        }
    }

    graph(id, name, labels, graphData) {
        let data = {
            labels: labels,
            datasets: [
                {
                    label: name,
                    backgroundColor: 'rgb(87,239,0)',
                    borderColor: 'rgb(87,239,0)',
                    data: graphData,
                },
            ]
        };
        let config = {
            type: 'line',
            data,
            options: {
                elements: {
                    point:{
                        radius: 0
                    }
                },
                plugins: {
                    legend: {
                        display: false
                    }
                }
            }
        };

        let element = document.getElementById('stockCanvas' + id);
        let chart = new Chart(
            element,
            config
        );

        element.style.display = 'block';
        let spinner = document.getElementById('stockSpinner' + id);
        spinner.style.display = 'none';
    }
}

/**
 * Rufe die Klasse auf
 */
$(document).ready(function () {
    if (window.location.href.indexOf("web/stock/") > -1) {
        let stock = new Stock();
        stock.index();
    }
});
