<?php
/**
 * Contacts.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 01.11.2019
 * Time: 13:23
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_GUID = 'guid';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    const COLUMN_NUMBER = 'number';

    /**
     * @var string
     */
    const COLUMN_CREATED_AT = 'createdAt';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'contacts';

    /**
     * @param Dto\Contacts $dto
     */
    public function insertEntry(Dto\Contacts $dto)
    {
        $this->insert([
            self::COLUMN_ID         => $dto->getId(),
            self::COLUMN_GUID       => $dto->getGuid(),
            self::COLUMN_NAME       => $dto->getName(),
            self::COLUMN_NUMBER     => $dto->getNumber(),
            self::COLUMN_CREATED_AT => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\Contacts $dto
     */
    public function updateEntry(Dto\Contacts $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update([
            self::COLUMN_GUID   => $dto->getGuid(),
            self::COLUMN_NAME   => $dto->getName(),
            self::COLUMN_NUMBER => $dto->getNumber(),
        ]);
    }

    /**
     * @param Dto\Contacts $dto
     */
    public function deleteEntry(Dto\Contacts $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param Contacts $entry
     * @return Dto\Contacts
     */
    public static function entryAsDto(Contacts $entry): Dto\Contacts
    {
        return (new Dto\Contacts())
            ->setId($entry->{self::COLUMN_ID})
            ->setGuid($entry->{self::COLUMN_GUID})
            ->setName($entry->{self::COLUMN_NAME})
            ->setNumber($entry->{self::COLUMN_NUMBER});
    }
}
