<?php
/**
 * Transactions
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 01.11.2019
 * Time: 14:31
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

use Carbon\Carbon;

class Transactions
{
    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var int
     */
    private $tx = 0;

    /**
     * @var int
     */
    private $rx = 0;

    /**
     * @var int
     */
    private $amount = 0;

    /**
     * @var string
     */
    private $type = '';

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Transactions
     */
    public function setId(?int $id): Transactions
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getTx(): int
    {
        return $this->tx;
    }

    /**
     * @param int $tx
     * @return Transactions
     */
    public function setTx(int $tx): Transactions
    {
        $this->tx = $tx;
        return $this;
    }

    /**
     * @return int
     */
    public function getRx(): int
    {
        return $this->rx;
    }

    /**
     * @param int $rx
     * @return Transactions
     */
    public function setRx(int $rx): Transactions
    {
        $this->rx = $rx;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     * @return Transactions
     */
    public function setAmount(int $amount): Transactions
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Transactions
     */
    public function setType(string $type): Transactions
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return Transactions
     */
    public function setCreatedAt(?Carbon $createdAt): Transactions
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}
