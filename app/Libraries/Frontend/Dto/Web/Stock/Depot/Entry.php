<?php
/**
 * Entry
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 02.09.2021
 * Time: 17:46
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Dto\Web\Stock\Depot;


class Entry
{
    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var float
     */
    private $price = 0.00;

    /**
     * @var float
     */
    private $buyPrice = 0.00;

    /**
     * @var float
     */
    private $buyTotalPrice = 0.00;

    /**
     * @var float
     */
    private $currentTotalValue = 0.00;

    /**
     * @var float
     */
    private $currentValue = 0.00;

    /**
     * @var string
     */
    private $currentValuePercent = '';

    /**
     * @var \App\Libraries\Frontend\Dto\Web\Stock\Depot\Entry\Entry[]
     */
    private $entries = [];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Entry
     */
    public function setId(int $id): Entry
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Entry
     */
    public function setName(string $name): Entry
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return Entry
     */
    public function setPrice(float $price): Entry
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return float
     */
    public function getBuyPrice(): float
    {
        return $this->buyPrice;
    }

    /**
     * @param float $buyPrice
     * @return Entry
     */
    public function setBuyPrice(float $buyPrice): Entry
    {
        $this->buyPrice = $buyPrice;
        return $this;
    }

    /**
     * @return float
     */
    public function getBuyTotalPrice(): float
    {
        return $this->buyTotalPrice;
    }

    /**
     * @param float $buyTotalPrice
     * @return Entry
     */
    public function setBuyTotalPrice(float $buyTotalPrice): Entry
    {
        $this->buyTotalPrice = $buyTotalPrice;
        return $this;
    }

    /**
     * @return float
     */
    public function getCurrentTotalValue(): float
    {
        return $this->currentTotalValue;
    }

    /**
     * @param float $currentTotalValue
     * @return Entry
     */
    public function setCurrentTotalValue(float $currentTotalValue): Entry
    {
        $this->currentTotalValue = $currentTotalValue;
        return $this;
    }

    /**
     * @return float
     */
    public function getCurrentValue(): float
    {
        return $this->currentValue;
    }

    /**
     * @param float $currentValue
     * @return Entry
     */
    public function setCurrentValue(float $currentValue): Entry
    {
        $this->currentValue = $currentValue;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrentValuePercent(): string
    {
        return $this->currentValuePercent;
    }

    /**
     * @param string $currentValuePercent
     * @return Entry
     */
    public function setCurrentValuePercent(string $currentValuePercent): Entry
    {
        $this->currentValuePercent = $currentValuePercent;
        return $this;
    }

    /**
     * @return Entry\Entry[]
     */
    public function getEntries(): array
    {
        return $this->entries;
    }

    /**
     * @param Entry\Entry[] $entries
     * @return Entry
     */
    public function setEntries(array $entries): Entry
    {
        $this->entries = $entries;
        return $this;
    }
}
