<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 10.04.2020
 * Time: 16:00
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('api.layout.app')
@section('title')
    <i class="fas fa-box"></i>
    Gibt einem Spieler eine bestimmte Anzahl eines Items
@endsection
@section('content')
    <form method="post" action="{{ route('api.player.addItem.save') }}">
        {{ csrf_field() }}

        @include('api.includes.select', [
            'name' => 'pid',
            'label' => 'Spieler',
            'entries' => $players,
            'currentSelect' => $currentPlayer,
        ])

        @include('api.includes.select', [
           'name' => 'item',
           'label' => 'Item',
           'entries' => $items,
           'currentSelect' => null,
        ])

        <div class="form-group">
            <label for="amount">Anzahl</label>
            <input type="number" class="form-control" id="amount" name="amount" value="1">
        </div>
        <button type="submit" class="btn btn-danger">Bestätigen</button>
    </form>
    <br/>
@endsection
