@extends('admin.layout.app')
@section('title')
    <i class="fab fa-wpforms"></i>
    Eintrag erstellen
@endsection
@section('content')
    <form action="{{ route('admin.supportForm.infringement.store') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="userName">Name IC</label>
            <input type="text" class="form-control" id="userName" name="userName">
        </div>
        <div class="form-group">
            <label for="videoLink">Videolink</label>
            <input type="text" class="form-control" id="videoLink" name="videoLink">
        </div>
        <div class="form-group">
            <label for="reason">Fallgrund</label>
            <textarea class="form-control" id="reason" name="reason"></textarea>
        </div>
        <div class="form-group">
            <label for="reportedBy">Gemeldet von ...</label>
            <input type="text" class="form-control" id="reportedBy" name="reportedBy">
        </div>
        <div class="form-group">
            <label for="description">Grobe Fallbeschreibung</label>
            <textarea class="form-control" id="description" name="description"></textarea>
        </div>
        <div class="form-group">
            <label for="action">Durchgeführte Maßnahme</label>
            <textarea class="form-control" id="action" name="action"></textarea>
        </div>
        <br/>
        <button type="submit" class="btn btn-dark">Erstellen</button>
    </form>
@endsection

