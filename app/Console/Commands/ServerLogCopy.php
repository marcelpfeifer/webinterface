<?php
/**
 * CopyServerLog
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 24.09.2021
 * Time: 16:58
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Console\Commands;


use App\Libraries\Api\Server\System\Status;
use App\Libraries\Common\Directory;
use App\Libraries\Common\File;
use App\Model\Dto\WebServerStatus;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use phpseclib3\Crypt\PublicKeyLoader;
use phpseclib3\Net\SFTP;

class ServerLogCopy extends Command
{
    /**
     * @var string
     */
    protected $signature = 'serverLog:copy';

    /**
     * @var string
     */
    protected $description = 'Kopiert den Server Log vom Server';

    public function handle()
    {
        $host = env('ALTV_HOST');
        $username = env('ALTV_USERNAME');
        $keyStoragePath = env('ALTV_KEY_STORAGE_PATH');
        $remoteFile = env('ALTV_REMOTE_FILE');
        $keyPath = storage_path($keyStoragePath);
        if (!$username || !$host || !$remoteFile || !file_exists($keyPath)) {
            Log::error('[ServerLogCopy]: Konnte .env Daten nicht finden');
        }

        $key = PublicKeyLoader::load(file_get_contents($keyPath));
        $sftp = new SFTP($host);

        if ($sftp->login($username, $key)) {
            $storageDir = storage_path(\App\Libraries\Log\Server\Directory::STORAGE_DIR);
            $localFile = $storageDir . \App\Libraries\Log\Server\Directory::FILENAME;

            Directory::create($storageDir);
            $sftp->get($remoteFile, $localFile);
            File::changePermission($localFile);
        } else {
            Log::error('[ServerLogCopy]: Konnte sich nicht zum Server verbinden');
        }
    }
}
