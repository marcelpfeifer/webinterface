<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.10.2021
 * Time: 13:52
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('api.layout.app')
@section('title')
    <i class="fas fa-play-circle"></i>
    Server
@endsection
@section('content')
    <div class="row m-2">
        <div class="col-3">
            Hauptserver
        </div>
        <div class="col-3">
            <form action="{{ route('api.server.server.save') }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="action" value="{{ \App\Http\Request\Enum\Api\Server\Server\Action::START }}">
                <input type="hidden" name="type" value="{{ \App\Libraries\Api\Server\Enum\Type::MAIN_SERVER }}">
                <button class="btn btn-danger" type="submit">
                    <i class="fas fa-play-circle"></i>
                    Starten
                </button>
            </form>
        </div>
        <div class="col-3">
            <form action="{{ route('api.server.server.save') }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="action" value="{{ \App\Http\Request\Enum\Api\Server\Server\Action::STOP }}">
                <input type="hidden" name="type" value="{{ \App\Libraries\Api\Server\Enum\Type::MAIN_SERVER }}">
                <button class="btn btn-danger" type="submit">
                    <i class="fas fa-stop-circle"></i>
                    Stoppen
                </button>
            </form>
        </div>
        <div class="col-3">
            <form action="{{ route('api.server.server.save') }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="action" value="{{ \App\Http\Request\Enum\Api\Server\Server\Action::STATUS }}">
                <input type="hidden" name="type" value="{{ \App\Libraries\Api\Server\Enum\Type::MAIN_SERVER }}">
                <button class="btn btn-danger" type="submit">
                    <i class="fas fa-running"></i>
                    Status
                </button>
            </form>
        </div>
    </div>
    <div class="row m-2">
        <div class="col-3">
            Testserver
        </div>
        <div class="col-3">
            <form action="{{ route('api.server.server.save') }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="action" value="{{ \App\Http\Request\Enum\Api\Server\Server\Action::START }}">
                <input type="hidden" name="type" value="{{ \App\Libraries\Api\Server\Enum\Type::TEST_SERVER }}">
                <button class="btn btn-danger" type="submit">
                    <i class="fas fa-play-circle"></i>
                    Starten
                </button>
            </form>
        </div>
        <div class="col-3">
            <form action="{{ route('api.server.server.save') }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="action" value="{{ \App\Http\Request\Enum\Api\Server\Server\Action::STOP }}">
                <input type="hidden" name="type" value="{{ \App\Libraries\Api\Server\Enum\Type::TEST_SERVER }}">
                <button class="btn btn-danger" type="submit">
                    <i class="fas fa-stop-circle"></i>
                    Stoppen
                </button>
            </form>
        </div>
        <div class="col-3">
            <form action="{{ route('api.server.server.save') }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="action" value="{{ \App\Http\Request\Enum\Api\Server\Server\Action::STATUS }}">
                <input type="hidden" name="type" value="{{ \App\Libraries\Api\Server\Enum\Type::TEST_SERVER }}">
                <button class="btn btn-danger" type="submit">
                    <i class="fas fa-running"></i>
                    Status
                </button>
            </form>
        </div>
    </div>
    <div class="row m-2">
        <div class="col-3">
            TS3-Server
        </div>
        <div class="col-3">
            <form action="{{ route('api.server.server.save') }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="action" value="{{ \App\Http\Request\Enum\Api\Server\Server\Action::START }}">
                <input type="hidden" name="type" value="{{ \App\Libraries\Api\Server\Enum\Type::TS_SERVER }}">
                <button class="btn btn-danger" type="submit">
                    <i class="fas fa-play-circle"></i>
                    Starten
                </button>
            </form>
        </div>
        <div class="col-3">
            <form action="{{ route('api.server.server.save') }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="action" value="{{ \App\Http\Request\Enum\Api\Server\Server\Action::STOP }}">
                <input type="hidden" name="type" value="{{ \App\Libraries\Api\Server\Enum\Type::TS_SERVER }}">
                <button class="btn btn-danger" type="submit">
                    <i class="fas fa-stop-circle"></i>
                    Stoppen
                </button>
            </form>
        </div>
        <div class="col-3">
            <form action="{{ route('api.server.server.save') }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="action" value="{{ \App\Http\Request\Enum\Api\Server\Server\Action::STATUS }}">
                <input type="hidden" name="type" value="{{ \App\Libraries\Api\Server\Enum\Type::TS_SERVER }}">
                <button class="btn btn-danger" type="submit">
                    <i class="fas fa-running"></i>
                    Status
                </button>
            </form>
        </div>
    </div>

    <br/>
@endsection
