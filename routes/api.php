<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.01.2021
 * Time: 19:42
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

Route::namespace('Api')->prefix('api')->name('api.')->middleware(
    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_API
)->group(
    function () {
        Route::get('/', 'IndexController@index')->name('index');
        Route::namespace('Player')->prefix('player')->name('player.')->group(
            function () {
                Route::prefix('tp')->name('tp.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_API_TP
                )->group(
                    function () {
                        Route::get('index', 'TpController@index')->name('index');
                        Route::post('save', 'TpController@save')->name('save');
                    }
                );
                Route::prefix('vanish')->name('vanish.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_API_INVISIBLE
                )->group(
                    function () {
                        Route::get('index', 'VanishController@index')->name('index');
                        Route::post('save', 'VanishController@save')->name('save');
                    }
                );
                Route::prefix('fly')->name('fly.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_API_FLY
                )->group(
                    function () {
                        Route::get('index', 'FlyController@index')->name('index');
                        Route::post('save', 'FlyController@save')->name('save');
                    }
                );
                Route::prefix('revive')->name('revive.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_API_REVIVE
                )->group(
                    function () {
                        Route::get('index', 'ReviveController@index')->name('index');
                        Route::post('save', 'ReviveController@save')->name('save');
                    }
                );
                Route::prefix('addItem')->name('addItem.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_API_GIVE_ITEM
                )->group(
                    function () {
                        Route::get('index', 'AddItemController@index')->name('index');
                        Route::post('save', 'AddItemController@save')->name('save');
                    }
                );
                Route::prefix('giveMoney')->name('giveMoney.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_API_GIVE_MONEY
                )->group(
                    function () {
                        Route::get('index', 'GiveMoneyController@index')->name('index');
                        Route::post('save', 'GiveMoneyController@save')->name('save');
                    }
                );
                Route::prefix('removeMoney')->name('removeMoney.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_API_REMOVE_MONEY
                )->group(
                    function () {
                        Route::get('index', 'RemoveMoneyController@index')->name('index');
                        Route::post('save', 'RemoveMoneyController@save')->name('save');
                    }
                );
                Route::prefix('setMoney')->name('setMoney.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_API_SET_MONEY
                )->group(
                    function () {
                        Route::get('index', 'SetMoneyController@index')->name('index');
                        Route::post('save', 'SetMoneyController@save')->name('save');
                    }
                );
                Route::prefix('dimension')->name('dimension.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_API_DIMENSION
                )->group(
                    function () {
                        Route::get('index', 'DimensionController@index')->name('index');
                        Route::post('save', 'DimensionController@save')->name('save');
                    }
                );
                Route::prefix('godMode')->name('godMode.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_API_GOD_MODE
                )->group(
                    function () {
                        Route::get('index', 'GodModeController@index')->name('index');
                        Route::post('save', 'GodModeController@save')->name('save');
                    }
                );
                Route::prefix('faction')->name('faction.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_API_FACTION
                )->group(
                    function () {
                        Route::get('index', 'FactionController@index')->name('index');
                        Route::post('save', 'FactionController@save')->name('save');
                    }
                );
                Route::prefix('model')->name('model.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_API_MODEL
                )->group(
                    function () {
                        Route::get('index', 'ModelController@index')->name('index');
                        Route::post('save', 'ModelController@save')->name('save');
                    }
                );
                Route::prefix('kick')->name('kick.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_API_KICK
                )->group(
                    function () {
                        Route::get('index', 'KickController@index')->name('index');
                        Route::post('save', 'KickController@save')->name('save');
                    }
                );
                Route::prefix('spectate')->name('spectate.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_API_SPECTATE
                )->group(
                    function () {
                        Route::get('index', 'SpectateController@index')->name('index');
                        Route::post('save', 'SpectateController@save')->name('save');
                    }
                );
                Route::prefix('freeze')->name('freeze.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_API_FREEZE
                )->group(
                    function () {
                        Route::get('index', 'FreezeController@index')->name('index');
                        Route::post('save', 'FreezeController@save')->name('save');
                    }
                );
                Route::prefix('kill')->name('kill.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_API_KILL
                )->group(
                    function () {
                        Route::get('index', 'KillController@index')->name('index');
                        Route::post('save', 'KillController@save')->name('save');
                    }
                );
                Route::prefix('unJail')->name('unJail.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_API_PRISON
                )->group(
                    function () {
                        Route::get('index', 'UnJailController@index')->name('index');
                        Route::post('save', 'UnJailController@save')->name('save');
                    }
                );
                Route::prefix('showNames')->name('showNames.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_API_NAMES
                )->group(
                    function () {
                        Route::get('index', 'ShowNamesController@index')->name('index');
                        Route::post('save', 'ShowNamesController@save')->name('save');
                    }
                );
            }
        );
        Route::namespace('Vehicle')->prefix('vehicle')->name('vehicle.')->group(
            function () {
                Route::prefix('repairId')->name('repairId.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_API_REPAIR_ID
                )->group(
                    function () {
                        Route::get('index', 'RepairIdController@index')->name('index');
                        Route::post('save', 'RepairIdController@save')->name('save');
                    }
                );
                Route::prefix('repair')->name('repair.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_API_REPAIR_DRIVER
                )->group(
                    function () {
                        Route::get('index', 'RepairController@index')->name('index');
                        Route::post('save', 'RepairController@save')->name('save');
                    }
                );
                Route::prefix('park')->name('park.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_API_PARK
                )->group(
                    function () {
                        Route::get('index', 'ParkController@index')->name('index');
                        Route::post('save', 'ParkController@save')->name('save');
                    }
                );
            }
        );
        Route::namespace('Server')->prefix('server')->name('server.')->group(
            function () {
                Route::prefix('message')->name('message.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_API_MESSAGE
                )->group(
                    function () {
                        Route::get('index', 'MessageController@index')->name('index');
                        Route::post('save', 'MessageController@save')->name('save');
                    }
                );
                Route::prefix('server')->name('server.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_API_SERVER
                )->group(
                    function () {
                        Route::get('index', 'ServerController@index')->name('index');
                        Route::post('save', 'ServerController@save')->name('save');
                    }
                );
            }
        );
    }
);
