<?php
/**
 * WithCharacter
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 18.09.2020
 * Time: 18:43
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto\Report;

use App\Model\Dto\Character;
use App\Model\Dto\Report;

class WithCharacter
{

    /**
     * @var Report
     */
    private $report;

    /**
     * @var Character|null
     */
    private $character = null;

    /**
     * @return Report
     */
    public function getReport(): Report
    {
        return $this->report;
    }

    /**
     * @param Report $report
     * @return WithCharacter
     */
    public function setReport(Report $report): WithCharacter
    {
        $this->report = $report;
        return $this;
    }

    /**
     * @return Character|null
     */
    public function getCharacter(): ?Character
    {
        return $this->character;
    }

    /**
     * @param Character|null $character
     * @return WithCharacter
     */
    public function setCharacter(?Character $character): WithCharacter
    {
        $this->character = $character;
        return $this;
    }
}
