<?php
/**
 * Cashflow
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 13.08.2020
 * Time: 20:42
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

use Carbon\Carbon;

class Cashflow
{

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var int
     */
    private $guid = 0;

    /**
     * @var int
     */
    private $amount = 0;

    /**
     * @var string
     */
    private $reason = '';

    /**
     * @var string
     */
    private $source = '';

    /**
     * @var string
     */
    private $type = '';

    /**
     * @var Carbon
     */
    private $createdAt;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Cashflow
     */
    public function setId(?int $id): Cashflow
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getGuid(): int
    {
        return $this->guid;
    }

    /**
     * @param int $guid
     * @return Cashflow
     */
    public function setGuid(int $guid): Cashflow
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     * @return Cashflow
     */
    public function setAmount(int $amount): Cashflow
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getReason(): string
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     * @return Cashflow
     */
    public function setReason(string $reason): Cashflow
    {
        $this->reason = $reason;
        return $this;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @param string $source
     * @return Cashflow
     */
    public function setSource(string $source): Cashflow
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Cashflow
     */
    public function setType(string $type): Cashflow
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon $createdAt
     * @return Cashflow
     */
    public function setCreatedAt(Carbon $createdAt): Cashflow
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}
