<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 26.07.2020
 * Time: 16:13
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('darkWeb.layout.app')
@section('url')
    {{  md5('https://eternityV.de/loading') }}
@endsection
@section('content')
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <div class="row">
        <div class="col-12">
            <div class="text-center darkWebColor font-weight-bold">
                <div class="spinner-border" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
                Bitte warten...
            </div>
        </div>
    </div>
@endsection
