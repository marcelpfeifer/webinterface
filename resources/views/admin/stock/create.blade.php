<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 29.04.2020
 * Time: 17:25
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-coins"></i>
    Eintrag erstellen
@endsection
@section('content')
    <form action="{{ route('admin.stock.save') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="item">Name</label>
            <input type="text" class="form-control" id="name" name="name" required>
        </div>
        <div class="form-group">
            <label for="price">Preis</label>
            <input type="number" step="0.01" class="form-control" id="price" name="price" required>
        </div>
        <div class="form-group">
            <label for="stock">Lager</label>
            <input type="number" class="form-control" id="stock" name="stock" required>
        </div>
        <div class="form-group">
            <label for="minChange">Min. Veränderung</label>
            <input type="number" step="0.01" class="form-control" id="minChange" name="minChange" required>
        </div>
        <div class="form-group">
            <label for="maxChange">Max. Veränderung</label>
            <input type="number" step="0.01" class="form-control" id="maxChange" name="maxChange" required>
        </div>
        <br/>
        <button type="submit" class="btn btn-dark">Erstellen</button>
    </form>
@endsection

