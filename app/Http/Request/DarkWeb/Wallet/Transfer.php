<?php
/**
 * Transfer
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 22.08.2020
 * Time: 21:22
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\DarkWeb\Wallet;

use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;
use App\Model\Character;

class Transfer extends AValidator implements IValidator
{
    /**
     * @return string[]
     */
    public function getRules(): array
    {
        return [
            'hash'   => [
                'required',
                'string',
                'exists:character,bitcoinHashIllegal'
            ],
            'amount' => [
                'required',
                'int',
            ]
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'hash.required'   => 'Der Hash muss übergeben werden!',
            'hash.string'     => 'Der Hash darf nur Zeichen enthalten!',
            'hash.exists'     => 'Der Hash darf nur Zeichen enthalten!',
            'amount.required' => 'Der Betrag muss übergeben werden!',
            'amount.int'      => 'Der Betrag darf nur Zahlen enthalten!',
        ];
    }

    /**
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validate(): \Illuminate\Contracts\Validation\Validator
    {
        $validator = parent::validate();

        $validator->after(
            function ($validator) {
                $character = (new Character())->getEntryById($this->id);
                $amount = $this->request->input('amount', 0);
                if ($character->getBitcoinIllegal() < $amount) {
                    $validator->errors()->add('amount', 'Du hast nicht genug Bitcoins auf deinem Konto!');
                }
            }
        );

        return $validator;
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\DarkWeb\Wallet\Transfer())
            ->setHash($this->request->input('hash'))
            ->setAmount($this->request->input('amount'));
    }
}
