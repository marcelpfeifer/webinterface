<?php
/**
 * edit.blade.php
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 15.02.2020
 * Time: 17:05
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

/**
 * @var $character \App\Model\Dto\Character
 * @var $account \App\Model\Dto\Account
 */
?>
@extends('layouts.app')
@section('title')
    <i class="fas fa-user-circle"></i>
    Account bearbeiten
@endsection
@section('content')

    <form method="POST" action="{{ route('auth.account.save') }}" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <!-- left column -->
            <div class="col-md-3">
                <div class="text-center">
                    @if($account->getImageName())
                        <img src="{{ url('img/user/logo/'. $account->getId(). '.jpg') }}" height="125px"
                             width="125px">
                    @else
                        <i class="fas fa-image fa-8x text-primary img-fluid"></i>
                    @endif
                    <h6>Bild muss 500x500 Pixel sein und darf nicht größer als 500kb sein</h6>
                    <input type="file" name="image" value="" class="form-control-file">
                    <p class="small text-danger">
                        Nach ändern des Profilbilds bitte einmal Strg+F5 drücken um den Browser-Cache zu löschen
                    </p>
                </div>
            </div>

            <div class="col-md-9 personal-info">
                <h3>Informationen</h3>

                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="password">Neues Passwort:</label>
                        <div class="col-md-8">
                            <input class="form-control" name="password" id="password" type="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="password_confirmation">Bestätige Passwort:</label>
                        <div class="col-md-8">
                            <input class="form-control" name="password_confirmation" id="password_confirmation"
                                   type="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8">
                            <input type="submit" class="btn btn-primary" value="Speichern">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </form>

@endsection
