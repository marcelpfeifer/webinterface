<?php
/**
 * DeathLog
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.10.2021
 * Time: 13:07
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class DeathLog extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_VICTIM = 'victim';

    /**
     * @var string
     */
    const COLUMN_KILLER = 'killer';

    /**
     * @var string
     */
    const COLUMN_VICTIM_POSITION = 'victimPosition';

    /**
     * @var string
     */
    const COLUMN_KILLER_POSITION = 'killerPosition';

    /**
     * @var string
     */
    const COLUMN_IS_KILLER_IN_VEHICLE = 'isKillerInVehicle';

    /**
     * @var string
     */
    const COLUMN_WEAPON_HASH = 'weaponHash';

    /**
     * @var string
     */
    const COLUMN_CREATED_AT = 'createdAt';

    /**
     * @var string
     */
    const COLUMN_UPDATED_AT = 'updatedAt';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'death_log';

    /**
     * @var string[]
     */
    protected $dates = [
        self::COLUMN_CREATED_AT,
        self::COLUMN_UPDATED_AT,
    ];

    /**
     * @return LengthAwarePaginator
     */
    public function getLogEntriesPagination(): LengthAwarePaginator
    {
        return $this->orderBy(self::COLUMN_CREATED_AT, 'DESC')->paginate(15);
    }

    /**
     * @param DeathLog $entry
     * @return Dto\DeathLog
     */
    public static function entryAsDto(DeathLog $entry): Dto\DeathLog
    {
        return (new Dto\DeathLog())
            ->setId($entry->{self::COLUMN_ID})
            ->setVictim($entry->{self::COLUMN_VICTIM})
            ->setKiller($entry->{self::COLUMN_KILLER})
            ->setVictimPosition($entry->{self::COLUMN_VICTIM_POSITION})
            ->setKillerPosition($entry->{self::COLUMN_KILLER_POSITION})
            ->setIsKillerInVehicle($entry->{self::COLUMN_IS_KILLER_IN_VEHICLE})
            ->setWeaponHash($entry->{self::COLUMN_WEAPON_HASH})
            ->setCreatedAt($entry->{self::COLUMN_CREATED_AT})
            ->setUpdatedAt($entry->{self::COLUMN_UPDATED_AT});
    }
}
