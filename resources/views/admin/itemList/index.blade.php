<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 12.05.2020
 * Time: 19:40
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-search"></i>
    Item- und Fahrzeugsuche
@endsection
@section('content')
    <h2>Item Suche:</h2>
    <form action="{{ route('admin.itemList.searchItemList') }}" method="GET">
        <div class="form-group">
            <label for="name">Name</label>
            <select class="form-control selectWithSearch" id="name" name="name">
                @foreach($itemNames as $name)
                    <option value="{{ $name }}">{{ $name }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-dark">Suche</button>
    </form>
    <br/>
    <h2>Fahrzeug Suche:</h2>
    <form action="{{ route('admin.itemList.searchVehicles') }}" method="GET">
        <div class="form-group">
            <label for="name">Name</label>
            <select class="form-control selectWithSearch" id="name" name="name">
                @foreach($vehicleNames as $name)
                    <option value="{{ $name }}">{{ $name }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-dark">Suche</button>
    </form>
    <br/>
    <h2>Ergebnis:</h2>
    @if(isset($items))
        <div class="row">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Spieler ID</th>
                    <th scope="col">Spieler Name</th>
                    <th scope="col">Name</th>
                    <th scope="col">Anzahl</th>
                    <th scope="col">Storage</th>

                </tr>
                </thead>
                <tbody>
                @foreach($items as $item)
                    <tr>
                        <th scope="row">{{ $item->getId() }}</th>
                        <td>{{ $item->getCharacterId() }}</td>
                        <td>{{ $item->getPlayerName() }}</td>
                        <td>{{ $item->getName() }}</td>
                        <td>{{ $item->getQuantity() }}</td>
                        <td>{{ $item->getStorage() }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif
    @if(isset($vehicles))
        <div class="row">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Spieler ID</th>
                    <th scope="col">Spieler Name</th>
                    <th scope="col">Model</th>
                    <th scope="col">Nummerschild</th>
                    <th scope="col">Garage</th>

                </tr>
                </thead>
                <tbody>
                @foreach($vehicles as $vehicle)
                    <tr>
                        <th scope="row">{{ $vehicle->getVehicle()->getId() }}</th>
                        <td>{{ $vehicle->getVehicle()->getId() }}</td>
                        <td>{{ $vehicle->getCharacter() ? $vehicle->getCharacter()->getName() :''}}</td>
                        <td>{{ $vehicle->getVehicle()->getModel() }}</td>
                        <td>{{ $vehicle->getVehicle()->getNumberPlate() }}</td>
                        <td>{{ $vehicle->getVehicle()->getGId() }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif
@endsection
