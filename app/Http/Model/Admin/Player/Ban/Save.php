<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 07.01.2021
 * Time: 10:18
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Model\Admin\Player\Ban;

use App\Libraries\Api\Player\Kick;
use App\Model\Bans;
use App\Model\Dto\WebAdminBanAccount;
use App\Model\WebAdminBanPoints;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Save
{
    /**
     * @param WebAdminBanAccount $dto
     * @return bool
     */
    public static function save(WebAdminBanAccount $dto, string $userReason): bool
    {
        $punishment = self::getPunishmentToBan($dto, true);

        if ($punishment) {
            self::executePunishment($dto, $punishment, $userReason);
        }

        return true;
    }

    /**
     * @param WebAdminBanAccount $dto
     * @return \App\Model\Dto\WebAdminBanPoints|null
     */
    public static function getPunishmentToBan(WebAdminBanAccount $dto, bool $calculateAll = false)
    {
        $points = WebAdminBanPoints::all();
        $punishment = null;

        $accountPoints = $dto->getPoints();
        if ($calculateAll) {
            $accountPoints = 0;
            $bans = (new \App\Model\WebAdminBanAccount())->getBansByAccountId($dto->getAccountId());
            foreach ($bans as $ban) {
                $accountPoints += $ban->getPoints();
            }
        }

        foreach ($points as $point) {
            $pointDto = WebAdminBanPoints::entryAsDto($point);
            if ($accountPoints >= $pointDto->getPoints()) {
                if ($punishment) {
                    if ($punishment->getPoints() < $pointDto->getPoints()) {
                        $punishment = $pointDto;
                    }
                } else {
                    $punishment = $pointDto;
                }
            }
        }
        return $punishment;
    }

    /**
     * @param WebAdminBanAccount $dto
     * @param \App\Model\Dto\WebAdminBanPoints $pointDto
     */
    public static function executePunishment(
        WebAdminBanAccount $dto,
        \App\Model\Dto\WebAdminBanPoints $pointDto,
        string $userReason
    ) {
        $banDto = (new \App\Model\Dto\Bans())
            ->setGuid($dto->getAccountId())
            ->setReason($userReason);

        $banModel = new Bans();
        // Kein Perma
        if ($pointDto->getLength() !== 0) {
            $startDate = self::getStartDateForBan($dto->getAccountId());
            $banDto
                ->setTimeBan(1)
                ->setExpire(
                    $startDate
                        ->addHours($pointDto->getLength())
                        ->setTimezone('Europe/Berlin')
                        ->format('Y-m-d H:i:s')
                );
        }
        $banModel->insertEntry($banDto);

        // Wenn es eh schon ein Perma ist müssen die voherigen Strafen nicht gecheckt werden
        if ($pointDto->getLength() !== 0) {
            // Schaue durch alle Bestrafung und banne den Spieler ggf. Perma
            self::checkPreviousPunishments($dto);
        }

        // Schmeiße den Spieler vom Server
        Kick::call($dto->getAccountId(), $userReason);
    }

    /**
     * @param int $accountId
     * @return Carbon
     */
    private static function getStartDateForBan(int $accountId): Carbon
    {
        $banModel = new Bans();
        $startDate = Carbon::now();
        // Checks if Account has Previous active Bans
        foreach ($banModel->getBansByCharacterId($accountId) as $ban) {
            if ($ban->getExpire() == null) {
                continue;
            }
            $expireAsCarbon = Carbon::createFromFormat('Y-m-d H:i:s', $ban->getExpire());
            if ($expireAsCarbon->greaterThan($startDate)) {
                $startDate = $expireAsCarbon;
            }
        }
        return $startDate;
    }

    /**
     * @param WebAdminBanAccount $dto
     */
    public static function checkPreviousPunishments(WebAdminBanAccount $dto)
    {
        $bans = (new \App\Model\WebAdminBanAccount())->getBansByAccountId($dto->getAccountId());
        $punishments = [];
        foreach ($bans as $ban) {
            $punishment = self::getPunishmentToBan($ban);
            // Schließe Bans die länger als 30 Tage her sind aus
            if ($punishment && $ban->getCreatedAt()->diffInDays(Carbon::now()) <= 30) {
                $punishments[] = $punishment;
            }
        }

        if (count($punishments) <= 1) {
            return;
        }

        // Gehe alle Bestrafungen durch
        foreach ($punishments as $key => $punishment) {
            if ($punishment->getAdditional() >= 2) {
                $count = 1; // Aktuelle Strafe fließt mit ein
                foreach ($punishments as $otherPunishmentKey => $otherPunishment) {
                    // Gleicher Eintrag wird übersprungen
                    if ($key === $otherPunishmentKey) {
                        continue;
                    }
                    // Ist das eine mildere Strafe fließt diese hier nicht ein
                    if ($punishment->getPoints() > $otherPunishment->getPoints()) {
                        continue;
                    }
                    // Strafe zählt rein
                    $count++;
                }

                if ($count >= $punishment->getAdditional()) {
                    $banDto = (new \App\Model\Dto\Bans())
                        ->setGuid($dto->getAccountId())
                        ->setReason('PERMA durch zu viele voherige Strafen!');
                    (new Bans())->insertEntry($banDto);
                    break;
                }
            }
        }
    }
}
