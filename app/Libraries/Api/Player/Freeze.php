<?php
/**
 * Freeze
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 13.04.2020
 * Time: 22:22
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Api\Player;

use App\Libraries\Api\AClient;
use App\Model\WebConfig;

class Freeze extends AClient
{
    /**
     * @var string
     */
    const URL = 'player/freeze';

    /**
     * @param int $pid
     * @param bool $value
     * @return bool
     */
    public static function call(int $pid, bool $value): bool
    {
        $response = self::sendRequest('POST', WebConfig::getEntryByKey('api_url')->getValue() . self::URL, [
            'query' => [
                'pid'   => $pid,
                'value' => $value,
            ]
        ]);
        return $response && $response->getStatusCode() == 200;
    }
}
