<?php
/**
 * User
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 27.08.2020
 * Time: 19:29
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Tickets;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_ORG_ID = 'org_id';

    /**
     * @var string
     */
    const COLUMN_DEFAULT_EMAIL_ID = 'default_email_id';

    /**
     * @var string
     */
    const COLUMN_STATUS = 'status';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    const COLUMN_CREATED = 'created';

    /**
     * @var string
     */
    const COLUMN_UPDATED = 'updated';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'ost_user';

    /**
     * @var string
     */
    protected $connection = 'mysqlTickets';

    /**
     * @param Dto\User $dto
     */
    public function insertEntry(Dto\User $dto)
    {
        $this->insert(
            [
                self::COLUMN_ID               => $dto->getId(),
                self::COLUMN_ORG_ID           => $dto->getOrgId(),
                self::COLUMN_DEFAULT_EMAIL_ID => $dto->getEmailId(),
                self::COLUMN_STATUS           => $dto->getStatus(),
                self::COLUMN_NAME             => $dto->getName(),
                self::COLUMN_CREATED          => Carbon::now(),
                self::COLUMN_UPDATED          => Carbon::now(),
            ]
        );
    }

    public function truncateTable()
    {
        self::truncate();
    }

}
