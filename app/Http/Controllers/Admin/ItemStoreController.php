<?php
/**
 * ItemStoreController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 19.09.2020
 * Time: 09:33
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin;


use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Model\ItemStore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ItemStoreController extends Controller
{
    /**
     * ItemStoreController constructor.
     */
    public function __construct()
    {
        $this->middleware(
            'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_ITEM_STORE
        );
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $results = [];
        foreach (ItemStore::all() as $vehicle) {
            $results[] = ItemStore::entryAsDto($vehicle);
        }
        return view(
            'admin.itemStore.index',
            [
                'results' => $results,
            ]
        );
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.itemStore.create', []);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function store(Request $request)
    {
        $dto = (new \App\Model\Dto\ItemStore())
            ->setItem($request->input('item'))
            ->setPrice($request->input('price'))
            ->setStock($request->input('stock'))
            ->setShop($request->input('shop'));

        (new ItemStore())->insertEntry($dto);
        Logging::insert(Name::ITEM_STORE, Auth::id(), $dto);
        $messages[] = (new Message())
            ->setMessage("Eintrag wurde erstellt")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.itemStore.index')->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, int $id)
    {
        $entry = ItemStore::find($id);
        if (!($entry)) {
            return view('404');
        }
        $dto = ItemStore::entryAsDto($entry);

        return view(
            'admin.itemStore.show',
            [
                'dto' => $dto,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function update(Request $request, int $id)
    {
        $entry = ItemStore::find($id);
        if (!($entry)) {
            $messages[] = (new Message())
                ->setMessage("Eintrag wurde nicht gefunden")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
            return redirect()->route('admin.itemStore.show', $id)->with('messages', $messages);
        }

        $dto = ItemStore::entryAsDto($entry);
        $oldDto = clone $dto;
        $dto->setId($id)
            ->setItem($request->input('item'))
            ->setPrice($request->input('price'))
            ->setStock($request->input('stock'))
            ->setShop($request->input('shop'));

        (new ItemStore())->updateEntry($dto);
        Logging::update(Name::ITEM_STORE, Auth::id(), $oldDto, $dto);

        $messages[] = (new Message())
            ->setMessage("Eintrag wurde aktualisiert")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.itemStore.show', $id)->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function destroy(Request $request, int $id)
    {
        $entry = ItemStore::find($id);
        if (!($entry)) {
            $messages[] = (new Message())
                ->setMessage("Eintrag wurde nicht gefunden")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
            return redirect()->route('admin.itemStore.index', $id)->with('messages', $messages);
        }

        $dto = ItemStore::entryAsDto($entry);
        (new ItemStore())->deleteEntry($dto);
        Logging::delete(Name::ITEM_STORE, Auth::id(), $dto);

        $messages[] = (new Message())
            ->setMessage("Eintrag wurde gelöscht")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.itemStore.index')->with('messages', $messages);
    }
}
