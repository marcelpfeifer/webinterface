<?php
/**
 * WebDarkBuy
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 02.05.2020
 * Time: 17:56
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;


use Carbon\Carbon;

class WebDarkBuy
{

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var int
     */
    private $characterId = 0;

    /**
     * @var int|null
     */
    private $categoryId = null;

    /**
     * @var int|null
     */
    private $buyerId = null;

    /**
     * @var string
     */
    private $status = '';

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var int
     */
    private $price = 0;

    /**
     * @var string
     */
    private $imageUrl = '';

    /**
     * @var string
     */
    private $message = '';

    /**
     * @var Carbon
     */
    private $createAt;

    /**
     * @var Carbon
     */
    private $updatedAt;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebDarkBuy
     */
    public function setId(?int $id): WebDarkBuy
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCharacterId(): int
    {
        return $this->characterId;
    }

    /**
     * @param int $characterId
     * @return WebDarkBuy
     */
    public function setCharacterId(int $characterId): WebDarkBuy
    {
        $this->characterId = $characterId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCategoryId(): ?int
    {
        return $this->categoryId;
    }

    /**
     * @param int|null $categoryId
     * @return WebDarkBuy
     */
    public function setCategoryId(?int $categoryId): WebDarkBuy
    {
        $this->categoryId = $categoryId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getBuyerId(): ?int
    {
        return $this->buyerId;
    }

    /**
     * @param int|null $buyerId
     * @return WebDarkBuy
     */
    public function setBuyerId(?int $buyerId): WebDarkBuy
    {
        $this->buyerId = $buyerId;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return WebDarkBuy
     */
    public function setStatus(string $status): WebDarkBuy
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return WebDarkBuy
     */
    public function setName(string $name): WebDarkBuy
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @param int $price
     * @return WebDarkBuy
     */
    public function setPrice(int $price): WebDarkBuy
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return string
     */
    public function getImageUrl(): string
    {
        return $this->imageUrl;
    }

    /**
     * @param string $imageUrl
     * @return WebDarkBuy
     */
    public function setImageUrl(string $imageUrl): WebDarkBuy
    {
        $this->imageUrl = $imageUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return WebDarkBuy
     */
    public function setMessage(string $message): WebDarkBuy
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getCreateAt(): Carbon
    {
        return $this->createAt;
    }

    /**
     * @param Carbon $createAt
     * @return WebDarkBuy
     */
    public function setCreateAt(Carbon $createAt): WebDarkBuy
    {
        $this->createAt = $createAt;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getUpdatedAt(): Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon $updatedAt
     * @return WebDarkBuy
     */
    public function setUpdatedAt(Carbon $updatedAt): WebDarkBuy
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
