<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 14.04.2020
 * Time: 14:21
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

/**
 * @var $viewDto \App\Libraries\Frontend\Dto\Twitter\Index
 */
?>
@extends('web.layout.app')
@section('url')
    https://eternityV.de/social
@endsection
@section('content')
    <div class="container bg-white text-dark">
        <div class="row bg-primary p-2">
            <div class="col text-white">
                <img src="{{ url('img/web/social.png') }}" width="25px" height="25px">
                Social
            </div>
            <div class="col justify-content-end row">
                <div class="col-1">
                    <a title="Alle Nachrichten" class="{{ ($mode ===\App\Libraries\Frontend\Enum\Twitter\Mode::ALL) ?  'text-white': 'text-dark' }}"
                       href="{{ route('web.social.index',[
                'hash' => $character->getHash(),
                'mode' => \App\Libraries\Frontend\Enum\Twitter\Mode::ALL
                ]) }}">
                        <i class="fas fa-globe-europe"></i>
                    </a>
                </div>
                <div class="col-1">
                    <a title="Gelikte Nutzer" class="{{ ($mode ===\App\Libraries\Frontend\Enum\Twitter\Mode::FOLLOWED) ?  'text-white': 'text-dark' }}"
                       href="{{ route('web.social.index',[
                'hash' => $character->getHash(),
                'mode' => \App\Libraries\Frontend\Enum\Twitter\Mode::FOLLOWED
                ]) }}">
                        <i class="fas fa-heart"></i>
                    </a>
                </div>
                <div class="col-1">
                    <a title="Meine Nachrichten" class="{{ ($mode ===\App\Libraries\Frontend\Enum\Twitter\Mode::SELF) ?  'text-white': 'text-dark' }}"
                       href="{{ route('web.social.index',[
                'hash' => $character->getHash(),
                'mode' => \App\Libraries\Frontend\Enum\Twitter\Mode::SELF
                ]) }}">
                        <i class="fas fa-user"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="row border">
            <div class="container">
                <div class="row rounded shadow mb-3 pt-2 pb-1">
                    <div class="col-12">
                        <form method="post" class="form" action="{{ route('web.social.post', $character->getHash()) }}">
                            {{ csrf_field() }}
                            <div class="form-row">
                                <div class="col-2">
                                    <img class="rounded-circle"
                                         src="{{ url('img/user/logo/'. $character->getId(). '.jpg') }}"
                                         height="125px"
                                         width="125px">
                                </div>
                                <div class="col-10">
                                    <label for="message">Nachricht</label>
                                    <textarea class="form-control" id="message" name="message" rows="4" required></textarea>
                                </div>
                            </div>
                            <br/>
                            <div class="form-row justify-content-end">
                                <button type="submit" class="btn btn-primary">Senden</button>

                            </div>
                        </form>
                    </div>
                </div>

                @if($viewDto->getTotalCount() == 0)
                    Aktuell gibt es keine Tweets
                @else
                    @foreach($viewDto->getTweets() as $tweet)
                        <div class="card">
                            <div class="card-header bg-primary">
                    <span class="font-weight-bold">
                        {{ '@'. $tweet->getTweet()->getName() }}
                    </span>
                                @if($tweet->getTweet()->getCharacterId() !== $character->getId())
                                    @if(!in_array($tweet->getTweet()->getCharacterId(), $followers))
                                        <div class="float-right text-white">
                                            <a href="{{ route('web.social.follow',[
                                'hash' => $character->getHash(),
                                'followId' => $tweet->getTweet()->getCharacterId(),
                                'value' => 1,
                                ]) }}">
                                                <i class="fas fa-heart text-dark"></i>
                                            </a>
                                        </div>
                                    @else
                                        <div class="float-right text-white">
                                            <a href="{{ route('web.social.follow',[
                                'hash' => $character->getHash(),
                                'followId' => $tweet->getTweet()->getCharacterId(),
                                'value' => 0,
                                ]) }}">
                                                <i class="fas fa-heart text-danger"></i>
                                            </a>
                                        </div>
                                    @endif
                                @endif
                            </div>
                            <div>
                                <div class="row card-text p-3" style="min-height: 100px">
                                    <div class="col-2">
                                        <img class="rounded-circle"
                                             src="{{ url('img/user/logo/'. $tweet->getTweet()->getCharacterId(). '.jpg') }}"
                                             height="125px"
                                             width="125px">
                                    </div>
                                    <div class="col-10">
                                        @if($tweet->getTweet()->getRetweet())
                                            <blockquote class="blockquote border bg-light">
                                                <p class="m-1" style="font-size: 12px">
                                                    <b>{{ '@'.$tweet->getRetweet()->getName() }}</b>
                                                    <br/>
                                                    > {{ $tweet->getRetweet()->getMessage() }}
                                                </p>
                                            </blockquote>
                                        @endif
                                        {{ $tweet->getTweet()->getMessage() }}
                                    </div>
                                </div>
                                <div class="card-footer bg-primary">
                                    <a data-toggle="collapse" href="#retweet{{ $tweet->getTweet()->getId() }}"
                                       role="button"
                                       aria-expanded="false"
                                       aria-controls="#retweet{{ $tweet->getTweet()->getId() }}">
                                        <i class="fas fa-retweet text-dark"></i>
                                    </a>
                                    <div class="float-right">
                                        {{ $tweet->getTweet()->getCreatedAt()->format('Y-m-d H:i:s') }}
                                    </div>
                                    <div class="collapse" id="retweet{{ $tweet->getTweet()->getId() }}">
                                        <div class="card card-body">
                                            <form method="post" class="form"
                                                  action="{{ route('web.social.retweet', $character->getHash()) }}">

                                                {{ csrf_field() }}
                                                <input type="hidden" name="retweet"
                                                       value="{{ $tweet->getTweet()->getId() }}">
                                                <div class="form-row">
                                                    <label for="message">Nachricht</label>
                                                    <textarea class="form-control" id="message" name="message"
                                                              rows="4"></textarea>
                                                </div>
                                                <br/>
                                                <div class="form-row justify-content-end">
                                                    <button type="submit" class="btn btn-primary">Retweeten</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <span class="row m-1"></span>
                    @endforeach
                    @php
                        $pages = 1 + floor($viewDto->getTotalCount() /  $size);
                        $currentPage = 1 + floor($offset /  $size);
                    @endphp
                    @if($pages !== 1)
                        <nav class="shadow p-2">
                            <ul class="pagination justify-content-center">
                                @if($currentPage != 1)
                                    <li class="page-item">
                                        <a class="page-link" href="{{ route('web.social.index',[
                   'hash' => $character->getHash(),
                   'mode' => $mode,
                   'offset' => (floor($offset /  $size) * $size) - $size,
                    ]) }}" aria-label="Previous">
                                            <span aria-hidden="true">&laquo;</span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                    </li>
                                @endif
                                <li class="page-item"><a class="page-link  text-dark active">{{ $currentPage }}</a></li>
                                @if($currentPage != $pages)
                                    <li class="page-item">
                                        <a class="page-link" href="{{ route('web.social.index',[
                                'hash' => $character->getHash(),
                                'mode' => $mode,
                                'offset' => (floor($offset /  $size) * $size) + $size,
                            ]) }}" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </nav>
                    @endif
                @endif
            </div>
        </div>
    </div>
@endsection
