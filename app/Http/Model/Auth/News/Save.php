<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.08.2021
 * Time: 13:53
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Model\Auth\News;

use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Model\Dto\WebNews;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class Save
{

    /**
     * @param \App\Http\Request\Dto\Auth\News\Save $requestDto
     */
    public static function save(\App\Http\Request\Dto\Auth\News\Save $requestDto)
    {
        $dto = (new WebNews())
            ->setId($requestDto->getId())
            ->setWebNewsCategoryId($requestDto->getWebNewsCategoryId())
            ->setTitle($requestDto->getTitle())
            ->setImage($requestDto->getImage())
            ->setShortDescription($requestDto->getShortDescription())
            ->setDescription($requestDto->getDescription())
            ->setAuthor($requestDto->getAuthor())
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());

        $model = new \App\Model\WebNews();
        if ($dto->getId()) {
            $oldDto = $model::entryAsDto($model::find($dto->getId()));
            $model->updateEntry($dto);
            Logging::update(Name::NEWS, Auth::id(), $oldDto, $dto);
        } else {
            $model->insertEntry($dto);
            Logging::insert(Name::NEWS, Auth::id(), $dto);
        }
    }
}
