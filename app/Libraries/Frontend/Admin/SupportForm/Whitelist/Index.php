<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 13.10.2021
 * Time: 14:22
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Admin\SupportForm\Whitelist;

use App\Libraries\Frontend\Dto\Admin\SupportForm\Whitelist\Index\Result;
use App\Model\Account;
use App\Model\WebAdminWhitelist;

class Index
{

    /**
     * @return \App\Libraries\Frontend\Dto\Admin\SupportForm\Whitelist\Index
     */
    public static function getViewData(): \App\Libraries\Frontend\Dto\Admin\SupportForm\Whitelist\Index
    {
        $entries = WebAdminWhitelist::all();

        $results = [];
        foreach ($entries as $entry) {
            $results[] = (new Result())
                ->setEntry(WebAdminWhitelist::entryAsDto($entry))
                ->setAccount(Account::entryAsDto($entry->account));
        }

        return (new \App\Libraries\Frontend\Dto\Admin\SupportForm\Whitelist\Index())
            ->setResults($results);
    }
}
