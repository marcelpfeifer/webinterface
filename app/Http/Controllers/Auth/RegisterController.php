<?php

namespace App\Http\Controllers\Auth;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Model\Account;
use App\Model\Dto\Shortcuts;
use App\Model\WebRegistrationCodes;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/register';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $messages = [];
            foreach ($validator->errors()->all() as $error) {
                $messages[] = (new Message())
                    ->setMessage($error)
                    ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
            }
            return redirect($this->redirectPath())->with('messages',
                $messages)->withInput()->withErrors($validator->errors());
        }

        $account = $this->create($request->all());

        // https://gitlab.com/different-life/webinterface/-/issues/59
        // Fügt einen Eintrag mit der Account ID in die Shortcuts Tabelle
        $this->insertIntoShortcuts($account);

        $this->guard()->login($account);

        return $this->registered($request, $account) ?: redirect($this->redirectPath())->withInput();
    }


    /**
     * @param Account $account
     */
    private function insertIntoShortcuts(Account $account)
    {
        $dto = (new Shortcuts())
            ->setGuid($account->id);
        (new \App\Model\Shortcuts())->insertEntry($dto);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validator = Validator::make(
            $data,
            [
                'password' => ['required', 'string', 'min:8', 'confirmed'],
                'username' => ['required', 'string', 'max:255', 'unique:account'],
                'code'     => ['required', 'string', 'max:255'],
            ],
            [
                'password.required'  => 'Das Passwort muss angegeben werden',
                'password.string'    => 'Das Passwort muss ein String sein',
                'password.min'       => 'Das Passwort muss mindestens 8 Zeichen lang sein',
                'password.confirmed' => 'Die Passwörter müssen übereinstimmen',
                'username.required'  => 'Der Nutzername muss angegeben werden',
                'username.string'    => 'Der Nutzername muss Buchstaben enthalten',
                'username.max'       => 'Der Nutzername ist zu lang',
                'username.unique'    => 'Der Nutzername ist schon vorhanden',
                'code.required'      => 'Der Registrierungscode muss angegeben werden',
                'code.string'        => 'Der Registrierungscode darf nur Zeichen enthalten',
                'code.max'           => 'Der Registrierungscode ist zu lang',
            ]
        );

        $validator->after(function ($validator) use ($data) {
            if (!($this->checkCode($data['code']))) {
                $validator->errors()->add('code', 'Der Registrierungscode ist ungültig!');
            }
        });

        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return Account
     */
    protected function create(array $data)
    {
        // Nach geburtsdatum checken
        $account = Account::create([
            'username'       => $data['username'],
            'whitelisted'    => 1,
            'password'       => \hash('sha256', $data['password']),
            'imageName'      => '',
            'createdAt'      => Carbon::now(),
            'updatedAt'      => Carbon::now(),
            'remember_token' => '',
        ]);

        $this->deleteCode($data['code']);
        return $account;
    }

    /**
     * @param string $code
     * @return bool
     */
    private function checkCode(string $code): bool
    {
        $db = new WebRegistrationCodes();
        if ($dto = $db->getEntryByCode($code)) {
            return true;
        }
        return false;
    }

    /**
     * @param string $code
     * @return bool
     */
    private function deleteCode(string $code): bool
    {
        $db = new WebRegistrationCodes();
        if ($dto = $db->getEntryByCode($code)) {
            $db->deleteEntry($dto);
            return true;
        }
        return false;
    }
}
