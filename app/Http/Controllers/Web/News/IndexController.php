<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.08.2021
 * Time: 16:12
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Web\News;


use App\Http\Controllers\Controller;
use App\Model\WebNews;
use App\Model\WebNewsCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\View\View;

class IndexController extends Controller
{

    /**
     * @param Request $request
     * @param string $hash
     * @return View
     */
    public function index(Request $request, string $hash): View
    {
        $news = (new WebNews())->getNews();
        $categories = (new WebNewsCategory())->getActiveCategories();
        return view(
            'web.news.index',
            [
                'categories' => $categories,
                'categoryId' => null,
                'news'       => $news,
                'hash'       => $hash,
            ]
        );
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return View
     */
    public function category(Request $request, int $categoryId, string $hash): View
    {
        $news = (new WebNews())->getNews($categoryId);
        $categories = (new WebNewsCategory())->getActiveCategories();
        return view(
            'web.news.index',
            [
                'categories' => $categories,
                'categoryId' => $categoryId,
                'news'       => $news,
                'hash'       => $hash,
            ]
        );
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return View
     */
    public function show(Request $request, int $id, string $hash): View
    {
        $news = WebNews::entryAsDto(WebNews::find($id));
        $categories = (new WebNewsCategory())->getActiveCategories();
        return view(
            'web.news.show',
            [
                'categories' => $categories,
                'categoryId' => $news->getWebNewsCategoryId(),
                'news'       => $news,
                'hash'       => $hash,
            ]
        );
    }
}
