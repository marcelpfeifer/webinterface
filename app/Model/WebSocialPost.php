<?php
/**
 * WebSocialPost
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 14.04.2020
 * Time: 17:51
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use App\Libraries\Frontend\Dto\Twitter\Index;
use App\Libraries\Frontend\Dto\Twitter\Tweet;
use App\Libraries\Frontend\Enum\Twitter\Mode;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class WebSocialPost extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_CHARACTER_ID = 'characterId';

    /**
     * @var string
     */
    const COLUMN_RETWEET = 'retweet';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    const COLUMN_MESSAGE = 'message';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webSocialPost';

    /**
     * @param string $mode
     * @param Dto\Character $dto
     * @param int $offset
     * @return Index
     */
    public function getTweets(string $mode, Dto\Character $dto, int $offset): Index
    {

        $result = [];
        $tweets = [];
        $count = 0;
        switch ($mode) {
            case Mode::ALL:
                [$count, $tweets] = $this->getAllTweets($offset);
                break;
            case Mode::FOLLOWED:
                [$count, $tweets] = $this->getFollowedTweets($dto, $offset);
                break;
            case Mode::SELF:
                [$count, $tweets] = $this->getMyTweets($dto, $offset);
                break;
        }

        foreach ($tweets as $tweet) {
            $dto = self::entryAsDto($tweet);
            $retweet = null;
            if ($dto->getRetweet()) {
                $entry = self::find($dto->getRetweet());
                if ($entry) {
                    $retweet = self::entryAsDto($entry);
                }
            }
            $result[] = (new Tweet())
                ->setTweet($dto)
                ->setReTweet($retweet);
        }


        return (new Index())
            ->setTotalCount($count)
            ->setTweets($result);
    }

    /**
     * @param int $offset
     * @return mixed
     */
    private function getAllTweets(int $offset)
    {
        $count = $this->count();
        $tweets = $this
            ->take(25)
            ->skip($offset)
            ->orderBy(self::CREATED_AT, 'DESC')
            ->get();
        return [$count, $tweets];
    }

    /**
     * @param int $offset
     * @return mixed
     */
    private function getFollowedTweets(Dto\Character $dto, int $offset)
    {
        $followers = (new \App\Model\WebSocialFollow())->getEntriesByCharacterId($dto->getId());

        $count = $this
            ->whereIn(self::COLUMN_CHARACTER_ID, $followers)
            ->count();

        $tweets = $this
            ->whereIn(self::COLUMN_CHARACTER_ID, $followers)
            ->take(25)
            ->skip($offset)
            ->orderBy(self::CREATED_AT, 'DESC')
            ->get();
        return [$count, $tweets];
    }

    /**
     * @param Dto\Character $dto
     * @param int $offset
     * @return mixed
     */
    private function getMyTweets(Dto\Character $dto, int $offset)
    {
        $count = $this
            ->where(self::COLUMN_MESSAGE, 'LIKE', "%@{$dto->getName()}%")
            ->orWhere(self::COLUMN_CHARACTER_ID, '=', $dto->getId())
            ->count();

        $tweets = $this
            ->where(self::COLUMN_MESSAGE, 'LIKE', "%@{$dto->getName()}%")
            ->orWhere(self::COLUMN_CHARACTER_ID, '=', $dto->getId())
            ->take(25)
            ->skip($offset)
            ->orderBy(self::CREATED_AT, 'DESC')
            ->get();
        return [$count, $tweets];
    }


    /**
     * @param Dto\WebSocialPost $dto
     */
    public function insertEntry(Dto\WebSocialPost $dto)
    {
        $this->insert([
            self::COLUMN_ID           => $dto->getId(),
            self::COLUMN_CHARACTER_ID => $dto->getCharacterId(),
            self::COLUMN_RETWEET      => $dto->getRetweet(),
            self::COLUMN_NAME         => $dto->getName(),
            self::COLUMN_MESSAGE      => $dto->getMessage(),
            self::CREATED_AT          => Carbon::now(),
            self::UPDATED_AT          => Carbon::now(),
        ]);
    }


    /**
     * @param Dto\WebSocialPost $dto
     */
    public function updateEntry(Dto\WebSocialPost $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update([
            self::COLUMN_ID           => $dto->getId(),
            self::COLUMN_CHARACTER_ID => $dto->getCharacterId(),
            self::COLUMN_RETWEET      => $dto->getRetweet(),
            self::COLUMN_NAME         => $dto->getName(),
            self::COLUMN_MESSAGE      => $dto->getMessage(),
            self::UPDATED_AT          => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\WebSocialPost $dto
     */
    public function deleteEntry(\App\Model\Dto\WebSocialPost $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebSocialPost $entry
     * @return Dto\WebSocialPost
     */
    public static function entryAsDto(WebSocialPost $entry): Dto\WebSocialPost
    {
        return (new Dto\WebSocialPost())
            ->setId($entry->{self::COLUMN_ID})
            ->setCharacterId($entry->{self::COLUMN_CHARACTER_ID})
            ->setRetweet($entry->{self::COLUMN_RETWEET})
            ->setName($entry->{self::COLUMN_NAME})
            ->setMessage($entry->{self::COLUMN_MESSAGE})
            ->setCreatedAt($entry->{self::CREATED_AT})
            ->setUpdatedAt($entry->{self::UPDATED_AT});
    }
}
