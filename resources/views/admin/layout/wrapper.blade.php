<div class="container-fluid">
    <div class="row justify-content-center">
        @include('admin.sidebar')
        <div class="col-12 mt-2 mt-md-0 col-md-9">
            @include('layouts.messages')
            <div class="card border-primary bg-white">
                <div class="card-header bg-primary text-white"> @yield('title')</div>
                <div class="card-body text-dark">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</div>
