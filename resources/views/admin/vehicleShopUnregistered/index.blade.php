<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 29.04.2020
 * Time: 16:42
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@php
    /** @var $entry \App\Model\Dto\VehicleStore */
@endphp
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-car"></i>
    Fahrzeugshop
    <a class="btn btn-dark btn-sm float-right"
       href="{{ route('admin.vehicleShopUnregistered.create') }}"
       role="button"
    >
        Eintrag erstellen
    </a>
@endsection
@section('content')
    <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Model</th>
                <th scope="col">Preis</th>
                <th scope="col">Anzahl</th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($results as $entry)
                <tr>
                    <th scope="row">{{ $entry->getId() }}</th>
                    <td>{{ $entry->getModel() }}</td>
                    <td>{{ (isset($vehicles[$entry->getModel()])) ? $vehicles[$entry->getModel()] : 'Nicht gefunden' }}</td>
                    <td class="{{ ($entry->getStock() === 0) ? 'bg-danger' : '' }}">{{ $entry->getStock() }}</td>
                    <td class="text-center">
                        <a class="btn btn-primary"
                           href="{{ route('admin.vehicleShopUnregistered.show', $entry->getId()) }}">Anzeigen</a>
                    </td>
                    <td class="text-center">
                        <form action="{{ route('admin.vehicleShopUnregistered.destroy', $entry->getId())}}" method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="submit" class="btn btn-danger" value="Löschen"/>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
