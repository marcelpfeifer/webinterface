<?php
/**
 * WebRegistrationCodes.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 24.08.2019
 * Time: 10:39
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Users
 * @package App
 *
 * @property int id
 * @property int cash
 * @property int bank
 * @property int tax
 * @property string fines
 */
class WebRegistrationCodes extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_CODE = 'code';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webRegistrationCodes';

    /**
     * @param Dto\WebRegistrationCodes $dto
     */
    public function insertEntry(Dto\WebRegistrationCodes $dto)
    {
        $this->insert([
            self::COLUMN_ID   => $dto->getId(),
            self::COLUMN_CODE => $dto->getCode(),
            self::CREATED_AT  => Carbon::now(),
            self::UPDATED_AT  => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\WebRegistrationCodes $dto
     */
    public function updateEntry(Dto\WebRegistrationCodes $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update([
            self::COLUMN_ID   => $dto->getId(),
            self::COLUMN_CODE => $dto->getCode(),
            self::UPDATED_AT  => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\WebRegistrationCodes $dto
     */
    public function deleteEntry(Dto\WebRegistrationCodes $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param string $code
     * @return Dto\WebRegistrationCodes|null
     */
    public function getEntryByCode(string $code)
    {
        $entry = $this->where(self::COLUMN_CODE, $code)->first();
        return ($entry) ? self::entryAsDto($entry) : null;
    }

    /**
     * @param WebRegistrationCodes $entry
     * @return Dto\WebRegistrationCodes
     */
    public static function entryAsDto(WebRegistrationCodes $entry): Dto\WebRegistrationCodes
    {
        return (new Dto\WebRegistrationCodes())
            ->setId($entry->{self::COLUMN_ID})
            ->setCode($entry->{self::COLUMN_CODE});
    }
}
