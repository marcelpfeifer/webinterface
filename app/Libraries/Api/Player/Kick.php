<?php
/**
 * Kick
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 13.04.2020
 * Time: 22:02
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Api\Player;

use App\Libraries\Api\AClient;
use App\Model\WebConfig;

class Kick extends AClient
{
    /**
     * @var string
     */
    const URL = 'player/kick';

    /**
     * @param int $pid
     * @param string $message
     * @return bool
     */
    public static function call(int $pid, string $message): bool
    {
        $response = self::sendRequest('POST', WebConfig::getEntryByKey('api_url')->getValue() . self::URL, [
            'query' => [
                'pid'     => $pid,
                'message' => $message,
            ]
        ]);
        return $response && $response->getStatusCode() == 200;
    }
}
