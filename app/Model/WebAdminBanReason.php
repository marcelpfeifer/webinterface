<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class WebAdminBanReason
 * @package App\Model
 */
class WebAdminBanReason extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    const COLUMN_DESCRIPTION = 'description';

    /**
     * @var string
     */
    const COLUMN_POINTS = 'points';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webAdminBanReason';

    /**
     * @param Dto\WebAdminBanReason $dto
     * @return mixed
     */
    public function insertEntry(Dto\WebAdminBanReason $dto)
    {
        return $this->insertGetId(
            [
                self::COLUMN_NAME        => $dto->getName(),
                self::COLUMN_DESCRIPTION => $dto->getDescription(),
                self::COLUMN_POINTS      => $dto->getPoints(),
                self::CREATED_AT         => Carbon::now(),
                self::UPDATED_AT         => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebAdminBanReason $dto
     */
    public function updateEntry(Dto\WebAdminBanReason $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_NAME        => $dto->getName(),
                self::COLUMN_DESCRIPTION => $dto->getDescription(),
                self::COLUMN_POINTS      => $dto->getPoints(),
                self::UPDATED_AT         => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebAdminBanReason $dto
     */
    public function deleteEntry(\App\Model\Dto\WebAdminBanReason $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebAdminBanReason $entry
     * @return Dto\WebAdminBanReason
     */
    public static function entryAsDto(WebAdminBanReason $entry): Dto\WebAdminBanReason
    {
        return (new Dto\WebAdminBanReason())
            ->setId($entry->{self::COLUMN_ID})
            ->setName($entry->{self::COLUMN_NAME})
            ->setDescription($entry->{self::COLUMN_DESCRIPTION})
            ->setPoints($entry->{self::COLUMN_POINTS})
            ->setCreatedAt($entry->{self::CREATED_AT})
            ->setUpdatedAt($entry->{self::UPDATED_AT});
    }
}
