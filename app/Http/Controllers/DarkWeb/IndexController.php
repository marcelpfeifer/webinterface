<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 26.07.2020
 * Time: 14:16
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\DarkWeb;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Http\Request\DarkWeb\Save;
use App\Model\Character;
use App\Model\WebDarkWeb;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class IndexController extends Controller
{
    /**
     * @param Request $request
     * @param string $hash
     * @return View
     */
    public function index(Request $request, string $hash): View
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return view('darkWeb.layout.noAuth', ['hash' => $hash]);
        }
        $characterDto = Character::entryAsDto($character);
        $darkWebDto = (new WebDarkWeb())->getEntryByCharacterId($characterDto->getId());
        if (!($darkWebDto)) {
            return view('darkWeb.name', ['hash' => $hash]);
        }
        return view(
            'darkWeb.index',
            [
                'hash'         => $hash,
                'characterDto' => $characterDto,
                'darkWebDto'   => $darkWebDto,
            ]
        );
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return RedirectResponse
     */
    public function save(Request $request, string $hash): RedirectResponse
    {
        $validator = (new Save($request))->validate();
        if ($validator->fails()) {
            $messages = [];
            foreach ($validator->errors()->all() as $error) {
                $messages[] = (new Message())
                    ->setMessage($error)
                    ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
            }
            return redirect()->route('darkWeb.index', $hash)
                ->with('messages', $messages)
                ->withInput()
                ->withErrors($validator->errors());
        }

        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect('darkWeb.index', $hash);
        }
        $characterDto = Character::entryAsDto($character);
        $darkWebDto = (new WebDarkWeb())->getEntryByCharacterId($characterDto->getId());

        if ($darkWebDto) {
            $darkWebDto->setName($request->input('name'));
            (new WebDarkWeb())->updateEntry($darkWebDto);
        } else {
            $darkWebDto = (new \App\Model\Dto\WebDarkWeb())
                ->setName($request->input('name'))
                ->setCharacterId($characterDto->getId());
            (new WebDarkWeb())->insertEntry($darkWebDto);
        }

        return redirect()->route('darkWeb.index', $hash);
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return View
     */
    public function video(Request $request, string $hash): View
    {
        return view(
            'darkWeb.video',
            [
                'hash' => $hash
            ]
        );
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return View
     */
    public function twoThousandFortyEight(Request $request, string $hash): View
    {
        return view(
            'darkWeb.2048',
            [
                'hash' => $hash
            ]
        );
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return View
     */
    public function loading(Request $request, string $hash): View
    {
        return view(
            'darkWeb.loading',
            [
                'hash' => $hash
            ]
        );
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return View
     */
    public function weirdImages(Request $request, string $hash): View
    {
        $images = [
            "https://static.twentytwowords.com/wp-content/uploads/Screen-Shot-2018-02-02-at-2.21.57-PM.png",
            "https://static.twentytwowords.com/wp-content/uploads/Screen-Shot-2018-02-02-at-2.22.05-PM.png",
            "https://static.twentytwowords.com/wp-content/uploads/Screen-Shot-2018-02-02-at-2.22.24-PM-675x567.png",
            "https://static.twentytwowords.com/wp-content/uploads/Screen-Shot-2018-02-02-at-2.22.33-PM-505x675.png",
            "https://static.twentytwowords.com/wp-content/uploads/Screen-Shot-2018-02-02-at-2.23.34-PM-507x675.png",
            "https://static.twentytwowords.com/wp-content/uploads/Screen-Shot-2018-02-05-at-10.03.34-AM-506x675.png",
            "https://static.twentytwowords.com/wp-content/uploads/Screen-Shot-2018-02-05-at-10.03.58-AM-675x599.png",
            "https://static.twentytwowords.com/wp-content/uploads/Screen-Shot-2018-02-05-at-10.04.17-AM.png",
            "https://static.twentytwowords.com/wp-content/uploads/Screen-Shot-2018-02-05-at-10.04.53-AM-459x675.png",
            "https://static.twentytwowords.com/wp-content/uploads/Screen-Shot-2018-02-05-at-10.13.58-AM-675x431.png",
            "https://i.insider.com/5b3a76c386c52b19008b458c?width=700&format=jpeg&auto=webp",
            "https://i.insider.com/59c53b0bba785e4a132da6cc?width=700&format=jpeg&auto=webp",
            "https://i.insider.com/5afdfbba5e48ec4c008b4677?width=700&format=jpeg&auto=webp",
            "https://i.pinimg.com/originals/2c/5f/ed/2c5fedc11531be0693df54e8f23c2039.jpg",
            "https://i.pinimg.com/236x/4e/86/9c/4e869c79a929c06dfde71503d44c3ff3--creepy-old-photos-creepy-pictures.jpg",
            "https://i.pinimg.com/236x/ba/81/9a/ba819a6d6e4719b4d4b6f66bf32ba225.jpg",
        ];

        shuffle($images);
        return view(
            'darkWeb.weirdImages',
            [
                'images' => $images,
                'hash'   => $hash
            ]
        );
    }
}
