<?php
/**
 * Housing
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 12.05.2020
 * Time: 19:21
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Housing extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_HOUSE_ID = 'houseid';

    /**
     * @var string
     */
    const COLUMN_GUID = 'guid';

    /**
     * @var string
     */
    const COLUMN_PIN = 'pin';

    /**
     * @var string
     */
    const COLUMN_INVENTORY = 'inventory';

    /**
     * @var string
     */
    const COLUMN_CREATED_AT = 'createdAt';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'housing';

    /**
     * @param int $id
     * @return Dto\Housing|null
     */
    public function getById(int $id): ?Dto\Housing
    {
        $entry = $this->where(self::COLUMN_ID, $id)->first();

        if ($entry) {
            return self::entryAsDto($entry);
        }
        return null;
    }

    /**
     * @param int $characterId
     * @return Dto\Housing[]
     */
    public function getEntriesByCharacterId(int $characterId): array
    {
        $vehicles = [];
        $entries = $this->where(self::COLUMN_GUID, $characterId)->get();
        foreach ($entries as $entry) {
            $vehicles[] = self::entryAsDto($entry);
        }

        return $vehicles;
    }

    /**
     * @param Dto\Housing $dto
     */
    public function insertEntry(Dto\Housing $dto)
    {
        $this->insert([
            self::COLUMN_ID         => $dto->getId(),
            self::COLUMN_GUID       => $dto->getGuidId(),
            self::COLUMN_HOUSE_ID   => $dto->getHouseId(),
            self::COLUMN_PIN        => $dto->getPin(),
            self::COLUMN_INVENTORY  => $dto->getInventory(),
            self::COLUMN_CREATED_AT => Carbon::now(),

        ]);
    }

    /**
     * @param Dto\Housing $dto
     */
    public function updateEntry(Dto\Housing $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update([
            self::COLUMN_ID        => $dto->getId(),
            self::COLUMN_GUID      => $dto->getGuidId(),
            self::COLUMN_HOUSE_ID  => $dto->getHouseId(),
            self::COLUMN_PIN       => $dto->getPin(),
            self::COLUMN_INVENTORY => $dto->getInventory(),
        ]);
    }

    /**
     * @param Dto\Housing $dto
     */
    public function deleteEntry(Dto\Housing $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param int $guid
     */
    public function deleteEntryByGuid(int $guid)
    {
        $this->where(self::COLUMN_GUID, $guid)->delete();
    }

    /**
     * @param Housing $entry
     * @return Dto\Housing
     */
    public static function entryAsDto(Housing $entry): Dto\Housing
    {
        return (new Dto\Housing())
            ->setId($entry->{self::COLUMN_ID})
            ->setGuidId($entry->{self::COLUMN_GUID})
            ->setHouseId($entry->{self::COLUMN_HOUSE_ID})
            ->setPin($entry->{self::COLUMN_PIN})
            ->setInventory($entry->{self::COLUMN_INVENTORY});
    }
}
