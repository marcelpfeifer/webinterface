<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 13.04.2020
 * Time: 22:37
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('api.layout.app')
@section('title')
    <i class="fas fa-car"></i>
    Repariert ein Fahrzeug als Fahrer
@endsection
@section('content')
    <form method="post" action="{{ route('api.vehicle.repair.save') }}">
        {{ csrf_field() }}

        <div class="form-group">
            <label for="pid">Spieler</label>
            <select class="form-control" id="pid" name="pid">
                @foreach($players as $id => $playerName)
                    <option value="{{ $id }}">{{ $playerName }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-danger">Bestätigen</button>
    </form>
    <br/>
@endsection
