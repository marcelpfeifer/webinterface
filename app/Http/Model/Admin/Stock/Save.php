<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.08.2021
 * Time: 13:53
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Model\Admin\Stock;

use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Model\Dto\WebStock;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class Save
{

    /**
     * @param \App\Http\Request\Dto\Admin\Stock\Save $requestDto
     */
    public static function save(\App\Http\Request\Dto\Admin\Stock\Save $requestDto)
    {
        $dto = (new WebStock())
            ->setId($requestDto->getId())
            ->setName($requestDto->getName())
            ->setPrice($requestDto->getPrice())
            ->setStock($requestDto->getStock())
            ->setMinChange($requestDto->getMinChange())
            ->setMaxChange($requestDto->getMaxChange())
            ->setLastPrice($requestDto->getPrice())
            ->setPercentageChange(0.00)
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());

        $model = new \App\Model\WebStock();
        if ($dto->getId()) {
            $oldDto = $model::entryAsDto($model::find($dto->getId()));
            $dto->setPrice($oldDto->getPrice())
                ->setLastPrice($oldDto->getLastPrice())
                ->setPercentageChange($oldDto->getPercentageChange());
            $model->updateEntry($dto);
            Logging::update(Name::STOCK, Auth::id(), $oldDto, $dto);
        } else {
            $model->insertEntry($dto);
            Logging::insert(Name::STOCK, Auth::id(), $dto);
        }
    }
}
