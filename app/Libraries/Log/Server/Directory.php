<?php
/**
 * Directory
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 24.09.2021
 * Time: 19:00
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Log\Server;


use App\Libraries\Enum\AEnum;

class Directory extends AEnum
{

    /**
     * @var string
     */
    const STORAGE_DIR = 'app/log/server/';

    /**
     * @var string
     */
    const FILENAME = 'altv.log';
}
