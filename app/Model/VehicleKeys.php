<?php
/**
 * VehicleKeys.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 01.11.2019
 * Time: 13:23
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class VehicleKeys extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_GUID = 'guid';

    /**
     * @var string
     */
    const COLUMN_CAR_ID = 'carId';

    /**
     * @var string
     */
    const COLUMN_SECOND = 'second';

    /**
     * @var string
     */
    const COLUMN_CREATED_AT = 'createdAt';

    /**
     * @var string
     */
    const COLUMN_UPDATED_AT = 'updatedAt';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'vehicle_keys';

    /**
     * @param Dto\VehicleKeys $dto
     */
    public function insertEntry(Dto\VehicleKeys $dto)
    {
        $this->insert([
            self::COLUMN_ID         => $dto->getId(),
            self::COLUMN_GUID       => $dto->getGuid(),
            self::COLUMN_CAR_ID     => $dto->getCarId(),
            self::COLUMN_SECOND     => $dto->getSecond(),
            self::COLUMN_CREATED_AT => Carbon::now(),
            self::COLUMN_UPDATED_AT => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\VehicleKeys $dto
     */
    public function updateEntry(Dto\VehicleKeys $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update([
            self::COLUMN_ID         => $dto->getId(),
            self::COLUMN_GUID       => $dto->getGuid(),
            self::COLUMN_CAR_ID     => $dto->getCarId(),
            self::COLUMN_SECOND     => $dto->getSecond(),
            self::COLUMN_UPDATED_AT => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\VehicleKeys $dto
     */
    public function deleteEntry(Dto\VehicleKeys $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param int $carId
     */
    public function deleteEntryByCarId(int $carId)
    {
        $this->where(self::COLUMN_CAR_ID, $carId)->delete();
    }

    /**
     * @param int $guid
     */
    public function deleteEntryByGuid(int $guid)
    {
        $this->where(self::COLUMN_GUID, $guid)->delete();
    }

    /**
     * @param VehicleKeys $entry
     * @return Dto\VehicleKeys
     */
    public static function entryAsDto(VehicleKeys $entry): Dto\VehicleKeys
    {
        return (new Dto\VehicleKeys())
            ->setId($entry->{self::COLUMN_ID})
            ->setGuid($entry->{self::COLUMN_GUID})
            ->setCarId($entry->{self::COLUMN_CAR_ID})
            ->setSecond($entry->{self::COLUMN_SECOND});
    }
}
