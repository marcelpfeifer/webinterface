<?php
/**
 * Store
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.10.2021
 * Time: 10:43
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Admin\SupportForm\WhiteList;

use App\Http\Request\Dto\ADto;
use Carbon\Carbon;

class Store extends ADto
{

    /**
     * @var string
     */
    private $username = '';

    /**
     * @var string
     */
    private $status = '';

    /**
     * @var Carbon|null
     */
    private $retryDate = null;

    /**
     * @var string
     */
    private $message = '';

    /**
     * @var array
     */
    private $answer = [];

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return Store
     */
    public function setUsername(string $username): Store
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Store
     */
    public function setStatus(string $status): Store
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getRetryDate(): ?Carbon
    {
        return $this->retryDate;
    }

    /**
     * @param Carbon|null $retryDate
     * @return Store
     */
    public function setRetryDate(?Carbon $retryDate): Store
    {
        $this->retryDate = $retryDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return Store
     */
    public function setMessage(string $message): Store
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return array
     */
    public function getAnswer(): array
    {
        return $this->answer;
    }

    /**
     * @param array $answer
     * @return Store
     */
    public function setAnswer(array $answer): Store
    {
        $this->answer = $answer;
        return $this;
    }
}
