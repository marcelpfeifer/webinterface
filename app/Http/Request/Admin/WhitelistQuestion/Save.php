<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 29.08.2021
 * Time: 12:41
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Admin\WhitelistQuestion;

use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class Save extends AValidator implements IValidator
{
    /**
     * @return string[]
     */
    public function getRules(): array
    {
        return [
            'question' => [
                'required',
                'string',
            ],
            'answer'   => [
                'required',
                'string',
            ],
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'question.required' => 'Die Frage muss übergeben werden!',
            'question.string'   => 'Die Frage darf nur aus Zeichen bestehen!',
            'answer.required'   => 'Die Anwort muss übergeben werden!',
            'answer.string'     => 'Die Anwort darf nur aus Zeichen bestehen!',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Admin\WhitelistQuestion\Save())
            ->setId($this->id)
            ->setQuestion($this->request->input('question'))
            ->setAnswer($this->request->input('answer'));
    }
}
