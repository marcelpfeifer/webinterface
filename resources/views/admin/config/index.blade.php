<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 02.04.2020
 * Time: 17:17
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-cog"></i>
    Einstellungen
@endsection
@section('content')
    <form method="post" action="{{ route('admin.config.save') }}">
        {{ csrf_field() }}
        @php
            $entry = \App\Model\WebConfig::getEntryByKey(\App\Model\Enum\WebConfig::API_URL);
        @endphp
        <div class="form-group">
            <label for="{{ \App\Model\Enum\WebConfig::API_URL }}">API URL</label>
            <input type="text" class="form-control" id="{{ \App\Model\Enum\WebConfig::API_URL }}"
                   name="{{ \App\Model\Enum\WebConfig::API_URL }}" value="{{ $entry ? $entry->getValue() : null }}">
        </div>

        @php
            $entry = \App\Model\WebConfig::getEntryByKey(\App\Model\Enum\WebConfig::API_SYSTEM_URL);
        @endphp
        <div class="form-group">
            <label for="{{ \App\Model\Enum\WebConfig::API_SYSTEM_URL }}">API System URL</label>
            <input type="text" class="form-control" id="{{ \App\Model\Enum\WebConfig::API_SYSTEM_URL }}"
                   name="{{ \App\Model\Enum\WebConfig::API_SYSTEM_URL }}"
                   value="{{ $entry ? $entry->getValue() : null }}">
        </div>

        @php
            $entry = \App\Model\WebConfig::getEntryByKey(\App\Model\Enum\WebConfig::GITLAB_MAIL_URL);
        @endphp
        <div class="form-group">
            <label for="{{ \App\Model\Enum\WebConfig::GITLAB_MAIL_URL }}">Gitlab Mail URL</label>
            <input type="text" class="form-control" id="{{ \App\Model\Enum\WebConfig::GITLAB_MAIL_URL }}"
                   name="{{ \App\Model\Enum\WebConfig::GITLAB_MAIL_URL }}"
                   value="{{ $entry ? $entry->getValue() : null }}">
        </div>

        <button type="submit" class="btn btn-primary">Speichern</button>
    </form>
@endsection
