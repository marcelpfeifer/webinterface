<?php
/**
 * WebAdminTechnicalFaq.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 04.09.2019
 * Time: 21:29
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Users
 * @package App
 *
 * @property int id
 * @property string explanation
 * @property string errorCode
 * @property string troubleShooting
 * @property string troubleShooting2
 * @property boolean fixed
 * @property string supporter
 */
class WebAdminTechnicalFaq extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_USER_ID = 'userId';

    /**
     * @var string
     */
    const COLUMN_EXPLANATION = 'explanation';

    /**
     * @var string
     */
    const COLUMN_ERROR_CODE = 'errorCode';

    /**
     * @var string
     */
    const COLUMN_TROUBLE_SHOOTING = 'troubleShooting';

    /**
     * @var string
     */
    const COLUMN_TROUBLE_SHOOTING_2 = 'troubleShooting2';

    /**
     * @var string
     */
    const COLUMN_FIXED = 'fixed';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webAdminTechnicalFaq';

    /**
     * @return BelongsTo
     */
    public function account(): BelongsTo
    {
        return $this->belongsTo(Account::class, self::COLUMN_USER_ID, Account::COLUMN_ID);
    }

    /**
     * @param Dto\WebAdminTechnicalFaq $dto
     */
    public function insertEntry(Dto\WebAdminTechnicalFaq $dto)
    {
        $this->insert([
            self::COLUMN_ID                 => $dto->getId(),
            self::COLUMN_USER_ID            => $dto->getUserId(),
            self::COLUMN_EXPLANATION        => $dto->getExplanation(),
            self::COLUMN_ERROR_CODE         => $dto->getErrorCode(),
            self::COLUMN_TROUBLE_SHOOTING   => $dto->getTroubleShooting(),
            self::COLUMN_TROUBLE_SHOOTING_2 => $dto->getTroubleShooting2(),
            self::COLUMN_FIXED              => $dto->isFixed(),
            self::CREATED_AT                => Carbon::now(),
            self::UPDATED_AT                => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\WebAdminTechnicalFaq $dto
     */
    public function updateEntry(Dto\WebAdminTechnicalFaq $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update([
            self::COLUMN_ID                 => $dto->getId(),
            self::COLUMN_USER_ID            => $dto->getUserId(),
            self::COLUMN_EXPLANATION        => $dto->getExplanation(),
            self::COLUMN_ERROR_CODE         => $dto->getErrorCode(),
            self::COLUMN_TROUBLE_SHOOTING   => $dto->getTroubleShooting(),
            self::COLUMN_TROUBLE_SHOOTING_2 => $dto->getTroubleShooting2(),
            self::COLUMN_FIXED              => $dto->isFixed(),
            self::UPDATED_AT                => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\WebAdminTechnicalFaq $dto
     */
    public function deleteEntry(\App\Model\Dto\WebAdminTechnicalFaq $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebAdminTechnicalFaq $entry
     * @return Dto\WebAdminTechnicalFaq
     */
    public static function entryAsDto(WebAdminTechnicalFaq $entry): Dto\WebAdminTechnicalFaq
    {
        return (new Dto\WebAdminTechnicalFaq())
            ->setId($entry->{self::COLUMN_ID})
            ->setUserId($entry->{self::COLUMN_USER_ID})
            ->setExplanation($entry->{self::COLUMN_EXPLANATION})
            ->setErrorCode($entry->{self::COLUMN_ERROR_CODE})
            ->setTroubleShooting($entry->{self::COLUMN_TROUBLE_SHOOTING})
            ->setTroubleShooting2($entry->{self::COLUMN_TROUBLE_SHOOTING_2})
            ->setFixed($entry->{self::COLUMN_FIXED})
            ->setCreatedAt($entry->{self::CREATED_AT})
            ->setUpdatedAt($entry->{self::UPDATED_AT});
    }
}
