<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 10.04.2020
 * Time: 16:00
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('api.layout.app')
@section('title')
    <i class="fas fa-cross"></i>
    Belebt den Spieler wieder
@endsection
@section('content')
    <form method="post" action="{{ route('api.player.revive.save') }}">
        {{ csrf_field() }}

        @include('api.includes.select', [
        'name' => 'pid',
        'label' => 'Spieler',
        'entries' => $players,
        'currentSelect' => $currentPlayer,
        ])

        <button type="submit" class="btn btn-danger">Bestätigen</button>
    </form>
    <br/>
@endsection
