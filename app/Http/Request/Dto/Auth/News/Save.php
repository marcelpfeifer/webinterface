<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.08.2021
 * Time: 14:53
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Auth\News;


use App\Http\Request\Dto\ADto;

class Save extends ADto
{
    /**
     * @var int|null
     */
    private $id = null;

    /**
     * @var int|null
     */
    private $webNewsCategoryId = null;

    /**
     * @var string
     */
    private $title = '';

    /**
     * @var string
     */
    private $image = '';

    /**
     * @var string
     */
    private $shortDescription = '';

    /**
     * @var string
     */
    private $description = '';

    /**
     * @var string
     */
    private $author = '';

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Save
     */
    public function setId(?int $id): Save
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getWebNewsCategoryId(): ?int
    {
        return $this->webNewsCategoryId;
    }

    /**
     * @param int|null $webNewsCategoryId
     * @return Save
     */
    public function setWebNewsCategoryId(?int $webNewsCategoryId): Save
    {
        $this->webNewsCategoryId = $webNewsCategoryId;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Save
     */
    public function setTitle(string $title): Save
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return Save
     */
    public function setImage(string $image): Save
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return string
     */
    public function getShortDescription(): string
    {
        return $this->shortDescription;
    }

    /**
     * @param string $shortDescription
     * @return Save
     */
    public function setShortDescription(string $shortDescription): Save
    {
        $this->shortDescription = $shortDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Save
     */
    public function setDescription(string $description): Save
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * @param string $author
     * @return Save
     */
    public function setAuthor(string $author): Save
    {
        $this->author = $author;
        return $this;
    }
}
