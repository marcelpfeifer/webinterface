<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 16.04.2020
 * Time: 15:13
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Dto\Twitter;

use App\Model\Dto\WebSocialPost;

class Index
{

    /**
     * @var Index[]
     */
    private $tweets;

    /**
     * @var int
     */
    private $totalCount;

    /**
     * @return Index[]
     */
    public function getTweets(): array
    {
        return $this->tweets;
    }

    /**
     * @param Index[] $tweets
     * @return Index
     */
    public function setTweets(array $tweets): Index
    {
        $this->tweets = $tweets;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalCount(): int
    {
        return $this->totalCount;
    }

    /**
     * @param int $totalCount
     * @return Index
     */
    public function setTotalCount(int $totalCount): Index
    {
        $this->totalCount = $totalCount;
        return $this;
    }
}
