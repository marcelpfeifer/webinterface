<?php
/**
 * BanController
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 25.12.2019
 * Time: 21:45
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\SupportForm;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Model\Account;
use App\Model\WebAdminBan;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BanController extends Controller
{

    /**
     * BanController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_SUPPORT_FORM_BANS);
    }

    /**
     * @return View
     */
    public function index(): View
    {
        $entries = WebAdminBan::all();

        $result = [];
        foreach ($entries as $entry) {
            $dto = WebAdminBan::entryAsDto($entry);
            $user = Account::entryAsDto($entry->account);
            $result[] = [
                'dto'  => $dto,
                'user' => $user,
            ];
        }

        return view(
            'admin.supportForm.ban.index',
            [
                "entries" => $result
            ]
        );
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return view('admin.supportForm.ban.create');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $dto = (new \App\Model\Dto\WebAdminBan())
            ->setUserName($request->input('userName'))
            ->setIpAddress($request->input('ipAddress'))
            ->setSocialClubName($request->input('socialClubName'))
            ->setReason($request->input('reason'))
            ->setLength($request->input('length'))
            ->setUserId(Auth::user()->{Account::COLUMN_ID});

        (new WebAdminBan())->insertEntry($dto);
        Logging::insert(Name::SUPPORT_FORM_BAN, Auth::id(), $dto);
        $messages[] = (new Message())
            ->setMessage("Eintrag wurde erstellt")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.supportForm.ban.index')->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return View
     */
    public function show(Request $request, int $id): View
    {
        $entry = WebAdminBan::find($id);
        $dto = WebAdminBan::entryAsDto($entry);
        $user = Account::entryAsDto($entry->account);

        return view(
            'admin.supportForm.ban.show',
            [
                'entry' => $dto,
                'user'  => $user
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(Request $request, int $id): RedirectResponse
    {
        $entry = WebAdminBan::find($id);
        $dto = WebAdminBan::entryAsDto($entry);

        (new WebAdminBan())->deleteEntry($dto);
        Logging::delete(Name::SUPPORT_FORM_BAN, Auth::id(), $dto);
        $messages[] = (new Message())
            ->setMessage("Eintrag wurde gelöscht")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.supportForm.ban.index')->with('messages', $messages);
    }
}
