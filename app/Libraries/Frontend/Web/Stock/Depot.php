<?php
/**
 * Depot
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 30.08.2021
 * Time: 15:55
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Web\Stock;


use App\Libraries\Common\Math;
use App\Libraries\Common\Price;
use App\Libraries\Frontend\Dto\Web\Stock\Depot\Entry;
use App\Libraries\Frontend\Dto\Web\Stock\Depot\Total;
use App\Model\WebStockCharacter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Depot
{
    /**
     * @param int $characterId
     * @param string $hash
     * @return array
     */
    public static function getViewData(int $characterId, string $hash): array
    {
        /**
         * @var $stocks Entry[]
         */
        $stocks = [];
        $entries = (new WebStockCharacter())->getEntries($characterId);

        $totalValue = 0;
        $totalGained = 0;
        foreach ($entries as $entry) {
            $currentTotalValue = $entry->getWebStockCharacter()->getAmount() * $entry->getWebStock()->getPrice();
            $buyPrice = $entry->getWebStockCharacter()->getTotalPrice() / $entry->getWebStockCharacter()->getAmount();
            $currentValue = $currentTotalValue - $entry->getWebStockCharacter()->getTotalPrice();
            $percentChange = Math::getPercentageChange(
                $entry->getWebStockCharacter()->getTotalPrice(),
                $currentTotalValue
            );

            $stockEntry = (new \App\Libraries\Frontend\Dto\Web\Stock\Depot\Entry\Entry())
                ->setId($entry->getWebStockCharacter()->getId())
                ->setAmount($entry->getWebStockCharacter()->getAmount())
                ->setBuyTotalPrice(Price::format($entry->getWebStockCharacter()->getTotalPrice()))
                ->setBuyPrice(Price::format($buyPrice))
                ->setCurrentTotalValue(Price::format($currentTotalValue))
                ->setCurrentValue(Price::format($currentValue))
                ->setCurrentValuePercent(Price::format($percentChange));

            if (!isset($stocks[$entry->getWebStock()->getId()])) {
                $stocks[$entry->getWebStock()->getId()] = (new Entry())
                    ->setId($entry->getWebStock()->getId())
                    ->setName($entry->getWebStock()->getName())
                    ->setPrice($entry->getWebStock()->getPrice())
                    ->setBuyTotalPrice($entry->getWebStockCharacter()->getTotalPrice())
                    ->setBuyPrice($buyPrice)
                    ->setCurrentTotalValue($currentTotalValue)
                    ->setCurrentValue($currentValue)
                    ->setCurrentValuePercent(Price::format($percentChange))
                    ->setEntries([$stockEntry]);
            } else {
                $currentStockEntry = $stocks[$entry->getWebStock()->getId()];
                $percentChange = Math::getPercentageChange(
                    $currentStockEntry->getBuyTotalPrice() + $entry->getWebStockCharacter()->getTotalPrice(),
                    $currentStockEntry->getCurrentTotalValue() + $currentTotalValue
                );
                $currentEntries = $currentStockEntry->getEntries();
                array_push($currentEntries, $stockEntry);
                $currentStockEntry
                    ->setBuyTotalPrice(
                        $currentStockEntry->getBuyTotalPrice() + $entry->getWebStockCharacter()->getTotalPrice()
                    )
                    ->setBuyPrice($currentStockEntry->getBuyPrice() + $buyPrice)
                    ->setCurrentTotalValue($currentStockEntry->getCurrentTotalValue() + $currentTotalValue)
                    ->setCurrentValue($currentStockEntry->getCurrentValue() + $currentValue)
                    ->setCurrentValuePercent($percentChange)
                    ->setEntries($currentEntries);
            }

            $totalValue += $entry->getWebStockCharacter()->getTotalPrice();
            $totalGained += $currentTotalValue;
        }

        $depot = (new \App\Libraries\Frontend\Dto\Web\Stock\Depot())
            ->setTotal(
                (new Total())
                    ->setTotal(Price::format($totalValue))
                    ->setGained(Price::format($totalGained - $totalValue))
                    ->setGainedPercent(Price::format(Math::getPercentageChange($totalValue, $totalGained)))
            )
            ->setEntries($stocks);
        return [
            'depot' => $depot,
            'hash'  => $hash,
        ];
    }
}
