<?php
/**
 * WebAdminWhiteList
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 28.04.2020
 * Time: 17:01
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

use App\Libraries\Logging\ILogging;
use App\Libraries\Logging\Traits\Variables;
use Carbon\Carbon;

/**
 * Class WebAdminBan
 * @package App\Model\Dto
 */
class WebAdminWhitelist implements ILogging
{
    use Variables;

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var null|int
     */
    private $userId = null;

    /**
     * @var string
     */
    private $status = '';

    /**
     * @var null|Carbon
     */
    private $retryDate = null;

    /**
     * @var string
     */
    private $userName = '';

    /**
     * @var string
     */
    private $message = '';

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @var null|Carbon
     */
    private $updatedAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebAdminWhitelist
     */
    public function setId(?int $id): WebAdminWhitelist
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @param int|null $userId
     * @return WebAdminWhitelist
     */
    public function setUserId(?int $userId): WebAdminWhitelist
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return WebAdminWhitelist
     */
    public function setStatus(string $status): WebAdminWhitelist
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getRetryDate(): ?Carbon
    {
        return $this->retryDate;
    }

    /**
     * @param Carbon|null $retryDate
     * @return WebAdminWhitelist
     */
    public function setRetryDate(?Carbon $retryDate): WebAdminWhitelist
    {
        $this->retryDate = $retryDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @param string $userName
     * @return WebAdminWhitelist
     */
    public function setUserName(string $userName): WebAdminWhitelist
    {
        $this->userName = $userName;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return WebAdminWhitelist
     */
    public function setMessage(string $message): WebAdminWhitelist
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return WebAdminWhitelist
     */
    public function setCreatedAt(?Carbon $createdAt): WebAdminWhitelist
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon|null $updatedAt
     * @return WebAdminWhitelist
     */
    public function setUpdatedAt(?Carbon $updatedAt): WebAdminWhitelist
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
