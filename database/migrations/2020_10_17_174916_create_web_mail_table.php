<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebMailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webMail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('from')->index();
            $table->string('to')->nullable()->index();
            $table->string('sender');
            $table->string('receiver');
            $table->string('title');
            $table->longText('message');
            $table->boolean('seen')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webMail');
    }
}
