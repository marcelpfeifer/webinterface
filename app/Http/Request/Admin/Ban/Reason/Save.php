<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 03.08.2021
 * Time: 10:29
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Admin\Ban\Reason;

use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class Save extends AValidator implements IValidator
{
    /**
     * @return string[]
     */
    public function getRules(): array
    {
        return [
            'name'        => [
                'required',
                'string',
            ],
            'description' => [
                'string',
                'nullable',
            ],
            'points'      => [
                'required',
                'int',
            ],
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'name.required'      => 'Der Name muss übergeben werden!',
            'name.string'        => 'Der Name darf nur aus Zeichen bestehen!',
            'description.string' => 'Die Beschreibung darf nur aus Zeichen bestehen!',
            'points.required'    => 'Die Punkte muss übergeben werden!',
            'points.int'         => 'Die Punkte müssen eine Zahl sein!',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Admin\Ban\Reason\Save())
            ->setName($this->request->input('name'))
            ->setDescription($this->request->input('description') ?? '')
            ->setPoints($this->request->input('points'));
    }
}
