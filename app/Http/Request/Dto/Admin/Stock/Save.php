<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 29.08.2021
 * Time: 12:41
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Admin\Stock;


use App\Http\Request\Dto\ADto;

class Save extends ADto
{

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var float
     */
    private $price = 0.00;

    /**
     * @var int
     */
    private $stock = 0;

    /**
     * @var float
     */
    private $minChange = 0.00;

    /**
     * @var float
     */
    private $maxChange = 0.00;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Save
     */
    public function setId(?int $id): Save
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Save
     */
    public function setName(string $name): Save
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return Save
     */
    public function setPrice(float $price): Save
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     * @return Save
     */
    public function setStock(int $stock): Save
    {
        $this->stock = $stock;
        return $this;
    }

    /**
     * @return float
     */
    public function getMinChange(): float
    {
        return $this->minChange;
    }

    /**
     * @param float $minChange
     * @return Save
     */
    public function setMinChange(float $minChange): Save
    {
        $this->minChange = $minChange;
        return $this;
    }

    /**
     * @return float
     */
    public function getMaxChange(): float
    {
        return $this->maxChange;
    }

    /**
     * @param float $maxChange
     * @return Save
     */
    public function setMaxChange(float $maxChange): Save
    {
        $this->maxChange = $maxChange;
        return $this;
    }
}
