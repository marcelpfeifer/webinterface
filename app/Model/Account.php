<?php
/**
 * Account.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 01.11.2019
 * Time: 13:23
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Account extends Authenticatable
{
    use Notifiable;

    /**
     * @var string
     */
    public const COLUMN_ID = 'id';

    /**
     * @var string
     */
    public const COLUMN_USERNAME = 'username';

    /**
     * @var string
     */
    public const COLUMN_PASSWORD = 'password';

    /**
     * @var string
     */
    public const COLUMN_WHITELISTED = 'whitelisted';

    /**
     * @var string
     */
    public const COLUMN_GROUP_ID = 'groupId';

    /**
     * @var string
     */
    public const COLUMN_IMAGE_NAME = 'imageName';

    /**
     * @var string
     */
    public const COLUMN_CREATED_AT = 'createdAt';

    /**
     * @var string
     */
    public const COLUMN_UPDATED_AT = 'updatedAt';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'account';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function character(): HasOne
    {
        return $this->hasOne(Character::class, self::COLUMN_ID, Account::COLUMN_ID);
    }

    /**
     * @param Dto\Account $dto
     */
    public function insertEntry(Dto\Account $dto)
    {
        $this->insert(
            [
                self::COLUMN_ID          => $dto->getId(),
                self::COLUMN_USERNAME    => $dto->getUsername(),
                self::COLUMN_PASSWORD    => $dto->getPassword(),
                self::COLUMN_WHITELISTED => $dto->isWhitelisted(),
                self::COLUMN_GROUP_ID    => $dto->getGroupId(),
                self::COLUMN_IMAGE_NAME  => $dto->getImageName(),
                self::COLUMN_CREATED_AT  => Carbon::now(),
                self::COLUMN_UPDATED_AT  => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\Account $dto
     */
    public function updateEntry(Dto\Account $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_USERNAME    => $dto->getUsername(),
                self::COLUMN_PASSWORD    => $dto->getPassword(),
                self::COLUMN_WHITELISTED => $dto->isWhitelisted(),
                self::COLUMN_GROUP_ID    => $dto->getGroupId(),
                self::COLUMN_IMAGE_NAME  => $dto->getImageName(),
                self::COLUMN_UPDATED_AT  => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\Account $dto
     */
    public function deleteEntry(Dto\Account $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param Account $entry
     * @return Dto\Account
     */
    public static function entryAsDto(Account $entry): Dto\Account
    {
        return (new Dto\Account())
            ->setId($entry->{self::COLUMN_ID})
            ->setUsername($entry->{self::COLUMN_USERNAME})
            ->setPassword($entry->{self::COLUMN_PASSWORD})
            ->setWhitelisted($entry->{self::COLUMN_WHITELISTED})
            ->setGroupId($entry->{self::COLUMN_GROUP_ID})
            ->setImageName($entry->{self::COLUMN_IMAGE_NAME});
    }
}
