<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.01.2021
 * Time: 19:39
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

Route::namespace('Admin')->middleware(
    ['permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND, 'auth']
)->prefix('admin')->name('admin.')->group(
    function () {
        Route::get('', 'AdminController@index')->name('index');
        Route::namespace('Player')->prefix('player')->name('player.')->middleware(
            'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER
        )->group(
            function () {
                Route::get('/', 'IndexController@index')->name('index');
                Route::get('edit/{userId}', 'IndexController@edit')->name('edit');
                Route::post('/save/{userId}', 'IndexController@save')->name('save');

                Route::get('/releaseFromPrison/{userId}', 'IndexController@releaseFromPrison')
                    ->name('releaseFromPrison')
                    ->middleware(
                        'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PRISON
                    );
                Route::get('/resetDimension/{userId}', 'IndexController@resetDimension')
                    ->name('resetDimension')
                    ->middleware(
                        'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_DIMENSION
                    );
                Route::get('/deleteFactionLocker/{userId}', 'IndexController@deleteFactionLocker')
                    ->name('deleteFactionLocker')
                    ->middleware(
                        'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_DELETE_PLAYER_LOCKER
                    );
                Route::get('/deleteFactionLockerWeapon/{userId}', 'IndexController@deleteFactionLockerWeapon')
                    ->name('deleteFactionLockerWeapon')
                    ->middleware(
                        'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_DELETE_PLAYER_LOCKER
                    );
                Route::get('/playerResetPosition/{userId}', 'IndexController@playerResetPosition')
                    ->name('resetPosition')
                    ->middleware(
                        'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PLAYER_POSITION_RESET
                    );
                Route::get('/playerReset/{userId}', 'IndexController@playerReset')
                    ->name('reset')
                    ->middleware(
                        'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PLAYER_RESET
                    );

                Route::get('/inventoryReset/{userId}', 'IndexController@inventoryReset')
                    ->name('inventoryReset')
                    ->middleware(
                        'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PLAYER_INVENTORY_RESET
                    );

                Route::prefix('phone')->name('phone.')->group(
                    function () {
                        Route::get('/{userId}', 'PhoneController@index')->name('index');
                    }
                );

                Route::prefix('vehicle')->name('vehicle.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_VEHICLE
                )->group(
                    function () {
                        Route::get('/{characterId}', 'VehicleController@index')->name('index');
                        Route::get('/edit/{id}', 'VehicleController@edit')->name('edit');
                        Route::post('/edit/{id}', 'VehicleController@save')->name('save');
                        Route::post('/tune/{id}', 'VehicleController@tune')->name('tune');
                        Route::delete('/delete/{id}', 'VehicleController@delete')->name('delete');
                    }
                );

                Route::prefix('bans')->name('bans.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_BANS
                )->group(
                    function () {
                        Route::get('/create', 'BanController@create')->name('create');
                        Route::get('serverBans/{accountId}', 'BanController@serverBans')->name('serverBans');
                        Route::get('/{accountId}', 'BanController@index')->name('index');
                        Route::post('/save/{accountId}', 'BanController@save')->name('save');
                        Route::delete('/delete/{id}', 'BanController@delete')->name('delete');
                        Route::delete('serverBans/delete/{id}', 'BanController@deleteServerBans')
                            ->name('deleteServerBans');
                    }
                );
                Route::prefix('cashFlow')->name('cashFlow.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_CASH_FLOW
                )->group(
                    function () {
                        Route::get('/{characterId}', 'CashFlowController@index')->name('index');
                    }
                );

                Route::prefix('callHistory')->name('callHistory.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_CALL_HISTORY
                )->group(
                    function () {
                        Route::get('/{characterId}', 'CallHistoryController@index')->name('index');
                    }
                );
            }
        );

        Route::prefix('config')->name('config.')->middleware(
            'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_SETTINGS
        )->group(
            function () {
                Route::get('/', 'ConfigController@index')->name('index');
                Route::post('/', 'ConfigController@save')->name('save');
            }
        );

        Route::prefix('itemList')->name('itemList.')->middleware(
            'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_ITEM_LIST
        )->group(
            function () {
                Route::get('/', 'ItemListController@index')->name('index');
                Route::get('/search/itemList', 'ItemListController@searchItemList')->name('searchItemList');
                Route::get('/search/vehicles', 'ItemListController@searchVehicles')->name('searchVehicles');
            }
        );

        Route::prefix('code')->name('code.')->middleware(
            'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_CODE_GENERATOR
        )->group(
            function () {
                Route::get('/', 'CodeController@index')->name('index');
                Route::get('/generate', 'CodeController@generate')->name('generate');
                Route::delete('/delete/{id}', 'CodeController@delete')->name('delete');
            }
        );

        Route::prefix('group')->name('group.')->middleware(
            'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_GROUP
        )->group(
            function () {
                Route::get('/', 'GroupController@index')->name('index');
                Route::post('/create', 'GroupController@create')->name('create');
                Route::get('/show/{id}', 'GroupController@show')->name('show');
                Route::delete('/delete/{id}', 'GroupController@delete')->name('delete');
            }
        );

        Route::prefix('business')->name('business.')->middleware(
            'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_BUSINESS
        )->group(
            function () {
                Route::get('/', 'BusinessController@index')->name('index');
                Route::post('/create', 'BusinessController@create')->name('create');
                Route::get('/show/{id}', 'BusinessController@show')->name('show');
                Route::delete('/delete/{id}', 'BusinessController@delete')->name('delete');
            }
        );

        Route::prefix('log')->name('log.')->middleware(
            'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_LOGS
        )->group(
            function () {
                Route::get('/', 'LogController@index')->name('index');
                Route::get('/show/{id}', 'LogController@show')->name('show');
            }
        );

        Route::prefix('logApi')->name('logApi.')->middleware(
            'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_LOGS
        )->group(
            function () {
                Route::get('/', 'LogApiController@index')->name('index');
            }
        );

        Route::prefix('killLog')->name('killLog.')->middleware(
            'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_LOGS
        )->group(
            function () {
                Route::get('/', 'KillLogController@index')->name('index');
            }
        );

        Route::prefix('serverLog')->name('serverLog.')->middleware(
            'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_LOGS
        )->group(
            function () {
                Route::get('/', 'ServerLogController@index')->name('index');
                Route::get('/reload', 'ServerLogController@reload')->name('reload');
            }
        );

        Route::prefix('permission')->name('permission.')->middleware(
            'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PERMISSIONS
        )->group(
            function () {
                Route::get('/', 'PermissionController@index')->name('index');
                Route::post('/create', 'PermissionController@create')->name('create');
                Route::get('/edit/{id}', 'PermissionController@edit')->name('edit');
                Route::put('/save/{id}', 'PermissionController@save')->name('save');
                Route::delete('/delete/{id}', 'PermissionController@delete')->name('delete');
            }
        );

        Route::namespace('Web')->prefix('web')->name('web.')->group(
            function () {
                Route::prefix('happyBuy')->name('happyBuy.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_HAPPY_BUY_CATEGORY
                )->group(
                    function () {
                        Route::get('/', 'HappyBuyController@index')->name('index');
                        Route::post('/save', 'HappyBuyController@save')->name('save');
                        Route::delete('/delete/{id}', 'HappyBuyController@delete')->name('delete');
                    }
                );
            }
        );

        Route::prefix('report')->name('report.')->middleware(
            'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_REPORT
        )->group(
            function () {
                Route::get('/', 'ReportController@index')->name('index');
                Route::get('show/{id}', 'ReportController@show')->name('show');
                Route::delete('delete/{id}', 'ReportController@delete')->name('delete');
            }
        );

        Route::prefix('bugReport')->name('bugReport.')->middleware(
            'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_BUG_REPORT
        )->group(
            function () {
                Route::get('/', 'BugReportController@index')->name('index');
                Route::get('show/{id}', 'BugReportController@show')->name('show');
                Route::post('save/{id}', 'BugReportController@save')->name('save');
                Route::post('send/{id}', 'BugReportController@send')->name('send');
                Route::delete('delete/{id}', 'BugReportController@delete')->name('delete');
            }
        );

        Route::prefix('stock')->name('stock.')->middleware(
            'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_STOCK
        )->group(
            function () {
                Route::get('/', 'StockController@index')->name('index');
                Route::get('create/', 'StockController@create')->name('create');
                Route::get('edit/{id}', 'StockController@edit')->name('edit');
                Route::post('save/', 'StockController@save')->name('save');
                Route::post('saveEdit/{id}', 'StockController@saveEdit')->name('saveEdit');
                Route::delete('delete/{id}', 'StockController@delete')->name('delete');
            }
        );

        Route::prefix('whitelistQuestion')->name('whitelistQuestion.')->middleware(
            'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_WHITELIST_QUESTION
        )->group(
            function () {
                Route::get('/', 'WhitelistQuestionController@index')->name('index');
                Route::get('create/', 'WhitelistQuestionController@create')->name('create');
                Route::get('edit/{id}', 'WhitelistQuestionController@edit')->name('edit');
                Route::post('save/', 'WhitelistQuestionController@save')->name('save');
                Route::post('saveEdit/{id}', 'WhitelistQuestionController@saveEdit')->name('saveEdit');
                Route::delete('delete/{id}', 'WhitelistQuestionController@delete')->name('delete');
            }
        );

        Route::prefix('ban')->name('ban.')->middleware(
            'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_BAN
        )->group(
            function () {
                Route::get('/', 'BanController@index')->name('index');
                Route::namespace('Ban')->group(
                    function () {
                        Route::prefix('points')->name('points.')->group(
                            function () {
                                Route::get('/', 'PointsController@index')->name('index');
                                Route::get('create/', 'PointsController@create')->name('create');
                                Route::get('edit/{id}', 'PointsController@edit')->name('edit');
                                Route::delete('delete/{id}', 'PointsController@delete')->name('delete');
                                Route::post('save', 'PointsController@save')->name('save');
                                Route::post('update/{id}', 'PointsController@update')->name('update');
                            }
                        );
                        Route::prefix('reason')->name('reason.')->group(
                            function () {
                                Route::get('/', 'ReasonController@index')->name('index');
                                Route::get('create/', 'ReasonController@create')->name('create');
                                Route::get('edit/{id}', 'ReasonController@edit')->name('edit');
                                Route::delete('delete/{id}', 'ReasonController@delete')->name('delete');
                                Route::post('save', 'ReasonController@save')->name('save');
                                Route::post('update/{id}', 'ReasonController@update')->name('update');
                            }
                        );
                    }
                );
            }
        );

        Route::resources(
            [
                'vehicleShop'             => 'VehicleShopController',
                'vehicleShopUnregistered' => 'VehicleShopUnregisteredController',
                'itemStore'               => 'ItemStoreController',
            ]
        );


        Route::namespace('SupportForm')->prefix('supportForm')->name('supportForm.')->group(
            function () {
                Route::get('technicalFaq/solve/{id}', 'TechnicalFaqController@solve')->name(
                    'technicalFaq.solve'
                )->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_SUPPORT_FORM_TECHNICAL_FAQ
                );
                Route::resources(
                    [
                        'technicalFaq' => 'TechnicalFaqController',
                        'refund'       => 'RefundController',
                        'infringement' => 'InfringementController',
                        'ban'          => 'BanController',
                        'whitelist'    => 'WhitelistController',
                    ]
                );
            }
        );
    }
);
