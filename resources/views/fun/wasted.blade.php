<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 14.04.2020
 * Time: 10:01
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
<html>
<head>
    <title>eternityV</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ url('/img/favicon64.ico') }}">
</head>
<body
    style="background-image: linear-gradient(black, black), url('{{ url('img/gtav.jpg') }}'); background-repeat: no-repeat; overflow: hidden; background-size: cover;  background-blend-mode: saturation;">
<audio autoplay>
    <source src="{{ url('img/wasted.mp3') }}" type="audio/mpeg">
</audio>
<div style="background-color: rgba(0,0,0,0.9); padding-bottom: 5px; text-align: center;  position: absolute;
  top: 50%;
  width: 105%;
  left: -1%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);">
    <p style="color: #4c110f; font-weight: bold; font-size: 50px">
        WASTED
    </p>
    <p style="color: white">
        <a style="color: white" href="{{ route('index') }}">Zurück zur Startseite</a>
    </p>
</div>
</body>
</html>
