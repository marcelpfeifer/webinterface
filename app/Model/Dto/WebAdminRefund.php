<?php
/**
 * WebAdminRefund
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 22.11.2019
 * Time: 20:42
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

use App\Libraries\Logging\ILogging;
use App\Libraries\Logging\Traits\Variables;
use Carbon\Carbon;

class WebAdminRefund implements ILogging
{
    use Variables;

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var null|int
     */
    private $userId = null;

    /**
     * @var string
     */
    private $userName = '';

    /**
     * @var string
     */
    private $itemType = '';

    /**
     * @var string
     */
    private $amount = '';

    /**
     * @var string
     */
    private $description = '';

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @var null|Carbon
     */
    private $updatedAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebAdminRefund
     */
    public function setId(?int $id): WebAdminRefund
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @param int|null $userId
     * @return WebAdminRefund
     */
    public function setUserId(?int $userId): WebAdminRefund
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @param string $userName
     * @return WebAdminRefund
     */
    public function setUserName(string $userName): WebAdminRefund
    {
        $this->userName = $userName;
        return $this;
    }

    /**
     * @return string
     */
    public function getItemType(): string
    {
        return $this->itemType;
    }

    /**
     * @param string $itemType
     * @return WebAdminRefund
     */
    public function setItemType(string $itemType): WebAdminRefund
    {
        $this->itemType = $itemType;
        return $this;
    }

    /**
     * @return string
     */
    public function getAmount(): string
    {
        return $this->amount;
    }

    /**
     * @param string $amount
     * @return WebAdminRefund
     */
    public function setAmount(string $amount): WebAdminRefund
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return WebAdminRefund
     */
    public function setDescription(string $description): WebAdminRefund
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return WebAdminRefund
     */
    public function setCreatedAt(?Carbon $createdAt): WebAdminRefund
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon|null $updatedAt
     * @return WebAdminRefund
     */
    public function setUpdatedAt(?Carbon $updatedAt): WebAdminRefund
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
