<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 07.05.2020
 * Time: 19:33
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('web.layout.app')
@section('url')
    https://eternityV.de/marktplatz/{{ $categoryDto->getName() }}
@endsection
@section('pageTitle')
    {{ $categoryDto->getName() }}
@endsection
@section('content')
    <div class="container bg-white text-dark">
        @include('web.marketplace.includes.nav')
        @include('layouts.messages')
        @include('web.marketplace.includes.offers')
    </div>
@endsection

