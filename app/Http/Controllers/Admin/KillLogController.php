<?php
/**
 * KillLogController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.10.2021
 * Time: 13:31
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\DeathLog;
use Illuminate\View\View;

class KillLogController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        $entries = (new DeathLog())->getLogEntriesPagination();
        return \view(
            'admin.killLog.index',
            [
                'entries' => $entries
            ]
        );
    }
}
