<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 16.08.2020
 * Time: 16:02
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('darkWeb.layout.app')
@section('url')
    {{  md5('https://eternityV.de/wallet') }}
@endsection
@section('content')
    <h1 class="darkWebColor"> Bitcoin Wallet </h1>
    @if($character->getBitcoinHashIllegal())
        <table class="table-responsive darkWebColor">
            <tbody>
            <tr>
                <th scope="row">Hash</th>
                <td>{{ $character->getBitcoinHashIllegal() }}</td>
            </tr>
            <tr>
                <th scope="row">Guthaben</th>
                <td>{{ $character->getBitcoinIllegal() }}</td>
            </tr>
            </tbody>
        </table>
        <div class="row text-white mt-2">
            <div class="col-12">
                <a href="{{ route('darkWeb.wallet.transactions', $hash) }}" class="darkWebColor">
                   Transaktionen
                </a>
            </div>
        </div>
        <div class="row text-white mt-2">
            <div class="col-12">
                <a href="{{ route('darkWeb.wallet.transfer', $hash) }}" class="darkWebColor">
                    Transfer
                </a>
            </div>
        </div>
        <div class="row text-white mt-2">
            <div class="col-12">
                <a href="{{ route('darkWeb.wallet.exchange', $hash) }}" class="darkWebColor">
                    Umwandeln
                </a>
            </div>
        </div>
    @else
        @php
            $price = \App\Libraries\Frontend\DarkWeb\Wallet::PRICE_CREATE_WALLET;
        @endphp
        <p class="darkWebColor"> Du hast bisher kein Illegales Bitcoin Konto! </p>
        <form method="post" class="form" action="{{ route('darkWeb.wallet.create', $hash) }}">
            @csrf
            <button type="submit"
                    class="btn-dark p-1 border darkWebBorder darkWebColor">
                {{$price}}
                <i class="fa fa-bitcoin darkWebColor" aria-hidden="true"></i>
                Erstellen
            </button>
        </form>
    @endif
@endsection
