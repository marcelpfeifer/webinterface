<?php
/**
 * Store
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 12.10.2021
 * Time: 15:44
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Admin\VehicleShopUnregistered;

use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Model\Dto\ShopUnregisteredVehicles;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class Store
{

    /**
     * @param \App\Http\Request\Dto\Admin\ShopUnregisteredVehicles\Store $requestDto
     * @return bool
     */
    public static function save(\App\Http\Request\Dto\Admin\ShopUnregisteredVehicles\Store $requestDto): bool
    {
        $dto = (new ShopUnregisteredVehicles())
            ->setModel($requestDto->getModel())
            ->setStock($requestDto->getStock())
            ->setPosX($requestDto->getPositionX())
            ->setPosY($requestDto->getPositionY())
            ->setPosZ($requestDto->getPositionZ())
            ->setRotX($requestDto->getRotationX())
            ->setRotY($requestDto->getRotationY())
            ->setRotZ($requestDto->getRotationZ())
            ->setMapId($requestDto->getMapId())
            ->setNpcPosX($requestDto->getNpcPositionX())
            ->setNpcPosY($requestDto->getNpcPositionY())
            ->setNpcPosZ($requestDto->getNpcPositionZ())
            ->setNpcRot($requestDto->getNpcRotation())
            ->setNpcModel($requestDto->getNpcModel())
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());

        Logging::insert(Name::VEHICLE_STORE_UNREGISTERED, Auth::id(), $dto);

        return (new \App\Model\ShopUnregisteredVehicles())->insertEntry($dto);
    }
}
