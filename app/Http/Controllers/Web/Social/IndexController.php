<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 14.04.2020
 * Time: 14:14
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Web\Social;

use App\Http\Controllers\Controller;
use App\Libraries\Frontend\Enum\Twitter\Mode;
use App\Model\Character;
use App\Model\Dto\WebSocialFollow;
use App\Model\Dto\WebSocialPost;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class IndexController extends Controller
{

    /**
     * @param Request $request
     * @param string $hash
     * @return View
     */
    public function index(Request $request, string $hash): View
    {
        $character = (new Character())->getEntryByHash($hash);
        $mode = $request->input('mode', Mode::ALL);
        $offset = $request->input('offset', 0);
        if (!($character)) {
            return view('web.social.notFound');
        }
        $dto = Character::entryAsDto($character);
        $viewDto = (new \App\Model\WebSocialPost())->getTweets($mode, $dto, $offset);
        $followers = (new \App\Model\WebSocialFollow())->getEntriesByCharacterId($dto->getId());

        return view('web.social.index', [
            'character' => $dto,
            'mode'      => $mode,
            'viewDto'   => $viewDto,
            'followers' => $followers,
            'offset'    => $offset,
            'size'      => 25,
            'hash'      => $hash
        ]);
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\Http\RedirectResponse
     */
    public function post(Request $request, string $hash)
    {
        $message = $request->input('message');
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('web.social.index', $hash);
        }
        $characterDto = Character::entryAsDto($character);
        $dto = (new WebSocialPost())
            ->setCharacterId($characterDto->getId())
            ->setRetweet(null)
            ->setName(Character::entryAsDto($character)->getName())
            ->setMessage($message);
        (new \App\Model\WebSocialPost())->insertEntry($dto);

        return redirect()->route('web.social.index', $hash);
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\Http\RedirectResponse
     */
    public function retweet(Request $request, string $hash)
    {
        $reweet = $request->input('retweet');
        $message = $request->input('message');

        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('web.social.index', $hash);
        }
        $characterDto = Character::entryAsDto($character);
        $dto = (new WebSocialPost())
            ->setCharacterId($characterDto->getId())
            ->setRetweet($reweet)
            ->setName(Character::entryAsDto($character)->getName())
            ->setMessage($message);
        (new \App\Model\WebSocialPost())->insertEntry($dto);

        return redirect()->route('web.social.index', $hash);
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\Http\RedirectResponse
     */
    public function follow(Request $request, string $hash): RedirectResponse
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('web.social.index', $hash);
        }
        $characterDto = Character::entryAsDto($character);

        $followId = $request->input('followId');
        $value = $request->input('value');
        if ($value) {
            $dto = (new WebSocialFollow())
                ->setCharacterId($characterDto->getId())
                ->setFollowId($followId);
            (new \App\Model\WebSocialFollow())->insertEntry($dto);
        } else {
            $dto = (new WebSocialFollow())
                ->setCharacterId($characterDto->getId())
                ->setFollowId($followId);
            (new \App\Model\WebSocialFollow())->deleteEntry($dto);
        }

        return redirect()->route('web.social.index', $hash);
    }
}
