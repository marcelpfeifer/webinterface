<?php
/**
 * Create
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 13.11.2021
 * Time: 15:08
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Dto\Admin\SupportForm\Whitelist;


use App\Model\Dto\WebAdminWhitelistQuestion;

class Create
{

    /**
     * @var WebAdminWhitelistQuestion[]
     */
    private $questions = [];

    /**
     * @return WebAdminWhitelistQuestion[]
     */
    public function getQuestions(): array
    {
        return $this->questions;
    }

    /**
     * @param WebAdminWhitelistQuestion[] $questions
     * @return Create
     */
    public function setQuestions(array $questions): Create
    {
        $this->questions = $questions;
        return $this;
    }
}
