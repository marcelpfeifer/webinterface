<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 29.08.2021
 * Time: 12:41
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Admin\WhitelistQuestion;

use App\Http\Request\Dto\ADto;

class Save extends ADto
{

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var string
     */
    private $question = '';

    /**
     * @var string
     */
    private $answer = '';

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Save
     */
    public function setId(?int $id): Save
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getQuestion(): string
    {
        return $this->question;
    }

    /**
     * @param string $question
     * @return Save
     */
    public function setQuestion(string $question): Save
    {
        $this->question = $question;
        return $this;
    }

    /**
     * @return string
     */
    public function getAnswer(): string
    {
        return $this->answer;
    }

    /**
     * @param string $answer
     * @return Save
     */
    public function setAnswer(string $answer): Save
    {
        $this->answer = $answer;
        return $this;
    }
}
