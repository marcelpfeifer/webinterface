<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebAdminWhiteList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webAdminWhitelist', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('userId');
            $table->enum('status', [
                'APPROVED',
                'DENIED',
                'PENDING',
                'FINISHED'
            ]);
            $table->string('userName');
            $table->text('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webAdminWhitelist');
    }
}
