<?php
/**
 * LogController.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 22.08.2019
 * Time: 21:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Model\WebAdminLogging;
use App\Model\WebAdminLoggingValue;
use Illuminate\Support\Facades\Request;
use Illuminate\View\View;

class LogController extends Controller
{

    /**
     * @return View
     */
    public function index()
    {
        $entries = (new WebAdminLogging())->getLogEntriesPagination();
        return \view(
            'admin.log.index',
            [
                'entries' => $entries
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @return View
     */
    public function show(Request $request, int $id)
    {
        $entries = (new WebAdminLoggingValue())->getLogEntriesByLoggingId($id);
        return \view(
            'admin.log.show',
            [
                'entries' => $entries
            ]
        );
    }
}
