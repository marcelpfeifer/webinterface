<?php
/**
 * Store
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.10.2021
 * Time: 10:43
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Admin\SupportForm\WhiteList;

use App\Http\Request\Dto\ADto;
use Carbon\Carbon;

class Update extends ADto
{

    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var string
     */
    private $username = '';

    /**
     * @var string
     */
    private $status = '';

    /**
     * @var Carbon|null
     */
    private $retryDate = null;

    /**
     * @var string
     */
    private $message = '';

    /**
     * @var array
     */
    private $answer = [];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Update
     */
    public function setId(int $id): Update
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return Update
     */
    public function setUsername(string $username): Update
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Update
     */
    public function setStatus(string $status): Update
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getRetryDate(): ?Carbon
    {
        return $this->retryDate;
    }

    /**
     * @param Carbon|null $retryDate
     * @return Update
     */
    public function setRetryDate(?Carbon $retryDate): Update
    {
        $this->retryDate = $retryDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return Update
     */
    public function setMessage(string $message): Update
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return array
     */
    public function getAnswer(): array
    {
        return $this->answer;
    }

    /**
     * @param array $answer
     * @return Update
     */
    public function setAnswer(array $answer): Update
    {
        $this->answer = $answer;
        return $this;
    }


}
