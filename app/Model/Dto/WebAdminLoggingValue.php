<?php
/**
 * WebAdminLoggingValue
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 10.04.2020
 * Time: 09:25
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

class WebAdminLoggingValue
{

    /**
     * @var int|null
     */
    private $id;

    /**
     * @var int
     */
    private $loggingId = 0;

    /**
     * @var string
     */
    private $tableName = '';

    /**
     * @var string
     */
    private $columnName = '';

    /**
     * @var string
     */
    private $oldValue = '';

    /**
     * @var string
     */
    private $newValue = '';

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebAdminLoggingValue
     */
    public function setId(?int $id): WebAdminLoggingValue
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getLoggingId(): int
    {
        return $this->loggingId;
    }

    /**
     * @param int $loggingId
     * @return WebAdminLoggingValue
     */
    public function setLoggingId(int $loggingId): WebAdminLoggingValue
    {
        $this->loggingId = $loggingId;
        return $this;
    }

    /**
     * @return string
     */
    public function getTableName(): string
    {
        return $this->tableName;
    }

    /**
     * @param string $tableName
     * @return WebAdminLoggingValue
     */
    public function setTableName(string $tableName): WebAdminLoggingValue
    {
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * @return string
     */
    public function getColumnName(): string
    {
        return $this->columnName;
    }

    /**
     * @param string $columnName
     * @return WebAdminLoggingValue
     */
    public function setColumnName(string $columnName): WebAdminLoggingValue
    {
        $this->columnName = $columnName;
        return $this;
    }

    /**
     * @return string
     */
    public function getOldValue(): string
    {
        return $this->oldValue;
    }

    /**
     * @param string $oldValue
     * @return WebAdminLoggingValue
     */
    public function setOldValue(string $oldValue): WebAdminLoggingValue
    {
        $this->oldValue = $oldValue;
        return $this;
    }

    /**
     * @return string
     */
    public function getNewValue(): string
    {
        return $this->newValue;
    }

    /**
     * @param string $newValue
     * @return WebAdminLoggingValue
     */
    public function setNewValue(string $newValue): WebAdminLoggingValue
    {
        $this->newValue = $newValue;
        return $this;
    }
}
