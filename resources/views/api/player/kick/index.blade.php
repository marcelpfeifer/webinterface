<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 13.04.2020
 * Time: 22:03
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('api.layout.app')
@section('title')
    <i class="far fa-futbol"></i>
    Kickt einen Spieler vom Server.
@endsection
@section('content')
    <form method="post" action="{{ route('api.player.kick.save') }}">
        {{ csrf_field() }}
        @include('api.includes.select', [
            'name' => 'pid',
            'label' => 'Spieler',
            'entries' => $players,
            'currentSelect' => $currentPlayer,
        ])
        <div class="form-group">
            <label for="message">Nachricht</label>
            <textarea class="form-control" id="message" name="message" rows="3"></textarea>
        </div>
        <button type="submit" class="btn btn-danger">Bestätigen</button>
    </form>
    <br/>
@endsection
