<div class="container-fluid">
    <div class="row justify-content-center">
        @include('api.layout.sidebar')
        <div class="col-12 mt-2 mt-md-0 col-md-9">
            @include('layouts.messages')
            <div class="card bg-white border-danger text-secondary">
                <div class="card-header bg-danger text-white"> @yield('title')</div>
                <div class="card-body text-dark">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</div>
