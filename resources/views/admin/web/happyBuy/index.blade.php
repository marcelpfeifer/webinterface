<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 03.05.2020
 * Time: 10:41
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@php
    /** @var $result \App\Model\Dto\WebHappyBuyCategory[] */
@endphp
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-folder"></i>
    Kategorien
@endsection
@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($result as $entry)
            <tr>
                <th scope="row">{{ $entry->getId() }}</th>
                <td>{{ $entry->getName() }}</td>
                <td>
                    <form action="{{ route('admin.web.happyBuy.delete', $entry->getId()) }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE"/>
                        <button class="btn btn-danger" type="submit">Löschen</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <h2> Erstellen </h2>
    <form action="{{ route('admin.web.happyBuy.save') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name">
        </div>
        <button type="submit" class="btn btn-dark">Erstellen</button>
    </form>
@endsection
