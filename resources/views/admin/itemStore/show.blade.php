<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 29.04.2020
 * Time: 17:25
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-box"></i>
    Eintrag bearbeiten
    <a class="btn btn-dark btn-sm float-right"
       href="{{ route('admin.itemStore.index') }}"
       role="button"
    >
        Zurück
    </a>
@endsection
@section('content')
    <form action="{{ route('admin.itemStore.update', $dto->getId()) }}" method="POST">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="item">Item</label>
            <input type="text" class="form-control" id="item" name="item" value="{{ $dto->getItem() }}" required>
        </div>
        <div class="form-group">
            <label for="price">Preis</label>
            <input type="number" class="form-control" id="price" name="price" value="{{ $dto->getPrice() }}" required>
        </div>
        <div class="form-group">
            <label for="stock">Lager</label>
            <input type="number" class="form-control" id="stock" name="stock" value="{{ $dto->getStock() }}" required>
        </div>
        <div class="form-group">
            <label for="shop">Shop</label>
            <input type="text" class="form-control" id="shop" name="shop" value="{{ $dto->getShop() }}" required>
        </div>
        <br/>
        <button type="submit" class="btn btn-dark">Aktualisieren</button>
    </form>
@endsection
