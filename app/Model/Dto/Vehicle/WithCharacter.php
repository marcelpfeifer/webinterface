<?php
/**
 * WithCharacter
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 18.09.2020
 * Time: 18:37
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto\Vehicle;

use App\Model\Dto\Character;
use App\Model\Dto\Vehicle;

class WithCharacter
{

    /**
     * @var Vehicle
     */
    private $vehicle;

    /**
     * @var Character|null
     */
    private $character = null;

    /**
     * @return Vehicle
     */
    public function getVehicle(): Vehicle
    {
        return $this->vehicle;
    }

    /**
     * @param Vehicle $vehicle
     * @return WithCharacter
     */
    public function setVehicle(Vehicle $vehicle): WithCharacter
    {
        $this->vehicle = $vehicle;
        return $this;
    }

    /**
     * @return Character|null
     */
    public function getCharacter(): ?Character
    {
        return $this->character;
    }

    /**
     * @param Character|null $character
     * @return WithCharacter
     */
    public function setCharacter(?Character $character): WithCharacter
    {
        $this->character = $character;
        return $this;
    }
}
