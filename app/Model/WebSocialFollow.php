<?php
/**
 * WebSocialFollow
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 14.04.2020
 * Time: 17:51
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class WebSocialFollow extends Model
{
    /**
     * @var string
     */
    const COLUMN_CHARACTER_ID = 'characterId';

    /**
     * @var string
     */
    const COLUMN_FOLLOW_ID = 'followId';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'webSocialFollow';

    /**
     * @param int $key
     * @return array
     */
    public static function getEntriesByCharacterId(int $key): array
    {
        $entries = self::where(self::COLUMN_CHARACTER_ID, $key)->get();
        $result = [];
        foreach ($entries as $entry) {
            $dto = self::entryAsDto($entry);
            $result[] = $dto->getFollowId();
        }
        return $result;
    }

    /**
     * @param Dto\WebSocialFollow $dto
     */
    public function insertEntry(Dto\WebSocialFollow $dto)
    {
        $this->insert([
            self::COLUMN_CHARACTER_ID => $dto->getCharacterId(),
            self::COLUMN_FOLLOW_ID    => $dto->getFollowId(),
        ]);
    }


    /**
     * @param Dto\WebSocialFollow $dto
     */
    public function deleteEntry(\App\Model\Dto\WebSocialFollow $dto)
    {
        $this->where(self::COLUMN_CHARACTER_ID, $dto->getCharacterId())
            ->where(self::COLUMN_FOLLOW_ID, $dto->getFollowId())
            ->delete();
    }

    /**
     * @param WebSocialFollow $entry
     * @return Dto\WebSocialFollow
     */
    public static function entryAsDto(WebSocialFollow $entry): Dto\WebSocialFollow
    {
        return (new Dto\WebSocialFollow())
            ->setCharacterId($entry->{self::COLUMN_CHARACTER_ID})
            ->setFollowId($entry->{self::COLUMN_FOLLOW_ID});
    }
}
