<?php
/**
 * MailController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 12.08.2021
 * Time: 11:54
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Web\Mail;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Http\Controllers\HashController;
use App\Http\Request\Auth\Mail\Mail\Save;
use App\Model\Character;
use App\Model\WebMail;
use App\Model\WebMailAddress;
use App\Model\WebMailAddressToAccount;
use App\Model\WebMailAttachment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class MailController extends HashController
{

    /**
     * @param Request $request
     * @param int $webMailAddressId
     * @param string $hash
     * @return View
     */
    public function index(Request $request, int $webMailAddressId, string $hash): View
    {
        $mailAddress = $this->getMailBoxEntry($webMailAddressId, $hash);
        if (!$mailAddress) {
            return view('404');
        }

        $mails = (new WebMail())->getAllMailsByTo($mailAddress->getId());

        return view(
            'web.mailbox.mail.index',
            [
                'dto'              => $mailAddress,
                'hash'             => $hash,
                'mails'            => $mails,
                'webMailAddressId' => $webMailAddressId,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $webMailAddressId
     * @param string $hash
     * @return View
     */
    public function outgoing(Request $request, int $webMailAddressId, string $hash): View
    {
        $mailAddress = $this->getMailBoxEntry($webMailAddressId, $hash);
        if (!$mailAddress) {
            return view('404');
        }

        $mails = (new WebMail())->getAllMailsByFrom($mailAddress->getId());

        return view(
            'web.mailbox.mail.outgoing',
            [
                'dto'              => $mailAddress,
                'mails'            => $mails,
                'hash'             => $hash,
                'webMailAddressId' => $webMailAddressId,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $webMailAddressId
     * @param string $hash
     * @return View
     */
    public function create(Request $request, int $webMailAddressId, string $hash): View
    {
        $mailAddress = $this->getMailBoxEntry($webMailAddressId, $hash);
        if (!$mailAddress) {
            return view('404');
        }

        return view(
            'web.mailbox.mail.create',
            [
                'dto'              => $mailAddress,
                'webMailAddressId' => $webMailAddressId,
                'hash'             => $hash,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $webMailAddressId
     * @param int $id
     * @param string $hash
     * @return View
     */
    public function reply(Request $request, int $webMailAddressId, int $id, string $hash): View
    {
        $mailAddress = $this->getMailBoxEntry($webMailAddressId, $hash);
        if (!$mailAddress) {
            return view('404');
        }


        // Überprüfung ob es die Mail überhaupt gibt
        $mail = WebMail::find($id);
        if (!($mail)) {
            return view('404');
        }
        $mailDto = WebMail::entryAsDto($mail);

        // Schaut ob die Mail auch wirklich an dieses Postfach ging
        if (!$this->checkIfMailIsAllowed($mailDto, $mailAddress)) {
            return view('404');
        }

        return view(
            'web.mailbox.mail.reply',
            [
                'dto'              => $mailAddress,
                'mail'             => $mailDto,
                'webMailAddressId' => $webMailAddressId,
                'hash'             => $hash,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $webMailAddressId
     * @param int $id
     * @param string $hash
     * @return View
     */
    public function share(Request $request, int $webMailAddressId, int $id, string $hash): View
    {
        $mailAddress = $this->getMailBoxEntry($webMailAddressId, $hash);
        if (!$mailAddress) {
            return view('404');
        }

        // Überprüfung ob es die Mail überhaupt gibt
        $mail = WebMail::find($id);
        if (!($mail)) {
            return view('404');
        }
        $mailDto = WebMail::entryAsDto($mail);

        // Schaut ob die Mail auch wirklich an dieses Postfach ging
        if (!$this->checkIfMailIsAllowed($mailDto, $mailAddress)) {
            return view('404');
        }

        return view(
            'web.mailbox.mail.share',
            [
                'dto'              => $mailAddress,
                'mail'             => $mailDto,
                'webMailAddressId' => $webMailAddressId,
                'hash'             => $hash,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $webMailAddressId
     * @param int $id
     * @return View
     */
    public function show(Request $request, int $webMailAddressId, int $id, string $hash): View
    {
        $mailAddress = $this->getMailBoxEntry($webMailAddressId, $hash);
        if (!$mailAddress) {
            return view('404');
        }
        // Überprüfung ob es die Mail überhaupt gibt
        $mail = WebMail::find($id);
        if (!($mail)) {
            return view('404');
        }
        $mailDto = WebMail::entryAsDto($mail);

        // Schaut ob die Mail auch wirklich an dieses Postfach ging
        if (!$this->checkIfMailIsAllowed($mailDto, $mailAddress)) {
            return view('404');
        }
        // Setze Mail auf gesehen
        $mailDto->setSeen(true);
        (new WebMail())->updateEntry($mailDto);

        $attachments = [];
        foreach ($mail->attachments as $attachment) {
            $attachments[] = WebMailAttachment::entryAsDto($attachment);
        }

        return view(
            'web.mailbox.mail.show',
            [
                'dto'              => $mailAddress,
                'attachments'      => $attachments,
                'mail'             => $mailDto,
                'webMailAddressId' => $webMailAddressId,
                'hash'             => $hash,
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request, int $webMailAddressId, string $hash)
    {
        if (!$this->getMailBoxEntry($webMailAddressId, $hash)) {
            return redirect()->route('web.mailbox.index', $hash);
        }

        // Validiere die Anfrage
        $validator = new Save($request, $webMailAddressId);
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            return redirect()->route('web.mailbox.mail.create', [$webMailAddressId, $hash])
                ->with('messages', $validator->getErrorsAsMessageArray($validatedData))
                ->withInput()
                ->withErrors($validatedData->errors());
        }

        \App\Http\Model\Auth\Mail\Mail\Save::save($validator->toDto());

        $messages[] = (new Message())
            ->setMessage('Mail wurde erfolgreich versendet!')
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('web.mailbox.mail.index', [$webMailAddressId, $hash])->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $webMailAddressId
     * @param int $id
     * @param string $hash
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request, int $webMailAddressId, int $id, string $hash)
    {
        $entry = $this->getMailBoxEntry($webMailAddressId, $hash);
        if (!$entry) {
            return redirect()->route('web.mailbox.index', $hash);
        }

        // Überprüfung ob es die Mail überhaupt gibt
        $mail = WebMail::find($id);
        if (!($mail)) {
            $messages[] = (new Message())
                ->setMessage('Mail konnte nicht gelöscht werden!')
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
            return redirect()->route('web.mailbox.mail.index', [$webMailAddressId, $hash])->with(
                'messages',
                $messages
            );
        }
        $dto = WebMail::entryAsDto($mail);

        if (!$this->checkIfMailIsAllowed($dto, $entry)) {
            $messages[] = (new Message())
                ->setMessage('Mail konnte nicht gelöscht werden!')
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
            return redirect()->route('web.mailbox.mail.index', [$webMailAddressId, $hash])->with('messages', $messages);
        }

        (new WebMail())->deleteEntry($dto);

        $messages[] = (new Message())
            ->setMessage('Mail wurde erfolgreich gelöscht!')
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('web.mailbox.mail.index', [$webMailAddressId, $hash])->with('messages', $messages);
    }


    /**
     * @param \App\Model\Dto\WebMail $mailDto
     * @param \App\Model\Dto\WebMailAddress $mailAddressDto
     * @return bool
     */
    private function checkIfMailIsAllowed(
        \App\Model\Dto\WebMail $mailDto,
        \App\Model\Dto\WebMailAddress $mailAddressDto
    ): bool {
        return ($mailAddressDto->getId() === $mailDto->getTo())
            || ($mailAddressDto->getId() === $mailDto->getFrom() && $mailDto->getTo() === null);
    }

    /**
     * @param int $webMailAddressId
     * @param string $hash
     * @return \App\Model\Dto\WebMailAddress|null
     */
    private function getMailBoxEntry(int $webMailAddressId, string $hash)
    {
        $characterDto = $this->getCharacterByHash($hash);
        if (!($characterDto)) {
            return null;
        }
        $entry = (new WebMailAddressToAccount())->getEntryByAddressIdAndAccountId(
            $webMailAddressId,
            $characterDto->getId()
        );
        return $entry ? WebMailAddress::entryAsDto($entry->webMailAddress) : null;
    }
}
