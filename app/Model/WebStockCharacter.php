<?php
/**
 * WebStockCharacter
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 30.08.2021
 * Time: 13:07
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;


use App\Model\Dto\WebStockCharacter\WithWebStock;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class WebStockCharacter extends Model
{
    /**
     * @var string
     */
    public const COLUMN_ID = 'id';

    /**
     * @var string
     */
    public const COLUMN_WEB_STOCK_ID = 'webStockId';

    /**
     * @var string
     */
    public const COLUMN_CHARACTER_ID = 'characterId';

    /**
     * @var string
     */
    public const COLUMN_AMOUNT = 'amount';

    /**
     * @var string
     */
    public const COLUMN_TOTAL_PRICE = 'totalPrice';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webStockCharacter';

    /**
     * @return BelongsTo
     */
    public function stock(): BelongsTo
    {
        return $this->belongsTo(WebStock::class, self::COLUMN_WEB_STOCK_ID, WebStock::COLUMN_ID);
    }

    /**
     * @param int $characterId
     * @return Dto\WebStockCharacter\WithWebStock[]
     */
    public function getEntries(int $characterId): array
    {
        $entries = $this->where(self::COLUMN_CHARACTER_ID, $characterId)
            ->get();
        $result = [];
        foreach ($entries as $entry) {
            $result[] = (new WithWebStock())
                ->setWebStock(WebStock::entryAsDto($entry->stock))
                ->setWebStockCharacter(self::entryAsDto($entry));
        }
        return $result;
    }

    /**
     * @param Dto\WebStockCharacter $dto
     */
    public function insertEntry(Dto\WebStockCharacter $dto)
    {
        $this->insert(
            [
                self::COLUMN_WEB_STOCK_ID => $dto->getWebStockId(),
                self::COLUMN_CHARACTER_ID => $dto->getCharacterId(),
                self::COLUMN_AMOUNT       => $dto->getAmount(),
                self::COLUMN_TOTAL_PRICE  => $dto->getTotalPrice(),
                self::CREATED_AT          => Carbon::now(),
                self::UPDATED_AT          => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebStockCharacter $dto
     */
    public function updateEntry(Dto\WebStockCharacter $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_WEB_STOCK_ID => $dto->getWebStockId(),
                self::COLUMN_CHARACTER_ID => $dto->getCharacterId(),
                self::COLUMN_AMOUNT       => $dto->getAmount(),
                self::COLUMN_TOTAL_PRICE  => $dto->getTotalPrice(),
                self::UPDATED_AT          => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebStockCharacter $dto
     */
    public function deleteEntry(\App\Model\Dto\WebStockCharacter $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebStockCharacter $entry
     * @return Dto\WebStockCharacter
     */
    public static function entryAsDto(WebStockCharacter $entry): Dto\WebStockCharacter
    {
        return (new Dto\WebStockCharacter())
            ->setId($entry->{self::COLUMN_ID})
            ->setWebStockId($entry->{self::COLUMN_WEB_STOCK_ID})
            ->setCharacterId($entry->{self::COLUMN_CHARACTER_ID})
            ->setAmount($entry->{self::COLUMN_AMOUNT})
            ->setTotalPrice($entry->{self::COLUMN_TOTAL_PRICE})
            ->setCreatedAt($entry->{self::CREATED_AT})
            ->setUpdatedAt($entry->{self::UPDATED_AT});
    }
}
