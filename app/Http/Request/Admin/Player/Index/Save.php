<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.10.2021
 * Time: 10:07
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Admin\Player\Index;

use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;
use App\Libraries\Common\Enum\Db\Length;

class Save extends AValidator implements IValidator
{
    /**
     * @return string[]
     */
    public function getRules(): array
    {
        return [
            'whitelist'      => [
                'bool',
                'required',
            ],
            'groupId'        => [
                'int',
                'nullable',
                'max:' . Length::INTEGER
            ],
            'image'          => [
                'file',
                'nullable',
            ],
            'password'       => [
                'string',
                'nullable',
                'max:' . Length::VARCHAR
            ],
            'name'           => [
                'string',
                'required',
                'max:' . Length::VARCHAR

            ],
            'medicRank'      => [
                'int',
                'required',
                'max:9'
            ],
            'fibRank'        => [
                'int',
                'required',
                'max:9'
            ],
            'copRank'        => [
                'int',
                'required',
                'max:9'
            ],
            'bwfiRank'       => [
                'int',
                'required',
                'max:9'
            ],
            'group'          => [
                'int',
                'required',
                'max:' . Length::INTEGER
            ],
            'groupRank'      => [
                'int',
                'required',
                'max:' . Length::INTEGER
            ],
            'cash'           => [
                'int',
                'required',
                'max:' . Length::INTEGER
            ],
            'bank'           => [
                'int',
                'required',
                'max:' . Length::INTEGER
            ],
            'bitcoin'        => [
                'int',
                'required',
                'max:' . Length::INTEGER
            ],
            'bitcoinIllegal' => [
                'int',
                'required',
                'max:' . Length::INTEGER
            ],
            'dojRank'        => [
                'int',
                'required',
                'max:9'
            ],
            'lawyerRank'        => [
                'int',
                'required',
                'max:9'
            ],
            'businessId'        => [
                'int',
                'required',
                'max:' . Length::INTEGER
            ],
            'businessRank'        => [
                'int',
                'required',
                'max:' . Length::INTEGER
            ],
            'taxiRank'        => [
                'int',
                'required',
                'max:9',
            ],
            'socialPoints'        => [
                'int',
                'required',
                'max:' . Length::INTEGER
            ],
            'drivingLicenseA'      => [
                'bool',
                'required',
            ],
            'drivingLicenseB'      => [
                'bool',
                'required',
            ],
            'drivingLicenseCE'      => [
                'bool',
                'required',
            ],
            'gunLicense'      => [
                'bool',
                'required',
            ],
            'onDuty'      => [
                'bool',
                'required',
            ],
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Admin\Player\Index\Save())
            ->setWhitelist((bool)$this->request->input('whitelist'))
            ->setGroupId($this->request->input('groupId'))
            ->setImage($this->request->input('image'))
            ->setPassword((string)$this->request->input('password'))
            ->setName($this->request->input('name'))
            ->setMedicRank((int)$this->request->input('medicRank'))
            ->setFibRank((int)$this->request->input('fibRank'))
            ->setCopRank((int)$this->request->input('copRank'))
            ->setBwfiRank((int)$this->request->input('bwfiRank'))
            ->setGroup((int)$this->request->input('group'))
            ->setGroupRank((int)$this->request->input('groupRank'))
            ->setCash((int)$this->request->input('cash'))
            ->setBank((int)$this->request->input('bank'))
            ->setBitcoin((int)$this->request->input('bitcoin'))
            ->setBitcoinIllegal((int)$this->request->input('bitcoinIllegal'))
            ->setDojRank((int)$this->request->input('dojRank'))
            ->setLawyerRank((int)$this->request->input('lawyerRank'))
            ->setBusinessId((int)$this->request->input('businessId'))
            ->setBusinessRank((int)$this->request->input('businessRank'))
            ->setTaxiRank((int)$this->request->input('taxiRank'))
            ->setSocialPoints((int)$this->request->input('socialPoints'))
            ->setDrivingLicenseA((bool)$this->request->input('drivingLicenseA'))
            ->setDrivingLicenseB((bool)$this->request->input('drivingLicenseB'))
            ->setDrivingLicenseCE((bool)$this->request->input('drivingLicenseCE'))
            ->setGunLicense((bool)$this->request->input('gunLicense'))
            ->setOnDuty((bool)$this->request->input('onDuty'));
    }
}
