<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 01.05.2020
 * Time: 12:36
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('darkWeb.layout.app')
@section('url')
    {{  md5('https://eternityV.de/darkBuy/create') }}
@endsection
@section('content')
    <script>
        $(document).ready(function () {
            var quill = new Quill('#editor', {
                theme: 'snow'
            });
            $('form').on('submit', function () {
                $('#message').val($('#editor').find('.ql-editor').html());
            });
        });
    </script>
    <div class="container bg-dark darkWebColor">
        <div class="row">
            <div class="col-12">
                @include('darkWeb.darkBuy.includes.nav')
                <br/>
                <h2> Neues Angebot erstellen</h2>
                <form method="post" class="form" action="{{ route('darkWeb.darkBuy.save', $hash) }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="categoryId">Kategorie</label>
                        <select class="form-control" id="categoryId" name="categoryId">
                            <option value="">Keine Kategorie</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->getId() }}">{{ $category->getName() }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-row">
                        <div class="col-12">
                            <label for="imageUrl">Bild</label>
                            <input class="form-control" id="imageUrl" name="imageUrl" type="text">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-12">
                            <label for="price">Preis          <i class="fa fa-bitcoin darkWebColor" aria-hidden="true"></i></label>
                            <input class="form-control" id="price" name="price" type="number">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-12 ">
                            <label for="message">Text</label>
                            <div class="bg-white">
                            <div id="editor" class="form-control bg-white">

                            </div>
                            </div>
                            <input type="hidden" class="form-control bg-white" id="message" name="message">
                        </div>
                    </div>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <div class="form-row justify-content-end">
                        <button type="submit" class="btn btn-dark darkWebColor">Erstellen</button>
                    </div>
                </form>
                <br/>
            </div>
        </div>
    </div>
@endsection
