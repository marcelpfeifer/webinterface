<?php
/**
 * WebAdminItemList
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 11.05.2020
 * Time: 22:03
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use App\Model\Dto\WebAdminItemList\WithName;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class WebAdminItemList
 * @package App\Model
 */
class WebAdminItemList extends Model
{
    /**
     * @var string
     */
    public const COLUMN_ID = 'id';

    /**
     * @var string
     */
    public const COLUMN_CHARACTER_ID = 'characterId';

    /**
     * @var string
     */
    public const COLUMN_STORAGE = 'storage';

    /**
     * @var string
     */
    public const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    public const COLUMN_QUANTITY = 'quantity';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webAdminItemList';

    /**
     * @return mixed
     */
    public function getPossibleNames()
    {
        return $this
            ->select(self::COLUMN_NAME)
            ->groupBy(self::COLUMN_NAME)
            ->pluck(self::COLUMN_NAME)
            ->toArray();
    }

    /**
     * @param string $name
     * @return array
     */
    public function getItemsByName(string $name): array
    {
        $items = [];
        $results = $this->where(self::COLUMN_NAME, 'LIKE', "%$name%")->get();
        foreach ($results as $result) {
            $items[] = self::entryAsDtoWithName($result);
        }

        return $items;
    }

    /**
     * @param Dto\WebAdminItemList $dto
     */
    public function insertEntry(Dto\WebAdminItemList $dto)
    {
        $this->insert(
            [
                self::COLUMN_ID           => $dto->getId(),
                self::COLUMN_CHARACTER_ID => $dto->getCharacterId(),
                self::COLUMN_STORAGE      => $dto->getStorage(),
                self::COLUMN_NAME         => $dto->getName(),
                self::COLUMN_QUANTITY     => $dto->getQuantity(),
                self::CREATED_AT          => Carbon::now(),
                self::UPDATED_AT          => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebAdminItemList $dto
     */
    public function updateEntry(Dto\WebAdminItemList $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_CHARACTER_ID => $dto->getCharacterId(),
                self::COLUMN_STORAGE      => $dto->getStorage(),
                self::COLUMN_NAME         => $dto->getName(),
                self::COLUMN_QUANTITY     => $dto->getQuantity(),
                self::UPDATED_AT          => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebAdminItemList $dto
     */
    public function deleteEntry(Dto\WebAdminItemList $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebAdminItemList $entry
     * @return WithName
     */
    public static function entryAsDtoWithName(WebAdminItemList $entry): WithName
    {
        $character = Character::find($entry->{self::COLUMN_CHARACTER_ID});
        $playerName = '';
        if($character){
            $dto = Character::entryAsDto($character);
            $playerName = $dto->getName();
        }
        return (new WithName())
            ->setId($entry->{self::COLUMN_ID})
            ->setCharacterId($entry->{self::COLUMN_CHARACTER_ID})
            ->setPlayerName($playerName)
            ->setStorage($entry->{self::COLUMN_STORAGE})
            ->setName($entry->{self::COLUMN_NAME})
            ->setQuantity($entry->{self::COLUMN_QUANTITY})
            ->setCreatedAt($entry->{self::CREATED_AT})
            ->setUpdatedAt($entry->{self::UPDATED_AT});
    }
    /**
     * @param WebAdminItemList $entry
     * @return Dto\WebAdminItemList
     */
    public static function entryAsDto(WebAdminItemList $entry): Dto\WebAdminItemList
    {
        return (new Dto\WebAdminItemList())
            ->setId($entry->{self::COLUMN_ID})
            ->setCharacterId($entry->{self::COLUMN_CHARACTER_ID})
            ->setStorage($entry->{self::COLUMN_STORAGE})
            ->setName($entry->{self::COLUMN_NAME})
            ->setQuantity($entry->{self::COLUMN_QUANTITY})
            ->setCreatedAt($entry->{self::CREATED_AT})
            ->setUpdatedAt($entry->{self::UPDATED_AT});
    }
}
