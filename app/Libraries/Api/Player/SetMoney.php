<?php
/**
 * SetMoney
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 12.04.2020
 * Time: 12:45
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Api\Player;

use App\Libraries\Api\AClient;
use App\Model\WebConfig;

class SetMoney extends AClient
{
    /**
     * @var string
     */
    const URL = 'player/setMoney';

    /**
     * @param int $pid
     * @param int $amount
     * @return bool
     */
    public static function call(int $pid, int $amount): bool
    {
        $response = self::sendRequest('POST', WebConfig::getEntryByKey('api_url')->getValue() . self::URL, [
            'query' => [
                'pid'    => $pid,
                'amount' => $amount,
            ]
        ]);
        return $response && $response->getStatusCode() == 200;
    }
}
