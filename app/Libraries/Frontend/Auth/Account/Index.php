<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 18.08.2021
 * Time: 14:59
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Auth\Account;


use App\Model\WebServerStatus;

class Index
{
    /**
     * @return array[]
     */
    public static function getViewData(): array
    {
        $entry = WebServerStatus::latest()->first();
        $dto = null;
        if($entry) {
            $dto = WebServerStatus::entryAsDto($entry);
        }

        return [
            'serverStatus'   => $dto,
        ];
    }
}
