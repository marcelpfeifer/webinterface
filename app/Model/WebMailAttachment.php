<?php
/**
 * WebMailAttachment
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 11.12.2020
 * Time: 21:47
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class WebMailAttachment extends Model
{
    /**
     * @var string
     */
    public const COLUMN_ID = 'id';

    /**
     * @var string
     */
    public const COLUMN_WEB_MAIL_ID = 'webMailId';

    /**
     * @var string
     */
    public const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    public const COLUMN_PATH = 'path';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webMailAttachment';

    /**
     * @param Dto\WebMailAttachment $dto
     */
    public function insertEntry(Dto\WebMailAttachment $dto)
    {
        $this->insert(
            [
                self::COLUMN_WEB_MAIL_ID => $dto->getWebMailId(),
                self::COLUMN_NAME        => $dto->getName(),
                self::COLUMN_PATH        => $dto->getPath(),
                self::CREATED_AT         => Carbon::now(),
                self::UPDATED_AT         => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebMailAttachment $dto
     */
    public function updateEntry(Dto\WebMailAttachment $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_WEB_MAIL_ID => $dto->getWebMailId(),
                self::COLUMN_NAME        => $dto->getName(),
                self::COLUMN_PATH        => $dto->getPath(),
                self::UPDATED_AT         => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebMailAttachment $dto
     */
    public function deleteEntry(\App\Model\Dto\WebMailAttachment $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebMailAttachment $entry
     * @return Dto\WebMailAttachment
     */
    public static function entryAsDto(WebMailAttachment $entry): Dto\WebMailAttachment
    {
        return (new Dto\WebMailAttachment())
            ->setId($entry->{self::COLUMN_ID})
            ->setWebMailId($entry->{self::COLUMN_WEB_MAIL_ID})
            ->setPath($entry->{self::COLUMN_PATH})
            ->setName($entry->{self::COLUMN_NAME});
    }
}
