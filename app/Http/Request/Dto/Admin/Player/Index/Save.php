<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.10.2021
 * Time: 10:07
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Admin\Player\Index;

use App\Http\Request\Dto\ADto;

class Save extends ADto
{

    /**
     * @var bool
     */
    private $whitelist = false;

    /**
     * @var int|null
     */
    private $groupId = null;

    /**
     * @var \Illuminate\Http\UploadedFile|null
     */
    private $image = null;

    /**
     * @var string
     */
    private $password = '';

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var int
     */
    private $medicRank = 0;

    /**
     * @var int
     */
    private $fibRank = 0;

    /**
     * @var int
     */
    private $copRank = 0;

    /**
     * @var int
     */
    private $bwfiRank = 0;

    /**
     * @var int
     */
    private $group = 0;

    /**
     * @var int
     */
    private $groupRank = 0;

    /**
     * @var int
     */
    private $cash = 0;

    /**
     * @var int
     */
    private $bank = 0;

    /**
     * @var int
     */
    private $bitcoin = 0;

    /**
     * @var int
     */
    private $bitcoinIllegal = 0;

    /**
     * @var int
     */
    private $dojRank = 0;

    /**
     * @var int
     */
    private $lawyerRank = 0;

    /**
     * @var int
     */
    private $businessId = 0;

    /**
     * @var int
     */
    private $businessRank = 0;

    /**
     * @var int
     */
    private $taxiRank = 0;

    /**
     * @var int
     */
    private $socialPoints = 0;

    /**
     * @var bool
     */
    private $drivingLicenseA = false;

    /**
     * @var bool
     */
    private $drivingLicenseB = false;

    /**
     * @var bool
     */
    private $drivingLicenseCE = false;

    /**
     * @var bool
     */
    private $gunLicense = false;

    /**
     * @var bool
     */
    private $onDuty = false;

    /**
     * @return bool
     */
    public function isWhitelist(): bool
    {
        return $this->whitelist;
    }

    /**
     * @param bool $whitelist
     * @return Save
     */
    public function setWhitelist(bool $whitelist): Save
    {
        $this->whitelist = $whitelist;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getGroupId(): ?int
    {
        return $this->groupId;
    }

    /**
     * @param int|null $groupId
     * @return Save
     */
    public function setGroupId(?int $groupId): Save
    {
        $this->groupId = $groupId;
        return $this;
    }

    /**
     * @return \Illuminate\Http\UploadedFile|null
     */
    public function getImage(): ?\Illuminate\Http\UploadedFile
    {
        return $this->image;
    }

    /**
     * @param \Illuminate\Http\UploadedFile|null $image
     * @return Save
     */
    public function setImage(?\Illuminate\Http\UploadedFile $image): Save
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return Save
     */
    public function setPassword(string $password): Save
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Save
     */
    public function setName(string $name): Save
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getMedicRank(): int
    {
        return $this->medicRank;
    }

    /**
     * @param int $medicRank
     * @return Save
     */
    public function setMedicRank(int $medicRank): Save
    {
        $this->medicRank = $medicRank;
        return $this;
    }

    /**
     * @return int
     */
    public function getFibRank(): int
    {
        return $this->fibRank;
    }

    /**
     * @param int $fibRank
     * @return Save
     */
    public function setFibRank(int $fibRank): Save
    {
        $this->fibRank = $fibRank;
        return $this;
    }

    /**
     * @return int
     */
    public function getCopRank(): int
    {
        return $this->copRank;
    }

    /**
     * @param int $copRank
     * @return Save
     */
    public function setCopRank(int $copRank): Save
    {
        $this->copRank = $copRank;
        return $this;
    }

    /**
     * @return int
     */
    public function getBwfiRank(): int
    {
        return $this->bwfiRank;
    }

    /**
     * @param int $bwfiRank
     * @return Save
     */
    public function setBwfiRank(int $bwfiRank): Save
    {
        $this->bwfiRank = $bwfiRank;
        return $this;
    }

    /**
     * @return int
     */
    public function getGroup(): int
    {
        return $this->group;
    }

    /**
     * @param int $group
     * @return Save
     */
    public function setGroup(int $group): Save
    {
        $this->group = $group;
        return $this;
    }

    /**
     * @return int
     */
    public function getGroupRank(): int
    {
        return $this->groupRank;
    }

    /**
     * @param int $groupRank
     * @return Save
     */
    public function setGroupRank(int $groupRank): Save
    {
        $this->groupRank = $groupRank;
        return $this;
    }

    /**
     * @return int
     */
    public function getCash(): int
    {
        return $this->cash;
    }

    /**
     * @param int $cash
     * @return Save
     */
    public function setCash(int $cash): Save
    {
        $this->cash = $cash;
        return $this;
    }

    /**
     * @return int
     */
    public function getBank(): int
    {
        return $this->bank;
    }

    /**
     * @param int $bank
     * @return Save
     */
    public function setBank(int $bank): Save
    {
        $this->bank = $bank;
        return $this;
    }

    /**
     * @return int
     */
    public function getBitcoin(): int
    {
        return $this->bitcoin;
    }

    /**
     * @param int $bitcoin
     * @return Save
     */
    public function setBitcoin(int $bitcoin): Save
    {
        $this->bitcoin = $bitcoin;
        return $this;
    }

    /**
     * @return int
     */
    public function getBitcoinIllegal(): int
    {
        return $this->bitcoinIllegal;
    }

    /**
     * @param int $bitcoinIllegal
     * @return Save
     */
    public function setBitcoinIllegal(int $bitcoinIllegal): Save
    {
        $this->bitcoinIllegal = $bitcoinIllegal;
        return $this;
    }

    /**
     * @return int
     */
    public function getDojRank(): int
    {
        return $this->dojRank;
    }

    /**
     * @param int $dojRank
     * @return Save
     */
    public function setDojRank(int $dojRank): Save
    {
        $this->dojRank = $dojRank;
        return $this;
    }

    /**
     * @return int
     */
    public function getLawyerRank(): int
    {
        return $this->lawyerRank;
    }

    /**
     * @param int $lawyerRank
     * @return Save
     */
    public function setLawyerRank(int $lawyerRank): Save
    {
        $this->lawyerRank = $lawyerRank;
        return $this;
    }

    /**
     * @return int
     */
    public function getBusinessId(): int
    {
        return $this->businessId;
    }

    /**
     * @param int $businessId
     * @return Save
     */
    public function setBusinessId(int $businessId): Save
    {
        $this->businessId = $businessId;
        return $this;
    }

    /**
     * @return int
     */
    public function getBusinessRank(): int
    {
        return $this->businessRank;
    }

    /**
     * @param int $businessRank
     * @return Save
     */
    public function setBusinessRank(int $businessRank): Save
    {
        $this->businessRank = $businessRank;
        return $this;
    }

    /**
     * @return int
     */
    public function getTaxiRank(): int
    {
        return $this->taxiRank;
    }

    /**
     * @param int $taxiRank
     * @return Save
     */
    public function setTaxiRank(int $taxiRank): Save
    {
        $this->taxiRank = $taxiRank;
        return $this;
    }

    /**
     * @return int
     */
    public function getSocialPoints(): int
    {
        return $this->socialPoints;
    }

    /**
     * @param int $socialPoints
     * @return Save
     */
    public function setSocialPoints(int $socialPoints): Save
    {
        $this->socialPoints = $socialPoints;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDrivingLicenseA(): bool
    {
        return $this->drivingLicenseA;
    }

    /**
     * @param bool $drivingLicenseA
     * @return Save
     */
    public function setDrivingLicenseA(bool $drivingLicenseA): Save
    {
        $this->drivingLicenseA = $drivingLicenseA;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDrivingLicenseB(): bool
    {
        return $this->drivingLicenseB;
    }

    /**
     * @param bool $drivingLicenseB
     * @return Save
     */
    public function setDrivingLicenseB(bool $drivingLicenseB): Save
    {
        $this->drivingLicenseB = $drivingLicenseB;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDrivingLicenseCE(): bool
    {
        return $this->drivingLicenseCE;
    }

    /**
     * @param bool $drivingLicenseCE
     * @return Save
     */
    public function setDrivingLicenseCE(bool $drivingLicenseCE): Save
    {
        $this->drivingLicenseCE = $drivingLicenseCE;
        return $this;
    }

    /**
     * @return bool
     */
    public function isGunLicense(): bool
    {
        return $this->gunLicense;
    }

    /**
     * @param bool $gunLicense
     * @return Save
     */
    public function setGunLicense(bool $gunLicense): Save
    {
        $this->gunLicense = $gunLicense;
        return $this;
    }

    /**
     * @return bool
     */
    public function isOnDuty(): bool
    {
        return $this->onDuty;
    }

    /**
     * @param bool $onDuty
     * @return Save
     */
    public function setOnDuty(bool $onDuty): Save
    {
        $this->onDuty = $onDuty;
        return $this;
    }
}
