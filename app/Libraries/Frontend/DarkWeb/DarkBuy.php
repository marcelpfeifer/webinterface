<?php
/**
 * DarkBuy
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 13.08.2020
 * Time: 20:26
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\DarkWeb;

use App\Libraries\Character\Payment\BitcoinIllegal;
use App\Model\Dto\Character;
use App\Model\Dto\WebDarkBuy;

class DarkBuy
{
    /**
     * @param Character $buyerDto
     * @param WebDarkBuy $dto
     * @return bool
     */
    public static function pay(Character $buyerDto, WebDarkBuy $dto): bool
    {
        // Käufer hat nicht genug Bitcoins um das Angebot zu kaufen
        if ($buyerDto->getBitcoinIllegal() < $dto->getPrice()) {
            return false;
        }
        $characterDto = (new \App\Model\Character())->getEntryById($dto->getCharacterId());

        return BitcoinIllegal::transfer($buyerDto, $characterDto, $dto->getPrice(), 'darkBuyOffer');
    }
}
