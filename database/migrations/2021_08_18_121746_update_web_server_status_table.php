<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateWebServerStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'webServerStatus',
            function (Blueprint $table) {
                $table->boolean('altv')->default(false)->after('cpu');
                $table->boolean('altvTest')->default(false)->after('altv');
                $table->boolean('ts')->default(false)->after('altvTest');
                $table->boolean('forum')->default(false)->after('ts');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'webServerStatus',
            function (Blueprint $table) {
                $table->dropColumn(
                    [
                        'altv',
                        'altvTest',
                        'ts',
                        'forum',
                    ]
                );
            }
        );
    }
}
