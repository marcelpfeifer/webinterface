<?php
/**
 * Transactions.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 01.11.2019
 * Time: 13:23
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */


namespace App\Model;

use App\Model\Dto\Transactions\TransactionsWithCharacters;
use App\Model\Enum\Transactions\Type;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_TX = 'tx';

    /**
     * @var string
     */
    const COLUMN_RX = 'rx';

    /**
     * @var string
     */
    const COLUMN_AMOUNT = 'amount';

    /**
     * @var string
     */
    const COLUMN_TYPE = 'type';

    /**
     * @var string
     */
    const COLUMN_CREATED_AT = 'createdAt';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'transactions';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function characterTx()
    {
        return $this->belongsTo(Character::class, self::COLUMN_TX);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function characterRx()
    {
        return $this->belongsTo(Character::class, self::COLUMN_RX);
    }

    /**
     * @param int $guid
     * @return TransactionsWithCharacters[]
     */
    public function getTransactionsBitcoinIllegalByGuid(int $guid)
    {
        $results = [];

        $entries = $this
            ->where(
                function ($query) use ($guid) {
                    $query->where(self::COLUMN_RX, $guid)
                        ->orWhere(self::COLUMN_TX, $guid);
                }
            )
            ->with('characterTx')
            ->with('characterRx')
            ->where(self::COLUMN_TYPE, Type::BITCOIN_ILLEGAL)
            ->get();

        foreach ($entries as $entry) {
            $results[] = (new TransactionsWithCharacters())
                ->setRx(Character::entryAsDto($entry->characterRx))
                ->setTx(Character::entryAsDto($entry->characterTx))
                ->setTransaction(self::entryAsDto($entry));
        }

        return $results;
    }

    /**
     * @param Dto\Transactions $dto
     */
    public function insertEntry(Dto\Transactions $dto)
    {
        $this->insert(
            [
                self::COLUMN_ID         => $dto->getId(),
                self::COLUMN_TX         => $dto->getTx(),
                self::COLUMN_RX         => $dto->getRx(),
                self::COLUMN_AMOUNT     => $dto->getAmount(),
                self::COLUMN_TYPE       => $dto->getType(),
                self::COLUMN_CREATED_AT => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\Transactions $dto
     */
    public function updateEntry(Dto\Transactions $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_TX     => $dto->getTx(),
                self::COLUMN_RX     => $dto->getRx(),
                self::COLUMN_AMOUNT => $dto->getAmount(),
                self::COLUMN_TYPE   => $dto->getType(),
            ]
        );
    }

    /**
     * @param Dto\Transactions $dto
     */
    public function deleteEntry(Dto\Transactions $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param Transactions $entry
     * @return Dto\Transactions
     */
    public static function entryAsDto(Transactions $entry): Dto\Transactions
    {
        return (new Dto\Transactions())
            ->setId($entry->{self::COLUMN_ID})
            ->setTx($entry->{self::COLUMN_TX})
            ->setRx($entry->{self::COLUMN_RX})
            ->setAmount($entry->{self::COLUMN_AMOUNT})
            ->setType($entry->{self::COLUMN_TYPE});
    }
}
