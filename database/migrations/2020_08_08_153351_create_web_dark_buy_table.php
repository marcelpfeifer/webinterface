<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebDarkBuyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'webDarkBuy',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedInteger('characterId');
                $table->unsignedBigInteger('categoryId')->nullable();
                $table->foreign('categoryId')->references('id')->on('webHappyBuyCategory');
                $table->unsignedInteger('buyerId')->nullable();
                $table->enum(
                    'status',
                    [
                        'OPEN',
                        'BOUGHT',
                    ]
                );
                $table->string('name');
                $table->integer('price');
                $table->string('imageUrl');
                $table->text('message');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webDarkBuy');
    }
}
