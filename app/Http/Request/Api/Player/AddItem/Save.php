<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.10.2021
 * Time: 15:35
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Api\Player\AddItem;


use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class Save extends AValidator implements IValidator
{
    /**
     * @return string[]
     */
    public function getRules(): array
    {
        return [
            'pid'    => [
                'required',
                'int',
                'exists:character,id'
            ],
            'item'   => [
                'required',
                'string',
            ],
            'amount' => [
                'required',
                'int',
            ],
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'pid.required'    => 'Die Spieler ID muss übergeben werden!',
            'pid.int'         => 'Die Spieler ID muss ein Integer sein!',
            'pid.exists'      => 'Die Spieler ID existiert nicht!',
            'item.required'   => 'Das Item muss übergeben werden!',
            'item.string'     => 'Das Itemmuss ein String sein!',
            'amount.required' => 'Die Anzahl muss übergeben werden!',
            'amount.int'      => 'Die Anzahl muss ein Integer sein!',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Api\Player\AddItem\Save())
            ->setPlayerId($this->request->input('pid'))
            ->setItem($this->request->input('item'))
            ->setAmount($this->request->input('amount'));
    }
}
