<?php
/**
 * Type
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 11.08.2021
 * Time: 16:05
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Api\Server\Enum;

use App\Libraries\Enum\AEnum;

class Type extends AEnum
{

    /**
     * @var string
     */
    const MAIN_SERVER = 'altv';

    /**
     * @var string
     */
    const TEST_SERVER = 'altv_test';

    /**
     * @var string
     */
    const TS_SERVER = 'ts';


    /**
     * @var string[]
     */
    public static $mappedToTranslation = [
        self::MAIN_SERVER => 'Hauptserver',
        self::TEST_SERVER => 'Testserver',
        self::TS_SERVER   => 'TS3-Server',
    ];
}
