<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 12.04.2020
 * Time: 13:20
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('api.layout.app')
@section('title')
    <i class="fas fa-expand-arrows-alt"></i>
    Setzt die Dimension einen Spieler
@endsection
@section('content')
    <form method="post" action="{{ route('api.player.dimension.save') }}">
        {{ csrf_field() }}

        @include('api.includes.select', [
         'name' => 'pid',
         'label' => 'Spieler',
         'entries' => $players,
         'currentSelect' => $currentPlayer,
       ])

        <div class="form-group">
            <label for="dimension">Dimension</label>
            <input type="number" class="form-control" id="dimension" name="dimension" value="0">
        </div>
        <button type="submit" class="btn btn-danger">Bestätigen</button>
    </form>
    <br/>
@endsection
