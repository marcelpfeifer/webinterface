<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', 'Auth\RegisterController@showRegistrationForm')->name('index');
Route::get('/wasted', 'IndexController@wasted')->name('wasted');
Auth::routes();

Route::namespace('BugReport')->prefix('bugReport')->name('bugReport.')->group(
    function () {
        Route::get('{hash}', 'IndexController@index')->name('index');
        Route::post('/save/{hash}', 'IndexController@save')->name('save');
    }
);

Route::namespace('Web')->prefix('web')->name('web.')->group(
    function () {
        Route::get('{hash}', 'IndexController@index')->name('index');
        Route::namespace('Marketplace')->prefix('marketplace')->name('marketplace.')->group(
            function () {
                Route::get('{hash}', 'IndexController@index')->name('index');
                Route::get('category/{id}/{hash}', 'IndexController@category')->name('category');
                Route::get('offers/{hash}', 'IndexController@offers')->name('offers');
                Route::get('create/{hash}', 'IndexController@create')->name('create');
                Route::post('{hash}', 'IndexController@save')->name('save');
                Route::delete('{id}/{hash}', 'IndexController@delete')->name('delete');
            }
        );

        Route::namespace('News')->prefix('news')->name('news.')->group(
            function () {
                Route::get('/{hash}', 'IndexController@index')->name('index');
                Route::get('/{categoryId}/{hash}', 'IndexController@category')->name('category');
                Route::get('show/{id}/{hash}', 'IndexController@show')->name('show');
            }
        );

        Route::namespace('Hunt')->prefix('hunt')->name('hunt.')->group(
            function () {
                Route::get('/{hash}', 'IndexController@index')->name('index');
            }
        );

        Route::namespace('Stock')->prefix('stock')->name('stock.')->group(
            function () {
                Route::get('/{hash}', 'IndexController@index')->name('index');
                Route::get('depot/{hash}', 'IndexController@depot')->name('depot');
                Route::get('show/{id}/{hash}', 'IndexController@show')->name('show');
                Route::get('buy/{id}/{hash}', 'IndexController@buy')->name('buy');
                Route::get('sell/{id}/{hash}', 'IndexController@sell')->name('sell');
                Route::post('buy/{id}/{hash}', 'IndexController@buyPost')->name('buyPost');
                Route::post('sell/{id}/{hash}', 'IndexController@sellPost')->name('sellPost');
                Route::get('getStockData/{id}', 'IndexController@getStockData')->name('getStockData');
            }
        );

        Route::namespace('Mail')->prefix('mailbox')->name('mailbox.')->group(
            function () {
                Route::get('index/{hash}', 'MailboxController@index')->name('index');
                Route::get('create/{hash}', 'MailboxController@create')->name('create');
                Route::get('add/{hash}', 'MailboxController@add')->name('add');
                Route::post('save/{hash}', 'MailboxController@save')->name('save');
                Route::post('addSave/{hash}', 'MailboxController@addSave')->name('addSave');
                Route::prefix('mail')->name('mail.')->group(
                    function () {
                        Route::get('index/{webMailAddressId}/{hash}', 'MailController@index')->name('index');
                        Route::get('outgoing/{webMailAddressId}/{hash}', 'MailController@outgoing')->name('outgoing');
                        Route::get('create/{webMailAddressId}/{hash}', 'MailController@create')->name('create');
                        Route::get('reply/{webMailAddressId}/{id}/{hash}', 'MailController@reply')->name('reply');
                        Route::get('share/{webMailAddressId}/{id}/{hash}', 'MailController@share')->name('share');
                        Route::get('show/{webMailAddressId}/{id}/{hash}', 'MailController@show')->name('show');
                        Route::post('save/{webMailAddressId}/{hash}', 'MailController@save')->name('save');
                        Route::delete('delete/{webMailAddressId}/{id}/{hash}', 'MailController@delete')->name('delete');
                    }
                );
            }
        );
        Route::namespace('YellowPage')->prefix('yellowPage')->name('yellowPage.')->group(
            function () {
                Route::get('{hash}', 'IndexController@index')->name('index');
                Route::post('save/{hash}', 'IndexController@save')->name('save');
                Route::get('call/{id}/{hash}', 'IndexController@call')->name('call');
                Route::delete('delete/{hash}', 'IndexController@delete')->name('delete');
            }
        );

        Route::namespace('Social')->prefix('social')->name('social.')->group(
            function () {
                Route::get('{hash}', 'IndexController@index')->name('index');
                Route::get('follow/{hash}', 'IndexController@follow')->name('follow');
                Route::post('retweet/{hash}', 'IndexController@retweet')->name('retweet');
                Route::post('{hash}', 'IndexController@post')->name('post');
            }
        );
        Route::prefix('medic')->name('medic.')->group(
            function () {
                Route::get(
                    '{hash}',
                    function (\Illuminate\Http\Request $request, string $hash) {
                        return view('web.medic.index', ['hash' => $hash]);
                    }
                )->name('index');
            }
        );
        Route::prefix('police')->name('police.')->group(
            function () {
                Route::get(
                    '{hash}',
                    function (\Illuminate\Http\Request $request, string $hash) {
                        return view('web.police.index', ['hash' => $hash]);
                    }
                )->name('index');
            }
        );
    }
);

Route::namespace('DarkWeb')->prefix('darkWeb')->name('darkWeb.')->group(
    function () {
        Route::get('{hash}', 'IndexController@index')->name('index');
        Route::get('weirdImages/{hash}', 'IndexController@weirdImages')->name('weirdImages');
        Route::get('video/{hash}', 'IndexController@video')->name('video');
        Route::get('2048/{hash}', 'IndexController@twoThousandFortyEight')->name('2048');
        Route::get('loading/{hash}', 'IndexController@loading')->name('loading');
        Route::post('save/{hash}', 'IndexController@save')->name('save');
        Route::namespace('DarkBuy')->prefix('darkBuy')->name('darkBuy.')->group(
            function () {
                Route::get('{hash}', 'IndexController@index')->name('index');
                Route::get('category/{category}/{hash}', 'IndexController@category')->name('category');
                Route::get('offers/{hash}', 'IndexController@offers')->name('offers');
                Route::get('create/{hash}', 'IndexController@create')->name('create');
                Route::post('{hash}', 'IndexController@save')->name('save');
                Route::get('pay/{id}/{hash}', 'IndexController@pay')->name('pay');
                Route::get('bought/{hash}', 'IndexController@bought')->name('bought');
                Route::get('call/{id}/{hash}', 'IndexController@call')->name('call');
                Route::delete('{id}/{hash}', 'IndexController@delete')->name('delete');
            }
        );
        Route::namespace('Wallet')->prefix('wallet')->name('wallet.')->group(
            function () {
                Route::get('{hash}', 'IndexController@index')->name('index');
                Route::post('create/{hash}', 'IndexController@create')->name('create');
                Route::get('transactions/{hash}', 'IndexController@transactions')->name('transactions');
                Route::get('transfer/{hash}', 'IndexController@transfer')->name('transfer');
                Route::post('transfer/{hash}', 'IndexController@transferPost')->name('transferPost');
                Route::get('exchange/{hash}', 'IndexController@exchange')->name('exchange');
                Route::post('exchange/{hash}', 'IndexController@exchangePost')->name('exchangePost');
            }
        );
    }
);

Route::namespace('Auth')->middleware('auth')->prefix('auth')->name('auth.')->group(
    function () {
        Route::prefix('account')->name('account.')->group(
            function () {
                Route::get('index', 'AccountController@index')->name('index');
                Route::get('edit', 'AccountController@edit')->name('edit');
                Route::post('save', 'AccountController@save')->name('save');
            }
        );
        Route::prefix('character')->name('character.')->group(
            function () {
                Route::get('index', 'CharacterController@index')->name('index');
                Route::post('save', 'CharacterController@save')->name('save');
            }
        );

        Route::namespace('News')->prefix('news')->name('news.')->middleware(
            'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_NEWS
        )->group(
            function () {
                Route::get('index', 'IndexController@index')->name('index');
                Route::get('create', 'IndexController@create')->name('create');
                Route::get('edit/{id}', 'IndexController@edit')->name('edit');
                Route::post('save/{id}', 'IndexController@saveEdit')->name('saveEdit');
                Route::post('save', 'IndexController@save')->name('save');
                Route::delete('delete/{id}', 'IndexController@delete')->name('delete');
                Route::prefix('category')->name('category.')->middleware(
                    'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_NEWS_CATEGORY
                )->group(
                    function () {
                        Route::get('index', 'CategoryController@index')->name('index');
                        Route::get('create', 'CategoryController@create')->name('create');
                        Route::get('edit/{id}', 'CategoryController@edit')->name('edit');
                        Route::post('save/{id}', 'CategoryController@saveEdit')->name('saveEdit');
                        Route::post('save', 'CategoryController@save')->name('save');
                        Route::delete('delete/{id}', 'CategoryController@delete')->name('delete');
                    }
                );
            }
        );

        Route::namespace('Mail')->prefix('mailbox')->name('mailbox.')->group(
            function () {
                Route::get('index', 'MailboxController@index')->name('index');
                Route::get('create', 'MailboxController@create')->name('create');
                Route::get('add', 'MailboxController@add')->name('add');
                Route::get('edit/{id}', 'MailboxController@edit')->name('edit');
                Route::get('generatePassword/{id}', 'MailboxController@generatePassword')->name('generatePassword');
                Route::delete('deleteAccount/{id}/{accountId}', 'MailboxController@deleteAccount')->name(
                    'deleteAccount'
                );
                Route::post('save', 'MailboxController@save')->name('save');
                Route::post('addSave', 'MailboxController@addSave')->name('addSave');
                Route::prefix('mail')->name('mail.')->group(
                    function () {
                        Route::get('index/{webMailAddressId}', 'MailController@index')->name('index');
                        Route::get('outgoing/{webMailAddressId}', 'MailController@outgoing')->name('outgoing');
                        Route::get('create/{webMailAddressId}', 'MailController@create')->name('create');
                        Route::get('reply/{webMailAddressId}/{id}', 'MailController@reply')->name('reply');
                        Route::get('share/{webMailAddressId}/{id}', 'MailController@share')->name('share');
                        Route::get('show/{webMailAddressId}/{id}', 'MailController@show')->name('show');
                        Route::post('save/{webMailAddressId}', 'MailController@save')->name('save');
                        Route::delete('delete/{webMailAddressId}/{id}', 'MailController@delete')->name('delete');
                    }
                );
            }
        );
    }
);

Route::get('/dataProtection', 'StaticController@dataProtection')->name('dataProtection');

