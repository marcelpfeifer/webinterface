<?php
/**
 * WebNewsCategory
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.08.2021
 * Time: 11:53
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;


use App\Libraries\Logging\ILogging;
use App\Libraries\Logging\Traits\Variables;

class WebNewsCategory implements ILogging
{

    use Variables;

    /**
     * @var int|null
     */
    private $id = null;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var bool
     */
    private $show = false;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebNewsCategory
     */
    public function setId(?int $id): WebNewsCategory
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return WebNewsCategory
     */
    public function setName(string $name): WebNewsCategory
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return bool
     */
    public function isShow(): bool
    {
        return $this->show;
    }

    /**
     * @param bool $show
     * @return WebNewsCategory
     */
    public function setShow(bool $show): WebNewsCategory
    {
        $this->show = $show;
        return $this;
    }


}
