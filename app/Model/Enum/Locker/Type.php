<?php
/**
 * Type
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 27.10.2021
 * Time: 08:44
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Enum\Locker;

use App\Libraries\Enum\AEnum;

class Type extends AEnum
{

    /**
     * @var int
     */
    const NORMAL = 1;

    /**
     * @var int
     */
    const WEAPON = 2;
}
