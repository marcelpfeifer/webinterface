<?php
/**
 * WebAdminLogging
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 10.04.2020
 * Time: 09:25
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class WebAdminLoggingValue extends Model
{

    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_LOGGING_ID = 'loggingId';

    /**
     * @var string
     */
    const COLUMN_TABLE_NAME = 'tableName';

    /**
     * @var string
     */
    const COLUMN_COLUMN_NAME = 'columnName';

    /**
     * @var string
     */
    const COLUMN_OLD_VALUE = 'oldValue';

    /**
     * @var string
     */
    const COLUMN_NEW_VALUE = 'newValue';

    /**
     * @var string
     */
    protected $table = 'webAdminLoggingValue';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @param int $loggingId
     * @return Dto\WebAdminLoggingValue[]
     */
    public function getLogEntriesByLoggingId(int $loggingId): array
    {
        $result = [];
        $entries = $this->where(self::COLUMN_LOGGING_ID, $loggingId)->get();
        foreach ($entries as $entry) {
            $result[] = self::entryAsDto($entry);
        }
        return $result;
    }

    /**
     * @param Dto\WebAdminLoggingValue $dto
     */
    public function insertEntry(Dto\WebAdminLoggingValue $dto)
    {
        $this->insert(
            [
                self::COLUMN_LOGGING_ID  => $dto->getLoggingId(),
                self::COLUMN_TABLE_NAME  => $dto->getTableName(),
                self::COLUMN_COLUMN_NAME => $dto->getColumnName(),
                self::COLUMN_OLD_VALUE   => $dto->getOldValue(),
                self::COLUMN_NEW_VALUE   => $dto->getNewValue(),
            ]
        );
    }

    /**
     * @param Dto\WebAdminLoggingValue $dto
     */
    public function updateEntry(Dto\WebAdminLoggingValue $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_LOGGING_ID  => $dto->getLoggingId(),
                self::COLUMN_TABLE_NAME  => $dto->getTableName(),
                self::COLUMN_COLUMN_NAME => $dto->getColumnName(),
                self::COLUMN_OLD_VALUE   => $dto->getOldValue(),
                self::COLUMN_NEW_VALUE   => $dto->getNewValue(),
            ]
        );
    }

    /**
     * @param Dto\WebAdminLoggingValue $dto
     */
    public function deleteEntry(Dto\WebAdminLoggingValue $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebAdminLoggingValue $entry
     * @return Dto\WebAdminLoggingValue
     */
    public static function entryAsDto(WebAdminLoggingValue $entry): Dto\WebAdminLoggingValue
    {
        return (new Dto\WebAdminLoggingValue())
            ->setId($entry->{self::COLUMN_ID})
            ->setTableName($entry->{self::COLUMN_TABLE_NAME})
            ->setColumnName($entry->{self::COLUMN_COLUMN_NAME})
            ->setNewValue($entry->{self::COLUMN_NEW_VALUE})
            ->setOldValue($entry->{self::COLUMN_OLD_VALUE});
    }
}
