<?php
/**
 * Prison.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 01.11.2019
 * Time: 13:23
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Prison extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_MIN = 'min';

    /**
     * @var string
     */
    const COLUMN_INVENTORY = 'inventory';

    /**
     * @var string
     */
    const COLUMN_MONEY = 'money';

    /**
     * @var string
     */
    const COLUMN_HANDY = 'handy';

    /**
     * @var string
     */
    const COLUMN_CREATED_AT = 'createdAt';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'prison';

    /**
     * @param Dto\Prison $dto
     */
    public function insertEntry(Dto\Prison $dto)
    {
        $this->insert(
            [
                self::COLUMN_ID         => $dto->getId(),
                self::COLUMN_MIN        => $dto->getMin(),
                self::COLUMN_INVENTORY  => $dto->getInventory(),
                self::COLUMN_MONEY      => $dto->getMoney(),
                self::COLUMN_HANDY      => $dto->getHandy(),
                self::COLUMN_CREATED_AT => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\Prison $dto
     */
    public function updateEntry(Dto\Prison $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_MIN       => $dto->getMin(),
                self::COLUMN_INVENTORY => $dto->getInventory(),
                self::COLUMN_MONEY     => $dto->getMoney(),
                self::COLUMN_HANDY     => $dto->getHandy(),
            ]
        );
    }

    /**
     * @param Dto\Prison $dto
     */
    public function deleteEntry(Dto\Prison $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param Prison $entry
     * @return Dto\Prison
     */
    public static function entryAsDto(Prison $entry): Dto\Prison
    {
        return (new Dto\Prison())
            ->setId($entry->{self::COLUMN_ID})
            ->setMin($entry->{self::COLUMN_MIN})
            ->setInventory($entry->{self::COLUMN_INVENTORY})
            ->setMoney($entry->{self::COLUMN_MONEY})
            ->setHandy($entry->{self::COLUMN_HANDY});
    }
}
