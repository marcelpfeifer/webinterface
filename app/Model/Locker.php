<?php
/**
 * Locker
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 20.09.2020
 * Time: 09:43
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Locker extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_GUID = 'guid';

    /**
     * @var string
     */
    const COLUMN_TYPE = 'type';

    /**
     * @var string
     */
    const COLUMN_FACTION = 'faction';

    /**
     * @var string
     */
    const COLUMN_CREATED_AT = 'createdAt';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $dates = [
        self::COLUMN_CREATED_AT,
    ];

    /**
     * @var string
     */
    protected $table = 'locker';

    /**
     * @param Dto\Locker $dto
     */
    public function insertEntry(Dto\Locker $dto)
    {
        $this->insert(
            [
                self::COLUMN_ID         => $dto->getId(),
                self::COLUMN_GUID       => $dto->getGuid(),
                self::COLUMN_TYPE       => $dto->getType(),
                self::COLUMN_FACTION    => $dto->getFaction(),
                self::COLUMN_CREATED_AT => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\Locker $dto
     */
    public function updateEntry(Dto\Locker $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_GUID    => $dto->getGuid(),
                self::COLUMN_TYPE    => $dto->getType(),
                self::COLUMN_FACTION => $dto->getFaction(),
            ]
        );
    }

    /**
     * @param Dto\Locker $dto
     */
    public function deleteEntry(\App\Model\Dto\Locker $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param int $guid
     * @param int $type
     */
    public function deleteEntryByGuidAndType(int $guid, int $type)
    {
        $this->where(self::COLUMN_GUID, $guid)
            ->where(self::COLUMN_TYPE, $type)
            ->delete();
    }

    /**
     * @param Locker $entry
     * @return Dto\Locker
     */
    public static function entryAsDto(Locker $entry): Dto\Locker
    {
        return (new Dto\Locker())
            ->setId($entry->{self::COLUMN_ID})
            ->setGuid($entry->{self::COLUMN_GUID})
            ->setType($entry->{self::COLUMN_TYPE})
            ->setFaction($entry->{self::COLUMN_FACTION})
            ->setCreatedAt($entry->{self::CREATED_AT});
    }
}
