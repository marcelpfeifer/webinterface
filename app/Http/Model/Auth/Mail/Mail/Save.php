<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 23.11.2020
 * Time: 09:26
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Model\Auth\Mail\Mail;

use App\Model\Character;
use App\Model\Dto\WebMail;
use App\Model\Dto\WebMailAttachment;
use App\Model\WebMailAddress;

class Save
{
    /**
     * @var string
     */
    public const STORAGE_PATH = '/mail/attachment/';

    /**
     * @param \App\Http\Request\Dto\Auth\Mail\Mail\Save $dto
     */
    public static function save(\App\Http\Request\Dto\Auth\Mail\Mail\Save $dto)
    {
        $webMailAddress = WebMailAddress::entryAsDto(WebMailAddress::find($dto->getWebMailAddressId()));
        $saveDto = (new WebMail())
            ->setFrom($dto->getWebMailAddressId())
            ->setSender($webMailAddress->getEmail())
            ->setReceiver($dto->getTo())
            ->setMessage($dto->getMessage())
            ->setTitle($dto->getTitle());
        $webMailId = (new \App\Model\WebMail())->insertEntry($saveDto);

        $attachments = [];
        foreach ($dto->getFiles() as $uploadedFile) {
            $path = self::STORAGE_PATH . $webMailId . DIRECTORY_SEPARATOR;
            $uploadedFile->storePubliclyAs(
                'public' . $path,
                $uploadedFile->getClientOriginalName()
            );
            $attachmentDto = (new WebMailAttachment())
                ->setName($uploadedFile->getClientOriginalName())
                ->setWebMailId($webMailId)
                ->setPath('storage' . $path . $uploadedFile->getClientOriginalName());
            (new \App\Model\WebMailAttachment())->insertEntry($attachmentDto);
            $attachments[] = $attachmentDto;
        }

        $mails = explode(';', $dto->getTo());
        foreach ($mails as $mail) {
            $mailAddress = (new WebMailAddress())->getEntryByMail($mail);
            if ($mailAddress) {
                $saveDto->setTo($mailAddress->getId());
                $webMailId = (new \App\Model\WebMail())->insertEntry($saveDto);
                foreach ($attachments as $attachment) {
                    $attachment->setWebMailId($webMailId);
                    (new \App\Model\WebMailAttachment())->insertEntry($attachment);
                }
            }
        }
    }
}
