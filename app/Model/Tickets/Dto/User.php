<?php
/**
 * User
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 27.08.2020
 * Time: 19:29
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Tickets\Dto;


class User
{

    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var int
     */
    private $orgId = 0;

    /**
     * @var int
     */
    private $emailId = 0;

    /**
     * @var int
     */
    private $status = 0;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $created = '';

    /**
     * @var string
     */
    private $updated = '';

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return User
     */
    public function setId(int $id): User
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrgId(): int
    {
        return $this->orgId;
    }

    /**
     * @param int $orgId
     * @return User
     */
    public function setOrgId(int $orgId): User
    {
        $this->orgId = $orgId;
        return $this;
    }

    /**
     * @return int
     */
    public function getEmailId(): int
    {
        return $this->emailId;
    }

    /**
     * @param int $emailId
     * @return User
     */
    public function setEmailId(int $emailId): User
    {
        $this->emailId = $emailId;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return User
     */
    public function setStatus(int $status): User
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName(string $name): User
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreated(): string
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return User
     */
    public function setCreated(string $created): User
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return string
     */
    public function getUpdated(): string
    {
        return $this->updated;
    }

    /**
     * @param string $updated
     * @return User
     */
    public function setUpdated(string $updated): User
    {
        $this->updated = $updated;
        return $this;
    }
}
