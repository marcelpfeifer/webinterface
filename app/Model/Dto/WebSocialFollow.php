<?php
/**
 * WebSocialFollow
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 14.04.2020
 * Time: 17:51
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

/**
 * Class WebSocialFollow
 * @package App\Model\Dto
 */
class WebSocialFollow
{

    /**
     * @var int
     */
    private $characterId = 0;

    /**
     * @var int
     */
    private $followId = 0;

    /**
     * @return int
     */
    public function getCharacterId(): int
    {
        return $this->characterId;
    }

    /**
     * @param int $characterId
     * @return WebSocialFollow
     */
    public function setCharacterId(int $characterId): WebSocialFollow
    {
        $this->characterId = $characterId;
        return $this;
    }

    /**
     * @return int
     */
    public function getFollowId(): int
    {
        return $this->followId;
    }

    /**
     * @param int $followId
     * @return WebSocialFollow
     */
    public function setFollowId(int $followId): WebSocialFollow
    {
        $this->followId = $followId;
        return $this;
    }
}
