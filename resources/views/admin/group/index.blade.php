<?php
/**
 * index.blade.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 29.08.2019
 * Time: 21:55
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-user-friends"></i>
    Gruppenübersicht
@endsection
@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Typ</th>
            <th scope="col">Geld</th>
            <th scope="col"></th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($groups as $group)
            <tr>
                <th scope="row">{{ $group->id }}</th>
                <td>{{ $group->name }}</td>
                <td>{{ $group->type }}</td>
                <td>{{ $group->money }}</td>
                <td>
                    <a href="{{ route('admin.group.show', $group->id) }}" class="btn btn-primary">
                        Anzeigen
                    </a>
                </td>
                <td>
                    <form action="{{ route('admin.group.delete', $group->id) }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE"/>
                        <button class="btn btn-danger" type="submit">Löschen</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <h2> Erstellen </h2>
    <form action="{{ route('admin.group.create') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name">
        </div>
        <div class="form-group">
            <label for="type">Typ</label>
            <select class="form-control" id="type" name="type">
                <option value="0">
                    Gang
                </option>
                <option value="1">
                    Mafia
                </option>
            </select>
        </div>
        <div class="form-group">
            <label for="money">Geld</label>
            <input type="number" class="form-control" id="money" name="money" value="0">
        </div>
        <button type="submit" class="btn btn-dark">Erstellen</button>
    </form>
@endsection


