<?php
/**
 * AController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.10.2021
 * Time: 17:35
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Api;

use App\Dto\Message;
use App\Http\Request\AValidator;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

abstract class AController extends \App\Http\Controllers\Controller implements IController
{
    /**
     * @param AValidator $validator
     * @param string $viewPath
     * @return View
     */
    public function processIndex(AValidator $validator, string $viewPath): View
    {
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            return view('api.index')
                ->with('messages', $validator->getErrorsAsMessageArray($validatedData));
        }

        $requestDto = $validator->toDto();

        return view(
            $viewPath,
            $this->getIndexData($requestDto)
        );
    }

    /**
     * @param AValidator $validator
     * @param string $redirectPath
     * @return RedirectResponse
     */
    public function processSave(AValidator $validator, string $redirectPath): RedirectResponse
    {
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            return redirect()->route($redirectPath)
                ->with('messages', $validator->getErrorsAsMessageArray($validatedData));
        }

        $requestDto = $validator->toDto();

        $successful = $this->execApiCall($requestDto);
        if ($successful) {
            $messages[] = (new Message())
                ->setMessage('Erfolgreich ausgeführt')
                ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        } else {
            $messages[] = (new Message())
                ->setMessage('Anfrage fehlgeschlagen')
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
        }

        return redirect()
            ->route($redirectPath, $this->getRedirectData($requestDto))
            ->with('messages', $messages);
    }

}
