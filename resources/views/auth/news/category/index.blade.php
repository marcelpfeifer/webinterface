<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.08.2021
 * Time: 13:12
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('layouts.app')
@section('title')
    <i class="fas fa-newspaper"></i>
    News Kategorien
    <a href="{{ route('auth.news.category.create') }}" class="btn btn-secondary float-right">
        Neu
    </a>
@endsection
@section('content')
    <table class="table table-striped mt-2">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Anzeigen</th>
            <th scope="col"></th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
            <tr>
                <td>{{ $category->getId() }}</td>
                <td>{{ $category->getName() }}</td>
                <td>{{ $category->isShow() }}</td>
                <td>
                    <a href="{{ route('auth.news.category.edit', $category->getId()) }}"
                       class="text-primary">
                        Bearbeiten
                    </a>
                </td>
                <td>
                    <form action="{{ route('auth.news.category.delete', $category->getId()) }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE"/>
                        <button class="btn" type="submit"><i class="text-danger fas fa-trash"></i>
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
