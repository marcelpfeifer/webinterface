<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 28.08.2021
 * Time: 13:04
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('web.layout.app')
@section('url')
    https://eternityV.de/news/
@endsection
@section('content')
    <div class="container transparentBg bg-white text-dark min-vh-100">
        @include('web.news.component.nav', [
            'categoryId' => $categoryId,
        ])

        <div class="row">
            <div class="col-12 text-secondary small">
                 <span class="float-left text-primary">
                     {{ (isset($categories[$categoryId]) ? $categories[$categoryId]->getName(): '') }}
                 </span>
                <span class="float-right">
                     {{ $news->getCreatedAt()->format('d.m H:i') }} von {{ $news->getAuthor() }}
                </span>
            </div>
            <div class="col-12">
                <h1 class="text-center"><u> {{ $news->getTitle() }}</u></h1>
            </div>
            <div class="col-12">
                <em>   {!!  $news->getShortDescription() !!}</em>
            </div>
            <div class="col-12 mt-3 text-center">
                <img class="img-fluid" src="{{ $news->getImage() }}">
            </div>
            <div class="col-12 mt-3">
                {!!  $news->getDescription() !!}
            </div>

        </div>
    </div>
@endsection

