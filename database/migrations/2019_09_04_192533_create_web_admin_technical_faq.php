<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebAdminTechnicalFaq extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webAdminTechnicalFaq', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('userId');
            $table->string('explanation');
            $table->string('errorCode');
            $table->text('troubleShooting');
            $table->text('troubleShooting2');
            $table->boolean('fixed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webAdminTechnicalFaq');
    }
}
