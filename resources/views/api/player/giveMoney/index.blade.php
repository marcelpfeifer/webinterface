<?php
/**
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 12.04.2020
 * Time: 12:32
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('api.layout.app')
@section('title')
    <i class="fas fa-money-bill"></i>
    Gibt einem Spieler Geld auf die Hand
@endsection
@section('content')
    <form method="post" action="{{ route('api.player.giveMoney.save') }}">
        {{ csrf_field() }}

        @include('api.includes.select', [
          'name' => 'pid',
          'label' => 'Spieler',
          'entries' => $players,
          'currentSelect' => $currentPlayer,
        ])

        <div class="form-group">
            <label for="amount">Anzahl</label>
            <input type="number" class="form-control" id="amount" name="amount" value="1">
        </div>
        <button type="submit" class="btn btn-danger">Bestätigen</button>
    </form>
    <br/>
@endsection
