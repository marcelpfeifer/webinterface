<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 23.08.2021
 * Time: 14:10
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('layouts.app')
@section('title')
    <i class="fas fa-envelope"></i>
    Email-Adresse bearbeiten
    <a class="btn btn-secondary btn-sm float-right"
       href="{{ route('auth.mailbox.index') }}"
       role="button"
    >
        Zurück
    </a>
@endsection
@section('content')
    <a class="btn mr-2 btn-secondary"
       href="{{ route('auth.mailbox.generatePassword', $webMailAddressId) }}"
       role="button"
    >
        <i class="fas fa-arrow-circle-right"></i> Password neu generieren
    </a>
    <table class="table table-striped mt-2">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($characters as $character)
            <tr>
                <td>{{ $character->getName() }}</td>
                <td>
                    <form action="{{ route('auth.mailbox.deleteAccount', [$webMailAddressId,$character->getId()]) }}"
                          method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE"/>
                        <button class="btn btn-danger" type="submit">Löschen</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
