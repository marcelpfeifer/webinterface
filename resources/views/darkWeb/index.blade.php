<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 26.07.2020
 * Time: 14:18
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('darkWeb.layout.app')
@section('url')
    {{  md5('https://eternityV.de/') }}
@endsection
@section('content')

    <div class="row text-white mt-2">
        <div class="col-6">
            <a href="{{ route('darkWeb.darkBuy.index', $hash) }}" class="darkWebColor">
                {{  md5('DarkBuy')  }}
            </a>
        </div>
        <div class="col-6">
            A bad workman always blames his tools
        </div>
    </div>
    <div class="row text-white mt-2">
        <div class="col-6">
            <a href="{{ route('darkWeb.2048', $hash) }}" class="darkWebColor">
                {{  md5('2048')  }}
            </a>
        </div>
        <div class="col-6">
            A bird in hand is worth two in the bush
        </div>
    </div>
    <div class="row text-white mt-2">
        <div class="col-6">
            <a href="{{ route('darkWeb.weirdImages', $hash) }}" class="darkWebColor">
                {{  md5('Komische Bilder')  }}
            </a>
        </div>
        <div class="col-6">
            Absence makes the heart grow fonder
        </div>
    </div>

    <div class="row text-white mt-2">
        <div class="col-6">
            <a href="{{ route('darkWeb.wallet.index', $hash) }}" class="darkWebColor">
                {{  md5('wallet')  }}
            </a>
        </div>
        <div class="col-6">
            A cat has nine lives
        </div>
    </div>

    <div class="row text-white mt-2">
        <div class="col-6">
            <a href="#" class="darkWebColor">
                {{  md5('blubfgfg65')  }}
            </a>
        </div>
        <div class="col-6">
            A chain is only as strong as its weakest link
        </div>
    </div>

    <div class="row text-white mt-2">
        <div class="col-6">
            <a href="{{ route('darkWeb.video', $hash) }}" class="darkWebColor">
                {{  md5('Video')  }}
            </a>
        </div>
        <div class="col-6">
            A drowning man will clutch at a straw
        </div>
    </div>

    <div class="row text-white mt-2">
        <div class="col-6">
            <a href="#" class="darkWebColor">
                {{  md5('blubfgfg656767565665')  }}
            </a>
        </div>
        <div class="col-6">
            Adversity and loss make a man wise
        </div>
    </div>

    <div class="row text-white mt-2">
        <div class="col-6">
            <a href="#" class="darkWebColor">
                {{  md5('blubfgfg656767565665gffgfggf')  }}
            </a>
        </div>
        <div class="col-6">
            A fool and his money are soon parted
        </div>
    </div>

    <div class="row text-white mt-2">
        <div class="col-6">
            <a href="#" class="darkWebColor">
                {{  md5('blubfgfg656767565665gffgfggf3223')  }}
            </a>
        </div>
        <div class="col-6">
            A journey of thousand miles begins with a single step
        </div>
    </div>

    <div class="row text-white mt-2">
        <div class="col-6">
            <a href="{{ route('darkWeb.loading', $hash) }}" class="darkWebColor">
                {{  md5('loading')  }}
            </a>
        </div>
        <div class="col-6">
            A leopard can’t/ doesn’t change its spots
        </div>
    </div>

@endsection
