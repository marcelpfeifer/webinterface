<?php
/**
 * Store
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 12.10.2021
 * Time: 15:29
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Admin\ShopUnregisteredVehicles;

use App\Http\Request\Dto\ADto;

class Store extends ADto
{

    /**
     * @var string
     */
    private $model = '';

    /**
     * @var int
     */
    private $stock = 0;

    /**
     * @var string
     */
    private $positionX = '';

    /**
     * @var string
     */
    private $positionY = '';

    /**
     * @var string
     */
    private $positionZ = '';

    /**
     * @var string
     */
    private $rotationX = '';

    /**
     * @var string
     */
    private $rotationY = '';

    /**
     * @var string
     */
    private $rotationZ = '';

    /**
     * @var int
     */
    private $mapId = 0;

    /**
     * @var string
     */
    private $npcPositionX = '';

    /**
     * @var string
     */
    private $npcPositionY = '';

    /**
     * @var string
     */
    private $npcPositionZ = '';

    /**
     * @var string
     */
    private $npcRotation = '';

    /**
     * @var string
     */
    private $npcModel = '';

    /**
     * @return string
     */
    public function getModel(): string
    {
        return $this->model;
    }

    /**
     * @param string $model
     * @return Store
     */
    public function setModel(string $model): Store
    {
        $this->model = $model;
        return $this;
    }

    /**
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     * @return Store
     */
    public function setStock(int $stock): Store
    {
        $this->stock = $stock;
        return $this;
    }

    /**
     * @return string
     */
    public function getPositionX(): string
    {
        return $this->positionX;
    }

    /**
     * @param string $positionX
     * @return Store
     */
    public function setPositionX(string $positionX): Store
    {
        $this->positionX = $positionX;
        return $this;
    }

    /**
     * @return string
     */
    public function getPositionY(): string
    {
        return $this->positionY;
    }

    /**
     * @param string $positionY
     * @return Store
     */
    public function setPositionY(string $positionY): Store
    {
        $this->positionY = $positionY;
        return $this;
    }

    /**
     * @return string
     */
    public function getPositionZ(): string
    {
        return $this->positionZ;
    }

    /**
     * @param string $positionZ
     * @return Store
     */
    public function setPositionZ(string $positionZ): Store
    {
        $this->positionZ = $positionZ;
        return $this;
    }

    /**
     * @return string
     */
    public function getRotationX(): string
    {
        return $this->rotationX;
    }

    /**
     * @param string $rotationX
     * @return Store
     */
    public function setRotationX(string $rotationX): Store
    {
        $this->rotationX = $rotationX;
        return $this;
    }

    /**
     * @return string
     */
    public function getRotationY(): string
    {
        return $this->rotationY;
    }

    /**
     * @param string $rotationY
     * @return Store
     */
    public function setRotationY(string $rotationY): Store
    {
        $this->rotationY = $rotationY;
        return $this;
    }

    /**
     * @return string
     */
    public function getRotationZ(): string
    {
        return $this->rotationZ;
    }

    /**
     * @param string $rotationZ
     * @return Store
     */
    public function setRotationZ(string $rotationZ): Store
    {
        $this->rotationZ = $rotationZ;
        return $this;
    }

    /**
     * @return int
     */
    public function getMapId(): int
    {
        return $this->mapId;
    }

    /**
     * @param int $mapId
     * @return Store
     */
    public function setMapId(int $mapId): Store
    {
        $this->mapId = $mapId;
        return $this;
    }

    /**
     * @return string
     */
    public function getNpcPositionX(): string
    {
        return $this->npcPositionX;
    }

    /**
     * @param string $npcPositionX
     * @return Store
     */
    public function setNpcPositionX(string $npcPositionX): Store
    {
        $this->npcPositionX = $npcPositionX;
        return $this;
    }

    /**
     * @return string
     */
    public function getNpcPositionY(): string
    {
        return $this->npcPositionY;
    }

    /**
     * @param string $npcPositionY
     * @return Store
     */
    public function setNpcPositionY(string $npcPositionY): Store
    {
        $this->npcPositionY = $npcPositionY;
        return $this;
    }

    /**
     * @return string
     */
    public function getNpcPositionZ(): string
    {
        return $this->npcPositionZ;
    }

    /**
     * @param string $npcPositionZ
     * @return Store
     */
    public function setNpcPositionZ(string $npcPositionZ): Store
    {
        $this->npcPositionZ = $npcPositionZ;
        return $this;
    }

    /**
     * @return string
     */
    public function getNpcRotation(): string
    {
        return $this->npcRotation;
    }

    /**
     * @param string $npcRotation
     * @return Store
     */
    public function setNpcRotation(string $npcRotation): Store
    {
        $this->npcRotation = $npcRotation;
        return $this;
    }

    /**
     * @return string
     */
    public function getNpcModel(): string
    {
        return $this->npcModel;
    }

    /**
     * @param string $npcModel
     * @return Store
     */
    public function setNpcModel(string $npcModel): Store
    {
        $this->npcModel = $npcModel;
        return $this;
    }
}
