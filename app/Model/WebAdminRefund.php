<?php
/**
 * WebAdminRefund
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 22.11.2019
 * Time: 20:47
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class WebAdminRefund
 * @package App\Model
 */
class WebAdminRefund extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_USER_ID = 'userId';

    /**
     * @var string
     */
    const COLUMN_USER_NAME = 'userName';

    /**
     * @var string
     */
    const COLUMN_ITEM_TYPE = 'itemType';

    /**
     * @var string
     */
    const COLUMN_AMOUNT = 'amount';

    /**
     * @var string
     */
    const COLUMN_DESCRIPTION = 'description';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webAdminRefund';

    /**
     * @return BelongsTo
     */
    public function account(): BelongsTo
    {
        return $this->belongsTo(Account::class, self::COLUMN_USER_ID, Account::COLUMN_ID);
    }

    /**
     * @param Dto\WebAdminRefund $dto
     */
    public function insertEntry(Dto\WebAdminRefund $dto)
    {
        $this->insert([
            self::COLUMN_ID          => $dto->getId(),
            self::COLUMN_USER_ID     => $dto->getUserId(),
            self::COLUMN_USER_NAME   => $dto->getUserName(),
            self::COLUMN_ITEM_TYPE   => $dto->getItemType(),
            self::COLUMN_AMOUNT      => $dto->getAmount(),
            self::COLUMN_DESCRIPTION => $dto->getDescription(),
            self::CREATED_AT         => Carbon::now(),
            self::UPDATED_AT         => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\WebAdminRefund $dto
     */
    public function updateEntry(Dto\WebAdminRefund $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update([
            self::COLUMN_ID          => $dto->getId(),
            self::COLUMN_USER_ID     => $dto->getUserId(),
            self::COLUMN_USER_NAME   => $dto->getUserName(),
            self::COLUMN_ITEM_TYPE   => $dto->getItemType(),
            self::COLUMN_AMOUNT      => $dto->getAmount(),
            self::COLUMN_DESCRIPTION => $dto->getDescription(),
            self::UPDATED_AT         => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\WebAdminRefund $dto
     */
    public function deleteEntry(\App\Model\Dto\WebAdminRefund $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebAdminRefund $entry
     * @return Dto\WebAdminRefund
     */
    public static function entryAsDto(WebAdminRefund $entry): Dto\WebAdminRefund
    {
        return (new Dto\WebAdminRefund())
            ->setId($entry->{self::COLUMN_ID})
            ->setUserId($entry->{self::COLUMN_USER_ID})
            ->setUserName($entry->{self::COLUMN_USER_NAME})
            ->setItemType($entry->{self::COLUMN_ITEM_TYPE})
            ->setAmount($entry->{self::COLUMN_AMOUNT})
            ->setDescription($entry->{self::COLUMN_DESCRIPTION})
            ->setCreatedAt($entry->{self::CREATED_AT})
            ->setUpdatedAt($entry->{self::UPDATED_AT});
    }
}
