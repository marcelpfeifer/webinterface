<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 06.09.2020
 * Time: 11:37
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-ban"></i>
    Begründungsmanagement
    <a class="btn btn-dark btn-sm float-right"
       href="{{ route('admin.ban.reason.create') }}"
       role="button"
    >
        Eintrag erstellen
    </a>
@endsection
@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Punkte</th>
            <th scope="col"></th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($entries as $entry)
            <tr>
                <th scope="row">{{ $entry->getId() }}</th>
                <td>{{ $entry->getName() }}</td>
                <td>{{ $entry->getPoints() }}</td>
                <td>
                    <a href="{{ route('admin.ban.reason.edit', $entry->getId()) }}" class="btn btn-primary">
                        Bearbeiten
                    </a>
                </td>
                <td>
                    <form action="{{ route('admin.ban.reason.delete', $entry->getId()) }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE"/>
                        <button class="btn btn-danger" type="submit">Löschen</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
