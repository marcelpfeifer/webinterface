<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.08.2021
 * Time: 13:13
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('layouts.app')
@section('title')
    <i class="fas fa-newspaper"></i>
    News Kategorien
    <a href="{{ route('auth.news.category.index') }}" class="btn btn-secondary float-right">
        Zurück
    </a>
@endsection
@section('content')
    <form action="{{ route('auth.news.category.save') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" autocomplete="off" required>
        </div>
        <div class="form-group">
            <label for="show">Anzeigen</label>
            <input type="checkbox" class="form-check" id="show" name="show" autocomplete="off">
        </div>
        <button type="submit" class="btn btn-primary">Erstellen</button>
    </form>
@endsection

