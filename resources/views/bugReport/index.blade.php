<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="{{ url('/img/favicon64.ico') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://kit.fontawesome.com/fe55b35558.js" defer></script>
</head>
<style>
    body {
        font-size: 18px;
    }
</style>
<body>
<div class="container">
    <br/>
    <br/>
    <div class="row">
        <div class="col-12">
            <h1 class="text-center">Bug Report</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <span class="font-weight-bold text-danger"><i class="fas fa-exclamation"></i></span>
            Bitte gebe eine möglichst ausführliche Beschreibung des Problems. <br/>
            <span class="font-weight-bold text-danger"><i class="fas fa-exclamation"></i></span>
            Dein Report wird von einem Support überprüft und ggf. Rückfragen an dich stellen, bevor das Problem an die
            DEVs weitergegeben wird. <br/>
            <span class="font-weight-bold text-danger"><i class="fas fa-exclamation"></i></span>
            Dein aktueller Standpunkt sowie dein Charactername wird automatisch mit übergeben.
        </div>
    </div>
    @include('layouts.messages')
    <form action="{{ route('bugReport.save', $hash) }}" method="POST">
        {{ csrf_field() }}
        <div class="row mt-1">
            <div class="form-group col-12">
                <label class="font-weight-bold" for="title">Titel</label>
                <input type="text" name="title" required autocomplete="off" id="title" class="form-control">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-12">
                <label class="font-weight-bold" for="description">Beschreibung</label>
                <textarea rows="3" name="description" required autocomplete="off" id="description"
                          class="form-control"></textarea>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-12">
                <label class="font-weight-bold" for="evidence">
                    Bild/Video URL`s von dem Problem (mehrere mit einem ; trennen)
                </label>
                <input type="text" name="evidence" autocomplete="off" id="evidence" class="form-control">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-12 font-weight-bold">
                <button type="submit" class="btn btn-primary">Abschicken</button>
            </div>
        </div>
    </form>
</div>
</body>
</html>
