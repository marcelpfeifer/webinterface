<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateWebAdminWhitelistTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Illuminate\Support\Facades\DB::statement(
            "ALTER TABLE webAdminWhitelist MODIFY status ENUM('APPROVED','DENIED','RETRY') NOT NULL"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Illuminate\Support\Facades\DB::statement(
            "ALTER TABLE webAdminWhitelist MODIFY status ENUM('APPROVED','DENIED') NOT NULL"
        );
    }
}
