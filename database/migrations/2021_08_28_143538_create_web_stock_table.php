<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'webStock',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name');
                $table->float('price');
                $table->integer('stock')->comment('-1 for unlimited');
                $table->float('minChange');
                $table->float('maxChange');
                $table->float('lastPrice');
                $table->float('percentageChange');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webStock');
    }
}
