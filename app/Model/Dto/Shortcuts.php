<?php
/**
 * Shortcuts
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 11.05.2020
 * Time: 21:14
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

class Shortcuts
{

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var int
     */
    private $guid = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Shortcuts
     */
    public function setId(?int $id): Shortcuts
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getGuid(): int
    {
        return $this->guid;
    }

    /**
     * @param int $guid
     * @return Shortcuts
     */
    public function setGuid(int $guid): Shortcuts
    {
        $this->guid = $guid;
        return $this;
    }
}
