<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebPermissionToGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'webPermissionToWebPermission',
            function (Blueprint $table) {
                $table->unsignedBigInteger('webPermissionId');
                $table->unsignedBigInteger('webPermissionGroupId');
                $table->foreign('webPermissionId')->references('id')->on('webPermission');
                $table->foreign('webPermissionGroupId')->references('id')->on('webPermissionGroup');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webPermissionToWebPermission');
    }
}
