<?php
/**
 * WebYellowPage
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 12.12.2020
 * Time: 22:26
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class WebYellowPage extends Model
{
    /**
     * @var string
     */
    public const COLUMN_ID = 'id';

    /**
     * @var string
     */
    public const COLUMN_CHARACTER_ID = 'characterId';

    /**
     * @var string
     */
    public const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    public const COLUMN_MESSAGE = 'message';

    /**
     * @var string
     */
    protected $table = 'webYellowPage';

    /**
     * @param int[] $onlinePlayers
     */
    public function getOnlineEntries(array $onlinePlayers = [])
    {
        $entries = $this->whereIn(self::COLUMN_CHARACTER_ID, $onlinePlayers)
            ->orderBy(self::CREATED_AT, 'DESC')
            ->get();

        $results = [];
        foreach ($entries as $entry) {
            $results[] = self::entryAsDto($entry);
        }

        return $results;
    }

    /**
     * @param Dto\WebYellowPage $dto
     */
    public function insertEntry(Dto\WebYellowPage $dto)
    {
        $this->insert(
            [
                self::COLUMN_ID           => $dto->getId(),
                self::COLUMN_CHARACTER_ID => $dto->getCharacterId(),
                self::COLUMN_NAME         => $dto->getName(),
                self::COLUMN_MESSAGE      => $dto->getMessage(),
                self::CREATED_AT          => Carbon::now(),
                self::UPDATED_AT          => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebYellowPage $dto
     */
    public function updateEntry(Dto\WebYellowPage $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_CHARACTER_ID => $dto->getCharacterId(),
                self::COLUMN_NAME         => $dto->getName(),
                self::COLUMN_MESSAGE      => $dto->getMessage(),
                self::UPDATED_AT          => Carbon::now(),
            ]
        );
    }

    /**
     * @param int $id
     */
    public function deleteEntryByCharacterId(int $id)
    {
        $this->where(self::COLUMN_CHARACTER_ID, $id)->delete();
    }

    /**
     * @param Dto\WebYellowPage $dto
     */
    public function deleteEntry(\App\Model\Dto\WebYellowPage $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebYellowPage $entry
     * @return Dto\WebYellowPage
     */
    public static function entryAsDto(WebYellowPage $entry): Dto\WebYellowPage
    {
        return (new Dto\WebYellowPage())
            ->setId($entry->{self::COLUMN_ID})
            ->setCharacterId($entry->{self::COLUMN_CHARACTER_ID})
            ->setName($entry->{self::COLUMN_NAME})
            ->setMessage($entry->{self::COLUMN_MESSAGE})
            ->setCreateAt($entry->{self::CREATED_AT})
            ->setUpdatedAt($entry->{self::UPDATED_AT});
    }
}
