<?php
/**
 * Buy
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 02.09.2021
 * Time: 17:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Model\Web\Stock;


use App\Dto\Message;
use App\Model\Character;
use App\Model\Dto\Cashflow;
use App\Model\Dto\WebStockCharacter;
use App\Model\Enum\Cashflow\Type;
use App\Model\WebStock;
use Carbon\Carbon;

class Buy
{
    /**
     * @param \App\Http\Request\Dto\Web\Stock\Buy $requestDto
     * @return Message[]
     */
    public static function save(\App\Http\Request\Dto\Web\Stock\Buy $requestDto): array
    {
        $messages = [];

        $priceTotal = $requestDto->getWebStock()->getPrice() * $requestDto->getAmount();

        // Ziehe das Geld den Character ab
        $characterDto = $requestDto->getCharacter()->setBank(
            $requestDto->getCharacter()->getBank() - $priceTotal
        );
        (new Character())->updateEntry($characterDto);

        // Aktualisiere den Stock
        (new WebStock())->subStock($requestDto->getWebStock()->getId(), $requestDto->getAmount());

        // Füge den Stock hinzu
        $dto = (new WebStockCharacter())
            ->setCharacterId($requestDto->getCharacter()->getId())
            ->setWebStockId($requestDto->getWebStock()->getId())
            ->setAmount($requestDto->getAmount())
            ->setTotalPrice($priceTotal)
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());
        (new \App\Model\WebStockCharacter())->insertEntry($dto);

        $cashFlow = (new Cashflow())
            ->setType(Type::BANK)
            ->setAmount(-$priceTotal)
            ->setGuid($characterDto->getId())
            ->setReason('stockBuy')
            ->setSource('-1');
        (new \App\Model\Cashflow())->insertEntry($cashFlow);

        $messages[] = (new Message())
            ->setMessage("Aktien wurden erfolgreich gekauft.")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return $messages;
    }
}
