<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.10.2021
 * Time: 15:35
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Api\Player\Tp;


use App\Http\Request\Dto\ADto;

class Save extends ADto
{
    /**
     * @var int
     */
    private $playerFrom = 0;

    /**
     * @var int
     */
    private $playerTo = 0;

    /**
     * @return int
     */
    public function getPlayerFrom(): int
    {
        return $this->playerFrom;
    }

    /**
     * @param int $playerFrom
     * @return Save
     */
    public function setPlayerFrom(int $playerFrom): Save
    {
        $this->playerFrom = $playerFrom;
        return $this;
    }

    /**
     * @return int
     */
    public function getPlayerTo(): int
    {
        return $this->playerTo;
    }

    /**
     * @param int $playerTo
     * @return Save
     */
    public function setPlayerTo(int $playerTo): Save
    {
        $this->playerTo = $playerTo;
        return $this;
    }
}
