<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.10.2021
 * Time: 15:35
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Api\Player\AddItem;


use App\Http\Request\Dto\ADto;

class Save extends ADto
{

    /**
     * @var int
     */
    private $playerId = 0;

    /**
     * @var string
     */
    private $item = '';

    /**
     * @var int
     */
    private $amount = 0;

    /**
     * @return int
     */
    public function getPlayerId(): int
    {
        return $this->playerId;
    }

    /**
     * @param int $playerId
     * @return Save
     */
    public function setPlayerId(int $playerId): Save
    {
        $this->playerId = $playerId;
        return $this;
    }

    /**
     * @return string
     */
    public function getItem(): string
    {
        return $this->item;
    }

    /**
     * @param string $item
     * @return Save
     */
    public function setItem(string $item): Save
    {
        $this->item = $item;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     * @return Save
     */
    public function setAmount(int $amount): Save
    {
        $this->amount = $amount;
        return $this;
    }
}
