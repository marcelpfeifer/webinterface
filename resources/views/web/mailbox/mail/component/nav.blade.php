<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 18.08.2021
 * Time: 10:11
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
<div class="row bg-dark p-2">
    <div class="col text-white">
        <i class="fas fa-envelope"></i>
        @if($email)
            Postfach: {{ $email }}
        @endif
    </div>
    <div class="col justify-content-end row">
        <div class="col-3">
            <a class="btn btn-secondary btn-sm float-right"
               href="{{ $link }}"
               role="button"
            >
                Zurück
            </a>
        </div>
    </div>
</div>
