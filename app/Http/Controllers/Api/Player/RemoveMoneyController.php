<?php
/**
 * RemoveMoneyController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 12.04.2020
 * Time: 12:37
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Api\Player;

use App\Dto\Message;
use App\Http\Controllers\Api\AController;
use App\Http\Controllers\Api\IController;
use App\Http\Controllers\Controller;
use App\Http\Request\Api\Player\RemoveMoney\Index;
use App\Http\Request\Api\Player\RemoveMoney\Save;
use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Libraries\Api\Player\GiveMoney;
use App\Libraries\Api\Player\Online;
use App\Libraries\Api\Player\RemoveMoney;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class RemoveMoneyController extends AController implements IController
{
    /**
     * @param Request $request
     * @return View
     */
    public function index(Request $request): View
    {
        $validator = new Index($request);
        return $this->processIndex($validator, 'api.player.removeMoney.index');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function save(Request $request): RedirectResponse
    {
        $validator = new Save($request);
        return $this->processSave($validator, 'api.player.removeMoney.index');
    }

    /**
     * @param \App\Http\Request\Dto\Api\Player\RemoveMoney\Index $requestDto
     * @return array
     */
    public function getIndexData(ADto $requestDto): array
    {
        return [
            'players'       => Online::getPlayers(),
            'currentPlayer' => $requestDto->getCurrentPlayer(),
        ];
    }

    /**
     * @param \App\Http\Request\Dto\Api\Player\RemoveMoney\Save $requestDto
     * @return bool
     */
    public function execApiCall(ADto $requestDto): bool
    {
        return RemoveMoney::call($requestDto->getPlayerId(), $requestDto->getAmount());
    }

    /**
     * @param \App\Http\Request\Dto\Api\Player\RemoveMoney\Save $requestDto
     * @return array
     */
    public function getRedirectData(ADto $requestDto): array
    {
        return [
            'currentPlayer' => $requestDto->getPlayerId()
        ];
    }
}
