<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('admin.layout.head')
<body class="bg-info">
    @include('admin.layout.header')
    <main class="py-4">
        @include('admin.layout.wrapper')
    </main>
</body>
</html>
