<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 09.08.2020
 * Time: 19:24
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('darkWeb.layout.app')
@section('url')
    {{  md5('https://eternityV.de/darkBuy/bought') }}
@endsection
@section('content')
    <div class="container bg-dark darkWebColor">
        @include('darkWeb.darkBuy.includes.nav')
        <h2> Gekaufte Angebote </h2>
        @include('darkWeb.darkBuy.includes.offers')
    </div>
@endsection
