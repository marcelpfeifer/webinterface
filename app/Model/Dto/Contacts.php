<?php
/**
 * Contacts
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 01.11.2019
 * Time: 14:03
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

use Carbon\Carbon;

class Contacts
{

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var int
     */
    private $guid = 0;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $number = '';

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Contacts
     */
    public function setId(?int $id): Contacts
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getGuid(): int
    {
        return $this->guid;
    }

    /**
     * @param int $guid
     * @return Contacts
     */
    public function setGuid(int $guid): Contacts
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Contacts
     */
    public function setName(string $name): Contacts
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return Contacts
     */
    public function setNumber(string $number): Contacts
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return Contacts
     */
    public function setCreatedAt(?Carbon $createdAt): Contacts
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}
