<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.01.2021
 * Time: 19:09
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PLAYER_MONEY)
<h4>Geld bearbeiten</h4>
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="cash">Cash</label>
        <input type="number" class="form-control" id="cash" name="cash"
               value="{{ $character->getCash() }}">
    </div>
    <div class="form-group col-md-6">
        <label for="bank">Bank</label>
        <input type="number" class="form-control" id="bank" name="bank"
               value="{{ $character->getBank() }}">
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="bitcoin">Bitcoin</label>
        <input type="number" class="form-control" id="bitcoin" name="bitcoin"
               value="{{ $character->getBitcoin() }}">
    </div>
    <div class="form-group col-md-6">
        <label for="bitcoinIllegal">Bitcoin Illegal</label>
        <input type="number" class="form-control" id="bitcoinIllegal" name="bitcoinIllegal"
               value="{{ $character->getBitcoinIllegal() }}">
    </div>
</div>
<br/>
@endpermission
