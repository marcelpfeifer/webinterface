<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebAdminWhitelistAnswerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'webAdminWhitelistAnswer',
            function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('webAdminWhitelistId');
                $table->foreign('webAdminWhitelistId')->references('id')->on('webAdminWhitelist')->onDelete('cascade');
                $table->unsignedBigInteger('webAdminWhitelistQuestionId');
                $table->foreign('webAdminWhitelistQuestionId')->references('id')->on('webAdminWhitelistQuestion')->onDelete('cascade');
                $table->enum('accuracy', ['RIGHT', 'UNSURE', 'WRONG']);
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webAdminWhitelistAnswer');
    }
}
