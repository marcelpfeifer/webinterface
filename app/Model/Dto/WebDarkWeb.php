<?php
/**
 * WebDarkWeb
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 07.08.2020
 * Time: 20:40
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;


class WebDarkWeb
{
    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var int
     */
    private $characterId = 0;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebDarkWeb
     */
    public function setId(?int $id): WebDarkWeb
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCharacterId(): int
    {
        return $this->characterId;
    }

    /**
     * @param int $characterId
     * @return WebDarkWeb
     */
    public function setCharacterId(int $characterId): WebDarkWeb
    {
        $this->characterId = $characterId;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return WebDarkWeb
     */
    public function setName(string $name): WebDarkWeb
    {
        $this->name = $name;
        return $this;
    }
}
