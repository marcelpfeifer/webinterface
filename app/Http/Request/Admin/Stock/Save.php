<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 29.08.2021
 * Time: 12:41
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Admin\Stock;

use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class Save extends AValidator implements IValidator
{
    /**
     * @return string[]
     */
    public function getRules(): array
    {
        return [
            'name'      => [
                'required',
                'string',
            ],
            'price'     => [
                'required',
                'numeric',
            ],
            'stock'     => [
                'required',
                'integer',
            ],
            'minChange' => [
                'required',
                'numeric',
            ],
            'maxChange' => [
                'required',
                'numeric',
            ],
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'name.required'      => 'Der Name muss übergeben werden!',
            'name.string'        => 'Der Name darf nur aus Zeichen bestehen!',
            'price.required'     => 'Der Preis muss übergeben werden!',
            'price.numeric'      => 'Der Preis darf muss aus Zahlen bestehen!',
            'stock.required'     => 'Die Anzahl muss übergeben werden!',
            'stock.integer'      => 'Die Anzahl darf muss aus Zahlen bestehen!',
            'minChange.required' => 'Die mindest Veränderung muss übergeben werden!',
            'minChange.numeric'  => 'Die mindest Veränderung muss aus Zahlen bestehen!',
            'maxChange.required' => 'Die maximale Veränderung muss übergeben werden!',
            'maxChange.numeric'  => 'Die maximale Veränderung darf muss aus Zahlen bestehen!',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Admin\Stock\Save())
            ->setId($this->id)
            ->setName($this->request->input('name'))
            ->setPrice($this->request->input('price'))
            ->setStock($this->request->input('stock'))
            ->setMinChange($this->request->input('minChange'))
            ->setMaxChange($this->request->input('maxChange'));
    }
}
