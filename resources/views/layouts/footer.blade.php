<?php
/**
 * footer.blade.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 31.08.2019
 * Time: 20:24
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>

<footer class="container">
    <div class="card bg-dark text-white">
        <div class="card-body">
            <span>Copyright © EternityV {{ \Carbon\Carbon::now()->year }}</span>
            <div class="float-right" >
                <a href="https://eternityv.de/legal-notice/" target="_blank" class="text-white">Impressum & Disclaimer</a>
                · <a href="{{ route('dataProtection') }}" class="text-white">Datenschutzerklärung</a>
            </div>
        </div>
    </div>
</footer>
