<?php
/**
 * CloseApp
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 11.09.2021
 * Time: 14:39
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Api\Player;


use App\Libraries\Api\AClient;
use App\Model\WebConfig;

class CloseApp extends AClient
{
    /**
     * @var string
     */
    const URL = 'player/closeApp';

    /**
     * @param int $pid
     * @return bool
     */
    public static function call(int $pid): bool
    {
        $response = self::sendRequest(
            'POST',
            WebConfig::getEntryByKey('api_url')->getValue() . self::URL,
            [
                'query' => [
                    'pid' => $pid,
                ]
            ],
            false
        );
        return $response && $response->getStatusCode() == 200;
    }
}
