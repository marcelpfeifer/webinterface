<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.10.2021
 * Time: 15:35
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Api\Player\Dimension;


use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class Save extends AValidator implements IValidator
{
    /**
     * @return string[]
     */
    public function getRules(): array
    {
        return [
            'pid'       => [
                'required',
                'int',
                'exists:character,id'
            ],
            'dimension' => [
                'required',
                'int',
            ],
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'pid.required'       => 'Die Spieler ID muss übergeben werden!',
            'pid.int'            => 'Die Spieler ID muss ein Integer sein!',
            'pid.exists'         => 'Die Spieler ID existiert nicht!',
            'dimension.required' => 'Die Dimension muss übergeben werden!',
            'dimension.int'      => 'Die Dimension muss ein Integer sein!',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Api\Player\Dimension\Save())
            ->setPlayerId($this->request->input('pid'))
            ->setDimension($this->request->input('dimension'));
    }
}
