<?php
/**
 * Player.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 23.08.2019
 * Time: 16:32
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Model\Admin;

use App\Http\Request\Dto\Admin\Player\Index\Save;
use App\Libraries\Api\Player\SetFaction;
use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Libraries\Permission\Permission;
use App\Model\Account;
use App\Model\Character;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Player
{
    /**
     * @param Save $requestDto
     * @param int $id
     * @return bool
     */
    public static function save(Save $requestDto, int $id): bool
    {
        $account = Account::find($id);
        if (!($account)) {
            return false;
        }

        $accountDto = Account::entryAsDto($account);
        $oldAccountDto = clone $accountDto;
        $accountDto = self::diffAccount($requestDto, $accountDto);

        $character = Character::find($id);
        $characterDto = Character::entryAsDto($character);
        $oldCharacterDto = clone $characterDto;
        $characterDto = self::diffCharacter($requestDto, $characterDto);

        // Log Changes
        $loggingId = Logging::update(Name::CHARACTER, Auth::id(), $oldAccountDto, $accountDto);
        Logging::update(Name::CHARACTER, Auth::id(), $oldCharacterDto, $characterDto, $loggingId);

        // Schreibe Datenbank Einträge
        (new Account())->updateEntry($accountDto);
        (new Character())->updateEntry($characterDto);

        return true;
    }

    /**
     * @param Save $requestDto
     * @param \App\Model\Dto\Account $dto
     * @return  \App\Model\Dto\Account
     */
    private static function diffAccount(Save $requestDto, \App\Model\Dto\Account $dto): \App\Model\Dto\Account
    {
        if (Permission::allowed(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_WHITELIST)) {
            if ($dto->isWhitelisted() != $requestDto->isWhitelist()) {
                $dto->setWhitelisted($requestDto->isWhitelist());
            }
        }

        if (Permission::allowed(
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PLAYER_PASSWORD
        )) {
            if ($requestDto->getPassword()) {
                $dto->setPassword(\hash('sha256', $requestDto->getPassword()));
            }
        }

        if (Permission::allowed(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PLAYER_IMAGE)) {
            // Lädt das Bild hoch falls es gesetz wurde
            $dto = Account\ImageName::upload($requestDto->getImage(), $dto);
        }

        if (Permission::allowed(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PERMISSION)) {
            $dto->setGroupId($requestDto->getGroupId());
        }

        return $dto;
    }

    /**
     * @param Save $requestDto
     * @param \App\Model\Dto\Character $dto
     * @return  \App\Model\Dto\Character
     */
    private static function diffCharacter(Save $requestDto, \App\Model\Dto\Character $dto): \App\Model\Dto\Character
    {
        if (Permission::allowed(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PLAYER_MONEY)) {
            $dto->setCash($requestDto->getCash())
                ->setBank($requestDto->getBank())
                ->setBitcoin($requestDto->getBitcoin())
                ->setBitcoinIllegal($requestDto->getBitcoinIllegal());
        }

        if (Permission::allowed(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PLAYER_NAME)) {
            if ($requestDto->getName()) {
                $dto->setName($requestDto->getName());
            }
        }

        if (Permission::allowed(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_SOCIAL_POINTS)) {
            $dto->setSocialPoints($requestDto->getSocialPoints());
        }

        if (Permission::allowed(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_FACTION)) {
            if ($dto->getMedRank() != $requestDto->getMedicRank()) {
                SetFaction::call($dto->getId(), \App\Http\Request\Enum\Api\Player\Faction\Faction::MED);
                $dto->setMedRank($requestDto->getMedicRank());
            }

            if ($dto->getFibRank() != $requestDto->getFibRank()) {
                SetFaction::call($dto->getId(), \App\Http\Request\Enum\Api\Player\Faction\Faction::FIB);
                $dto->setFibRank($requestDto->getFibRank());
            }

            if ($dto->getCopRank() != $requestDto->getCopRank()) {
                SetFaction::call($dto->getId(), \App\Http\Request\Enum\Api\Player\Faction\Faction::COP);
                $dto->setCopRank($requestDto->getCopRank());
            }

            if ($dto->getDojRank() != $requestDto->getDojRank()) {
                SetFaction::call($dto->getId(), \App\Http\Request\Enum\Api\Player\Faction\Faction::DOJ);
                $dto->setDojRank($requestDto->getDojRank());
            }

            if ($dto->getLawyerRank() != $requestDto->getLawyerRank()) {
                SetFaction::call($dto->getId(), \App\Http\Request\Enum\Api\Player\Faction\Faction::LAWYER);
                $dto->setLawyerRank($requestDto->getLawyerRank());
            }


            $dto->setTaxiRank($requestDto->getTaxiRank())
                ->setBwfiRank($requestDto->getBwfiRank())
                ->setIsDuty($requestDto->isOnDuty());
        }

        if (Permission::allowed(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_GROUP)) {
            $dto->setGroup($requestDto->getGroup());
            if ($requestDto->getGroupRank() == 0) {
                $dto->setGroup(0);
            }
            $dto->setGroupRank($requestDto->getGroupRank());
        }

        if (Permission::allowed(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_BUSINESS)) {
            $dto->setBusinessId($requestDto->getBusinessId());

            $dto->setBusinessRank($requestDto->getBusinessRank());
            if ($requestDto->getBusinessRank() == 0) {
                $dto->setBusinessId(0);
            }
        }

        $dto->setDrivingLicenseA($requestDto->isDrivingLicenseA())
            ->setDrivingLicenseB($requestDto->isDrivingLicenseB())
            ->setDrivingLicenseCE($requestDto->isDrivingLicenseCE())
            ->setGunLicense($requestDto->isGunLicense());
        return $dto;
    }
}
