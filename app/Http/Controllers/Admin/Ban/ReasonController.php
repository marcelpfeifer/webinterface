<?php
/**
 * ReasonController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 06.09.2020
 * Time: 11:16
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\Ban;


use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Http\Request\Admin\Ban\Reason\Save;
use App\Http\Request\AValidator;
use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Model\WebAdminBanReason;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class ReasonController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        $entries = [];
        foreach (WebAdminBanReason::all() as $entry) {
            $entries[] = WebAdminBanReason::entryAsDto($entry);
        }
        return \view(
            'admin.ban.reason.index',
            [
                'entries' => $entries,
            ]
        );
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return \view('admin.ban.reason.create', []);
    }

    /**
     * @return View
     */
    public function edit(Request $request, int $id): View
    {
        $dto = null;
        $entry = WebAdminBanReason::find($id);
        if ($entry) {
            $dto = WebAdminBanReason::entryAsDto($entry);
        }
        return \view(
            'admin.ban.reason.edit',
            [
                'dto' => $dto,
            ]
        );
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function save(Request $request): RedirectResponse
    {
        $validator = (new Save($request));
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            return redirect()->route('admin.ban.reason.create')
                ->with('messages', $validator->getErrorsAsMessageArray($validatedData))
                ->withInput()
                ->withErrors($validatedData->errors());
        }

        /**
         * @var $requestDto \App\Http\Request\Dto\Admin\Ban\Reason\Save
         */
        $requestDto = $validator->toDto();

        $dto = (new \App\Model\Dto\WebAdminBanReason())
            ->setName($requestDto->getName())
            ->setPoints($requestDto->getPoints())
            ->setDescription($requestDto->getDescription());

        Logging::insert(Name::BANS, Auth::id(), $dto);
        (new WebAdminBanReason())->insertEntry($dto);

        $messages[] = (new Message())
            ->setMessage("Der Eintrag wurde angelegt")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.ban.reason.index')->with('messages', $messages);
    }

    /**
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $entry = WebAdminBanReason::find($id);
        if (!$entry) {
            $messages[] = (new Message())
                ->setMessage("Der Eintrag wurde nicht gefunden")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
            return redirect()->route('admin.ban.reason.index')->with('messages', $messages);
        }

        $oldDto = WebAdminBanReason::entryAsDto($entry);
        $dto = (clone $oldDto)->setName($request->input('name'))
            ->setPoints($request->input('points'))
            ->setDescription($request->input('description'));

        Logging::update(Name::BANS, Auth::id(), $oldDto, $dto);
        (new WebAdminBanReason())->updateEntry($dto);

        $messages[] = (new Message())
            ->setMessage("Der Eintrag wurde aktualisiert")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.ban.reason.index')->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function delete(Request $request, int $id): RedirectResponse
    {
        $entry = WebAdminBanReason::find($id);
        if (!($entry)) {
            $messages[] = (new Message())
                ->setMessage("Der Eintrag wurde nicht gefunden")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
            return redirect()->route('admin.ban.reason.index');
        }

        $dto = WebAdminBanReason::entryAsDto($entry);
        (new WebAdminBanReason())->deleteEntry($dto);
        Logging::delete(Name::BANS, Auth::id(), $dto);

        $messages[] = (new Message())
            ->setMessage("Der Eintrag wurde gelöscht")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.ban.reason.index')->with('messages', $messages);
    }
}
