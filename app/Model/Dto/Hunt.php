<?php
/**
 * Hunt
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 16.09.2021
 * Time: 16:25
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;


class Hunt
{

    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var int
     */
    private $guid = 0;

    /**
     * @var string
     */
    private $animal = '';

    /**
     * @var int
     */
    private $weight = 0;

    /**
     * @var int
     */
    private $distance = 0;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Hunt
     */
    public function setId(int $id): Hunt
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getGuid(): int
    {
        return $this->guid;
    }

    /**
     * @param int $guid
     * @return Hunt
     */
    public function setGuid(int $guid): Hunt
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return string
     */
    public function getAnimal(): string
    {
        return $this->animal;
    }

    /**
     * @param string $animal
     * @return Hunt
     */
    public function setAnimal(string $animal): Hunt
    {
        $this->animal = $animal;
        return $this;
    }

    /**
     * @return int
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     * @return Hunt
     */
    public function setWeight(int $weight): Hunt
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @return int
     */
    public function getDistance(): int
    {
        return $this->distance;
    }

    /**
     * @param int $distance
     * @return Hunt
     */
    public function setDistance(int $distance): Hunt
    {
        $this->distance = $distance;
        return $this;
    }
}
