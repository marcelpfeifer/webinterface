<?php
/**
 * Length
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 01.10.2021
 * Time: 13:33
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Common\Enum\Db;


use App\Libraries\Enum\AEnum;

class Length extends AEnum
{

    /**
     * @var int
     */
    const VARCHAR = 255;

    /**
     * @var int
     */
    const TEXT = 65535;

    /**
     * @var int
     */
    const INTEGER = 2147483647;
}
