<?php
/**
 * WithCharacter
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 18.09.2020
 * Time: 18:37
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto\ReportValues;

use App\Model\Dto\Character;
use App\Model\Dto\ReportValues;

class WithCharacter
{

    /**
     * @var ReportValues
     */
    private $reportValue;

    /**
     * @var Character|null
     */
    private $character = null;

    /**
     * @return ReportValues
     */
    public function getReportValue(): ReportValues
    {
        return $this->reportValue;
    }

    /**
     * @param ReportValues $reportValue
     * @return WithCharacter
     */
    public function setReportValue(ReportValues $reportValue): WithCharacter
    {
        $this->reportValue = $reportValue;
        return $this;
    }

    /**
     * @return Character|null
     */
    public function getCharacter(): ?Character
    {
        return $this->character;
    }

    /**
     * @param Character|null $character
     * @return WithCharacter
     */
    public function setCharacter(?Character $character): WithCharacter
    {
        $this->character = $character;
        return $this;
    }
}
