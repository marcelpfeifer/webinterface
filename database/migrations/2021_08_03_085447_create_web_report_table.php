<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'webBugReport',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedInteger('characterId')->index();
                $table->string('title');
                $table->string('description');
                $table->string('evidence');
                $table->string('position');
                $table->boolean('approved');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webBugReport');
    }
}
