<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 10.10.2021
 * Time: 12:17
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Admin\ServerLog;

use App\Http\Request\Dto\ADto;

class Index extends ADto
{

    /**
     * @var array
     */
    private $filter = [];

    /**
     * @return array
     */
    public function getFilter(): array
    {
        return $this->filter;
    }

    /**
     * @param array $filter
     * @return Index
     */
    public function setFilter(array $filter): Index
    {
        $this->filter = $filter;
        return $this;
    }
}
