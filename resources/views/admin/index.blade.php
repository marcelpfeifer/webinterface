<?php
/**
 * index.blade.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 31.08.2019
 * Time: 20:30
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('admin.layout.app')
@section('title')
    Admin Dashboard
@endsection
@section('content')
    Willkommen im Admin Dashboard

    <div id="content"
         data-labels="{{ $labels }}"
         data-ram-max="{{ $ramMax }}"
         data-ram="{{ $ram }}"
         data-cpu="{{ $cpu }}"
         data-altv="{{ $altv }}"
         data-altv-test="{{ $altvTest }}"
         data-ts="{{ $ts }}"
         data-forum="{{ $forum }}">
        <div>
            <h2> Server Status</h2>
            <canvas id="server"></canvas>
        </div>
        <div>
            <h2> RAM Usage </h2>
            <canvas id="ram"></canvas>
        </div>

        <div>
            <h2> CPU Usage </h2>
            <canvas id="cpu"></canvas>
        </div>
    </div>
@endsection
