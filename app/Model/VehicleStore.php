<?php
/**
 * VehicleStore
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 29.04.2020
 * Time: 17:03
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class VehicleStore extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_STOCK = 'stock';

    /**
     * @var string
     */
    const COLUMN_POS_X = 'posx';

    /**
     * @var string
     */
    const COLUMN_POS_Y = 'posy';

    /**
     * @var string
     */
    const COLUMN_POS_Z = 'posz';

    /**
     * @var string
     */
    const COLUMN_FACTION = 'faction';

    /**
     * @var string
     */
    const COLUMN_ROT_X = 'rotx';

    /**
     * @var string
     */
    const COLUMN_ROT_Y = 'roty';

    /**
     * @var string
     */
    const COLUMN_ROT_Z = 'rotz';

    /**
     * @var string
     */
    const COLUMN_MODEL = 'model';

    /**
     * @var string
     */
    const COLUMN_CREATED_AT = 'createdAt';

    /**
     * @var string
     */
    const COLUMN_UPDATED_AT = 'updatedAt';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $dates = [
        self::COLUMN_CREATED_AT,
        self::COLUMN_UPDATED_AT,
    ];

    /**
     * @var string
     */
    protected $table = 'vehicle_store';


    /**
     * @param Dto\VehicleStore $dto
     */
    public function insertEntry(Dto\VehicleStore $dto)
    {
        $this->insert([
            self::COLUMN_ID         => $dto->getId(),
            self::COLUMN_STOCK      => $dto->getStock(),
            self::COLUMN_POS_X      => $dto->getPosX(),
            self::COLUMN_POS_Y      => $dto->getPosY(),
            self::COLUMN_POS_Z      => $dto->getPosZ(),
            self::COLUMN_FACTION    => $dto->getFaction(),
            self::COLUMN_ROT_X      => $dto->getRotX(),
            self::COLUMN_ROT_Y      => $dto->getRotY(),
            self::COLUMN_ROT_Z      => $dto->getRotZ(),
            self::COLUMN_MODEL      => $dto->getModel(),
            self::COLUMN_CREATED_AT => Carbon::now(),
            self::COLUMN_UPDATED_AT => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\VehicleStore $dto
     */
    public function updateEntry(Dto\VehicleStore $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update([
            self::COLUMN_ID         => $dto->getId(),
            self::COLUMN_STOCK      => $dto->getStock(),
            self::COLUMN_POS_X      => $dto->getPosX(),
            self::COLUMN_POS_Y      => $dto->getPosY(),
            self::COLUMN_POS_Z      => $dto->getPosZ(),
            self::COLUMN_FACTION    => $dto->getFaction(),
            self::COLUMN_ROT_X      => $dto->getRotX(),
            self::COLUMN_ROT_Y      => $dto->getRotY(),
            self::COLUMN_ROT_Z      => $dto->getRotZ(),
            self::COLUMN_MODEL      => $dto->getModel(),
            self::COLUMN_UPDATED_AT => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\VehicleStore $dto
     */
    public function deleteEntry(\App\Model\Dto\VehicleStore $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param VehicleStore $entry
     * @return Dto\VehicleStore
     */
    public static function entryAsDto(VehicleStore $entry): Dto\VehicleStore
    {
        return (new Dto\VehicleStore())
            ->setId($entry->{self::COLUMN_ID})
            ->setStock($entry->{self::COLUMN_STOCK})
            ->setPosX($entry->{self::COLUMN_POS_X})
            ->setPosY($entry->{self::COLUMN_POS_Y})
            ->setPosZ($entry->{self::COLUMN_POS_Z})
            ->setFaction($entry->{self::COLUMN_FACTION})
            ->setRotX($entry->{self::COLUMN_ROT_X})
            ->setRotY($entry->{self::COLUMN_ROT_Y})
            ->setRotZ($entry->{self::COLUMN_ROT_Z})
            ->setModel($entry->{self::COLUMN_MODEL});
    }
}
