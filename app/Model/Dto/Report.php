<?php
/**
 * Report
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 18.09.2020
 * Time: 17:34
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;


use App\Libraries\Logging\ILogging;
use App\Libraries\Logging\Traits\Variables;
use Carbon\Carbon;

class Report implements ILogging
{
    use Variables;

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var int
     */
    private $guid = 0;

    /**
     * @var float
     */
    private $posX = 0.00;

    /**
     * @var float
     */
    private $posY = 0.00;

    /**
     * @var float
     */
    private $posZ = 0.00;

    /**
     * @var Carbon
     */
    private $createdAt;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Report
     */
    public function setId(?int $id): Report
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getGuid(): int
    {
        return $this->guid;
    }

    /**
     * @param int $guid
     * @return Report
     */
    public function setGuid(int $guid): Report
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return float
     */
    public function getPosX(): float
    {
        return $this->posX;
    }

    /**
     * @param float $posX
     * @return Report
     */
    public function setPosX(float $posX): Report
    {
        $this->posX = $posX;
        return $this;
    }

    /**
     * @return float
     */
    public function getPosY(): float
    {
        return $this->posY;
    }

    /**
     * @param float $posY
     * @return Report
     */
    public function setPosY(float $posY): Report
    {
        $this->posY = $posY;
        return $this;
    }

    /**
     * @return float
     */
    public function getPosZ(): float
    {
        return $this->posZ;
    }

    /**
     * @param float $posZ
     * @return Report
     */
    public function setPosZ(float $posZ): Report
    {
        $this->posZ = $posZ;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon $createdAt
     * @return Report
     */
    public function setCreatedAt(Carbon $createdAt): Report
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}
