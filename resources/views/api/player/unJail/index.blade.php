<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 13.04.2020
 * Time: 22:30
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('api.layout.app')
@section('title')
    <i class="fas fa-dungeon"></i>
    Befreit den Spieler aus dem Gefängnis
@endsection
@section('content')
    <form method="post" action="{{ route('api.player.unJail.save') }}">
        {{ csrf_field() }}

        @include('api.includes.select', [
         'name' => 'pid',
         'label' => 'Spieler',
         'entries' => $players,
         'currentSelect' => $currentPlayer,
        ])
        <button type="submit" class="btn btn-danger">Bestätigen</button>
    </form>
    <br/>
@endsection
