<?php
/**
 * index.blade.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 29.08.2019
 * Time: 21:55
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-building"></i>
    Firmenübersicht
@endsection
@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Geld</th>
            <th scope="col"></th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($businesses as $business)
            <tr>
                <th scope="row">{{ $business->getId() }}</th>
                <td>{{ $business->getName() }}</td>
                <td>{{ $business->getMoney() }}</td>
                <td>
                    <a href="{{ route('admin.business.show', $business->getId()) }}" class="btn btn-primary">
                        Anzeigen
                    </a>
                </td>
                <td>
                    <form action="{{ route('admin.business.delete', $business->getId()) }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE"/>
                        <button class="btn btn-danger" type="submit">Löschen</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <h2> Erstellen </h2>
    <form action="{{ route('admin.business.create') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name">
        </div>
        <div class="form-group">
            <label for="money">Geld</label>
            <input type="number" class="form-control" id="money" name="money" value="0">
        </div>
        <button type="submit" class="btn btn-dark">Erstellen</button>
    </form>
@endsection


