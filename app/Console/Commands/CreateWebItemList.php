<?php
/**
 * CreateWebItemList
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 17.05.2020
 * Time: 14:42
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Console\Commands;

use App\Model\Character;
use App\Model\Enum\WebAdminItemList\Storage;
use App\Model\Housing;
use App\Model\Vehicle;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CreateWebItemList extends Command
{
    /**
     * @var string
     */
    protected $signature = 'webItemList:create';

    /**
     * @var string
     */
    protected $description = 'Erstellt die Einträge in der WebItemList Tabelle';

    public function handle()
    {
        // Leere alle Einträge
        \App\Model\WebAdminItemList::truncate();

        Character::chunk(
            100,
            function ($entries) {
                foreach ($entries as $entry) {
                    $dto = Character::entryAsDto($entry);
                    $this->info("Importiere Items von {$dto->getId()}...");
                    // Gehe durchs Spieler Inventar
                    $inventory = json_decode($dto->getInventory());
                    $this->processInventory($dto->getId(), $inventory, Storage::PERSON);

                    // Gehe durch alle Fahrzeuge
                    $vehicles = (new Vehicle())->getVehiclesByCharacterId($dto->getId());
                    foreach ($vehicles as $vehicle) {
                        $inventory = json_decode($vehicle->getInventory());
                        $this->processInventory($dto->getId(), $inventory, Storage::VEHICLE);
                    }

                    // Gehe durch alle Häuser
                    $houses = (new Housing())->getEntriesByCharacterId($dto->getId());
                    foreach ($houses as $house) {
                        $inventory = json_decode($house->getInventory());
                        $this->processInventory($dto->getId(), $inventory, Storage::HOUSE);
                    }
                    $this->info("Fertig");
                }
            }
        );
    }

    /**
     * @param int $characterId
     * @param array $inventory
     * @param string $storage
     */
    private function processInventory(int $characterId, array $inventory, string $storage)
    {
        foreach ($inventory as $item) {
            if ($item) {
                if (isset($item->inventory)) {
                    $this->processInventory($characterId, $item->inventory, $storage);
                } else {
                    $webItemListDto = (new \App\Model\Dto\WebAdminItemList())
                        ->setCharacterId($characterId)
                        ->setName($item->label)
                        ->setQuantity($item->quantity)
                        ->setStorage($storage);
                    (new \App\Model\WebAdminItemList())->insertEntry($webItemListDto);
                }
            }
        }
    }
}

