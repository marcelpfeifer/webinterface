<?php
/**
 * Permission
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 25.07.2020
 * Time: 11:10
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Permission;

use App\Model\Account;
use App\Model\WebPermissionToWebPermission;
use Illuminate\Support\Facades\Auth;

class Permission
{

    /**
     * @var array|null
     */
    private static $allowedPermissions = null;

    /**
     * @param int $permissionId
     * @return bool
     */
    public static function allowed(int $permissionId): int
    {
        if (is_null(self::$allowedPermissions)) {
            self::getAllowedPermissions();
        }
        return in_array($permissionId, self::$allowedPermissions);
    }

    private static function getAllowedPermissions(): void
    {
        $user = Auth::user();
        if (!($user)) {
            self::$allowedPermissions = [];
            return;
        }
        $groupId = $user->{Account::COLUMN_GROUP_ID};
        if (is_null($groupId)) {
            self::$allowedPermissions = [];
            return;
        }
        self::$allowedPermissions = (new WebPermissionToWebPermission())->getWebPermissionsIdsByPermissionGroupId(
            $groupId
        );
    }
}
