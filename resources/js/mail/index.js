class Mail {

    /**
     * @public
     */
    index() {
        $('.check').click(function () {
            var $this = $(this);
            if($this.is(':checked')) {
                $this.siblings('.password')[0].type = 'text';
            } else {
                $this.siblings('.password')[0].type = 'password';
            }
        });
    }

    /**
     * @public
     */
    create() {
        var quill = new Quill('#editor', {
            theme: 'snow'
        });

        var form = document.getElementById('mailCreate');
        form.addEventListener('submit', function () {
            let value = document.getElementsByClassName('ql-editor')[0].innerHTML;
            let field = document.getElementById('message');
            field.value = value;
        });

    }
}

/**
 * Rufe die Klasse auf
 */
$(document).ready(function () {
    if (window.location.href.indexOf("auth/mailbox/index") > -1) {
        let mail = new Mail();
        mail.index();
    }

    // Auth
    if (window.location.href.indexOf("auth/mailbox/mail/create") > -1
        || window.location.href.indexOf("auth/mailbox/mail/reply") > -1
        || window.location.href.indexOf("auth/mailbox/mail/share") > -1) {
        let mail = new Mail();
        mail.create();
    }

    // Web
    if (window.location.href.indexOf("web/mailbox/mail/create") > -1
        || window.location.href.indexOf("web/mailbox/mail/reply") > -1
        || window.location.href.indexOf("web/mailbox/mail/share") > -1) {
        let mail = new Mail();
        mail.create();
    }
});
