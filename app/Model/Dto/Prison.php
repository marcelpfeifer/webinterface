<?php
/**
 * Prison
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 01.11.2019
 * Time: 14:22
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;


use App\Libraries\Logging\ILogging;
use App\Libraries\Logging\Traits\Variables;
use Carbon\Carbon;

class Prison implements ILogging
{
    use Variables;

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var int
     */
    private $min = 0;

    /**
     * @var string
     */
    private $inventory = '';

    /**
     * @var int
     */
    private $money = 0;

    /**
     * @var int
     */
    private $handy = 0;

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Prison
     */
    public function setId(?int $id): Prison
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getMin(): int
    {
        return $this->min;
    }

    /**
     * @param int $min
     * @return Prison
     */
    public function setMin(int $min): Prison
    {
        $this->min = $min;
        return $this;
    }

    /**
     * @return string
     */
    public function getInventory(): string
    {
        return $this->inventory;
    }

    /**
     * @param string $inventory
     * @return Prison
     */
    public function setInventory(string $inventory): Prison
    {
        $this->inventory = $inventory;
        return $this;
    }

    /**
     * @return int
     */
    public function getMoney(): int
    {
        return $this->money;
    }

    /**
     * @param int $money
     * @return Prison
     */
    public function setMoney(int $money): Prison
    {
        $this->money = $money;
        return $this;
    }

    /**
     * @return int
     */
    public function getHandy(): int
    {
        return $this->handy;
    }

    /**
     * @param int $handy
     * @return Prison
     */
    public function setHandy(int $handy): Prison
    {
        $this->handy = $handy;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return Prison
     */
    public function setCreatedAt(?Carbon $createdAt): Prison
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}
