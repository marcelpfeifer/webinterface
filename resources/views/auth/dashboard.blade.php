@extends('layouts.app')
@section('title')
    Dashboard
@endsection
@section('content')
    <h1>Herzlich Willkommen auf dem Dashboard :)</h1>
    <hr>
    @if($serverStatus)
        <h2> Server Status</h2>
        <div class="row">
            <div class="col-12">
                (vom {{ $serverStatus->getCreatedAt()->setTimezone('Europe/Berlin')->format('d.m.Y H:i:s') }})
            </div>
            <div class="col-12 p-2 border rounded mt-1 shadow-sm">
                <i class="fas fa-2x fa-server text-success"></i> Webinterface
            </div>
            <div class="col-12 p-2 border rounded mt-1 shadow-sm">
                <i class="fas fa-2x fa-server {{ $serverStatus->isAltv() ? 'text-success' : 'text-danger' }}"></i>
                GTA-Server
            </div>
            <div class="col-12 p-2 border rounded mt-1 shadow-sm">
                <i class="fas fa-2x fa-server {{ $serverStatus->isTs() ? 'text-success' : 'text-danger' }}"></i>
                Teamspeak
            </div>
            <div class="col-12 p-2 border rounded mt-1 shadow-sm">
                <i class="fas fa-2x fa-server {{ $serverStatus->isForum() ? 'text-success' : 'text-danger' }}"></i>
                Forum
            </div>
        </div>
        <hr>
    @endif
@endsection
