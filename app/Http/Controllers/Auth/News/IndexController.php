<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.08.2021
 * Time: 10:46
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Auth\News;


use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Http\Request\Auth\News\Save;
use App\Http\Request\AValidator;
use App\Model\Dto\WebNews;
use App\Model\Dto\WebNewsCategory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class IndexController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        $news = [];
        foreach (\App\Model\WebNews::all() as $category) {
            $news[] = \App\Model\WebNews::entryAsDto($category);
        }
        return \view('auth.news.index',[
            'news' => $news
        ]);
    }

    /**
     * @return View
     */
    public function create(): View
    {
        $categories = [];
        foreach (\App\Model\WebNewsCategory::all() as $category) {
            $categories[] = \App\Model\WebNewsCategory::entryAsDto($category);
        }
        return \view(
            'auth.news.create',
            [
                'categories' => $categories
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @return View
     */
    public function edit(Request $request, int $id): View
    {
        $entry = \App\Model\WebNews::find($id);
        if (!$entry) {
            return \view('404');
        }

        $categories = [];
        foreach (\App\Model\WebNewsCategory::all() as $category) {
            $categories[] = \App\Model\WebNewsCategory::entryAsDto($category);
        }
        return \view(
            'auth.news.edit',
            [
                'categories' => $categories,
                'dto'        => \App\Model\WebNews::entryAsDto($entry),
            ]
        );
    }

    /**
     * @param Request $request
     * @param int|null $id
     * @return RedirectResponse
     */
    public function save(Request $request, int $id = null): RedirectResponse
    {
        // Validiere die Anfrage
        $validator = new Save($request, $id);
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            if ($id) {
                return redirect()->route('auth.news.edit', $id)
                    ->with('messages', $validator->getErrorsAsMessageArray($validatedData))
                    ->withInput()
                    ->withErrors($validatedData->errors());
            } else {
                return redirect()->route('auth.news.create')
                    ->with('messages', $validator->getErrorsAsMessageArray($validatedData))
                    ->withInput()
                    ->withErrors($validatedData->errors());
            }
        }

        \App\Http\Model\Auth\News\Save::save($validator->toDto());

        $messages[] = (new Message())
            ->setMessage("Wurde erfolgreich gespeichert")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('auth.news.index')->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int|null $id
     * @return RedirectResponse
     */
    public function saveEdit(Request $request, int $id): RedirectResponse
    {
        return $this->save($request, $id);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function delete(Request $request, int $id): RedirectResponse
    {
        $dto = (new WebNews())
            ->setId($id);
        (new \App\Model\WebNews())->deleteEntry($dto);
        $messages[] = (new Message())
            ->setMessage("Wurde gelöscht")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('auth.news.index')->with('messages', $messages);
    }
}
