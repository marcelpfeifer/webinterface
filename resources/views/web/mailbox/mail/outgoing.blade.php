<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 17.10.2020
 * Time: 20:05
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

/**
 * @var $mails \App\Model\Dto\WebMail[]
 */

?>
@extends('web.layout.app')
@section('url')
    https://eternityV.de/mail/mailfach/
@endsection
@section('content')
    <div class="container bg-white text-dark pb-2">
        @include('web.mailbox.mail.component.nav', [
            'email' => $dto->getEmail(),
            'link' => route('web.mailbox.index', [$hash]),
        ])
        <br/>
        @include('layouts.messages')
        <div class="row">
            <div class="col-2">
                @include('web.mailbox.mail.component.leftSideBar')
            </div>
            <div class="col-10">
                <div class="row col-12">
                    <a class="btn btn-secondary mb-2"
                       href="{{ route('web.mailbox.mail.create', [$webMailAddressId, $hash]) }}">
                        Neue Email
                    </a>
                </div>

                <table class="table table-striped border">
                    <thead>
                    <tr>
                        <th scope="col">Betreff</th>
                        <th scope="col">Von</th>
                        <th scope="col">Datum</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($mails as $mail)
                        <tr class="{{ $mail->isSeen() ? '' : 'font-weight-bold' }}">
                            <td class="align-middle">{{ $mail->getTitle() }}</td>
                            <td class="align-middle">{{ $mail->getSender() }}</td>
                            <td class="align-middle">{{ $mail->getCreatedAt()->format('Y-m-d H:i:s') }}</td>
                            <td class="align-middle"><a
                                    href="{{ route('web.mailbox.mail.show',[$webMailAddressId, $mail->getId(),$hash]) }}">
                                    Anzeigen </a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
