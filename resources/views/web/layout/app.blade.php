<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 30.04.2020
 * Time: 19:13
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
    <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('web.layout.head')
<body class="web bg-light" style="overflow-x: hidden">
<header class="bg-dark fixed-top">
    <div class="row p-4 w-100 ">
        <div class="col-3 text-white btn">
            Eternity-Browser v0.1
        </div>
        <div class="col-6 bg-white rounded text-success">
            <a class="btn" href="{{ route('web.index', $hash) }}">
                <i class="fas text-dark fa-home"></i>

            </a>
            <span class="text-dark">|</span>
            <i class="fas fa-lock"></i>
            <span style="letter-spacing: 1px">@yield('url')</span>
        </div>
        <div class="col-3">

        </div>
    </div>
</header>
<br/>
<br/>
<br/>
<br/>

<div class="container-fluid containerBody text-white position-absolute">
    <div class="content">
        @yield('content')
    </div>
</div>
@yield('modals')
</body>
</html>

