<?php
/**
 * WebAdminBanAccount
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 05.09.2020
 * Time: 14:13
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class WebAdminBanAccount
 * @package App\Model
 */
class WebAdminBanAccount extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_ACCOUNT_ID = 'accountId';

    /**
     * @var string
     */
    const COLUMN_POINTS = 'points';

    /**
     * @var string
     */
    const COLUMN_REASON = 'reason';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webAdminBanAccount';

    /**
     * @param int $accountId
     * @return Dto\WebAdminBanAccount[]
     */
    public function getBansByAccountId(int $accountId): array
    {
        $result = [];
        $entries = $this->where(self::COLUMN_ACCOUNT_ID, $accountId)->get();
        foreach ($entries as $entry) {
            $result[] = self::entryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param Dto\WebAdminBanAccount $dto
     * @return int
     */
    public function insertEntry(Dto\WebAdminBanAccount $dto): int
    {
        return $this->insertGetId(
            [
                self::COLUMN_ACCOUNT_ID => $dto->getAccountId(),
                self::COLUMN_POINTS     => $dto->getPoints(),
                self::COLUMN_REASON     => $dto->getReason(),
                self::CREATED_AT        => Carbon::now(),
                self::UPDATED_AT        => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebAdminBanAccount $dto
     */
    public function updateEntry(Dto\WebAdminBanAccount $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_ACCOUNT_ID => $dto->getAccountId(),
                self::COLUMN_POINTS     => $dto->getPoints(),
                self::COLUMN_REASON     => $dto->getReason(),
                self::UPDATED_AT        => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebAdminBanAccount $dto
     */
    public function deleteEntry(\App\Model\Dto\WebAdminBanAccount $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebAdminBanAccount $entry
     * @return Dto\WebAdminBanAccount
     */
    public static function entryAsDto(WebAdminBanAccount $entry): Dto\WebAdminBanAccount
    {
        return (new Dto\WebAdminBanAccount())
            ->setId($entry->{self::COLUMN_ID})
            ->setAccountId($entry->{self::COLUMN_ACCOUNT_ID})
            ->setPoints($entry->{self::COLUMN_POINTS})
            ->setReason($entry->{self::COLUMN_REASON})
            ->setCreatedAt($entry->{self::CREATED_AT})
            ->setUpdatedAt($entry->{self::UPDATED_AT});
    }
}
