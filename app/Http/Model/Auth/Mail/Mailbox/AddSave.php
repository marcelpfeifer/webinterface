<?php
/**
 * AddSave
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 12.08.2021
 * Time: 13:11
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Model\Auth\Mail\Mailbox;


use App\Model\Dto\WebMailAddressToAccount;
use App\Model\WebMailAddress;

class AddSave
{
    /**
     * @param \App\Http\Request\Dto\Auth\Mail\Mailbox\AddSave $requestDto
     * @return bool
     */
    public static function save(\App\Http\Request\Dto\Auth\Mail\Mailbox\AddSave $requestDto, int $accountId): bool
    {
        $entry = (new WebMailAddress())->getEntryByEmailAndPassword(
            $requestDto->getEmail(),
            $requestDto->getPassword()
        );
        if (!($entry)) {
            return false;
        }
        $dto = (new WebMailAddressToAccount())
            ->setWebMailAddressId($entry->getId())
            ->setAccountId($accountId)
            ->setOwner(false);
        (new \App\Model\WebMailAddressToAccount())->insertEntry($dto);
        return true;
    }
}
