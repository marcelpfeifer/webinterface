<?php
/**
 * Bans
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 05.05.2020
 * Time: 17:02
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

use App\Libraries\Logging\ILogging;
use App\Libraries\Logging\Traits\Variables;
use Carbon\Carbon;

/**
 * Class Bans
 * @package App\Model\Dto
 */
class Bans implements ILogging
{

    use Variables;

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var int
     */
    private $guid = 0;

    /**
     * @var int
     */
    private $timeBan = 0;

    /**
     * @var string
     */
    private $reason = '';

    /**
     * @var string|null
     */
    private $expire = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Bans
     */
    public function setId(?int $id): Bans
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getGuid(): int
    {
        return $this->guid;
    }

    /**
     * @param int $guid
     * @return Bans
     */
    public function setGuid(int $guid): Bans
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return int
     */
    public function getTimeBan(): int
    {
        return $this->timeBan;
    }

    /**
     * @param int $timeBan
     * @return Bans
     */
    public function setTimeBan(int $timeBan): Bans
    {
        $this->timeBan = $timeBan;
        return $this;
    }

    /**
     * @return string
     */
    public function getReason(): string
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     * @return Bans
     */
    public function setReason(string $reason): Bans
    {
        $this->reason = $reason;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getExpire(): ?string
    {
        return $this->expire;
    }

    /**
     * @param string|null $expire
     * @return Bans
     */
    public function setExpire(?string $expire): Bans
    {
        $this->expire = $expire;
        return $this;
    }
}
