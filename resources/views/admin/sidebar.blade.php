<div class="col-12 col-md-3">
    <div class="card bg-white text-secondary border-primary">
        <div class="card-header bg-primary text-white">
            <i class="fas fa-align-justify"></i>
            Navigation
        </div>
        <div class="card-body text-dark">
            <h4 class="font-italic border-bottom border-primary">Allgemein</h4>
            <ul class="nav flex-column">
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('admin.player.index') }}">
                        <i class="fas fa-user"></i>
                        Spielerübersicht
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_CODE_GENERATOR)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('admin.code.index') }}">
                        <i class="fas fa-qrcode"></i>
                        Code Generator
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_GROUP)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('admin.group.index') }}">
                        <i class="fas fa-user-friends"></i>
                        Gruppen
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_BUSINESS)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('admin.business.index') }}">
                        <i class="fas fa-building"></i>
                        Firmen
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_VEHICLE_SHOP)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('admin.vehicleShop.index') }}">
                        <i class="fas fa-car"></i>
                        Fahrzeugshop
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('admin.vehicleShopUnregistered.index') }}">
                        <i class="fas fa-car"></i>
                        Fahrzeugshop Unregistriert
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_ITEM_STORE)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('admin.itemStore.index') }}">
                        <i class="fas fa-box"></i>
                        ItemStore
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_HAPPY_BUY_CATEGORY)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('admin.web.happyBuy.index') }}">
                        <i class="fas fa-folder"></i>
                        Happy Buy Kategorien
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_ITEM_LIST)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('admin.itemList.index') }}">
                        <i class="fas fa-search"></i>
                        Item- und Fahrzeugsuche
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_SETTINGS)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('admin.config.index') }}">
                        <i class="fas fa-cog"></i>
                        Einstellungen
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PERMISSIONS)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('admin.permission.index') }}">
                        <i class="fas fa-balance-scale-right"></i>
                        Rechte Managment
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_REPORT)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('admin.report.index') }}">
                        <i class="fas fa-bug"></i>
                        Reportübersicht
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_BUG_REPORT)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('admin.bugReport.index') }}">
                        <i class="fas fa-bug"></i>
                        Bug-Report Übersicht
                    </a>
                </li>
                @endpermission

                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_BAN)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('admin.ban.index') }}">
                        <i class="fas fa-ban"></i>
                        Ban-Management
                    </a>
                </li>
                @endpermission

                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_LOGS)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('admin.log.index') }}">
                        <i class="fas fa-clipboard"></i>
                        Log-Einträge
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('admin.logApi.index') }}">
                        <i class="fas fa-clipboard"></i>
                        API Log-Einträge
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('admin.serverLog.index') }}">
                        <i class="fas fa-clipboard"></i>
                        Server Log-Einträge
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('admin.killLog.index') }}">
                        <i class="fas fa-clipboard"></i>
                        Kill Log-Einträge
                    </a>
                </li>
                @endpermission

                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_STOCK)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('admin.stock.index') }}">
                        <i class="fas fa-coins"></i>
                        Börse
                    </a>
                </li>
                @endpermission

                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_WHITELIST_QUESTION)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('admin.whitelistQuestion.index') }}">
                        <i class="fas fa-question"></i>
                        Whitelist Fragen
                    </a>
                </li>
                @endpermission
            </ul>

            <h4 class="font-italic border-bottom border-primary">Supportformulare</h4>
            <ul class="nav flex-column">
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_SUPPORT_FORM_WHITELIST)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('admin.supportForm.whitelist.index') }}">
                        <i class="fab fa-wpforms"></i>
                        Whitelist
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_SUPPORT_FORM_TECHNICAL_FAQ)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('admin.supportForm.technicalFaq.index') }}">
                        <i class="fab fa-wpforms"></i>
                        Technisches FAQ
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_SUPPORT_FORM_REFUND)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('admin.supportForm.refund.index') }}">
                        <i class="fab fa-wpforms"></i>
                        Erstattung
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_SUPPORT_FORM_INFRINGEMENT)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('admin.supportForm.infringement.index') }}">
                        <i class="fab fa-wpforms"></i>
                        Regelverstoß
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_SUPPORT_FORM_BANS)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('admin.supportForm.ban.index') }}">
                        <i class="fab fa-wpforms"></i>
                        Bans
                    </a>
                </li>
                @endpermission
            </ul>
            @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API)
            <div class="row mt-1 border-top border-primary">
                <div class="col-12 mt-1 text-center">
                    <a class="btn text-center btn-dark" href="{{ route('api.index') }}">Zur API</a>
                </div>
            </div>
            @endpermission
        </div>
    </div>
</div>
