<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 24.09.2021
 * Time: 17:26
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Admin\ServerLog;


use App\Libraries\Frontend\Dto\Admin\ServerLog\Index\Entry;
use App\Libraries\Frontend\Enum\Admin\ServerLog\Type;
use App\Libraries\Log\Server\Directory;

class Index
{

    /**
     * @param array $filter
     * @return \App\Libraries\Frontend\Dto\Admin\ServerLog\Index
     */
    public static function getViewData(array $filter): \App\Libraries\Frontend\Dto\Admin\ServerLog\Index
    {
        $entries = [];

        $filePath = storage_path(Directory::STORAGE_DIR . Directory::FILENAME);
        if (file_exists($filePath)) {
            $text = file_get_contents($filePath);
            $lines = self::splitLogIntoLines($text);

            foreach ($lines as $line) {
                $line = self::cleanLine($line);
                $type = self::getType($line);
                if (in_array(Type::WARNING, $filter) || in_array(Type::ERROR, $filter)) {
                    if (in_array(Type::WARNING, $filter) && Type::WARNING === $type) {
                        $entries[] = (new Entry())
                            ->setLine($line)
                            ->setType($type);
                        continue;
                    }
                    if (in_array(Type::ERROR, $filter) && Type::ERROR === $type) {
                        $entries[] = (new Entry())
                            ->setLine($line)
                            ->setType($type);
                        continue;
                    }
                    continue;
                }
                $entries[] = (new Entry())
                    ->setLine($line)
                    ->setType($type);
            }
        }

        return (new \App\Libraries\Frontend\Dto\Admin\ServerLog\Index())
            ->setEntries($entries);
    }

    /**
     * @param string $line
     * @return string
     */
    private static function getType(string $line): string
    {
        $type = '';
        if (strpos($line, '[Error]') !== false) {
            $type = Type::ERROR;
        }
        if (strpos($line, '[Warning]') !== false) {
            $type = Type::WARNING;
        }
        return $type;
    }

    /**
     * @param string $line
     * @return string
     */
    private static function cleanLine(string $line): string
    {
        $line = preg_replace("/\e/", '', $line);
        $line = str_replace('[37m', '', $line);
        $line = str_replace('[33m', '', $line);
        $line = str_replace('[92m', '', $line);
        $line = str_replace('[93m', '', $line);
        $line = str_replace('[91m', '', $line);
        return $line;
    }

    /**
     * @param string $text
     * @return array
     */
    private static function splitLogIntoLines(string $text): array
    {
        return preg_split("/\r\n|\n|\r/", $text);
    }
}
