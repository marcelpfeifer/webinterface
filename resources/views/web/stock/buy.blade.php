<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 02.09.2021
 * Time: 12:15
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('web.layout.app')
@section('url')
    https://eternityV.de/börse/kaufen
@endsection
@section('content')
    <div class="container transparentBg bg-white text-dark min-vh-100">
        @include('web.stock.component.nav',[])
        @include('layouts.messages')
        <form method="post" class="form" action="{{ route('web.stock.buyPost', [$stock->getId(),$hash]) }}">
            {{ csrf_field() }}
            <div class="form-row">
                <div class="col-12">
                    <label for="name">Name</label>
                    <input class="form-control" id="name" name="name" type="text" value="{{ $stock->getName() }}"
                           readonly>
                </div>
            </div>
            <div class="form-row">
                <div class="col-12">
                    <label for="price">Preis pro Aktie</label>
                    <input class="form-control" id="price" name="price" type="text" value="{{ $stock->getPrice() }}$"
                           readonly>
                </div>
            </div>

            <div class="form-row">
                <div class="col-12">
                    <label for="amount">Anzahl</label>
                    <input class="form-control" id="amount" name="amount" type="number" required
                           max="{{ ($stock->getStock() > -1) ? $stock->getStock() : '' }}">
                </div>
            </div>
            @if($stock->getStock() > -1)
                <div class="form-row">
                    <div class="col-12">
                        <label for="stock">Verfügbare Aktien</label>
                        <input class="form-control" id="stock" name="stock" value="{{ $stock->getStock() }}" readonly
                               type="number">
                    </div>
                </div>
            @endif
            <br/>
            <div class="form-row justify-content-end">
                @if($stock->getStock() == 0)
                    <a class="btn btn-danger text-white">Keine Aktien mehr verfügbar</a>
                @else
                    <button type="submit" class="btn btn-primary">Kaufen</button>
                @endif
            </div>
        </form>
    </div>
@endsection
