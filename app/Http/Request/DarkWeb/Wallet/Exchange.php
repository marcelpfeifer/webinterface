<?php
/**
 * Exchange
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 22.08.2020
 * Time: 21:22
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\DarkWeb\Wallet;

use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;
use App\Model\Character;

class Exchange extends AValidator implements IValidator
{
    /**
     * @return string[]
     */
    public function getRules(): array
    {
        return [
            'amount' => [
                'required',
                'int',
            ]
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'amount.required' => 'Der Betrag muss übergeben werden!',
            'amount.int'      => 'Der Betrag darf nur Zahlen enthalten!',
        ];
    }

    /**
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validate(): \Illuminate\Contracts\Validation\Validator
    {
        $validator = parent::validate();

        $validator->after(
            function ($validator) {
                $character = (new Character())->getEntryById($this->id);
                $amount = $this->request->input('amount', 0);
                if ($character->getBitcoin() < $amount) {
                    $validator->errors()->add('amount', 'Du hast nicht genug Bitcoins auf deinem Konto!');
                }
            }
        );

        return $validator;
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\DarkWeb\Wallet\Exchange())
            ->setAmount($this->request->input('amount'));
    }
}
