https://app.swaggerhub.com/apis/eternityV/eternityVUCP/0.0.1

Fertige Systeme
- Registrieren
- Codegenerieren
- Gruppen erstellen
- Geld bearbeiten (noch nicht Live)
- Namen bearbeiten
- Whitelisten
- Fraktionen setzten (noch nicht Live)
- Adminränge
- Jobs setzten
- Gruppe zuweisen
- Fahrzeugübersicht
- FAQ
- Supportformulare
- Email bearbeiten
- Profilbild bearbeiten
- Spieler zurück setzten
- Spieler aus Gefängnis holen (noch nicht Live)

In Planung:
- Spieler porten
- Fraktion setzten (live)
- Geld bearbeiten (live)
- Spieler aus Gefängnis holen (live)
- Kicken
- Adminnachrichten
