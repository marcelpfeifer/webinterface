<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 26.10.2020
 * Time: 19:47
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

/**
 * @var $mail \App\Model\Dto\WebMail
 */
?>
@extends('web.layout.app')
@section('url')
    https://eternityV.de/mail/mailfach/verschicken
@endsection
@section('content')
    <div class="container bg-white text-dark pb-2">
        @include('web.mailbox.mail.component.nav', [
            'email' => $dto->getEmail(),
            'link' => route('web.mailbox.mail.index', [$webMailAddressId,$hash]),
        ])
        <br/>
        @include('layouts.messages')
        <div class="row">
            <div class="col-2">
                @include('web.mailbox.mail.component.leftSideBar')
            </div>
            <div class="col-10">
                <form action="{{ route('web.mailbox.mail.save', [$webMailAddressId,$hash]) }}" method="POST" id="mailCreate"
                      enctype='multipart/form-data'>
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="to">An</label>
                        <input type="text" class="form-control" id="to" name="to"
                               placeholder="max.mustermann@eternity.de"
                               value="{{ ($mail) ? $mail->getSender() : ''}}" required>
                    </div>
                    <div class="form-group">
                        <label for="title">Betreff</label>
                        <input type="text" class="form-control" id="title" name="title"
                               value="{{ ($mail) ? 'RE: '. $mail->getTitle() : ''}}" required>
                    </div>
                    <div class="form-row">
                        <div class="col-12">
                            <label for="message">Text</label>
                            <div id="editor">
                                {!!  ($mail) ? '<br/><blockquote>'. $mail->getMessage() . '</blockquote>': '' !!}
                            </div>
                            <input type="hidden" id="message" name="message" required>
                        </div>
                    </div>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <div class="form-row">
                        <div class="col-12">
                            <label for="file">Anhang</label>
                            <input type="file" class="form-control-file" name="file[]" id="file" accept=".jpg,.png,.pdf"
                                   multiple>
                        </div>
                    </div>
                    <br/>
                    <button type="submit" class="btn btn-primary">Verschicken</button>
                </form>
            </div>
        </div>
    </div>
@endsection
