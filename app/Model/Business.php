<?php
/**
 * Business
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 04.05.2020
 * Time: 16:52
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    const COLUMN_MONEY = 'money';

    /**
     * @var string
     */
    const COLUMN_MESSAGE = 'message';

    /**
     * @var string
     */
    const COLUMN_CREATED_AT = 'createdAt';

    /**
     * @var string
     */
    const COLUMN_UPDATED_AT = 'updatedAt';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'business';

    /**
     * @param Dto\Business $dto
     */
    public function insertEntry(Dto\Business $dto)
    {
        $this->insert([
            self::COLUMN_ID         => $dto->getId(),
            self::COLUMN_NAME       => $dto->getName(),
            self::COLUMN_MONEY      => $dto->getMoney(),
            self::COLUMN_MESSAGE    => $dto->getMessages(),
            self::COLUMN_CREATED_AT => Carbon::now(),
            self::COLUMN_UPDATED_AT => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\Business $dto
     */
    public function updateEntry(Dto\Business $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update([
            self::COLUMN_ID         => $dto->getId(),
            self::COLUMN_NAME       => $dto->getName(),
            self::COLUMN_MONEY      => $dto->getMoney(),
            self::COLUMN_MESSAGE    => $dto->getMessages(),
            self::COLUMN_UPDATED_AT => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\Business $dto
     */
    public function deleteEntry(Dto\Business $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param Business $entry
     * @return Dto\Business
     */
    public static function entryAsDto(Business $entry): Dto\Business
    {
        return (new Dto\Business())
            ->setId($entry->{self::COLUMN_ID})
            ->setName($entry->{self::COLUMN_NAME})
            ->setMoney($entry->{self::COLUMN_MONEY})
            ->setMessages($entry->{self::COLUMN_MESSAGE});
    }
}
