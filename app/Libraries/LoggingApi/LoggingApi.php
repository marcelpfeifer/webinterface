<?php
/**
 * LoggingApi
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 07.03.2021
 * Time: 13:31
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\LoggingApi;

use App\Model\WebAdminLoggingApi;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class LoggingApi
{

    /**
     * @param string $url
     */
    public static function log(string $url, array $options)
    {
        $dto = (new \App\Model\Dto\WebAdminLoggingApi())
            ->setAccountId(Auth::id())
            ->setUrl($url)
            ->setOptions(print_r($options, true))
            ->setDateTime(Carbon::now());
        (new WebAdminLoggingApi())->insertEntry($dto);
    }
}
