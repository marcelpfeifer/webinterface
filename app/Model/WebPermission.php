<?php
/**
 * WebPermission
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 24.07.2020
 * Time: 17:55
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class WebPermission extends Model
{
    /**
     * @var string
     */
    public const COLUMN_ID = 'id';

    /**
     * @var string
     */
    public const COLUMN_NAME = 'name';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webPermission';

    /**
     * @param Dto\WebPermission $dto
     */
    public function insertEntry(Dto\WebPermission $dto)
    {
        $this->insert(
            [
                self::COLUMN_ID   => $dto->getId(),
                self::COLUMN_NAME => $dto->getName(),
                self::CREATED_AT  => Carbon::now(),
                self::UPDATED_AT  => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebPermission $dto
     */
    public function updateEntry(Dto\WebPermission $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_ID   => $dto->getId(),
                self::COLUMN_NAME => $dto->getName(),
                self::UPDATED_AT  => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebPermission $dto
     */
    public function deleteEntry(\App\Model\Dto\WebPermission $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebPermission $entry
     * @return Dto\WebPermission
     */
    public static function entryAsDto(WebPermission $entry): Dto\WebPermission
    {
        return (new Dto\WebPermission())
            ->setId($entry->{self::COLUMN_ID})
            ->setName($entry->{self::COLUMN_NAME});
    }
}
