<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 16.09.2021
 * Time: 18:50
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Web\Hunt\Index;


use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;
use App\Model\Enum\Hunt\Leaderboard\OrderBy;
use Illuminate\Validation\Rule;

class Index extends AValidator implements IValidator
{

    /**
     * @return string[]
     */
    public function getRules(): array
    {
        return [
            'orderBy' => [
                'nullable',
                'string',
                Rule::in(OrderBy::getConstants())

            ]
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'orderBy.string' => 'Die Reihenfolge darf nur Zeichen enthalten!',
            'orderBy.in'     => 'Die Reihenfolge gibt es nicht!',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Web\Hunt\Index\Index())
            ->setOrderBy($this->request->input('orderBy') ?? OrderBy::ANIMAL_COUNT);
    }
}
