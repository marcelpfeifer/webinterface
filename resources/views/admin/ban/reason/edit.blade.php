<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 06.01.2021
 * Time: 13:24
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-ban"></i>
    Eintrag {{ $dto->getId()}}
    <a class="btn btn-dark btn-sm float-right"
       href="{{ route('admin.ban.reason.index') }}"
       role="button"
    >
        Zurück
    </a>
@endsection
@section('content')
    @if(isset($dto))
        <form action="{{ route('admin.ban.reason.update', $dto->getId()) }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ $dto->getName() }}" required>
            </div>
            <div class="form-group">
                <label for="description">Beschreibung</label>
                <textarea class="form-control" id="description" name="description"
                          rows="3">{{ $dto->getDescription() }}</textarea>
            </div>
            <div class="form-group">
                <label for="points">Punkte</label>
                <input type="number" class="form-control" id="points" name="points" value="{{ $dto->getPoints() }}"
                       required>
            </div>
            <br/>
            <button type="submit" class="btn btn-dark">Aktualsieren</button>
        </form>
    @else
        Eintrag wurde nicht gefunden
    @endif
@endsection
