<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 18.08.2021
 * Time: 10:09
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
<div class="row">
    <a href="{{ route('web.mailbox.mail.index', [$webMailAddressId, $hash]) }}"
       class="col-12 border p-2">
        Eingang
    </a>
    <a href="{{ route('web.mailbox.mail.outgoing', [$webMailAddressId, $hash]) }}" class="col-12 border p-2">
        Ausgang
    </a>
</div>
