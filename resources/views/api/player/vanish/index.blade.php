<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 08.04.2020
 * Time: 17:46
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('api.layout.app')
@section('title')
    <i class="fas fa-smog"></i>
    Macht einen Spieler unsichtbar
@endsection
@section('content')
    <form method="post" action="{{ route('api.player.vanish.save') }}">
        {{ csrf_field() }}

        @include('api.includes.select', [
         'name' => 'pid',
         'label' => 'Spieler',
         'entries' => $players,
         'currentSelect' => $currentPlayer,
        ])
        <div class="form-check">
            <input type="checkbox" class="form-check-input" id="active" name="active">
            <label class="form-check-label" for="active"> Unsichtbar?</label>
        </div>

        <button type="submit" class="btn btn-danger">Bestätigen</button>
    </form>
    <br/>
@endsection
