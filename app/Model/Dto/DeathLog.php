<?php
/**
 * DeathLog
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.10.2021
 * Time: 13:07
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;


use Carbon\Carbon;

class DeathLog
{

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var int
     */
    private $victim = 0;

    /**
     * @var int
     */
    private $killer = 0;

    /**
     * @var string
     */
    private $victimPosition = '';

    /**
     * @var string
     */
    private $killerPosition = '';

    /**
     * @var bool
     */
    private $isKillerInVehicle = false;

    /**
     * @var string
     */
    private $weaponHash = '';

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @var null|Carbon
     */
    private $updatedAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return DeathLog
     */
    public function setId(?int $id): DeathLog
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getVictim(): int
    {
        return $this->victim;
    }

    /**
     * @param int $victim
     * @return DeathLog
     */
    public function setVictim(int $victim): DeathLog
    {
        $this->victim = $victim;
        return $this;
    }

    /**
     * @return int
     */
    public function getKiller(): int
    {
        return $this->killer;
    }

    /**
     * @param int $killer
     * @return DeathLog
     */
    public function setKiller(int $killer): DeathLog
    {
        $this->killer = $killer;
        return $this;
    }

    /**
     * @return string
     */
    public function getVictimPosition(): string
    {
        return $this->victimPosition;
    }

    /**
     * @param string $victimPosition
     * @return DeathLog
     */
    public function setVictimPosition(string $victimPosition): DeathLog
    {
        $this->victimPosition = $victimPosition;
        return $this;
    }

    /**
     * @return string
     */
    public function getKillerPosition(): string
    {
        return $this->killerPosition;
    }

    /**
     * @param string $killerPosition
     * @return DeathLog
     */
    public function setKillerPosition(string $killerPosition): DeathLog
    {
        $this->killerPosition = $killerPosition;
        return $this;
    }

    /**
     * @return bool
     */
    public function isKillerInVehicle(): bool
    {
        return $this->isKillerInVehicle;
    }

    /**
     * @param bool $isKillerInVehicle
     * @return DeathLog
     */
    public function setIsKillerInVehicle(bool $isKillerInVehicle): DeathLog
    {
        $this->isKillerInVehicle = $isKillerInVehicle;
        return $this;
    }

    /**
     * @return string
     */
    public function getWeaponHash(): string
    {
        return $this->weaponHash;
    }

    /**
     * @param string $weaponHash
     * @return DeathLog
     */
    public function setWeaponHash(string $weaponHash): DeathLog
    {
        $this->weaponHash = $weaponHash;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return DeathLog
     */
    public function setCreatedAt(?Carbon $createdAt): DeathLog
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon|null $updatedAt
     * @return DeathLog
     */
    public function setUpdatedAt(?Carbon $updatedAt): DeathLog
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
