<?php
/**
 * UserEmail
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 27.08.2020
 * Time: 19:30
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Tickets;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserEmail extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_USER_ID = 'user_id';

    /**
     * @var string
     */
    const COLUMN_FLAGS = 'flags';

    /**
     * @var string
     */
    const COLUMN_ADDRESS = 'address';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'ost_user_email';

    /**
     * @var string
     */
    protected $connection = 'mysqlTickets';

    /**
     * @param Dto\UserEmail $dto
     */
    public function insertEntry(Dto\UserEmail $dto)
    {
        $this->insert(
            [
                self::COLUMN_ID      => $dto->getId(),
                self::COLUMN_USER_ID => $dto->getUserId(),
                self::COLUMN_FLAGS   => $dto->getFlags(),
                self::COLUMN_ADDRESS => $dto->getAddress(),
            ]
        );
    }

    public function truncateTable()
    {
        self::truncate();
    }
}
