<?php
/**
 * WebAdminWhitelistAnswer
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 13.11.2021
 * Time: 13:09
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

use App\Libraries\Logging\ILogging;
use App\Libraries\Logging\Traits\Variables;
use Carbon\Carbon;

class WebAdminWhitelistAnswer implements ILogging
{
    use Variables;

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var int
     */
    private $webAdminWhitelistId = 0;

    /**
     * @var int
     */
    private $webAdminWhitelistQuestionId = 0;

    /**
     * @var string
     */
    private $accuracy = '';

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @var null|Carbon
     */
    private $updatedAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebAdminWhitelistAnswer
     */
    public function setId(?int $id): WebAdminWhitelistAnswer
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getWebAdminWhitelistId(): int
    {
        return $this->webAdminWhitelistId;
    }

    /**
     * @param int $webAdminWhitelistId
     * @return WebAdminWhitelistAnswer
     */
    public function setWebAdminWhitelistId(int $webAdminWhitelistId): WebAdminWhitelistAnswer
    {
        $this->webAdminWhitelistId = $webAdminWhitelistId;
        return $this;
    }

    /**
     * @return int
     */
    public function getWebAdminWhitelistQuestionId(): int
    {
        return $this->webAdminWhitelistQuestionId;
    }

    /**
     * @param int $webAdminWhitelistQuestionId
     * @return WebAdminWhitelistAnswer
     */
    public function setWebAdminWhitelistQuestionId(int $webAdminWhitelistQuestionId): WebAdminWhitelistAnswer
    {
        $this->webAdminWhitelistQuestionId = $webAdminWhitelistQuestionId;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccuracy(): string
    {
        return $this->accuracy;
    }

    /**
     * @param string $accuracy
     * @return WebAdminWhitelistAnswer
     */
    public function setAccuracy(string $accuracy): WebAdminWhitelistAnswer
    {
        $this->accuracy = $accuracy;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return WebAdminWhitelistAnswer
     */
    public function setCreatedAt(?Carbon $createdAt): WebAdminWhitelistAnswer
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon|null $updatedAt
     * @return WebAdminWhitelistAnswer
     */
    public function setUpdatedAt(?Carbon $updatedAt): WebAdminWhitelistAnswer
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
