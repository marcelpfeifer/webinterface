<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 14.04.2020
 * Time: 10:05
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class IndexController extends Controller
{

    /**
     * @return View
     */
    public function wasted(): View
    {
        Auth::logout();
        return \view('fun.wasted');
    }

}
