<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 13.12.2020
 * Time: 18:16
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Web\YellowPage;

use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class Save extends AValidator implements IValidator
{
    /**
     * @return string[]
     */
    public function getRules(): array
    {
        return [
            'message' => [
                'required',
                'string',
            ]
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'message.string'   => 'Die Nachricht darf nur Zeichen enthalten!',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Web\YellowPage\Save())
            ->setMessage($this->request->input('message'));
    }
}
