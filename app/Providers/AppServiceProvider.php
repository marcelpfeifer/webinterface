<?php

namespace App\Providers;

use App\Libraries\Permission\Permission;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::if(
            'permission',
            function (int $permissionId) {
                return Permission::allowed($permissionId);
            }
        );
        Paginator::useBootstrap();
    }
}
