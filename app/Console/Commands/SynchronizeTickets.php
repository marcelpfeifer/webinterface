<?php
/**
 * SynchonizeTickets
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 27.08.2020
 * Time: 19:52
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Console\Commands;

use App\Model\Account;
use App\Model\Character;
use App\Model\Tickets\User;
use App\Model\Tickets\UserAccount;
use App\Model\Tickets\UserEmail;
use Illuminate\Console\Command;

class SynchronizeTickets extends Command
{
    /**
     * @var string
     */
    protected $signature = 'tickets:synchronize';

    /**
     * @var string
     */
    protected $description = 'Synchronziere die Tickets Datenbank';

    public function handle()
    {
        $userDb = new User();
        $userAccountDb = new UserAccount();
        $userEmailDb = new UserEmail();

        $userDb->truncateTable();
        $userAccountDb->truncateTable();
        $userEmailDb->truncateTable();

        Account::chunk(
            100,
            function ($entries) use ($userDb, $userAccountDb, $userEmailDb) {
                foreach ($entries as $entry) {
                    $dto = Account::entryAsDto($entry);
                    $characterDto = (new Character())->getEntryById($dto->getId());
                    if (!$characterDto) {
                        continue;
                    }

                    $userDb->insertEntry(
                        (new \App\Model\Tickets\Dto\User())
                            ->setId($dto->getId())
                            ->setOrgId(0)
                            ->setEmailId($dto->getId())
                            ->setStatus(0)
                            ->setName($characterDto->getName())
                    );
                    $userAccountDb->insertEntry(
                        (new \App\Model\Tickets\Dto\UserAccount())
                            ->setId($dto->getId())
                            ->setUserId($dto->getId())
                            ->setStatus(1)
                            ->setUsername($dto->getUsername())
                            ->setPassword($dto->getPassword())
                    );

                    $userEmailDb->insertEntry(
                        (new \App\Model\Tickets\Dto\UserEmail())
                            ->setId($dto->getId())
                            ->setUserId($dto->getId())
                            ->setFlags(0)
                            ->setAddress(
                                $this->cleanString($characterDto->getName()) . "@eternityV.de"
                            )
                    );
                }
            }
        );
    }

    /**
     * @param $text
     * @return string|string[]|null
     */
    private function cleanString($text)
    {
        $utf8 = array(
            '/[áàâãªä]/u' => 'a',
            '/[ÁÀÂÃÄ]/u'  => 'A',
            '/[ÍÌÎÏ]/u'   => 'I',
            '/[íìîï]/u'   => 'i',
            '/[éèêë]/u'   => 'e',
            '/[ÉÈÊË]/u'   => 'E',
            '/[óòôõºö]/u' => 'o',
            '/[ÓÒÔÕÖ]/u'  => 'O',
            '/[úùûü]/u'   => 'u',
            '/[ÚÙÛÜ]/u'   => 'U',
            '/ç/'         => 'c',
            '/Ç/'         => 'C',
            '/ñ/'         => 'n',
            '/Ñ/'         => 'N',
            '/–/'         => '-', // UTF-8 hyphen to "normal" hyphen
            '/[’‘‹›‚]/u'  => ' ', // Literally a single quote
            '/[“”«»„]/u'  => ' ', // Double quote
            '/ /'         => ' ', // nonbreaking space (equiv. to 0x160)
        );
        return preg_replace(array_keys($utf8), array_values($utf8), $text);
    }
}
