<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 20.04.2020
 * Time: 17:46
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

/**
 * @var $characters \App\Model\Dto\Character[]
 */

/**
 * @var $group \App\Model\Dto\Groups
 */
?>

@extends('admin.layout.app')
@section('title')
    <i class="fas fa-user-friends"></i>
    Gruppe {{ $group->getName() }}
@endsection
@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Rang</th>
        </tr>
        </thead>
        <tbody>
        @foreach($characters as $character)
            <tr>
                <th scope="row">{{ $character->getId() }}</th>
                <td>{{ $character->getName() }}</td>
                <td>{{ $character->getGroupRank() }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="alert alert-secondary" role="alert">
        <ul>
            <li>
                Rang 1
                <ul>
                    <li>Noch nichts</li>
                </ul>
            </li>
            <li>
                Rang 2
                <ul>
                    <li>Noch nichts</li>
                </ul>
            </li>
            <li>
                Rang 3
                <ul>
                    <li> Geld abheben / einzahlen</li>
                    <li> Mitglieder befördern/degradieren</li>
                </ul>
            </li>
            <li>
                Rang 4
                <ul>
                    <li>Geld abheben / einzahlen</li>
                    <li>Mitglieder befördern/degradieren</li>
                </ul>
            </li>
        </ul>
    </div>
@endsection
