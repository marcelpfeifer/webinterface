<?php
/**
 * Faction
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 06.10.2021
 * Time: 14:06
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Enum\Api\Player\Faction;

use App\Libraries\Enum\AEnum;

class Faction extends AEnum
{
    /**
     * @var string
     */
    const CIV = 'civ';

    /**
     * @var string
     */
    const COP = 'cop';

    /**
     * @var string
     */
    const MED = 'med';

    /**
     * @var string
     */
    const FIB = 'fib';

    /**
     * @var string
     */
    const DOJ = 'doj';

    /**
     * @var string
     */
    const LAWYER = 'lawyer';
}
