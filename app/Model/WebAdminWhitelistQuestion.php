<?php
/**
 * WebAdminWhitelistQuestion
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 13.11.2021
 * Time: 13:02
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class WebAdminBan
 * @package App\Model
 */
class WebAdminWhitelistQuestion extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_QUESTION = 'question';

    /**
     * @var string
     */
    const COLUMN_ANSWER = 'answer';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webAdminWhitelistQuestion';

    /**
     * @var array
     */
    protected $dates = [
        self::CREATED_AT,
        self::UPDATED_AT,
    ];

    /**
     * @return HasMany
     */
    public function answers(): HasMany
    {
        return $this->hasMany(
            WebAdminWhitelistAnswer::class,
            WebAdminWhitelistAnswer::COLUMN_WEB_ADMIN_WHITELIST_QUESTION_ID,
            self::COLUMN_ID
        );
    }

    /**
     * @param Dto\WebAdminWhitelistQuestion $dto
     * @return bool
     */
    public function insertEntry(Dto\WebAdminWhitelistQuestion $dto): bool
    {
        return $this->insert(
            [
                self::COLUMN_ID       => $dto->getId(),
                self::COLUMN_QUESTION => $dto->getQuestion(),
                self::COLUMN_ANSWER   => $dto->getAnswer(),
                self::CREATED_AT      => Carbon::now(),
                self::UPDATED_AT      => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebAdminWhitelistQuestion $dto
     * @return bool
     */
    public function updateEntry(Dto\WebAdminWhitelistQuestion $dto): bool
    {
        return $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_ID       => $dto->getId(),
                self::COLUMN_QUESTION => $dto->getQuestion(),
                self::COLUMN_ANSWER   => $dto->getAnswer(),
                self::UPDATED_AT      => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebAdminWhitelistQuestion $dto
     */
    public function deleteEntry(\App\Model\Dto\WebAdminWhitelistQuestion $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebAdminWhitelistQuestion $entry
     * @return Dto\WebAdminWhitelistQuestion
     */
    public static function entryAsDto(WebAdminWhitelistQuestion $entry): Dto\WebAdminWhitelistQuestion
    {
        return (new Dto\WebAdminWhitelistQuestion())
            ->setId($entry->{self::COLUMN_ID})
            ->setQuestion($entry->{self::COLUMN_QUESTION})
            ->setAnswer($entry->{self::COLUMN_ANSWER})
            ->setCreatedAt($entry->{self::CREATED_AT})
            ->setUpdatedAt($entry->{self::UPDATED_AT});
    }
}
