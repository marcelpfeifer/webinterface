require('../../../../node_modules/chart.js/dist/chart.min');

class Index {

    /**
     * @public
     */
    init() {
        let element = document.getElementById('content');
        let labels = JSON.parse(element.dataset.labels);
        let memoryMax = JSON.parse(element.dataset.ramMax);
        let memory = JSON.parse(element.dataset.ram);
        let cpu = JSON.parse(element.dataset.cpu);
        let altv = JSON.parse(element.dataset.altv);
        let altvTest = JSON.parse(element.dataset.altvTest);
        let ts = JSON.parse(element.dataset.ts);
        let forum = JSON.parse(element.dataset.forum);

        this.server(labels, altv, altvTest, ts, forum);
        this.ram(labels, memoryMax, memory);
        this.cpu(labels, cpu);
    }

    /**
     *
     * @param labels
     * @param altv
     * @param altvTest
     * @param ts
     * @param forum
     */
    server(labels, altv, altvTest, ts, forum) {

        const data = {
            labels: labels,
            datasets: [
                {
                    label: 'ALTV Server',
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgb(255, 99, 132)',
                    data: altv,
                },
                {
                    label: 'ALTV Test Server',
                    backgroundColor: 'rgb(82,222,0)',
                    borderColor: 'rgb(82,222,0)',
                    data: altvTest,
                },
                {
                    label: 'TS',
                    backgroundColor: 'rgb(226,73,0)',
                    borderColor: 'rgb(226,73,0)',
                    data: ts,
                },
                {
                    label: 'Forum',
                    backgroundColor: 'rgb(99,120,255)',
                    borderColor: 'rgb(99,120,255)',
                    data: forum,
                },

            ]
        };
        const config = {
            type: 'line',
            data,
            options: {
                scales: {
                    y: {
                        type: 'linear',
                        min: 0,
                        max: 1,
                        display: false,
                    }
                }
            }
        };

        new Chart(
            document.getElementById('server'),
            config
        );
    }

    /**
     *
     * @param labels
     * @param memoryMax
     * @param memory
     */
    ram(labels, memoryMax, memory) {
        const data = {
            labels: labels,
            datasets: [
                {
                    label: 'Arbeitsspeicher',
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgb(255, 99, 132)',
                    data: memory,
                },
            ]
        };
        const config = {
            type: 'line',
            data,
            options: {
                scales: {
                    y: {
                        type: 'linear',
                        min: 0,
                        max: memoryMax
                    }
                }
            }
        };

        new Chart(
            document.getElementById('ram'),
            config
        );
    }

    /**
     *
     * @param labels
     * @param cpu
     */
    cpu(labels, cpu) {
        const data = {
            labels: labels,
            datasets: [{
                label: 'CPU',
                backgroundColor: 'rgb(0, 0, 255)',
                borderColor: 'rgb(0, 0, 255)',
                data: cpu,
            }]
        };
        const config = {
            type: 'line',
            data,
            options: {
                scales: {
                    y: {
                        type: 'linear',
                        min: 0,
                        max: 100
                    }
                }
            }
        };

        new Chart(
            document.getElementById('cpu'),
            config
        );
    }

}

/**
 * Rufe die Klasse auf
 */
$(document).ready(function () {
    if (window.location.href.indexOf("admin") > -1) {
        let chart = new Index();
        chart.init();
    }
});
