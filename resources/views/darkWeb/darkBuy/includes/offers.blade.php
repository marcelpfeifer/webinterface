<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 07.05.2020
 * Time: 19:22
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
<div class="row min-vh-100">
    <div class="col-12">
        <br/>
        @if(count($offers) == 0)
            Keine Angebote
        @else
            <div class="row">
                @foreach($offers as $offer)
                    <hr class="bg-white w-100"/>
                    <hr class="bg-white w-100"/>
                    <div class="col-12">
                        <div class="row">
                            @if($offer->getImageUrl())
                                <div class="col-12">
                                    <img class="img-fluid" style="max-height: 200px" src="{{ $offer->getImageUrl() }}">
                                </div>
                            @endif

                            <div class="col-12">
                                <h2>Beschreibung</h2>
                                {!! $offer->getMessage() !!}
                            </div>
                        </div>
                        <hr class="bg-danger"/>
                        <div class="row">
                            <div class="col-12">
                            </div>

                            <div class="col-12">
                                {{ $offer->getName() }}
                                - {{ $offer->getCreateAt()->format('Y-m-d H:i:s') }}
                            </div>
                        </div>
                        @if($offer->getStatus() === \App\Model\Enum\WebDarkBuy\Status::BOUGHT)
                            <div class="row">
                                <div class="col-12">
                                    <a href="{{ route('darkWeb.darkBuy.call',[
                                        'id' => $offer->getId(),
                                        'hash' => $hash
                                        ]) }}" class="btn-dark p-1 border darkWebBorder darkWebColor">
                                        <i class="fa fa-phone text-success" aria-hidden="true"></i>
                                        Anrufen
                                    </a>
                                </div>
                            </div>
                        @else
                            @if($character->getId() == $offer->getCharacterId())
                                <div class="row">
                                    <div class="col-12">
                                        <form action="{{ route('darkWeb.darkBuy.delete',[
                                        'id' => $offer->getId(),
                                        'hash' => $hash
                                        ]) }}" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE"/>
                                            <button class="btn darkWebBackgroundColor" type="submit">Löschen</button>
                                        </form>
                                    </div>
                                </div>
                            @else
                                <div class="row mt-1">
                                    <div class="col-12">
                                        <a href="{{ route('darkWeb.darkBuy.pay',[
                                        'id' => $offer->getId(),
                                        'hash' => $hash
                                        ]) }}" class="btn-dark p-1 border darkWebBorder darkWebColor">
                                            {{ $offer->getPrice() }}
                                            <i class="fa fa-bitcoin darkWebColor" aria-hidden="true"></i>
                                            Kaufen
                                        </a>
                                    </div>
                                </div>
                            @endif
                        @endif
                    </div>
                @endforeach
            </div>
        @endif

    </div>
</div>
