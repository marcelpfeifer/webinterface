<?php
/**
 * WithWebStock
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 30.08.2021
 * Time: 13:37
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto\WebStockCharacter;


use App\Model\Dto\WebStock;
use App\Model\Dto\WebStockCharacter;

class WithWebStock
{

    /**
     * @var WebStock
     */
    private $webStock;

    /**
     * @var WebStockCharacter
     */
    private $webStockCharacter;

    /**
     * @return WebStock
     */
    public function getWebStock(): WebStock
    {
        return $this->webStock;
    }

    /**
     * @param WebStock $webStock
     * @return WithWebStock
     */
    public function setWebStock(WebStock $webStock): WithWebStock
    {
        $this->webStock = $webStock;
        return $this;
    }

    /**
     * @return WebStockCharacter
     */
    public function getWebStockCharacter(): WebStockCharacter
    {
        return $this->webStockCharacter;
    }

    /**
     * @param WebStockCharacter $webStockCharacter
     * @return WithWebStock
     */
    public function setWebStockCharacter(WebStockCharacter $webStockCharacter): WithWebStock
    {
        $this->webStockCharacter = $webStockCharacter;
        return $this;
    }
}
