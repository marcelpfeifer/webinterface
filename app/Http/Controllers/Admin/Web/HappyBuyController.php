<?php
/**
 * HappyBuyController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 03.05.2020
 * Time: 10:26
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\Web;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Model\WebMarketplaceCategory;
use Illuminate\Http\Request;

class HappyBuyController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $result = [];
        foreach (WebMarketplaceCategory::all() as $entry) {
            $result[] = WebMarketplaceCategory::entryAsDto($entry);
        }

        return view('admin.web.happyBuy.index', [
            'result' => $result,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function save(Request $request)
    {
        $dto = (new \App\Model\Dto\WebMarketplaceCategory())
            ->setName($request->input('name'));

        (new WebMarketplaceCategory())->insertEntry($dto);

        $messages[] = (new Message())
            ->setMessage("Erfolgreich gespeichert")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.web.happyBuy.index')->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Request $request, int $id)
    {
        $dto = (new \App\Model\Dto\WebMarketplaceCategory())
            ->setId($id);
        (new WebMarketplaceCategory())->deleteEntry($dto);

        $messages[] = (new Message())
            ->setMessage("Erfolgreich gelöscht")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.web.happyBuy.index')->with('messages', $messages);
    }
}
