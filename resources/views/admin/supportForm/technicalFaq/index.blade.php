<?php
/**
 * index.blade.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 04.09.2019
 * Time: 21:42
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@php
    /** @var $entry \App\Model\Dto\WebAdminTechnicalFaq */
@endphp
@extends('admin.layout.app')
@section('title')
    <i class="fab fa-wpforms"></i>
    Technisches FAQ
    <a class="btn btn-dark btn-sm float-right"
       href="{{ route('admin.supportForm.technicalFaq.create') }}"
       role="button"
    >
        Eintrag erstellen
    </a>
@endsection
@section('content')
    <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th scope="col">Fall</th>
                <th scope="col">Fehlermeldung</th>
                <th scope="col">Fehlercode</th>
                <th scope="col">Gelöst</th>
                <th scope="col">Eingetragener Supporter</th>
                <th scope="col"></th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($entries as $entry)
                <tr>
                    <th scope="row">{{ $entry['dto']->getId() }}</th>
                    <td>{{ $entry['dto']->getExplanation() }}</td>
                    <td>{{ $entry['dto']->getErrorCode() }}</td>
                    <td>
                        @if($entry['dto']->isFixed())
                            <i class="fas fa-check"></i>
                        @else
                            <i class="fas fa-times"></i>
                        @endif
                    </td>
                    <td>{{ $entry['user']->getUsername() }}</td>
                    <td>
                        <a class="btn btn-primary"
                           href="{{ route('admin.supportForm.technicalFaq.show', $entry['dto']->getId()) }}">Anzeigen</a>
                    </td>
                    <td>
                        @if(!($entry['dto']->isFixed()))
                            <a class="btn btn-dark"
                               href="{{ route('admin.supportForm.technicalFaq.solve', $entry['dto']->getId()) }}">Lösen</a>
                        @endif
                    </td>
                    <td>
                        <form action="{{ route('admin.supportForm.technicalFaq.destroy', $entry['dto']->getId())}}"
                              method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="submit" class="btn btn-danger" value="Löschen"/>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
