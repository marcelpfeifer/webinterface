<?php
/**
 * RepairId
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 13.04.2020
 * Time: 22:34
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Api\Vehicle;

use App\Libraries\Api\AClient;
use App\Model\WebConfig;

class RepairId extends AClient
{
    /**
     * @var string
     */
    const URL = 'vehicle/repairId';

    /**
     * @param int $vehId
     * @return bool
     */
    public static function call(int $vehId): bool
    {
        $response = self::sendRequest('POST', WebConfig::getEntryByKey('api_url')->getValue() . self::URL, [
            'query' => [
                'vehId' => $vehId,
            ]
        ]);

        return $response && $response->getStatusCode() == 200;
    }
}
