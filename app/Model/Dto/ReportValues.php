<?php
/**
 * ReportValues
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 18.09.2020
 * Time: 17:35
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

use Carbon\Carbon;

class ReportValues
{

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var int
     */
    private $caseId = 0;

    /**
     * @var int
     */
    private $targetId = 0;

    /**
     * @var int
     */
    private $distance = 0;

    /**
     * @var Carbon
     */
    private $createdAt;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return ReportValues
     */
    public function setId(?int $id): ReportValues
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCaseId(): int
    {
        return $this->caseId;
    }

    /**
     * @param int $caseId
     * @return ReportValues
     */
    public function setCaseId(int $caseId): ReportValues
    {
        $this->caseId = $caseId;
        return $this;
    }

    /**
     * @return int
     */
    public function getTargetId(): int
    {
        return $this->targetId;
    }

    /**
     * @param int $targetId
     * @return ReportValues
     */
    public function setTargetId(int $targetId): ReportValues
    {
        $this->targetId = $targetId;
        return $this;
    }

    /**
     * @return int
     */
    public function getDistance(): int
    {
        return $this->distance;
    }

    /**
     * @param int $distance
     * @return ReportValues
     */
    public function setDistance(int $distance): ReportValues
    {
        $this->distance = $distance;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon $createdAt
     * @return ReportValues
     */
    public function setCreatedAt(Carbon $createdAt): ReportValues
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}
