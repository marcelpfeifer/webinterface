<?php
/**
 * WebPermissionToWebPermission
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 24.07.2020
 * Time: 18:30
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

/**
 * Class WebPermissionToWebPermission
 * @package App\Model\Dto
 */
class WebPermissionToWebPermission
{

    /**
     * @var int
     */
    private $webPermissionId = 0;

    /**
     * @var int
     */
    private $webPermissionGroupId = 0;

    /**
     * @return int
     */
    public function getWebPermissionId(): int
    {
        return $this->webPermissionId;
    }

    /**
     * @param int $webPermissionId
     * @return WebPermissionToWebPermission
     */
    public function setWebPermissionId(int $webPermissionId): WebPermissionToWebPermission
    {
        $this->webPermissionId = $webPermissionId;
        return $this;
    }

    /**
     * @return int
     */
    public function getWebPermissionGroupId(): int
    {
        return $this->webPermissionGroupId;
    }

    /**
     * @param int $webPermissionGroupId
     * @return WebPermissionToWebPermission
     */
    public function setWebPermissionGroupId(int $webPermissionGroupId): WebPermissionToWebPermission
    {
        $this->webPermissionGroupId = $webPermissionGroupId;
        return $this;
    }
}
