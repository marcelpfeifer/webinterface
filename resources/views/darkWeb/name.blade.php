<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 08.08.2020
 * Time: 13:20
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('darkWeb.layout.app')
@section('url')
    {{  md5('https://eternityV.de/') }}
@endsection
@section('content')
<form method="post" action="{{ route('darkWeb.save', $hash) }}">
@csrf

    <div class="form-row darkWebColor">
        <div class="form-group col-md-6">
            <label for="name">Pseudonym</label>
            <input type="text" class="form-control" id="name" name="name">
        </div>
    </div>
    <br/>
    <div class="form-row">
        <button type="submit" class="btn btn-dark btn-outline-danger darkWebColor">Aktualisieren</button>
    </div>
</form>
@endsection
