<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.10.2021
 * Time: 15:35
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Api\Player\Dimension;

use App\Http\Request\Dto\ADto;

class Save extends ADto
{
    /**
     * @var int
     */
    private $playerId = 0;

    /**
     * @var int
     */
    private $dimension = 0;

    /**
     * @return int
     */
    public function getPlayerId(): int
    {
        return $this->playerId;
    }

    /**
     * @param int $playerId
     * @return Save
     */
    public function setPlayerId(int $playerId): Save
    {
        $this->playerId = $playerId;
        return $this;
    }

    /**
     * @return int
     */
    public function getDimension(): int
    {
        return $this->dimension;
    }

    /**
     * @param int $dimension
     * @return Save
     */
    public function setDimension(int $dimension): Save
    {
        $this->dimension = $dimension;
        return $this;
    }
}
