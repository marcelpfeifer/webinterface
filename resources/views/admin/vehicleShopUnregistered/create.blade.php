<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 29.04.2020
 * Time: 17:25
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-car"></i>
    Eintrag erstellen
@endsection
@section('content')
    <form action="{{ route('admin.vehicleShopUnregistered.store') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="model">Model</label>
            <select class="form-control" name="model" id="model">
                @foreach(array_keys($vehicles) as $vehicle)
                    <option value="{{ $vehicle}}"> {{ $vehicle }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="stock">Anzahl</label>
            <input type="number" class="form-control" id="stock" name="stock" required>
        </div>
        <div class="form-group">
            <label for="posX">Position X</label>
            <input type="text" class="form-control" id="posX" name="posX" required>
        </div>
        <div class="form-group">
            <label for="posY">Position Y</label>
            <input type="text" class="form-control" id="posY" name="posY" required>
        </div>
        <div class="form-group">
            <label for="posZ">Position Z</label>
            <input type="text" class="form-control" id="posZ" name="posZ" required>
        </div>
        <div class="form-group">
            <label for="rotX">Rotation X</label>
            <input type="text" class="form-control" id="rotX" name="rotX" required>
        </div>
        <div class="form-group">
            <label for="rotY">Rotation Y</label>
            <input type="text" class="form-control" id="rotY" name="rotY" required>
        </div>
        <div class="form-group">
            <label for="rotZ">Rotation Z</label>
            <input type="text" class="form-control" id="rotZ" name="rotZ" required>
        </div>
        <div class="form-group">
            <label for="mapId">Map ID</label>
            <input type="number" class="form-control" id="mapId" name="mapId" required>
        </div>
        <div class="form-group">
            <label for="npcPosX">NPC Position X</label>
            <input type="text" class="form-control" id="npcPosX" name="npcPosX" required>
        </div>
        <div class="form-group">
            <label for="npcPosY">NPC Position Y</label>
            <input type="text" class="form-control" id="npcPosY" name="npcPosY" required>
        </div>
        <div class="form-group">
            <label for="npcPosZ">NPC Position Z</label>
            <input type="text" class="form-control" id="npcPosZ" name="npcPosZ" required>
        </div>
        <div class="form-group">
            <label for="npcRotation">NPC Rotation</label>
            <input type="text" class="form-control" id="npcRotation" name="npcRotation" required>
        </div>
        <div class="form-group">
            <label for="npcModel">NPC Model</label>
            <input type="text" class="form-control" id="npcModel" name="npcModel" required>
        </div>
        <br/>
        <button type="submit" class="btn btn-dark">Erstellen</button>
    </form>
@endsection

