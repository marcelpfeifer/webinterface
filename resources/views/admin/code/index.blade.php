<?php
/**
 * index.blade.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 24.08.2019
 * Time: 15:30
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-qrcode"></i>
    Registrierungscode
@endsection
@section('content')
    <div class="row ml-1">
        <a href="{{ route('admin.code.generate') }}" class="btn btn-dark btn-lg active" role="button"
           aria-pressed="true">
            Registrierungscode generieren
        </a>
    </div>
    <br/>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Code</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($codes as $code)
            <tr>
                <th scope="row">{{ $code->id }}</th>
                <td>{{ $code->code }}</td>
                <td>
                    <form action="{{ route('admin.code.delete', $code->id) }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE"/>
                        <button  class="btn btn-danger" type="submit">Löschen</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
