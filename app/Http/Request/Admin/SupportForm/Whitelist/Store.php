<?php
/**
 * Store
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.10.2021
 * Time: 10:45
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Admin\SupportForm\Whitelist;


use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;
use App\Libraries\Common\Enum\Db\Length;
use App\Model\Enum\WebAdminWhitelist\Status;
use App\Model\Enum\WebAdminWhitelistAnswer\Accuracy;
use Carbon\Carbon;
use Illuminate\Validation\Rule;

class Store extends AValidator implements IValidator
{
    /**
     * @return string[]
     */
    public function getRules(): array
    {
        return [
            'username'  => [
                'required',
                'string',
                'max:' . Length::VARCHAR,
            ],
            'status'    => [
                'required',
                'string',
                Rule::in(Status::getConstants()),
            ],
            'retryDate' => [
                'nullable',
                'string',
                'required_if:status,' . Status::DENIED,
                'date_format:Y-m-d',
            ],
            'message'   => [
                'required',
                'string',
                'max:' . Length::TEXT,
            ],
            'answer'    => [
                'array',
            ],
            'answer.*'  => [
                'required',
                'string',
                Rule::in(Accuracy::getConstants()),
            ],
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'username.required'     => 'Der IC Name muss übergeben werden',
            'username.max'          => 'Der IC Name darf nicht so lang sein',
            'status.required'       => 'Der Status muss übergeben werden',
            'retryDate.string'      => 'Das Wiederversuchsdatum muss als String übergeben werden',
            'retryDate.required_if' => 'Das Wiederversuchsdatum muss übergeben werden wenn die Whitelist abgelehnt wird',
            'retryDate.date_format' => 'Das Wiederversuchsdatum muss folgendes Format haben:Y-m-d',
            'message.required'      => 'Die Nachricht muss übergeben werden',
            'message.max'           => 'Die Nachricht ist zu lang',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        $retryDate = $this->request->input('retryDate');
        return (new \App\Http\Request\Dto\Admin\SupportForm\WhiteList\Store())
            ->setUsername($this->request->input('username'))
            ->setStatus($this->request->input('status'))
            ->setRetryDate($retryDate ? Carbon::createFromFormat('Y-m-d', $retryDate) : null)
            ->setMessage($this->request->input('message'))
            ->setAnswer($this->request->input('answer') ?? []);
    }
}
