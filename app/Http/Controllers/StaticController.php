<?php
/**
 * StaticController.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 05.09.2019
 * Time: 18:51
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers;

use Illuminate\View\View;

class StaticController extends Controller
{

    /**
     * @return View
     */
    public function dataProtection(): View
    {
        return view('static.dataProtection');
    }
}
