<?php
/**
 * WebDarkBuyCategory
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 02.05.2020
 * Time: 17:56
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class WebDarkBuyCategory extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webDarkBuyCategory';

    /**
     * @param Dto\WebDarkBuyCategory $dto
     */
    public function insertEntry(Dto\WebDarkBuyCategory $dto)
    {
        $this->insert([
            self::COLUMN_ID   => $dto->getId(),
            self::COLUMN_NAME => $dto->getName(),
            self::CREATED_AT  => Carbon::now(),
            self::UPDATED_AT  => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\WebDarkBuyCategory $dto
     */
    public function updateEntry(Dto\WebDarkBuyCategory $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update([
            self::COLUMN_ID   => $dto->getId(),
            self::COLUMN_NAME => $dto->getName(),
            self::UPDATED_AT  => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\WebDarkBuyCategory $dto
     */
    public function deleteEntry(\App\Model\Dto\WebDarkBuyCategory $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebDarkBuyCategory $entry
     * @return Dto\WebDarkBuyCategory
     */
    public static function entryAsDto(WebDarkBuyCategory $entry): Dto\WebDarkBuyCategory
    {
        return (new Dto\WebDarkBuyCategory())
            ->setId($entry->{self::COLUMN_ID})
            ->setName($entry->{self::COLUMN_NAME});
    }
}
