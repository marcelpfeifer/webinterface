<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 26.07.2020
 * Time: 15:39
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('darkWeb.layout.app')
@section('url')
    {{  md5('https://eternityV.de/video') }}
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <iframe width="100%" height="600px" src="https://www.youtube.com/embed/FbemVRN03OI?controls=0&autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope;" ></iframe>
    </div>
</div>

@endsection
