<?php
/**
 * SetModelController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 23.04.2020
 * Time: 16:00
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Api\Player;


use App\Dto\Message;
use App\Http\Controllers\Api\AController;
use App\Http\Controllers\Api\IController;
use App\Http\Controllers\Controller;
use App\Http\Request\Api\Player\Model\Index;
use App\Http\Request\Api\Player\Model\Save;
use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Libraries\Api\Player\Fly;
use App\Libraries\Api\Player\Online;
use App\Libraries\Api\Player\SetModel;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ModelController extends AController implements IController
{
    /**
     * @param Request $request
     * @return View
     */
    public function index(Request $request): View
    {
        $validator = new Index($request);
        return $this->processIndex($validator, 'api.player.model.index');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function save(Request $request): RedirectResponse
    {
        $validator = new Save($request);
        return $this->processSave($validator, 'api.player.model.index');
    }

    /**
     * @param \App\Http\Request\Dto\Api\Player\Model\Index $requestDto
     * @return array
     */
    public function getIndexData(ADto $requestDto): array
    {
        return [
            'players'       => Online::getPlayers(),
            'currentPlayer' => $requestDto->getCurrentPlayer(),
        ];
    }

    /**
     * @param \App\Http\Request\Dto\Api\Player\Model\Save $requestDto
     * @return bool
     */
    public function execApiCall(ADto $requestDto): bool
    {
        return SetModel::call($requestDto->getPlayerId(), $requestDto->getModel());
    }

    /**
     * @param \App\Http\Request\Dto\Api\Player\Model\Save $requestDto
     * @return array
     */
    public function getRedirectData(ADto $requestDto): array
    {
        return [
            'currentPlayer' => $requestDto->getPlayerId()
        ];
    }
}
