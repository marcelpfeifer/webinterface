<?php
/**
 * WebPermissionToWebPermission
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 24.07.2020
 * Time: 17:55
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class WebPermissionToWebPermission extends Model
{
    /**
     * @var string
     */
    public const COLUMN_WEB_PERMISSION_ID = 'webPermissionId';

    /**
     * @var string
     */
    public const COLUMN_WEB_PERMISSION_GROUP_ID = 'webPermissionGroupId';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'webPermissionToWebPermission';

    /**
     * @param int $permissionGroupId
     * @return int[]
     */
    public function getWebPermissionsIdsByPermissionGroupId(int $permissionGroupId)
    {
        $entries = $this
            ->where(self::COLUMN_WEB_PERMISSION_GROUP_ID, $permissionGroupId)
            ->get();
        $result = [];
        foreach ($entries as $entry) {
            $dto = self::entryAsDto($entry);
            $result[] = $dto->getWebPermissionId();
        }
        return $result;
    }

    /**
     * @param Dto\WebPermissionToWebPermission $dto
     */
    public function insertEntry(Dto\WebPermissionToWebPermission $dto)
    {
        $this->insert(
            [
                self::COLUMN_WEB_PERMISSION_ID       => $dto->getWebPermissionId(),
                self::COLUMN_WEB_PERMISSION_GROUP_ID => $dto->getWebPermissionGroupId(),
            ]
        );
    }

    /**
     * @param Dto\WebPermissionToWebPermission $dto
     */
    public function deleteEntry(\App\Model\Dto\WebPermissionToWebPermission $dto)
    {
        $this->where(self::COLUMN_WEB_PERMISSION_GROUP_ID, $dto->getWebPermissionGroupId())->delete();
    }

    /**
     * @param WebPermissionToWebPermission $entry
     * @return Dto\WebPermissionToWebPermission
     */
    public static function entryAsDto(WebPermissionToWebPermission $entry): Dto\WebPermissionToWebPermission
    {
        return (new Dto\WebPermissionToWebPermission())
            ->setWebPermissionId($entry->{self::COLUMN_WEB_PERMISSION_ID})
            ->setWebPermissionGroupId($entry->{self::COLUMN_WEB_PERMISSION_GROUP_ID});
    }
}
