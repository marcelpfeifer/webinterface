<?php
/**
 * SystemStatus
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 11.08.2021
 * Time: 16:30
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Console\Commands;

use App\Libraries\Api\Server\System\Status;
use App\Model\Dto\WebServerStatus;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SystemStatus extends Command
{
    /**
     * @var string
     */
    protected $signature = 'system:status';

    /**
     * @var string
     */
    protected $description = 'Fragt den aktullen Server Status an und fügt diesen in die Tabelle';

    public function handle()
    {
        $model = new \App\Model\WebServerStatus();
        // Lösche alte Einträge
        $model->deleteOldEntries();

        $status = Status::call();

        if ($status) {
            $dto = (new WebServerStatus())
                ->setRamMax($status->RamMax)
                ->setRamFree($status->RamFree)
                ->setCpu($status->Cpu)
                ->setAltv($status->Server->altv)
                ->setAltvTest($status->Server->altv_test)
                ->setTs($status->Server->ts)
                ->setForum(\App\Libraries\Api\Forum\Status::call());
            $model->insertEntry($dto);
        }
    }
}
