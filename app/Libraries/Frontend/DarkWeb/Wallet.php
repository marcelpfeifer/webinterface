<?php
/**
 * Wallet
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 16.08.2020
 * Time: 15:49
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\DarkWeb;

use App\Http\Request\Dto\DarkWeb\Wallet\Exchange;
use App\Http\Request\Dto\DarkWeb\Wallet\Transfer;
use App\Libraries\Api\Player\SubBitcoin;
use App\Libraries\Character\Payment\Bitcoin;
use App\Libraries\Character\Payment\BitcoinIllegal;
use App\Model\Dto\Cashflow;
use App\Model\Dto\Character;
use App\Model\Dto\Transactions;
use App\Model\Enum\Cashflow\Type;

class Wallet
{


    /**
     * @var int
     */
    const PRICE_CREATE_WALLET = 6;

    /**
     * @param Character $dto
     * @return bool
     */
    public static function create(Character $dto): bool
    {
        if ($dto->getBitcoin() < self::PRICE_CREATE_WALLET) {
            return false;
        }

        $dto->setBitcoin($dto->getBitcoin() - self::PRICE_CREATE_WALLET);
        $dto->setBitcoinHashIllegal(md5($dto->getId() . $dto->getName() . 'illegalHash'));
        (new \App\Model\Character())->updateEntry($dto);
        SubBitcoin::call($dto->getId(), self::PRICE_CREATE_WALLET, true);
        // Schreibe Log Einträge
        $cashFlow = (new Cashflow())
            ->setType(Type::BITCOIN)
            ->setAmount(-self::PRICE_CREATE_WALLET)
            ->setGuid($dto->getId())
            ->setReason('createBitcoinIllegalWallet')
            ->setSource(-1);
        (new \App\Model\Cashflow())->insertEntry($cashFlow);
        return true;
    }

    /**
     * @param Character $dto
     * @param Transfer $transferDto
     */
    public static function transfer(Character $dto, Transfer $transferDto): bool
    {
        if ($dto->getBitcoinIllegal() < $transferDto->getAmount()) {
            return false;
        }
        $receiverDto = (new \App\Model\Character())->getEntryByIllegalBitcoinHash($transferDto->getHash());
        if (!$receiverDto) {
            return false;
        }
        if ($receiverDto->getId() === $dto->getId()) {
            return false;
        }
        return BitcoinIllegal::transfer($dto, $receiverDto, $transferDto->getAmount(), 'darkWebWalletTransfer');
    }

    /**
     * @param Character $dto
     * @param Exchange $transferDto
     */
    public static function exchange(Character $dto, Exchange $transferDto): bool
    {
        if ($dto->getBitcoin() < $transferDto->getAmount()) {
            return false;
        }

        Bitcoin::sub($dto, $transferDto->getAmount(), 'darkWebWalletExchange');
        BitcoinIllegal::add($dto, $transferDto->getAmount(), 'darkWebWalletExchange');
        return true;
    }
}
