<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 09.01.2021
 * Time: 17:13
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
<nav class="navbar navbar-expand bg-primary navbar-light shadow">
    <a class="navbar-brand" href="{{ route('web.marketplace.index', $hash)}}">
        <i class="fas fa-shopping-cart"></i>
        Marketplace
    </a>
    <div class="container-fluid" id="navbarNav">
        <ul class="nav navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('web.marketplace.index', $hash)}}">Startseite</a>
            </li>
            @foreach($categories as $category)
                <li class="nav-item">
                    <a class="nav-link"
                       href="{{ route('web.marketplace.category', ['id'=> $category->getId(),'hash' => $hash])}}">
                        {{ $category->getName() }}
                    </a>
                </li>
            @endforeach
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('web.marketplace.offers', $hash) }}">Meine Angebote</a>
            </li>
            <li class="nav-item">
                <a class="btn text-white btn-primary nav-link" href="{{ route('web.marketplace.create', $hash) }}">
                    Angebote erstellen
                </a>
            </li>
        </ul>
    </div>
</nav>
