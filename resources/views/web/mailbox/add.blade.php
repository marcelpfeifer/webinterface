<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 12.08.2021
 * Time: 12:49
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('web.layout.app')
@section('url')
    https://eternityV.de/mail/hinzufügen
@endsection
@section('content')
    <div class="container bg-white text-dark pb-2">
        <div class="row bg-dark p-2">
            <div class="col text-white">
                <i class="fas fa-envelope"></i>
                Neue Email-Adresse hinzufügen
            </div>
            <div class="col justify-content-end row">
                <div class="col-3">
                    <a class="btn btn-secondary btn-sm float-right"
                       href="{{ route('web.mailbox.index', $hash) }}"
                       role="button"
                    >
                        Zurück
                    </a>
                </div>
            </div>
        </div>
        <br/>
        @include('layouts.messages')
        <form action="{{ route('web.mailbox.addSave', $hash) }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" id="email" name="email" autocomplete="off"
                       placeholder="max.mustermann@eternity.de" required>
            </div>
            <div class="form-group">
                <label for="password">Passwort</label>
                <input type="password" class="form-control" id="password" name="password" autocomplete="off" required>
            </div>
            <button type="submit" class="btn btn-primary">Hinzufügen</button>
        </form>
    </div>
@endsection
