<?php
/**
 * Result
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 13.10.2021
 * Time: 14:23
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Dto\Admin\SupportForm\Whitelist\Index;


use App\Model\Dto\Account;
use App\Model\Dto\WebAdminWhitelist;

class Result
{

    /**
     * @var WebAdminWhitelist
     */
    private $entry;

    /**
     * @var Account
     */
    private $account;

    /**
     * @return WebAdminWhitelist
     */
    public function getEntry(): WebAdminWhitelist
    {
        return $this->entry;
    }

    /**
     * @param WebAdminWhitelist $entry
     * @return Result
     */
    public function setEntry(WebAdminWhitelist $entry): Result
    {
        $this->entry = $entry;
        return $this;
    }

    /**
     * @return Account
     */
    public function getAccount(): Account
    {
        return $this->account;
    }

    /**
     * @param Account $account
     * @return Result
     */
    public function setAccount(Account $account): Result
    {
        $this->account = $account;
        return $this;
    }
}
