<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 06.01.2021
 * Time: 13:24
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-ban"></i>
    Eintrag erstellen
    <a class="btn btn-dark btn-sm float-right"
       href="{{ route('admin.ban.points.index') }}"
       role="button"
    >
        Zurück
    </a>
@endsection
@section('content')
    @if(isset($dto))
        <form action="{{ route('admin.ban.points.update', $dto->getId()) }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ $dto->getName() }}" required>
            </div>
            <div class="form-group">
                <label for="points">Punkte</label>
                <input type="number" class="form-control" id="points" name="points" value="{{ $dto->getPoints() }}"
                       required>
            </div>
            <div class="form-group">
                <label for="length">Länge (in Stunden)</label>
                <input type="number" class="form-control" id="length" name="length" value="{{ $dto->getLength() }}"
                       required>
                <p class="text-small"> 0 = PERMA</p>
            </div>
            <div class="form-group">
                <label for="additional">Zusatz (innerhalb eines Monates)</label>
                <input type="number" class="form-control" id="additional" name="additional"
                       value="{{ $dto->getAdditional() }}"
                       required>
            </div>
            <br/>
            <button type="submit" class="btn btn-dark">Erstellen</button>
        </form>
    @else
        Eintrag wurde nicht gefunden
    @endif
@endsection
