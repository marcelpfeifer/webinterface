<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class WebAdminLoggingApi extends Model
{

    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_ACCOUNT_ID = 'accountId';

    /**
     * @var string
     */
    const COLUMN_URL = 'url';

    /**
     * @var string
     */
    const COLUMN_OPTIONS = 'options';

    /**
     * @var string
     */
    const COLUMN_DATE_TIME = 'dateTime';

    /**
     * @var string
     */
    protected $table = 'webAdminLoggingApi';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $dates = [
        self::COLUMN_DATE_TIME
    ];

    /**
     * @return LengthAwarePaginator
     */
    public function getLogEntriesPagination(): LengthAwarePaginator
    {
        return $this->orderBy(self::COLUMN_DATE_TIME, 'DESC')->paginate(15);
    }

    /**
     * @param Dto\WebAdminLoggingApi $dto
     */
    public function insertEntry(Dto\WebAdminLoggingApi $dto)
    {
        return $this->insertGetId(
            [
                self::COLUMN_ACCOUNT_ID => $dto->getAccountId(),
                self::COLUMN_URL        => $dto->getUrl(),
                self::COLUMN_OPTIONS    => $dto->getOptions(),
                self::COLUMN_DATE_TIME  => $dto->getDateTime(),
            ]
        );
    }

    /**
     * @param WebAdminLoggingApi $entry
     * @return Dto\WebAdminLoggingApi
     */
    public static function entryAsDto(WebAdminLoggingApi $entry): Dto\WebAdminLoggingApi
    {
        return (new Dto\WebAdminLoggingApi())
            ->setId($entry->{self::COLUMN_ID})
            ->setAccountId($entry->{self::COLUMN_ACCOUNT_ID})
            ->setUrl($entry->{self::COLUMN_URL})
            ->setOptions($entry->{self::COLUMN_OPTIONS})
            ->setDateTime($entry->{self::COLUMN_DATE_TIME});
    }
}
