<?php
/**
 * MailController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 12.08.2021
 * Time: 11:54
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Auth\Mail;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Http\Request\Auth\Mail\Mail\Save;
use App\Model\Character;
use App\Model\WebMail;
use App\Model\WebMailAddress;
use App\Model\WebMailAddressToAccount;
use App\Model\WebMailAttachment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class MailController extends Controller
{

    /**
     * @param Request $request
     * @param int $webMailAddressId
     * @return View
     */
    public function index(Request $request, int $webMailAddressId): View
    {
        $mailAddress = $this->getMailBoxEntry($webMailAddressId);
        if (!$mailAddress) {
            return view('404');
        }

        $mails = (new WebMail())->getAllMailsByTo($mailAddress->getId());

        return view(
            'auth.mailbox.mail.index',
            [
                'dto'              => $mailAddress,
                'mails'            => $mails,
                'webMailAddressId' => $webMailAddressId,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $webMailAddressId
     * @return View
     */
    public function outgoing(Request $request, int $webMailAddressId): View
    {
        $mailAddress = $this->getMailBoxEntry($webMailAddressId);
        if (!$mailAddress) {
            return view('404');
        }

        $mails = (new WebMail())->getAllMailsByFrom($mailAddress->getId());

        return view(
            'auth.mailbox.mail.outgoing',
            [
                'dto'              => $mailAddress,
                'mails'            => $mails,
                'webMailAddressId' => $webMailAddressId,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $webMailAddressId
     * @return View
     */
    public function create(Request $request, int $webMailAddressId): View
    {
        $mailAddress = $this->getMailBoxEntry($webMailAddressId);
        if (!$mailAddress) {
            return view('404');
        }

        return view(
            'auth.mailbox.mail.create',
            [
                'dto'              => $mailAddress,
                'webMailAddressId' => $webMailAddressId,
            ]
        );
    }

    /**
     * @return View
     */
    public function reply(Request $request, int $webMailAddressId, int $id): View
    {
        $mailAddress = $this->getMailBoxEntry($webMailAddressId);
        if (!$mailAddress) {
            return view('404');
        }


        // Überprüfung ob es die Mail überhaupt gibt
        $mail = WebMail::find($id);
        if (!($mail)) {
            return view('404');
        }
        $mailDto = WebMail::entryAsDto($mail);

        // Schaut ob die Mail auch wirklich an dieses Postfach ging
        if (!$this->checkIfMailIsAllowed($mailDto, $mailAddress)) {
            return view('404');
        }

        return view(
            'auth.mailbox.mail.reply',
            [
                'dto'              => $mailAddress,
                'mail'             => $mailDto,
                'webMailAddressId' => $webMailAddressId,
            ]
        );
    }

    /**
     * @return View
     */
    public function share(Request $request, int $webMailAddressId, int $id): View
    {
        $mailAddress = $this->getMailBoxEntry($webMailAddressId);
        if (!$mailAddress) {
            return view('404');
        }

        // Überprüfung ob es die Mail überhaupt gibt
        $mail = WebMail::find($id);
        if (!($mail)) {
            return view('404');
        }
        $mailDto = WebMail::entryAsDto($mail);

        // Schaut ob die Mail auch wirklich an dieses Postfach ging
        if (!$this->checkIfMailIsAllowed($mailDto, $mailAddress)) {
            return view('404');
        }

        return view(
            'auth.mailbox.mail.share',
            [
                'dto'              => $mailAddress,
                'mail'             => $mailDto,
                'webMailAddressId' => $webMailAddressId,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $webMailAddressId
     * @param int $id
     * @return View
     */
    public function show(Request $request, int $webMailAddressId, int $id): View
    {
        $mailAddress = $this->getMailBoxEntry($webMailAddressId);
        if (!$mailAddress) {
            return view('404');
        }
        // Überprüfung ob es die Mail überhaupt gibt
        $mail = WebMail::find($id);
        if (!($mail)) {
            return view('404');
        }
        $mailDto = WebMail::entryAsDto($mail);

        // Schaut ob die Mail auch wirklich an dieses Postfach ging
        if (!$this->checkIfMailIsAllowed($mailDto, $mailAddress)) {
            return view('404');
        }
        // Setze Mail auf gesehen
        $mailDto->setSeen(true);
        (new WebMail())->updateEntry($mailDto);

        $attachments = [];
        foreach ($mail->attachments as $attachment) {
            $attachments[] = WebMailAttachment::entryAsDto($attachment);
        }

        return view(
            'auth.mailbox.mail.show',
            [
                'dto'              => $mailAddress,
                'attachments'      => $attachments,
                'mail'             => $mailDto,
                'webMailAddressId' => $webMailAddressId,
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request, int $webMailAddressId)
    {
        if (!$this->getMailBoxEntry($webMailAddressId)) {
            return redirect()->route('auth.mailbox.index');
        }

        // Validiere die Anfrage
        $validator = new Save($request, $webMailAddressId);
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            return redirect()->route('auth.mailbox.mail.create', $webMailAddressId)
                ->with('messages', $validator->getErrorsAsMessageArray($validatedData))
                ->withInput()
                ->withErrors($validatedData->errors());
        }

        \App\Http\Model\Auth\Mail\Mail\Save::save($validator->toDto());

        $messages[] = (new Message())
            ->setMessage('Mail wurde erfolgreich versendet!')
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('auth.mailbox.mail.index', $webMailAddressId)->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request, int $webMailAddressId, int $id)
    {
        $entry = $this->getMailBoxEntry($webMailAddressId);
        if (!$entry) {
            return redirect()->route('auth.mailbox.index');
        }

        // Überprüfung ob es die Mail überhaupt gibt
        $mail = WebMail::find($id);
        if (!($mail)) {
            $messages[] = (new Message())
                ->setMessage('Mail konnte nicht gelöscht werden!')
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
            return redirect()->route('auth.mailbox.mail.index', $webMailAddressId)->with('messages', $messages);
        }
        $dto = WebMail::entryAsDto($mail);

        if (!$this->checkIfMailIsAllowed($dto, $entry)) {
            $messages[] = (new Message())
                ->setMessage('Mail konnte nicht gelöscht werden!')
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
            return redirect()->route('auth.mailbox.mail.index', $webMailAddressId)->with('messages', $messages);
        }

        (new WebMail())->deleteEntry($dto);

        $messages[] = (new Message())
            ->setMessage('Mail wurde erfolgreich gelöscht!')
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('auth.mailbox.mail.index', $webMailAddressId)->with('messages', $messages);
    }


    /**
     * @param \App\Model\Dto\WebMail $mailDto
     * @param \App\Model\Dto\WebMailAddress $mailAddressDto
     */
    private function checkIfMailIsAllowed(
        \App\Model\Dto\WebMail $mailDto,
        \App\Model\Dto\WebMailAddress $mailAddressDto
    ): bool {
        return ($mailAddressDto->getId() === $mailDto->getTo())
            || ($mailAddressDto->getId() === $mailDto->getFrom() && $mailDto->getTo() === null);
    }

    /**
     * @param int $webMailAddressId
     * @return \App\Model\Dto\WebMailAddress|null
     */
    private function getMailBoxEntry(int $webMailAddressId)
    {
        $entry = (new WebMailAddressToAccount())->getEntryByAddressIdAndAccountId(
            $webMailAddressId,
            Auth::id()
        );
        return $entry ? WebMailAddress::entryAsDto($entry->webMailAddress) : null;
    }
}
