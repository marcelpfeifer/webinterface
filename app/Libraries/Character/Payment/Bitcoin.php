<?php
/**
 * Bitcoin
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 23.08.2020
 * Time: 10:47
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Character\Payment;


use App\Libraries\Api\Player\AddBitcoin;
use App\Libraries\Api\Player\SubBitcoin;
use App\Model\Dto\Cashflow;
use App\Model\Dto\Character;
use App\Model\Dto\Transactions;
use App\Model\Enum\Cashflow\Type;

class Bitcoin
{
    /**
     * @param Character $payerDto
     * @param Character $receiverDto
     * @param int $price
     * @param string $reason
     * @return bool
     */
    public static function transfer(Character $payerDto, Character $receiverDto, int $price, string $reason): bool
    {
        // Ziehe dem Käufer das Geld ab
        self::sub($payerDto, $price, $reason, $receiverDto->getId());

        // Gebe dem Ersteller das Geld
        self::add($receiverDto, $price, $reason, $payerDto->getId());

        $transaction = (new Transactions())
            ->setTx($payerDto->getId())
            ->setRx($receiverDto->getId())
            ->setType(\App\Model\Enum\Transactions\Type::BITCOIN)
            ->setAmount($price);
        (new \App\Model\Transactions())->insertEntry($transaction);
        return true;
    }

    /**
     * @param Character $dto
     * @param int $price
     * @param string $reason
     * @param string $source
     */
    public static function sub(Character $dto, int $price, string $reason, string $source = '-1')
    {
        // Ziehe dem Käufer das Geld ab
        SubBitcoin::call($dto->getId(), $price, true);
        $dto->setBitcoin($dto->getBitcoin() - $price);
        (new \App\Model\Character())->updateEntry($dto);

        $cashFlow = (new Cashflow())
            ->setType(Type::BITCOIN)
            ->setAmount(-$price)
            ->setGuid($dto->getId())
            ->setReason($reason)
            ->setSource($source);
        (new \App\Model\Cashflow())->insertEntry($cashFlow);
    }

    /**
     * @param Character $dto
     * @param int $price
     * @param string $reason
     * @param string $source
     */
    public static function add(Character $dto, int $price, string $reason, string $source = '-1')
    {
        // Ziehe dem Käufer das Geld ab
        AddBitcoin::call($dto->getId(), $price, true);
        $dto->setBitcoin($dto->getBitcoin() + $price);
        (new \App\Model\Character())->updateEntry($dto);

        $cashFlow = (new Cashflow())
            ->setType(Type::BITCOIN)
            ->setAmount($price)
            ->setGuid($dto->getId())
            ->setReason($reason)
            ->setSource($source);
        (new \App\Model\Cashflow())->insertEntry($cashFlow);
    }
}
