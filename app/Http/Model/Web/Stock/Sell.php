<?php
/**
 * Buy
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 02.09.2021
 * Time: 17:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Model\Web\Stock;


use App\Dto\Message;
use App\Model\Character;
use App\Model\Dto\Cashflow;
use App\Model\Dto\WebStockCharacter;
use App\Model\Enum\Cashflow\Type;
use App\Model\WebStock;
use Carbon\Carbon;

class Sell
{
    /**
     * @param \App\Http\Request\Dto\Web\Stock\Sell $requestDto
     * @return Message[]
     */
    public static function save(\App\Http\Request\Dto\Web\Stock\Sell $requestDto): array
    {
        $messages = [];

        $webStock = WebStock::find($requestDto->getWebStockCharacter()->getWebStockId());
        if (!($webStock)) {
            $messages[] = (new Message())
                ->setMessage("Aktien konnte nicht verkauft werden.")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
        }
        $webStockDto = WebStock::entryAsDto($webStock);
        $sellPrice = $webStockDto->getPrice() * $requestDto->getAmount();

        // Füge die Steuern hinzu falls die Aktie erst vor kurzen gekauft wurde
        if ($requestDto->getWebStockCharacter()->getCreatedAt()->diffInDays(Carbon::now()) < 30) {
            $sellPrice -= 0.05 * $sellPrice;
        }

        // Füge das Geld dem Character hinzu
        $characterDto = $requestDto->getCharacter()->setBank(
            $requestDto->getCharacter()->getBank() + $sellPrice
        );
        (new Character())->updateEntry($characterDto);
        $cashFlow = (new Cashflow())
            ->setType(Type::BANK)
            ->setAmount($sellPrice)
            ->setGuid($characterDto->getId())
            ->setReason('stockSell')
            ->setSource('-1');
        (new \App\Model\Cashflow())->insertEntry($cashFlow);

        // Aktualisiere den Stock
        (new WebStock())->addStock($requestDto->getWebStockCharacter()->getWebStockId(), $requestDto->getAmount());

        if ($requestDto->getAmount() == $requestDto->getWebStockCharacter()->getAmount()) {
            (new \App\Model\WebStockCharacter())->deleteEntry($requestDto->getWebStockCharacter());
        } else {
            $pricePerShare = $requestDto->getWebStockCharacter()->getTotalPrice() / $requestDto->getWebStockCharacter(
                )->getAmount();
            $newTotalPrice = $requestDto->getWebStockCharacter()->getTotalPrice(
                ) - $pricePerShare * $requestDto->getAmount();
            $requestDto->getWebStockCharacter()->setAmount(
                $requestDto->getWebStockCharacter()->getAmount() - $requestDto->getAmount()
            )->setTotalPrice($newTotalPrice);
            (new \App\Model\WebStockCharacter())->updateEntry($requestDto->getWebStockCharacter());
        }

        $messages[] = (new Message())
            ->setMessage("Aktien wurden erfolgreich verkauft.")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return $messages;
    }
}
