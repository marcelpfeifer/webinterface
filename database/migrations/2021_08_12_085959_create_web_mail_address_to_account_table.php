<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebMailAddressToAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'webMailAddressToAccount',
            function (Blueprint $table) {
                $table->unsignedBigInteger('webMailAddressId')->index();
                $table->unsignedInteger('accountId')->index();
                $table->foreign('webMailAddressId')->references('id')->on('webMailAddress')->onDelete('cascade');
                $table->boolean('owner');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webMailAddressToAccount');
    }
}
