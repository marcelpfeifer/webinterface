<?php
/**
 * ShopUnregisteredVehicles
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 11.10.2021
 * Time: 12:24
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ShopUnregisteredVehicles extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_MODEL = 'model';

    /**
     * @var string
     */
    const COLUMN_STOCK = 'stock';

    /**
     * @var string
     */
    const COLUMN_POS_X = 'posx';

    /**
     * @var string
     */
    const COLUMN_POS_Y = 'posy';

    /**
     * @var string
     */
    const COLUMN_POS_Z = 'posz';

    /**
     * @var string
     */
    const COLUMN_ROT_X = 'rotx';

    /**
     * @var string
     */
    const COLUMN_ROT_Y = 'roty';

    /**
     * @var string
     */
    const COLUMN_ROT_Z = 'rotz';

    /**
     * @var string
     */
    const COLUMN_MAP_ID = 'mapId';

    /**
     * @var string
     */
    const COLUMN_NPC_POS_X = 'npc_posx';

    /**
     * @var string
     */
    const COLUMN_NPC_POS_Y = 'npc_posy';

    /**
     * @var string
     */
    const COLUMN_NPC_POS_Z = 'npc_posz';

    /**
     * @var string
     */
    const COLUMN_NPC_ROT = 'npc_rot';

    /**
     * @var string
     */
    const COLUMN_NPC_MODEL = 'npc_model';

    /**
     * @var string
     */
    const COLUMN_CREATED_AT = 'createdAt';

    /**
     * @var string
     */
    const COLUMN_UPDATED_AT = 'updatedAt';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $dates = [
        self::COLUMN_CREATED_AT,
        self::COLUMN_UPDATED_AT,
    ];

    /**
     * @var string
     */
    protected $table = 'shop_unregistered_vehicles';

    /**
     * @param Dto\ShopUnregisteredVehicles $dto
     * @return bool
     */
    public function insertEntry(Dto\ShopUnregisteredVehicles $dto): bool
    {
        return $this->insert(
            [
                self::COLUMN_MODEL      => $dto->getModel(),
                self::COLUMN_STOCK      => $dto->getStock(),
                self::COLUMN_POS_X      => $dto->getPosX(),
                self::COLUMN_POS_Y      => $dto->getPosY(),
                self::COLUMN_POS_Z      => $dto->getPosZ(),
                self::COLUMN_ROT_X      => $dto->getRotX(),
                self::COLUMN_ROT_Y      => $dto->getRotY(),
                self::COLUMN_ROT_Z      => $dto->getRotZ(),
                self::COLUMN_MAP_ID     => $dto->getMapId(),
                self::COLUMN_NPC_POS_X  => $dto->getNpcPosX(),
                self::COLUMN_NPC_POS_Y  => $dto->getNpcPosY(),
                self::COLUMN_NPC_POS_Z  => $dto->getNpcPosZ(),
                self::COLUMN_NPC_ROT    => $dto->getNpcRot(),
                self::COLUMN_NPC_MODEL  => $dto->getNpcModel(),
                self::COLUMN_CREATED_AT => Carbon::now(),
                self::COLUMN_UPDATED_AT => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\ShopUnregisteredVehicles $dto
     * @return bool
     */
    public function updateEntry(Dto\ShopUnregisteredVehicles $dto): bool
    {
        return $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_MODEL      => $dto->getModel(),
                self::COLUMN_STOCK      => $dto->getStock(),
                self::COLUMN_POS_X      => $dto->getPosX(),
                self::COLUMN_POS_Y      => $dto->getPosY(),
                self::COLUMN_POS_Z      => $dto->getPosZ(),
                self::COLUMN_ROT_X      => $dto->getRotX(),
                self::COLUMN_ROT_Y      => $dto->getRotY(),
                self::COLUMN_ROT_Z      => $dto->getRotZ(),
                self::COLUMN_MAP_ID     => $dto->getMapId(),
                self::COLUMN_NPC_POS_X  => $dto->getNpcPosX(),
                self::COLUMN_NPC_POS_Y  => $dto->getNpcPosY(),
                self::COLUMN_NPC_POS_Z  => $dto->getNpcPosZ(),
                self::COLUMN_NPC_ROT    => $dto->getNpcRot(),
                self::COLUMN_NPC_MODEL  => $dto->getNpcModel(),
                self::COLUMN_UPDATED_AT => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\ShopUnregisteredVehicles $dto
     */
    public function deleteEntry(\App\Model\Dto\ShopUnregisteredVehicles $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param ShopUnregisteredVehicles $entry
     * @return Dto\ShopUnregisteredVehicles
     */
    public static function entryAsDto(ShopUnregisteredVehicles $entry): Dto\ShopUnregisteredVehicles
    {
        return (new Dto\ShopUnregisteredVehicles())
            ->setId($entry->{self::COLUMN_ID})
            ->setModel($entry->{self::COLUMN_MODEL})
            ->setStock($entry->{self::COLUMN_STOCK})
            ->setPosX($entry->{self::COLUMN_POS_X})
            ->setPosY($entry->{self::COLUMN_POS_Y})
            ->setPosZ($entry->{self::COLUMN_POS_Z})
            ->setRotX($entry->{self::COLUMN_ROT_X})
            ->setRotY($entry->{self::COLUMN_ROT_Y})
            ->setRotZ($entry->{self::COLUMN_ROT_Z})
            ->setMapId($entry->{self::COLUMN_MAP_ID})
            ->setNpcPosX($entry->{self::COLUMN_NPC_POS_X})
            ->setNpcPosY($entry->{self::COLUMN_NPC_POS_Y})
            ->setNpcPosZ($entry->{self::COLUMN_NPC_POS_Z})
            ->setNpcRot($entry->{self::COLUMN_NPC_ROT})
            ->setNpcModel($entry->{self::COLUMN_NPC_MODEL})
            ->setCreatedAt($entry->{self::CREATED_AT})
            ->setUpdatedAt($entry->{self::UPDATED_AT});
    }
}
