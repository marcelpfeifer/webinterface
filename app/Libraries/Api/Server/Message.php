<?php
/**
 * Message
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 11.04.2020
 * Time: 14:08
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Api\Server;

use App\Libraries\Api\AClient;
use App\Model\WebConfig;

class Message extends AClient
{
    /**
     * @var string
     */
    const URL = 'server/message';

    /**
     * @param string $message
     * @return bool
     */
    public static function call(string $message): bool
    {
        $response = self::sendRequest('POST', WebConfig::getEntryByKey('api_url')->getValue() . self::URL, [
            'query' => [
                'message' => $message,
            ]
        ]);
        return $response && $response->getStatusCode() == 200;
    }
}
