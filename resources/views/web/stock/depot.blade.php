<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 29.08.2021
 * Time: 16:23
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

/**
 * @var $depot \App\Libraries\Frontend\Dto\Web\Stock\Depot
 */
?>

@extends('web.layout.app')
@section('url')
    https://eternityV.de/börse/depot
@endsection
@section('content')
    <div class="container transparentBg bg-white text-dark min-vh-100">
        @include('web.stock.component.nav',[])
        <div class="row text-center">
            <div class="col-6">
                Kaufwert:
            </div>
            <div class="col-6">
                {{ $depot->getTotal()->getTotal() }}$
            </div>
        </div>
        <div class="row text-center">
            <div class="col-6">
                Gewinn:
            </div>
            <div class="col-6">
                {{ $depot->getTotal()->getGained() }}$
            </div>
        </div>
        <div class="row text-center">
            <div class="col-6">
                Gewinn in %:
            </div>
            <div class="col-6 {{ ($depot->getTotal()->getGainedPercent() > 0) ? 'text-success' :''}} {{ ($depot->getTotal()->getGainedPercent() < 0) ? 'text-danger' :''}}">
                {{ $depot->getTotal()->getGainedPercent() }}%
            </div>
        </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Kurs</th>
                <th scope="col">Menge</th>
                <th scope="col">
                    Kaufkurs<br/>
                    Kaufwert<br/>
                </th>
                <th scope="col">
                    Wert $<br/>
                    G/V $<br/>
                    G/V %<br/>
                </th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($depot->getEntries() as $stock)
                <tr>
                    <td>{{ $stock->getName() }}</td>
                    <td>{{ \App\Libraries\Common\Price::format($stock->getPrice()) }}$</td>
                    <td></td>
                    <td>
                        {{ \App\Libraries\Common\Price::format($stock->getBuyPrice()) }}$ <br/>
                        {{ \App\Libraries\Common\Price::format($stock->getBuyTotalPrice()) }}$
                    </td>
                    <td>
                        {{ \App\Libraries\Common\Price::format($stock->getCurrentTotalValue())}}$ <br/>
                        {{  \App\Libraries\Common\Price::format($stock->getCurrentValue())}}$ <br/>
                        <span
                            class="{{ ($stock->getCurrentValuePercent() > 0) ? 'text-success' :''}} {{ ($stock->getCurrentValuePercent() < 0) ? 'text-danger' :''}}">
                            {{ \App\Libraries\Common\Price::format($stock->getCurrentValuePercent())}}%
                        </span>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                @foreach($stock->getEntries() as $entry)
                    <tr class="small">
                        <td></td>
                        <td></td>
                        <td>
                            {{ $entry->getAmount() }}
                        </td>
                        <td>
                            {{ $entry->getBuyPrice() }}$ <br/>
                            {{ $entry->getBuyTotalPrice() }}$
                        </td>
                        <td>
                            {{ $entry->getCurrentTotalValue()}}$ <br/>
                            {{ $entry->getCurrentValue()}}$ <br/>
                            <span
                                class="{{ ($entry->getCurrentValuePercent() > 0) ? 'text-success' :''}} {{ ($entry->getCurrentValuePercent() < 0) ? 'text-danger' :''}}">
                            {{ $entry->getCurrentValuePercent()}}%
                        </span>
                        </td>
                        <td>
                            <a href="{{ route('web.stock.buy',[$stock->getId(), $hash]) }}" class="btn btn-primary">
                                Kaufen
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('web.stock.sell',[$entry->getId(), $hash]) }}" class="btn btn-danger">
                                Verkaufen
                            </a>
                        </td>
                    </tr>
                @endforeach
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
