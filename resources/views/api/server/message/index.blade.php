<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 11.04.2020
 * Time: 14:12
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('api.layout.app')
@section('title')
    <i class="fas fa-bullhorn"></i>
    Verschickt eine Nachricht an alle Spieler auf dem Server
@endsection
@section('content')
    <form method="post" action="{{ route('api.server.message.save') }}">
        {{ csrf_field() }}

        <div class="form-group">
            <label for="message">Nachricht</label>
            <textarea class="form-control" id="message" name="message" rows="3"></textarea>
        </div>
        <button type="submit" class="btn btn-danger">Bestätigen</button>
    </form>
    <br/>
@endsection
