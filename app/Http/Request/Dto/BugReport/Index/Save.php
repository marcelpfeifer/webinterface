<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.08.2021
 * Time: 17:05
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\BugReport\Index;

use App\Http\Request\Dto\ADto;

class Save extends ADto
{

    /**
     * @var string
     */
    private $title = '';

    /**
     * @var string
     */
    private $description = '';

    /**
     * @var string
     */
    private $evidence = '';

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Save
     */
    public function setTitle(string $title): Save
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Save
     */
    public function setDescription(string $description): Save
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getEvidence(): string
    {
        return $this->evidence;
    }

    /**
     * @param string $evidence
     * @return Save
     */
    public function setEvidence(string $evidence): Save
    {
        $this->evidence = $evidence;
        return $this;
    }
}
