<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 23.11.2020
 * Time: 10:29
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

/**
 * @var $mail \App\Model\Dto\WebMail
 */
?>
@extends('web.layout.app')
@section('url')
    https://eternityV.de/mail/mailfach/anzeigen
@endsection
@section('content')
    <div class="container bg-white text-dark pb-2">
        @include('web.mailbox.mail.component.nav', [
            'email' => $dto->getEmail(),
            'link' => route('web.mailbox.mail.index', [$webMailAddressId,$hash]),
        ])
        <br/>
        @include('layouts.messages')
        <div class="row">
            <div class="col-2">
                @include('web.mailbox.mail.component.leftSideBar')
            </div>
            <div class="col-10">
                <div class="row col-12">
                    <a class="btn mr-2 btn-secondary btn-sm float-right"
                       href="{{ route('web.mailbox.mail.reply', [$webMailAddressId, $mail->getId(), $hash]) }}"
                       role="button"
                    >
                        <i class="fas fa-reply"></i> Antworten
                    </a>
                    <a class="btn mr-2 btn-secondary btn-sm float-right"
                       href="{{ route('web.mailbox.mail.share', [$webMailAddressId, $mail->getId(), $hash]) }}"
                       role="button"
                    >
                        <i class="fas fa-share"></i> Weiterleiten
                    </a>
                </div>
                @if(count($attachments))
                    <div class="row">
                        @foreach($attachments as $attachment)
                            <div class="ml-2 p-1">
                                <button type="button" class="btn btn-outline-dark" data-toggle="modal"
                                        data-target="#attachment{{ $attachment->getId() }}">
                                    {{  strtoupper(pathinfo($attachment->getName(), PATHINFO_EXTENSION)) }}
                                    | {{ $attachment->getName() }}
                                </button>
                            </div>
                        @endforeach
                    </div>
                    <div class="border m-1"></div>
                @endif
                <div class="row m-1 p-2">
                    <div class="col-4 font-weight-bold"> Von:</div>
                    <div class="col-8">
                        {{ $mail->getSender() }}
                    </div>
                </div>
                <div class="row m-1 p-2">
                    <div class="col-4 font-weight-bold"> An:</div>
                    <div class="col-8">
                        {{ $mail->getReceiver() }}
                    </div>
                </div>
                <div class="row m-1 p-2">
                    <div class="col-4 font-weight-bold"> Betreff:</div>
                    <div class="col-8 ">
                        <u> {{ $mail->getTitle() }}</u>
                    </div>
                </div>
                <div class="row m-1 p-2">
                    <div class="col-12 border rounded m-3">{!! $mail->getMessage() !!}</div>
                </div>
                @foreach($attachments as $attachment)
                    <div class="modal m-1 fade" id="attachment{{ $attachment->getId() }}" tabindex="-1" role="dialog"
                         aria-hidden="true">
                        <div class="modal-dialog modal-xl" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">{{ $attachment->getName() }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <embed height="100%" src="{{ asset($attachment->getPath()) }}">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
