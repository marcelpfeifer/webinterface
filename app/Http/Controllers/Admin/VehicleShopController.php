<?php
/**
 * VehicleShopController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 29.04.2020
 * Time: 16:33
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Libraries\Api\Server\GetVehicles;
use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Model\VehicleStore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class VehicleShopController
 * @package App\Http\Controllers\Admin
 */
class VehicleShopController extends Controller
{
    public function __construct()
    {
        $this->middleware(
            'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_VEHICLE_SHOP
        );
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $results = [];
        foreach (VehicleStore::all() as $vehicle) {
            $results[] = VehicleStore::entryAsDto($vehicle);
        }
        $vehicles = GetVehicles::get();
        return view(
            'admin.vehicleShop.index',
            [
                'results'  => $results,
                'vehicles' => $vehicles,
            ]
        );
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $vehicles = GetVehicles::get();
        return view(
            'admin.vehicleShop.create',
            [
                'vehicles' => $vehicles,
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function store(Request $request)
    {
        $dto = (new \App\Model\Dto\VehicleStore())
            ->setStock($request->input('stock'))
            ->setPosX($request->input('posX'))
            ->setPosY($request->input('posY'))
            ->setPosZ($request->input('posZ'))
            ->setFaction($request->input('faction'))
            ->setRotX($request->input('rotX'))
            ->setRotY($request->input('rotY'))
            ->setRotZ($request->input('rotZ'))
            ->setModel($request->input('model'));

        (new VehicleStore())->insertEntry($dto);
        Logging::insert(Name::VEHICLE_STORE, Auth::id(), $dto);
        $messages[] = (new Message())
            ->setMessage("Eintrag wurde erstellt")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.vehicleShop.index')->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, int $id)
    {
        $vehicles = GetVehicles::get();
        $entry = VehicleStore::find($id);
        $dto = VehicleStore::entryAsDto($entry);

        return view(
            'admin.vehicleShop.show',
            [
                'dto'      => $dto,
                'vehicles' => $vehicles,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function update(Request $request, int $id)
    {
        $entry = VehicleStore::find($id);
        $dto = VehicleStore::entryAsDto($entry);
        $oldDto = clone $dto;
        $dto->setId($id)
            ->setStock($request->input('stock'))
            ->setPosX((float)$request->input('posX'))
            ->setPosY((float)$request->input('posY'))
            ->setPosZ((float)$request->input('posZ'))
            ->setFaction($request->input('faction'))
            ->setRotX((float)$request->input('rotX'))
            ->setRotY((float)$request->input('rotY'))
            ->setRotZ((float)$request->input('rotZ'))
            ->setModel($request->input('model'));

        (new VehicleStore())->updateEntry($dto);
        Logging::update(Name::VEHICLE_STORE, Auth::id(), $oldDto, $dto);
        $messages[] = (new Message())
            ->setMessage("Eintrag wurde aktualisiert")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.vehicleShop.show', $id)->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function destroy(Request $request, int $id)
    {
        $entry = VehicleStore::find($id);
        $dto = VehicleStore::entryAsDto($entry);
        (new VehicleStore())->deleteEntry($dto);
        Logging::delete(Name::VEHICLE_STORE, Auth::id(), $dto);
        $messages[] = (new Message())
            ->setMessage("Eintrag wurde gelöscht")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.vehicleShop.index')->with('messages', $messages);
    }
}
