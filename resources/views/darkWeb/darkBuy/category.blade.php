<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 07.05.2020
 * Time: 19:33
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('darkWeb.layout.app')
@section('url')
    {{  md5('https://eternityV.de/darkBuy/'.$categoryDto->getName() ) }}
@endsection
@section('content')
    <div class="container bg-dark darkWebColor">
        @include('darkWeb.darkBuy.includes.nav')
        <h2>Angebote in der Kategorie {{ $categoryDto->getName() }}</h2>
        @include('darkWeb.darkBuy.includes.offers')
    </div>
@endsection
