<?php
/**
 * Report
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 18.09.2020
 * Time: 17:34
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use App\Model\Dto\Report\WithCharacter;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Report extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_GUID = 'guid';

    /**
     * @var string
     */
    const COLUMN_POS_X = 'posx';

    /**
     * @var string
     */
    const COLUMN_POS_Y = 'posy';

    /**
     * @var string
     */
    const COLUMN_POS_Z = 'posz';

    /**
     * @var string
     */
    const COLUMN_CREATED_AT = 'createdAt';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'report';

    /**
     * @var string[]
     */
    protected $dates = [
        self::COLUMN_CREATED_AT
    ];

    /**
     * @param string|null $search
     * @return WithCharacter[]
     */
    public function getReports(?string $search = '')
    {
        $result = [];

        $query = $this;

        if ($search) {
            $query = $query->join(
                'character',
                function ($join) use ($search) {
                    $join->on('character.id', '=', 'report.guid')
                        ->where('character.name', 'LIKE', "%$search%");
                }
            );
        }
        $entries = $query->select('report.*')
            ->orderBy(self::COLUMN_CREATED_AT, 'desc')
            ->take(50)
            ->get();

        foreach ($entries as $entry) {
            $result[] = (new WithCharacter())
                ->setCharacter($entry->character ? Character::entryAsDto($entry->character) : null)
                ->setReport(self::entryAsDto($entry));
        }
        return $result;
    }

    /**
     * @param Report $entry
     * @return Dto\Report
     */
    public static function entryAsDto(Report $entry): Dto\Report
    {
        return (new Dto\Report())
            ->setId($entry->{self::COLUMN_ID})
            ->setGuid($entry->{self::COLUMN_GUID})
            ->setPosX($entry->{self::COLUMN_POS_X})
            ->setPosY($entry->{self::COLUMN_POS_Y})
            ->setPosZ($entry->{self::COLUMN_POS_Z})
            ->setCreatedAt($entry->{self::COLUMN_CREATED_AT});
    }

    /**
     * @param Dto\Report $dto
     */
    public function deleteEntry(\App\Model\Dto\Report $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @return BelongsTo
     */
    public function character(): BelongsTo
    {
        return $this->belongsTo(Character::class, self::COLUMN_GUID, Character::COLUMN_ID);
    }
}
