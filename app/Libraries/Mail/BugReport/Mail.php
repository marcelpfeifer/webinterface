<?php
/**
 * Mail
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 11.08.2021
 * Time: 12:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Mail\BugReport;


use App\Model\Dto\Character;
use App\Model\Dto\Account;
use App\Model\Dto\WebBugReport;
use Illuminate\Mail\Mailable;

class Mail extends Mailable
{
    /**
     * The order instance.
     *
     * @var WebBugReport
     */
    public $bugReport;

    /**
     * The order instance.
     *
     * @var Account
     */
    public $supporter;

    /**
     * The order instance.
     *
     * @var Character
     */
    public $reporter;


    /**
     * Mail constructor.
     * @param WebBugReport $bugReport
     */
    public function __construct(WebBugReport $bugReport, Account $supporter, Character $reporter)
    {
        $this->bugReport = $bugReport;
        $this->supporter = $supporter;
        $this->reporter = $reporter;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject($this->bugReport->getTitle())
            ->from(env('MAIL_FROM'))
            ->view('emails.bugReport.mail');
    }
}
