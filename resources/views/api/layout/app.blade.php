<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('api.layout.head')
<body class="bg-secondary">
<div>
    @include('api.layout.header')
    <main class="py-4">
        @include('api.layout.wrapper')
    </main>
</div>
</body>
</html>
