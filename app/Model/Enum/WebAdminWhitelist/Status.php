<?php
/**
 * Status
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 28.04.2020
 * Time: 17:00
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Enum\WebAdminWhitelist;

use App\Libraries\Enum\AEnum;

class Status extends AEnum
{

    /**
     * @var string
     */
    const APPROVED = 'APPROVED';

    /**
     * @var string
     */
    const RETRY = 'RETRY';

    /**
     * @var string
     */
    const DENIED = 'DENIED';
}
