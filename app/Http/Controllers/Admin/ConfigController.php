<?php
/**
 * ConfigController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 02.04.2020
 * Time: 17:11
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Model\Dto\WebConfig;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ConfigController extends Controller
{

    /**
     * @return View
     */
    public function index(): View
    {
        return view('admin.config.index');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function save(Request $request): RedirectResponse
    {
        $db = new \App\Model\WebConfig();
        foreach ($request->input() as $key => $value) {
            if ($key == '_token') {
                continue;
            }
            $dto = (new WebConfig())
                ->setKey($key)
                ->setValue($value);
            $db->insertOrUpdateEntry($dto);
        }

        $messages[] = (new Message())
            ->setMessage("Erfolgreich gespeichert")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.config.index')->with('messages', $messages);
    }
}
