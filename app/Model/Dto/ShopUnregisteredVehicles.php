<?php
/**
 * ShopUnregisteredVehicles
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 11.10.2021
 * Time: 12:32
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

use App\Libraries\Logging\ILogging;
use App\Libraries\Logging\Traits\Variables;
use Carbon\Carbon;

class ShopUnregisteredVehicles implements ILogging
{
    use Variables;

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var string
     */
    private $model = '';

    /**
     * @var int
     */
    private $stock = 0;

    /**
     * @var string
     */
    private $posX = '';

    /**
     * @var string
     */
    private $posY = '';

    /**
     * @var string
     */
    private $posZ ='';

    /**
     * @var string
     */
    private $rotX ='';

    /**
     * @var string
     */
    private $rotY ='';

    /**
     * @var string
     */
    private $rotZ = '';

    /**
     * @var int
     */
    private $mapId = 0;

    /**
     * @var string
     */
    private $npcPosX = '';

    /**
     * @var string
     */
    private $npcPosY = '';

    /**
     * @var string
     */
    private $npcPosZ = '';

    /**
     * @var string
     */
    private $npcRot = '';

    /**
     * @var string
     */
    private $npcModel = '';

    /**
     * @var Carbon|null
     */
    private $createdAt = null;

    /**
     * @var Carbon|null
     */
    private $updatedAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return ShopUnregisteredVehicles
     */
    public function setId(?int $id): ShopUnregisteredVehicles
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getModel(): string
    {
        return $this->model;
    }

    /**
     * @param string $model
     * @return ShopUnregisteredVehicles
     */
    public function setModel(string $model): ShopUnregisteredVehicles
    {
        $this->model = $model;
        return $this;
    }

    /**
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     * @return ShopUnregisteredVehicles
     */
    public function setStock(int $stock): ShopUnregisteredVehicles
    {
        $this->stock = $stock;
        return $this;
    }

    /**
     * @return string
     */
    public function getPosX(): string
    {
        return $this->posX;
    }

    /**
     * @param string $posX
     * @return ShopUnregisteredVehicles
     */
    public function setPosX(string $posX): ShopUnregisteredVehicles
    {
        $this->posX = $posX;
        return $this;
    }

    /**
     * @return string
     */
    public function getPosY(): string
    {
        return $this->posY;
    }

    /**
     * @param string $posY
     * @return ShopUnregisteredVehicles
     */
    public function setPosY(string $posY): ShopUnregisteredVehicles
    {
        $this->posY = $posY;
        return $this;
    }

    /**
     * @return string
     */
    public function getPosZ(): string
    {
        return $this->posZ;
    }

    /**
     * @param string $posZ
     * @return ShopUnregisteredVehicles
     */
    public function setPosZ(string $posZ): ShopUnregisteredVehicles
    {
        $this->posZ = $posZ;
        return $this;
    }

    /**
     * @return string
     */
    public function getRotX(): string
    {
        return $this->rotX;
    }

    /**
     * @param string $rotX
     * @return ShopUnregisteredVehicles
     */
    public function setRotX(string $rotX): ShopUnregisteredVehicles
    {
        $this->rotX = $rotX;
        return $this;
    }

    /**
     * @return string
     */
    public function getRotY(): string
    {
        return $this->rotY;
    }

    /**
     * @param string $rotY
     * @return ShopUnregisteredVehicles
     */
    public function setRotY(string $rotY): ShopUnregisteredVehicles
    {
        $this->rotY = $rotY;
        return $this;
    }

    /**
     * @return string
     */
    public function getRotZ(): string
    {
        return $this->rotZ;
    }

    /**
     * @param string $rotZ
     * @return ShopUnregisteredVehicles
     */
    public function setRotZ(string $rotZ): ShopUnregisteredVehicles
    {
        $this->rotZ = $rotZ;
        return $this;
    }

    /**
     * @return int
     */
    public function getMapId(): int
    {
        return $this->mapId;
    }

    /**
     * @param int $mapId
     * @return ShopUnregisteredVehicles
     */
    public function setMapId(int $mapId): ShopUnregisteredVehicles
    {
        $this->mapId = $mapId;
        return $this;
    }

    /**
     * @return string
     */
    public function getNpcPosX(): string
    {
        return $this->npcPosX;
    }

    /**
     * @param string $npcPosX
     * @return ShopUnregisteredVehicles
     */
    public function setNpcPosX(string $npcPosX): ShopUnregisteredVehicles
    {
        $this->npcPosX = $npcPosX;
        return $this;
    }

    /**
     * @return string
     */
    public function getNpcPosY(): string
    {
        return $this->npcPosY;
    }

    /**
     * @param string $npcPosY
     * @return ShopUnregisteredVehicles
     */
    public function setNpcPosY(string $npcPosY): ShopUnregisteredVehicles
    {
        $this->npcPosY = $npcPosY;
        return $this;
    }

    /**
     * @return string
     */
    public function getNpcPosZ(): string
    {
        return $this->npcPosZ;
    }

    /**
     * @param string $npcPosZ
     * @return ShopUnregisteredVehicles
     */
    public function setNpcPosZ(string $npcPosZ): ShopUnregisteredVehicles
    {
        $this->npcPosZ = $npcPosZ;
        return $this;
    }

    /**
     * @return string
     */
    public function getNpcRot(): string
    {
        return $this->npcRot;
    }

    /**
     * @param string $npcRot
     * @return ShopUnregisteredVehicles
     */
    public function setNpcRot(string $npcRot): ShopUnregisteredVehicles
    {
        $this->npcRot = $npcRot;
        return $this;
    }

    /**
     * @return string
     */
    public function getNpcModel(): string
    {
        return $this->npcModel;
    }

    /**
     * @param string $npcModel
     * @return ShopUnregisteredVehicles
     */
    public function setNpcModel(string $npcModel): ShopUnregisteredVehicles
    {
        $this->npcModel = $npcModel;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return ShopUnregisteredVehicles
     */
    public function setCreatedAt(?Carbon $createdAt): ShopUnregisteredVehicles
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon|null $updatedAt
     * @return ShopUnregisteredVehicles
     */
    public function setUpdatedAt(?Carbon $updatedAt): ShopUnregisteredVehicles
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
