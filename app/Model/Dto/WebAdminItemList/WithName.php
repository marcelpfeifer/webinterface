<?php
/**
 * WithName
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 08.08.2020
 * Time: 14:13
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto\WebAdminItemList;

use Carbon\Carbon;

class WithName
{
    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var int
     */
    private $characterId = 0;

    /**
     * @var string
     */
    private $storage = '';

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var int
     */
    private $quantity = 0;

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @var null|Carbon
     */
    private $updatedAt = null;

    /**
     * @var string
     */
    private $playerName = '';

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WithName
     */
    public function setId(?int $id): WithName
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCharacterId(): int
    {
        return $this->characterId;
    }

    /**
     * @param int $characterId
     * @return WithName
     */
    public function setCharacterId(int $characterId): WithName
    {
        $this->characterId = $characterId;
        return $this;
    }

    /**
     * @return string
     */
    public function getStorage(): string
    {
        return $this->storage;
    }

    /**
     * @param string $storage
     * @return WithName
     */
    public function setStorage(string $storage): WithName
    {
        $this->storage = $storage;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return WithName
     */
    public function setName(string $name): WithName
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return WithName
     */
    public function setQuantity(int $quantity): WithName
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return WithName
     */
    public function setCreatedAt(?Carbon $createdAt): WithName
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon|null $updatedAt
     * @return WithName
     */
    public function setUpdatedAt(?Carbon $updatedAt): WithName
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getPlayerName(): string
    {
        return $this->playerName;
    }

    /**
     * @param string $playerName
     * @return WithName
     */
    public function setPlayerName(string $playerName): WithName
    {
        $this->playerName = $playerName;
        return $this;
    }
}
