<?php
/**
 * MessageController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 11.04.2020
 * Time: 14:05
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Api\Server;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class MessageController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        return view('api.server.message.index', []);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function save(Request $request): RedirectResponse
    {
        $message = $request->input('message');

        if (\App\Libraries\Api\Server\Message::call($message)) {
            $messages[] = (new Message())
                ->setMessage('Erfolgreich ausgeführt')
                ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        } else {
            $messages[] = (new Message())
                ->setMessage('Anfrage fehlgeschlagen')
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
        }

        return redirect()->route('api.server.message.index')->with('messages', $messages);
    }
}
