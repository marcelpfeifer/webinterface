<?php
/**
 * BusinessController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 04.05.2020
 * Time: 16:52
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Model\Business;
use App\Model\Character;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class BusinessController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function index(): View
    {
        $businesses = [];
        foreach (Business::all() as $value) {
            $businesses[] = Business::entryAsDto($value);
        }

        return view('admin.business.index', [
            "businesses" => $businesses
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|View
     */
    public function show(Request $request, int $id)
    {
        $business = Business::find($id);
        if (!($business)) {
            $messages[] = (new Message())
                ->setMessage("Die Firma wurde nicht gefunden")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::NOTICE);
            return redirect()->route('admin.business.index')->with('messages', $messages);
        }
        $business = Business::entryAsDto($business);
        $characters = (new Character())->getAllUsersOfGroupByBusinessId($id);

        return view('admin.business.show', [
            "business"   => $business,
            "characters" => $characters,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|View
     */
    public function create(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name'  => 'required|string',
                'money' => 'required|int',
            ],
            [
                'name.required'  => 'Das Feld `Name` darf nicht leer sein',
                'name.string'    => 'Das Feld `Name` darf nur Zeichen enthalten',
                'name.int'       => 'Das Feld `Typ` darf nur Zahlenwerte enthalten',
                'money.required' => 'Das Geld `Typ` darf nicht leer sein',
                'money.int'      => 'Das Feld `Geld` darf nur Zahlenwerte enthalten',
            ]
        );
        if ($validator->fails()) {
            $messages = [];
            foreach ($validator->errors()->all() as $error) {
                $messages[] = (new Message())
                    ->setMessage($error)
                    ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
            }
            return redirect()->route('admin.business.index')->with('messages', $messages)->withInput();
        }


        $dto = (new \App\Model\Dto\Business())
            ->setName($request->input('name'))
            ->setMoney($request->input('money'))
            ->setMessages('Nachricht des Tages');

        Logging::insert(Name::BUSINESS, Auth::id(), $dto);
        (new Business())->insertEntry($dto);

        $messages[] = (new Message())
            ->setMessage("Die Firma erfolgreich angelegt")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.business.index')->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|View
     */
    public function delete(Request $request, int $id)
    {
        $business = Business::find($id);
        if (!($business)) {
            return redirect()->route('admin.business.index');
        }
        $dto = Business::entryAsDto($business);

        // Setze bei allen Benutzern die die Gruppe haben den Rank und die Gruppe auf 0
        (new Character())->deleteBusinessRank($dto);
        // Lösche die Gruppe
        (new Business())->deleteEntry($dto);

        Logging::delete(Name::BUSINESS, Auth::id(), $dto);

        $messages[] = (new Message())
            ->setMessage("Die Firma wurde gelöscht")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.business.index')->with('messages', $messages);
    }
}
