<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.08.2021
 * Time: 16:18
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('web.layout.app')
@section('url')
    https://eternityV.de/news/
@endsection
@section('content')
    <div class="container transparentBg bg-white text-dark min-vh-100">
        @include('web.news.component.nav',[
            'categoryId' => $categoryId,
        ])

        @foreach($news as $entry)
            <div class="row">
                <div class="col-4">
                    <img class="img-fluid" src="{{ $entry->getImage() }}">
                </div>
                <div class="col-8">
                    <div class="row">
                        <div class="col-12 text-primary small">
                            {{ (isset($categories[$categoryId]) ? $categories[$categoryId]->getName(): '') }}
                        </div>
                        <div class="col-12">
                            <h2><u>{{ $entry->getTitle() }}</u></h2>
                        </div>
                        <div class="col-12">
                            {{ $entry->getShortDescription() }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <a class="float-right" href="{{ route('web.news.show',[$entry->getId(), $hash]) }}">
                                Zum Artikel
                            </a>
                        </div>
                        <div class="col-12">
                            <span class="float-right small">
                                {{ $entry->getCreatedAt()->format('d.m H:i') }} von {{ $entry->getAuthor() }}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
        @endforeach
    </div>
@endsection
