<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 02.09.2021
 * Time: 12:15
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

/**
 * @var $stock \App\Model\Dto\WebStockCharacter
 */
?>
@extends('web.layout.app')
@section('url')
    https://eternityV.de/börse/kaufen
@endsection
@section('content')
    <div class="container transparentBg bg-white text-dark min-vh-100">
        @include('web.stock.component.nav',[])
        @include('layouts.messages')
        <form method="post" class="form" action="{{ route('web.stock.sellPost', [$stock->getId(),$hash]) }}">
            {{ csrf_field() }}
            <div class="form-row">
                <div class="col-12">
                    <label for="bought">Gekauft am:</label>
                    <input class="form-control" id="bought" name="bought" type="text" readonly value="{{ $stock->getCreatedAt()->format('d.m.Y') }}">
                </div>
            </div>
            <div class="form-row">
                <div class="col-12">
                    <label for="amount">Anzahl die zu verkaufen möchtest:</label>
                    <input class="form-control" id="amount" name="amount" type="number" required
                           max="{{ $stock->getAmount() }}">
                    <span class="small text-danger">
                        Es fällt eine Pauschale von 5% des verkauften Wertes an falls die gekauften Aktien noch keine 4 Wochen alt sind!
                    </span>
                </div>
            </div>
            <div class="form-row">
                <div class="col-12">
                    <label for="stock">Deine Aktienanteile:</label>
                    <input class="form-control" id="stock" name="stock" value="{{ $stock->getAmount() }}" readonly type="number">
                </div>
            </div>

            <br/>
            <div class="form-row justify-content-end">
                @if($stock->getAmount() == 0)
                    <a class="btn btn-danger text-white">Keine Aktien mehr verfügbar</a>
                @else
                    <button type="submit" class="btn btn-primary">Verkaufen</button>
                @endif
            </div>
        </form>
    </div>
@endsection
