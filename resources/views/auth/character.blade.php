<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 19.04.2020
 * Time: 13:45
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

/**
 * @var $dto \App\Model\Dto\Character
 */
?>
@extends('layouts.app')
@section('title')
    <i class="fas fa-user"></i>
    Character bearbeiten
@endsection
@section('content')
    @if(!$dto || !(
       $dto->getFibRank() !== 0
       || $dto->getLawyerRank() !== 0
       || $dto->getDojRank() !== 0
       || $dto->getCopRank() !== 0
       || $dto->getMedRank() !== 0
       ))
        Aktuell gibt es hier leider noch nichts für dich <3
    @else
    <form method="POST" action="{{ route('auth.character.save') }}">
        @csrf
        @if(
        $dto->getFibRank() !== 0
        || $dto->getLawyerRank() !== 0
        || $dto->getDojRank() !== 0
        || $dto->getCopRank() !== 0
        || $dto->getMedRank() !== 0
        )
            <div class="form-group">
                <label for="serviceNumber" class="col-lg-3 control-label">Service Nummer:</label>
                <div class="col-lg-8">
                    <input class="form-control" id="serviceNumber" name="serviceNumber" type="number"
                           value="{{ $dto->getServiceNumber() }}">
                </div>
            </div>
        @endif
        @if($dto->getFibRank())
            <div class="form-group">
                <label for="serviceName" class="col-lg-3 control-label">Service Name:</label>
                <div class="col-lg-8">
                    <input class="form-control" id="serviceName" name="serviceName" type="text"
                           value="{{ $dto->getServiceName() }}">
                </div>
            </div>
        @endif
        <button type="submit" class="btn btn-primary">Bestätigen</button>
    </form>
    @endif
@endsection
