<?php
/**
 * Price
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 02.09.2021
 * Time: 18:12
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Common;


class Price
{
    /**
     * @param float $price
     * @return string
     */
    public static function format(float $price): string
    {
        return number_format($price, 2);
    }
}
