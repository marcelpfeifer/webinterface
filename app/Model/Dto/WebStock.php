<?php
/**
 * WebStock
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 28.08.2021
 * Time: 16:44
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

use App\Libraries\Logging\ILogging;
use App\Libraries\Logging\Traits\Variables;
use Carbon\Carbon;

class WebStock implements ILogging
{

    use Variables;

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var float
     */
    private $price = 0.00;

    /**
     * @var int
     */
    private $stock = 0;

    /**
     * @var float
     */
    private $minChange = 0.00;

    /**
     * @var float
     */
    private $maxChange = 0.00;

    /**
     * @var float
     */
    private $lastPrice = 0.00;

    /**
     * @var float
     */
    private $percentageChange = 0.00;

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @var null|Carbon
     */
    private $updatedAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebStock
     */
    public function setId(?int $id): WebStock
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return WebStock
     */
    public function setName(string $name): WebStock
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return WebStock
     */
    public function setPrice(float $price): WebStock
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     * @return WebStock
     */
    public function setStock(int $stock): WebStock
    {
        $this->stock = $stock;
        return $this;
    }

    /**
     * @return float
     */
    public function getMinChange(): float
    {
        return $this->minChange;
    }

    /**
     * @param float $minChange
     * @return WebStock
     */
    public function setMinChange(float $minChange): WebStock
    {
        $this->minChange = $minChange;
        return $this;
    }

    /**
     * @return float
     */
    public function getMaxChange(): float
    {
        return $this->maxChange;
    }

    /**
     * @param float $maxChange
     * @return WebStock
     */
    public function setMaxChange(float $maxChange): WebStock
    {
        $this->maxChange = $maxChange;
        return $this;
    }

    /**
     * @return float
     */
    public function getLastPrice(): float
    {
        return $this->lastPrice;
    }

    /**
     * @param float $lastPrice
     * @return WebStock
     */
    public function setLastPrice(float $lastPrice): WebStock
    {
        $this->lastPrice = $lastPrice;
        return $this;
    }

    /**
     * @return float
     */
    public function getPercentageChange(): float
    {
        return $this->percentageChange;
    }

    /**
     * @param float $percentageChange
     * @return WebStock
     */
    public function setPercentageChange(float $percentageChange): WebStock
    {
        $this->percentageChange = $percentageChange;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return WebStock
     */
    public function setCreatedAt(?Carbon $createdAt): WebStock
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon|null $updatedAt
     * @return WebStock
     */
    public function setUpdatedAt(?Carbon $updatedAt): WebStock
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
