<?php
/**
 * Business
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 01.11.2019
 * Time: 14:11
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

use App\Libraries\Logging\ILogging;
use App\Libraries\Logging\Traits\Variables;
use Carbon\Carbon;

class Business implements ILogging
{
    use Variables;

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var int
     */
    private $money = 0;

    /**
     * @var string
     */
    private $messages = '';

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @var null|Carbon
     */
    private $updatedAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Business
     */
    public function setId(?int $id): Business
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Business
     */
    public function setName(string $name): Business
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getMoney(): int
    {
        return $this->money;
    }

    /**
     * @param int $money
     * @return Business
     */
    public function setMoney(int $money): Business
    {
        $this->money = $money;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessages(): string
    {
        return $this->messages;
    }

    /**
     * @param string $messages
     * @return Business
     */
    public function setMessages(string $messages): Business
    {
        $this->messages = $messages;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return Business
     */
    public function setCreatedAt(?Carbon $createdAt): Business
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon|null $updatedAt
     * @return Business
     */
    public function setUpdatedAt(?Carbon $updatedAt): Business
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
