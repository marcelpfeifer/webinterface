<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameHappyBuyToMarketplace extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('webHappyBuy','webMarketplaceBuy');
        Schema::rename('webHappyBuyCategory','webMarketplaceCategory');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('webMarketplaceBuy','webHappyBuy');
        Schema::rename('webMarketplaceCategory','webHappyBuyCategory');
    }
}
