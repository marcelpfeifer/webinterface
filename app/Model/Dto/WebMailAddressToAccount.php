<?php
/**
 * WebMailAddressToAccount
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 12.08.2021
 * Time: 11:39
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;


class WebMailAddressToAccount
{

    /**
     * @var int
     */
    private $webMailAddressId = 0;

    /**
     * @var int
     */
    private $accountId = 0;

    /**
     * @var bool
     */
    private $owner = false;

    /**
     * @return int
     */
    public function getWebMailAddressId(): int
    {
        return $this->webMailAddressId;
    }

    /**
     * @param int $webMailAddressId
     * @return WebMailAddressToAccount
     */
    public function setWebMailAddressId(int $webMailAddressId): WebMailAddressToAccount
    {
        $this->webMailAddressId = $webMailAddressId;
        return $this;
    }

    /**
     * @return int
     */
    public function getAccountId(): int
    {
        return $this->accountId;
    }

    /**
     * @param int $accountId
     * @return WebMailAddressToAccount
     */
    public function setAccountId(int $accountId): WebMailAddressToAccount
    {
        $this->accountId = $accountId;
        return $this;
    }

    /**
     * @return bool
     */
    public function isOwner(): bool
    {
        return $this->owner;
    }

    /**
     * @param bool $owner
     * @return WebMailAddressToAccount
     */
    public function setOwner(bool $owner): WebMailAddressToAccount
    {
        $this->owner = $owner;
        return $this;
    }
}
