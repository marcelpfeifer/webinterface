<?php
/**
 * Vehicle
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 01.04.2020
 * Time: 21:06
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Model\Admin\Player;

use App\Libraries\Api\Vehicle\Park;
use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Libraries\Vehicle\Customization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Vehicle
{
    /**
     * @param Request $request
     * @param int $id
     * @return bool
     */
    public static function save(Request $request, int $id): bool
    {
        $db = new \App\Model\Vehicle();
        $dto = $db->getVehicleById($id);
        $oldDto = clone $dto;
        if (!($dto)) {
            return false;
        }
        $dto->setParked(($request->input('parked')) ? true : false)
            ->setModel($request->input('model'))
            ->setNumberPlate($request->input('numberplate'))
            ->setFaction($request->input('faction'))
            ->setImpounded(($request->input('impounded')) ? true : false)
            ->setDestroyed(($request->input('destroyed')) ? true : false)
            ->setGId($request->input('gid'))
            ->setEngineDistance($request->input('engineDistance'));

        $db->updateEntry($dto);

        Logging::update(Name::VEHICLE, Auth::id(), $oldDto, $dto);
        return true;
    }

    /**
     * @param Request $request
     * @param int $id
     * @return bool
     */
    public static function tune(Request $request, int $id): bool
    {
        $db = new \App\Model\Vehicle();
        $dto = $db->getVehicleById($id);
        if (!($dto)) {
            return false;
        }

        // Wenn das Auto ausgeparkt ist, parke es wieder ein
        if (!($dto->isParked())) {
            Park::call($id);
        }

        $customization = (new \App\Libraries\Vehicle\Dto\Customization())
            ->setEngine($request->input('engine'))
            ->setBrakes($request->input('brakes'))
            ->setHorns($request->input('horns'))
            ->setSuspension($request->input('suspension'))
            ->setArmor($request->input('armor'))
            ->setTurbo(($request->input('turbo')) ? true : false)
            ->setCustomTireSmoke(($request->input('customTireSmoke')) ? true : false)
            ->setXenon(($request->input('xenon')) ? true : false);

        $dto->setCustomization(Customization::toJson($customization, $dto));

        $db->updateEntry($dto);

        return true;
    }
}
