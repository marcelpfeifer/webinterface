<?php
/**
 * HashController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 15.08.2021
 * Time: 13:52
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers;


use App\Model\Character;
use Illuminate\Database\Eloquent\Model;

class HashController extends Controller
{

    /**
     * @param string $hash
     * @return \App\Model\Dto\Character|null
     */
    protected function getCharacterByHash(string $hash): ?\App\Model\Dto\Character
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return null;
        }
        return Character::entryAsDto($character);
    }
}
