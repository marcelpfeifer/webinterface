<?php
/**
 * Spectate
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 13.04.2020
 * Time: 22:18
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Api\Player;

use App\Libraries\Api\AClient;
use App\Model\WebConfig;

class Spectate extends AClient
{
    /**
     * @var string
     */
    const URL = 'player/spectate';

    /**
     * @param int $pid
     * @param int $targetPid
     * @param bool $value
     * @return bool
     */
    public static function call(int $pid, int $targetPid, bool $value): bool
    {
        $response = self::sendRequest('POST', WebConfig::getEntryByKey('api_url')->getValue() . self::URL, [
            'query' => [
                'pid'       => $pid,
                'targetPid' => $targetPid,
                'value'     => $value,
            ]
        ]);
        return $response && $response->getStatusCode() == 200;
    }
}
