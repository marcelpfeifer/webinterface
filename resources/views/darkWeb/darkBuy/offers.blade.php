<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 01.05.2020
 * Time: 12:42
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('darkWeb.layout.app')
@section('url')
    {{  md5('https://eternityV.de/offers') }}
@endsection
@section('content')
    <div class="container bg-dark darkWebColor">
        @include('darkWeb.darkBuy.includes.nav')
        <h2> Meine Angebote </h2>
        @include('darkWeb.darkBuy.includes.offers')
    </div>
@endsection

