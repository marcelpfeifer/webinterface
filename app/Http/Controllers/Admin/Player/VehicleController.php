<?php
/**
 * VehicleController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 28.03.2020
 * Time: 14:55
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\Player;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Libraries\Vehicle\Customization;
use App\Model\Vehicle;
use App\Model\VehicleKeys;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VehicleController extends Controller
{

    /**
     * @param Request $request
     * @param int $characterId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, int $characterId)
    {
        // Lade alle Fahrzeuge
        $vehicles = (new Vehicle())->getVehiclesByCharacterId($characterId);

        return view(
            'admin.player.vehicle.index',
            [
                'vehicles'    => $vehicles,
                'characterId' => $characterId
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request, int $id)
    {
        $vehicle = (new Vehicle())->getVehicleById($id);

        return view(
            'admin.player.vehicle.edit',
            [
                'vehicle'       => $vehicle,
                'customization' => Customization::fromJson($vehicle),
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request, int $id)
    {
        // Speichert die Änderungen in der Datenbank
        $success = \App\Http\Model\Admin\Player\Vehicle::save($request, $id);

        if ($success) {
            $messages[] = (new Message())
                ->setMessage("Erfolgreich gespeichert")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        } else {
            $messages[] = (new Message())
                ->setMessage('Konnte nicht gespeichert werden')
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
        }
        return redirect()->route('admin.player.vehicle.edit', $id)->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function tune(Request $request, int $id)
    {
        // Speichert die Änderungen in der Datenbank
        $success = \App\Http\Model\Admin\Player\Vehicle::tune($request, $id);

        if ($success) {
            $messages[] = (new Message())
                ->setMessage("Erfolgreich gespeichert")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        } else {
            $messages[] = (new Message())
                ->setMessage('Konnte nicht gespeichert werden')
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
        }
        return redirect()->route('admin.player.vehicle.edit', $id)->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request, int $id)
    {
        $db = new Vehicle();
        $vehicle = $db->getVehicleById($id);
        $db->deleteEntry($vehicle);
        (new VehicleKeys())->deleteEntryByCarId($id);

        $vehicle->setInventory('');
        Logging::delete(Name::VEHICLE, Auth::id(), $vehicle);

        $messages[] = (new Message())
            ->setMessage('Wurde gelöscht')
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.player.vehicle.index', $vehicle->getGuid())->with('messages', $messages);
    }
}
