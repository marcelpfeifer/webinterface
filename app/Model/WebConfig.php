<?php
/**
 * WebAdminTechnicalFaq.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 04.09.2019
 * Time: 21:29
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Users
 * @package App
 *
 * @property int id
 * @property string key
 * @property string value
 */
class WebConfig extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_KEY = 'key';

    /**
     * @var string
     */
    const COLUMN_VALUE = 'value';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webConfig';

    /**
     * @param string $key
     * @return Dto\WebConfig|null
     */
    public static function getEntryByKey(string $key): ?Dto\WebConfig
    {
        $entry = self::where(self::COLUMN_KEY, $key)->first();
        if (is_null($entry)) {
            return null;
        }
        return self::entryAsDto($entry);
    }

    /**
     * @param Dto\WebConfig $dto
     */
    public function insertEntry(Dto\WebConfig $dto)
    {
        $this->insert([
            self::COLUMN_ID    => $dto->getId(),
            self::COLUMN_KEY   => $dto->getKey(),
            self::COLUMN_VALUE => $dto->getValue(),
            self::CREATED_AT   => Carbon::now(),
            self::UPDATED_AT   => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\WebConfig $dto
     */
    public function insertOrUpdateEntry(Dto\WebConfig $dto)
    {
        if (self::getEntryByKey($dto->getKey())) {
            $this->where(self::COLUMN_KEY, $dto->getKey())->update([
                self::COLUMN_VALUE => $dto->getValue(),
                self::UPDATED_AT   => Carbon::now(),
            ]);
            return;
        }
        $this->insertEntry($dto);
    }

    /**
     * @param Dto\WebConfig $dto
     */
    public function updateEntry(Dto\WebConfig $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update([
            self::COLUMN_ID    => $dto->getId(),
            self::COLUMN_KEY   => $dto->getKey(),
            self::COLUMN_VALUE => $dto->getValue(),
            self::UPDATED_AT   => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\WebConfig $dto
     */
    public function deleteEntry(\App\Model\Dto\WebConfig $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebConfig $entry
     * @return Dto\WebConfig
     */
    public static function entryAsDto(WebConfig $entry): Dto\WebConfig
    {
        return (new Dto\WebConfig())
            ->setId($entry->{self::COLUMN_ID})
            ->setKey($entry->{self::COLUMN_KEY})
            ->setValue($entry->{self::COLUMN_VALUE});
    }
}
