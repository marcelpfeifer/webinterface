<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 16.09.2021
 * Time: 16:45
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('web.layout.app')
@section('url')
    https://eternityV.de/hunt/
@endsection
@section('content')
    <div class="container transparentBg bg-white text-dark min-vh-100">
        <h1 class="text-center border-bottom"> Jagd Leaderboard</h1>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">
                    <a href="{{ route('web.hunt.index',['hash' => $hash, 'orderBy' => \App\Model\Enum\Hunt\Leaderboard\OrderBy::ANIMAL_COUNT]) }}">
                        Getötet
                        @if($orderBy === \App\Model\Enum\Hunt\Leaderboard\OrderBy::ANIMAL_COUNT)
                            <i class="fas fa-caret-down"></i>
                        @endif
                    </a>
                </th>
                <th scope="col">
                    <a href="{{ route('web.hunt.index',['hash' => $hash, 'orderBy' => \App\Model\Enum\Hunt\Leaderboard\OrderBy::TOTAL_WEIGHT]) }}">
                        Gesamtgewicht
                        @if($orderBy === \App\Model\Enum\Hunt\Leaderboard\OrderBy::TOTAL_WEIGHT)
                            <i class="fas fa-caret-down"></i>
                        @endif
                    </a>
                </th>
                <th scope="col">
                    <a href="{{ route('web.hunt.index',['hash' => $hash, 'orderBy' => \App\Model\Enum\Hunt\Leaderboard\OrderBy::DISTANCE]) }}">
                        Distanz
                        @if($orderBy === \App\Model\Enum\Hunt\Leaderboard\OrderBy::DISTANCE)
                            <i class="fas fa-caret-down"></i>
                        @endif
                    </a>
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($entries as $entry)
                <tr>
                    <th>
                        <i class="fas fa-user"></i>
                        {{ $entry->getName() }}
                    </th>
                    <td>
                        <i class="fas text-danger fa-crosshairs"></i>
                        {{ $entry->getAnimalCount() }}
                    </td>
                    <td>
                        <i class="fas fa-weight-hanging"></i>
                        {{ $entry->getTotalWeight() }}kg
                    </td>
                    <td>
                        <i class="fas fa-people-arrows"></i>
                        {{ $entry->getDistance() }}m
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
