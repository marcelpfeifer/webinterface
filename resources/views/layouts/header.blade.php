<nav class="navbar navbar-expand-md navbar-dark bg-dark shadow-sm">
    <div class="container">
        @guest
            <a class="navbar-brand" href="{{ route('register') }}">
                <div>
                    <img src="{{  url('img/logo.png')}}" alt="logo" height="100px">
                    Webinterface
                </div>
            </a>
        @else
            <a class="navbar-brand" href="{{ route('auth.account.index') }}">
                <div>
                    <img src="{{  url('img/logo.png')}}" alt="logo" height="100px">
                    Webinterface
                </div>
            </a>
        @endguest
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('auth.account.edit') }}">
                                <i class="fas fa-user-circle"></i>
                                {{ __('Account bearbeiten') }}
                            </a>
                            <a class="dropdown-item" href="{{ route('auth.character.index') }}">
                                <i class="fas fa-user"></i>
                                {{ __('Character bearbeiten') }}
                            </a>
                            <a class="dropdown-item" href="{{ route('auth.mailbox.index') }}">
                                <i class="fas fa-envelope"></i>
                                {{ __('Mail') }}
                            </a>
                            @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_NEWS)
                            <a class="dropdown-item" href="{{ route('auth.news.index') }}">
                                <i class="fas fa-newspaper"></i>
                                {{ __('News') }}
                            </a>
                            @endpermission
                            @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND)
                            <a class="dropdown-item" href="{{ route('admin.index') }}">
                                <i class="fas fa-cogs"></i>
                                {{ __('Admin') }}
                            </a>
                            @endpermission
                            @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API)
                            <a class="dropdown-item" href="{{ route('api.index') }}">
                                <i class="fas fa-cloud"></i>
                                {{ __('API') }}
                            </a>
                            @endpermission
                            <a class="dropdown-item" target="_blank" rel="noopener" href="https://eternityv.de/">
                                <i class="fab fa-wpforms"></i>
                                {{ __('Forum') }}
                            </a>
                            <a class="dropdown-item" href="{{ route('wasted') }}">
                                <i class="fas fa-sign-out-alt"></i>
                                {{ __('Logout') }}
                            </a>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
