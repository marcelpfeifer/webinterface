<?php
/**
 * Stop
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 11.08.2021
 * Time: 14:24
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Api\Server;


use App\Libraries\Api\AClient;
use App\Model\WebConfig;

class Stop extends AClient
{
    /**
     * @var string
     */
    const URL = '{type}/stop';

    /**
     * @param string $type
     * @return bool
     */
    public static function call(string $type): bool
    {
        $response = self::sendRequest(
            'POST',
            self::getUrl($type),
            []
        );
        return $response && $response->getStatusCode() == 200;
    }

    /**
     * @param string $type
     * @return array|string|string[]
     */
    private static function getUrl(string $type)
    {
        $url = WebConfig::getEntryByKey(\App\Model\Enum\WebConfig::API_SYSTEM_URL)->getValue() . self::URL;
        return str_replace('{type}', $type, $url);
    }
}
