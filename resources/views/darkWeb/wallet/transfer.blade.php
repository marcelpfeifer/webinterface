<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 16.08.2020
 * Time: 16:02
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('darkWeb.layout.app')
@section('url')
    {{  md5('https://eternityV.de/wallet/transfer') }}
@endsection
@section('content')
    <div class="row darkWebColor">
        <div class="col-12">
            <h1 class="darkWebColor"> Überweisen </h1>
        </div>
        <div class="col-12">
            <form method="post" class="form" action="{{ route('darkWeb.wallet.transfer', $hash) }}">
                @csrf
                <div class="form-row">
                    <div class="col-12">
                        <label for="hash">Nummer</label>
                        <input class="form-control" id="hash" name="hash" type="text">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12">
                        <label for="amount">Menge</label>
                        <input class="form-control" id="amount" name="amount" type="number">
                    </div>
                </div>
                <br/>
                <div class="form-row">
                    <button type="submit" class="btn-dark p-1 border darkWebBorder darkWebColor">
                        Überweisen
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
