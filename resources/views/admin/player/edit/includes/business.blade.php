<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.01.2021
 * Time: 19:13
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_BUSINESS)
<!-- Firma zuordnen -->
<h4>Firma + Rang zuweisen</h4>
<div class="form-group">
    <label for="businessId">Firma</label>
    <select class="form-control" id="businessId" name="businessId">
        <option value="0" {{ ($character->getBusinessId() == 0) ? 'selected' : '' }}>
            Keine Firma
        </option>
        @foreach($businesses as $business)
            <option
                value="{{ $business->id }}" {{ ($character->getBusinessId() == $business->id) ? 'selected' : '' }}>
                {{ $business->name }}
            </option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="businessRank">Rang</label>
    <select class="form-control" id="businessRank" name="businessRank">
        @for($i = 0; $i < 5; $i++)
            <option value="{{ $i }}" {{ ($character->getBusinessRank() == $i) ? 'selected' : '' }}>
                {{ $i }}
            </option>
        @endfor
    </select>
</div>
@endpermission
