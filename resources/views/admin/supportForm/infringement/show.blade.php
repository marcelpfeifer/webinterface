@php
    /** @var $entry \App\Model\Dto\WebAdminInfringement */
    /** @var $user \App\Model\Dto\Account */
@endphp
@extends('admin.layout.app')
@section('title')
    <i class="fab fa-wpforms"></i>
    Eintrag {{ $entry->getId() }}
    <a class="btn btn-dark btn-sm float-right"
       href="{{ route('admin.supportForm.infringement.index') }}"
       role="button">
        Zurück
    </a>
@endsection
@section('content')
    <dl class="row">
        <dt class="col-3">Datum</dt>
        <dd class="col-9">{{ $entry->getCreatedAt()->format('Y-m-d H:i:s') }}</dd>

        <dt class="col-3">Name IC</dt>
        <dd class="col-9">{{ $entry->getUserName() }}</dd>

        <dt class="col-3">Video Link</dt>
        <dd class="col-9">{{ $entry->getVideoLink() }}</dd>

        <dt class="col-3">Fallgrund</dt>
        <dd class="col-9">{{ $entry->getReason() }}</dd>

        <dt class="col-3">Gemeldet von ...</dt>
        <dd class="col-9">{{ $entry->getReportedBy() }}</dd>

        <dt class="col-3">Bearbeitet von...</dt>
        <dd class="col-9">{{ $user->getUsername() }}</dd>

        <dt class="col-3">Grobe Fallbeschreibung</dt>
        <dd class="col-9">{{ $entry->getDescription() }}</dd>

        <dt class="col-3">Durchgeführte Maßnahme</dt>
        <dd class="col-9">{{ $entry->getAction() }}</dd>
    </dl>
@endsection
