<?php
/**
 * Shortcuts
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 11.05.2020
 * Time: 21:13
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Shortcuts extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_GUID = 'guid';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'shortcuts';

    /**
     * @param Dto\Shortcuts $dto
     */
    public function insertEntry(Dto\Shortcuts $dto)
    {
        $this->insert([
            self::COLUMN_ID   => $dto->getId(),
            self::COLUMN_GUID => $dto->getGuid(),
        ]);
    }

    /**
     * @param Dto\Shortcuts $dto
     */
    public function updateEntry(Dto\Shortcuts $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update([
            self::COLUMN_GUID => $dto->getGuid(),
        ]);
    }

    /**
     * @param Dto\Shortcuts $dto
     */
    public function deleteEntry(Dto\Shortcuts $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param Shortcuts $entry
     * @return Dto\Shortcuts
     */
    public static function entryAsDto(Shortcuts $entry): Dto\Shortcuts
    {
        return (new Dto\Shortcuts())
            ->setId($entry->{self::COLUMN_ID})
            ->setGuid($entry->{self::COLUMN_GUID});
    }
}
