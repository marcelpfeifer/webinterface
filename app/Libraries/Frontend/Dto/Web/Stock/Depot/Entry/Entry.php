<?php
/**
 * Entry
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 02.09.2021
 * Time: 17:46
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Dto\Web\Stock\Depot\Entry;


class Entry
{
    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var string
     */
    private $amount = '';

    /**
     * @var string
     */
    private $buyPrice = '';

    /**
     * @var string
     */
    private $buyTotalPrice = '';

    /**
     * @var string
     */
    private $currentTotalValue = '';

    /**
     * @var string
     */
    private $currentValue = '';

    /**
     * @var string
     */
    private $currentValuePercent = '';

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Entry
     */
    public function setId(int $id): Entry
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getAmount(): string
    {
        return $this->amount;
    }

    /**
     * @param string $amount
     * @return Entry
     */
    public function setAmount(string $amount): Entry
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getBuyPrice(): string
    {
        return $this->buyPrice;
    }

    /**
     * @param string $buyPrice
     * @return Entry
     */
    public function setBuyPrice(string $buyPrice): Entry
    {
        $this->buyPrice = $buyPrice;
        return $this;
    }

    /**
     * @return string
     */
    public function getBuyTotalPrice(): string
    {
        return $this->buyTotalPrice;
    }

    /**
     * @param string $buyTotalPrice
     * @return Entry
     */
    public function setBuyTotalPrice(string $buyTotalPrice): Entry
    {
        $this->buyTotalPrice = $buyTotalPrice;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrentTotalValue(): string
    {
        return $this->currentTotalValue;
    }

    /**
     * @param string $currentTotalValue
     * @return Entry
     */
    public function setCurrentTotalValue(string $currentTotalValue): Entry
    {
        $this->currentTotalValue = $currentTotalValue;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrentValue(): string
    {
        return $this->currentValue;
    }

    /**
     * @param string $currentValue
     * @return Entry
     */
    public function setCurrentValue(string $currentValue): Entry
    {
        $this->currentValue = $currentValue;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrentValuePercent(): string
    {
        return $this->currentValuePercent;
    }

    /**
     * @param string $currentValuePercent
     * @return Entry
     */
    public function setCurrentValuePercent(string $currentValuePercent): Entry
    {
        $this->currentValuePercent = $currentValuePercent;
        return $this;
    }
}
