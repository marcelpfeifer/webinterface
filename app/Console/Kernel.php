<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('db:backup')->dailyAt('3:00');
        $schedule->command('webRegistrationCodes:clean')->dailyAt('4:00');
        $schedule->command('webSocialPosts:clean')->dailyAt('4:30');
        $schedule->command('webItemList:create')->dailyAt('5:00');
        $schedule->command('system:status')->everyTenMinutes();
        $schedule->command('serverLog:copy')->everyFiveMinutes();
        $schedule->command('stock:generate')
            ->weekdays()
            ->everyFiveMinutes()
            ->timezone('Europe/Berlin')
            ->between('10:00', '22:00');
        //  $schedule->command('tickets:synchronize')->dailyAt('6:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
