<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.10.2021
 * Time: 15:35
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Api\Player\Tp;

use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class Save extends AValidator implements IValidator
{
    /**
     * @return string[]
     */
    public function getRules(): array
    {
        return [
            'playerFrom'     => [
                'required',
                'int',
                'exists:character,id'
            ],
            'playerTo'     => [
                'required',
                'int',
                'exists:character,id'
            ],
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'playerFrom.required' => 'Die Spieler ID muss übergeben werden!',
            'playerFrom.int'      => 'Die Spieler ID muss ein Integer sein!',
            'playerFrom.exists'   => 'Die Spieler ID existiert nicht!',
            'playerTo.required' => 'Die Target Spieler ID muss übergeben werden!',
            'playerTo.int'      => 'Die Target Spieler ID muss ein Integer sein!',
            'playerTo.exists'   => 'Die Target Spieler ID existiert nicht!',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Api\Player\Tp\Save())
            ->setPlayerFrom($this->request->input('playerFrom'))
            ->setPlayerTo($this->request->input('playerTo'));
    }
}
