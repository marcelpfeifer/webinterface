<?php
/**
 * CallHistoryController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 24.11.2020
 * Time: 15:23
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\Player;

use App\Model\CallHistory;
use Illuminate\Http\Request;

class CallHistoryController
{

    /**
     * @param Request $request
     * @param int $characterId
     * @return \Illuminate\View\View
     */
    public function index(Request $request, int $characterId)
    {
        $history = (new CallHistory())->getCallHistoryByCharacterId($characterId);
        return view(
            'admin.player.callHistory.index',
            [
                'history'     => $history,
                'characterId' => $characterId
            ]
        );
    }
}
