<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 01.05.2020
 * Time: 09:48
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Web\Marketplace;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Model\Character;
use App\Model\Dto\WebMarketplaceBuy;
use App\Model\WebMarketplaceCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class IndexController extends Controller
{

    /**
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, string $hash)
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('web.marketplace.index', $hash);
        }
        $characterDto = Character::entryAsDto($character);
        return view('web.marketplace.index', [
            'hash'       => $hash,
            'character'  => $characterDto,
            'offers'     => \App\Model\WebMarketplaceBuy::getAllOffers(),
            'categories' => $this->getCategories()
        ]);
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function offers(Request $request, string $hash)
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('web.marketplace.index', $hash);
        }

        $characterDto = Character::entryAsDto($character);
        return view('web.marketplace.offers', [
            'hash'       => $hash,
            'character'  => $characterDto,
            'offers'     => \App\Model\WebMarketplaceBuy::getOffersByCharacterId($characterDto->getId()),
            'categories' => $this->getCategories()
        ]);
    }

    /**
     * @param Request $request
     * @param int $category
     * @param string $hash
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function category(Request $request, int $category, string $hash)
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('web.marketplace.index', $hash);
        }
        $category = WebMarketplaceCategory::find($category);
        if (!($category)) {
            return redirect()->route('web.marketplace.index', $hash);
        }
        $characterDto = Character::entryAsDto($character);

        return view('web.marketplace.category', [
            'hash'        => $hash,
            'character'   => $characterDto,
            'offers'      => \App\Model\WebMarketplaceBuy::getOffersByCategoryId($category->id),
            'categories'  => $this->getCategories(),
            'categoryDto' => WebMarketplaceCategory::entryAsDto($category),
        ]);
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request, string $hash)
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('web.marketplace.index', $hash);
        }
        $characterDto = Character::entryAsDto($character);

        return view('web.marketplace.create', [
            'hash'       => $hash,
            'character'  => $characterDto,
            'categories' => $this->getCategories()

        ]);
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request, string $hash)
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('web.marketplace.index', $hash);
        }
        // Validiere die Anfrage
        $validator = $this->validateRequest($request);
        if ($validator->fails()) {
            $messages = [];
            foreach ($validator->errors()->all() as $error) {
                $messages[] = (new Message())
                    ->setMessage($error)
                    ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
            }
            return redirect()->route('web.marketplace.create', $hash)
                ->with('messages', $messages)
                ->withInput()
                ->withErrors($validator->errors());
        }
        $characterDto = Character::entryAsDto($character);

        $imageUrl = $request->input('imageUrl');
        $message = $request->input('message');
        $categoryId = $request->input('categoryId');

        $dto = (new WebMarketplaceBuy())
            ->setCharacterId($characterDto->getId())
            ->setCategoryId($categoryId ?: null)
            ->setName($characterDto->getName())
            ->setImageUrl($imageUrl)
            ->setMessage($message);
        (new \App\Model\WebMarketplaceBuy())->insertEntry($dto);

        return redirect()->route('web.marketplace.offers', $hash);
    }

    /**
     * @param Request $request
     * @param int $id
     * @param string $hash
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request, int $id, string $hash)
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('web.marketplace.index', $hash);
        }

        $entry = \App\Model\WebMarketplaceBuy::find($id);
        if (!($entry)) {
            return redirect()->route('web.marketplace.index', $hash);
        }
        $dto = \App\Model\WebMarketplaceBuy::entryAsDto($entry);
        $characterDto = Character::entryAsDto($character);
        if ($dto->getCharacterId() !== $characterDto->getId()) {
            return redirect()->route('web.marketplace.index', $hash);
        }

        (new \App\Model\WebMarketplaceBuy())->deleteEntry($dto);

        return redirect()->route('web.marketplace.index', $hash);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    private function validateRequest(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'imageUrl'   => [
                    'url',
                    'required'
                ],
                'categoryId' => [
                    'nullable',
                    'int',
                ],
                'message'    => [
                    'string',
                    'required'
                ]
            ],
            [
                'imageUrl.url'      => 'Die Bilderurl muss eine valide URL sein',
                'imageUrl.required' => 'Die Bilderurl muss angegeben werden',
                'categoryId.int'    => 'Die Kategorie muss eine Zahl sein',
                'message.string'    => 'Die Nachricht muss ein Text sein',
                'message.required'  => 'Die Nachricht muss ausgefüllt werden',

            ]);

        return $validator;
    }

    /**
     * @return \App\Model\Dto\WebMarketplaceCategory[]
     */
    private function getCategories()
    {
        $categories = [];
        foreach (\App\Model\WebMarketplaceCategory::all() as $entry) {
            $categories[] = \App\Model\WebMarketplaceCategory::entryAsDto($entry);
        }
        return $categories;
    }
}
