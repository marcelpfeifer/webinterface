<?php
/**
 * WebAdminMail
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 17.10.2020
 * Time: 19:54
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class WebAdminRefund
 * @package App\Model
 */
class WebMail extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_FROM = 'from';

    /**
     * @var string
     */
    const COLUMN_TO = 'to';

    /**
     * @var string
     */
    const COLUMN_SENDER = 'sender';

    /**
     * @var string
     */
    const COLUMN_RECEIVER = 'receiver';


    /**
     * @var string
     */
    const COLUMN_TITLE = 'title';

    /**
     * @var string
     */
    const COLUMN_MESSAGE = 'message';

    /**
     * @var string
     */
    const COLUMN_SEEN = 'seen';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webMail';

    /**
     * @return BelongsTo
     */
    public function from(): BelongsTo
    {
        return $this->belongsTo(WebMailAddress::class, self::COLUMN_FROM, WebMailAddress::COLUMN_ID);
    }

    /**
     * @return BelongsTo
     */
    public function to(): BelongsTo
    {
        return $this->belongsTo(WebMailAddress::class, self::COLUMN_TO, WebMailAddress::COLUMN_ID);
    }

    /**
     * @return HasMany
     */
    public function attachments(): HasMany
    {
        return $this->hasMany(WebMailAttachment::class, WebMailAttachment::COLUMN_WEB_MAIL_ID, self::COLUMN_ID);
    }

    /**
     * @param int $to
     * @return Dto\WebMail[]
     */
    public function getAllMailsByTo(int $to): array
    {
        $results = [];

        $mails = $this
            ->where(self::COLUMN_TO, $to)
            ->orderBy(self::CREATED_AT, 'desc')
            ->get();

        foreach ($mails as $mail) {
            $results[] = self::entryAsDto($mail);
        }
        return $results;
    }

    /**
     * @param int $from
     * @return Dto\WebMail[]
     */
    public function getAllMailsByFrom(int $from): array
    {
        $results = [];

        $mails = $this
            ->where(self::COLUMN_FROM, $from)
            ->where(self::COLUMN_TO, null)
            ->orderBy(self::CREATED_AT, 'desc')
            ->get();

        foreach ($mails as $mail) {
            $results[] = self::entryAsDto($mail);
        }
        return $results;
    }

    /**
     * @param Dto\WebMail $dto
     * @return int
     */
    public function insertEntry(Dto\WebMail $dto): int
    {
        return $this->insertGetId(
            [
                self::COLUMN_ID       => $dto->getId(),
                self::COLUMN_FROM     => $dto->getFrom(),
                self::COLUMN_TO       => $dto->getTo(),
                self::COLUMN_SENDER   => $dto->getSender(),
                self::COLUMN_RECEIVER => $dto->getReceiver(),
                self::COLUMN_TITLE    => $dto->getTitle(),
                self::COLUMN_MESSAGE  => $dto->getMessage(),
                self::COLUMN_SEEN     => $dto->isSeen(),
                self::CREATED_AT      => Carbon::now(),
                self::UPDATED_AT      => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebMail $dto
     */
    public function updateEntry(Dto\WebMail $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_FROM     => $dto->getFrom(),
                self::COLUMN_TO       => $dto->getTo(),
                self::COLUMN_SENDER   => $dto->getSender(),
                self::COLUMN_RECEIVER => $dto->getReceiver(),
                self::COLUMN_TITLE    => $dto->getTitle(),
                self::COLUMN_MESSAGE  => $dto->getMessage(),
                self::COLUMN_SEEN     => $dto->isSeen(),
                self::UPDATED_AT      => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebMail $dto
     */
    public function deleteEntry(\App\Model\Dto\WebMail $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebMail $entry
     * @return Dto\WebMail
     */
    public static function entryAsDto(WebMail $entry): Dto\WebMail
    {
        return (new Dto\WebMail())
            ->setId($entry->{self::COLUMN_ID})
            ->setFrom($entry->{self::COLUMN_FROM})
            ->setTo($entry->{self::COLUMN_TO})
            ->setSender($entry->{self::COLUMN_SENDER})
            ->setReceiver($entry->{self::COLUMN_RECEIVER})
            ->setTitle($entry->{self::COLUMN_TITLE})
            ->setMessage($entry->{self::COLUMN_MESSAGE})
            ->setSeen($entry->{self::COLUMN_SEEN})
            ->setCreatedAt($entry->{self::CREATED_AT})
            ->setUpdatedAt($entry->{self::UPDATED_AT});
    }
}
