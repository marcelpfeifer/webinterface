<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameTwitterToSocial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('webTwitterFollow','webSocialFollow');
        Schema::rename('webTwitterPost','webSocialPost');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('webSocialFollow','webTwitterFollow');
        Schema::rename('webSocialPost','webTwitterPost');
    }
}
