<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 12.08.2021
 * Time: 13:01
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Auth\Mail\Mailbox;

use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class Save extends AValidator implements IValidator
{

    /**
     * @return string[]
     */
    public function getRules(): array
    {
        return [
            'email' => [
                'required',
                'string',
                'email',
                'unique:webMailAddress'
            ],
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'email.required' => 'Die Email muss übergeben werden!',
            'email.string'   => 'Die Email darf nur aus Zeichen bestehen!',
            'email.email'    => 'Die Email ist nicht valide!',
            'email.unique'   => 'Die Email gibt es bereits!',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Auth\Mail\Mailbox\Save())
            ->setEmail($this->request->input('email'));
    }
}
