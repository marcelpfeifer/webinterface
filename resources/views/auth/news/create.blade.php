<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.08.2021
 * Time: 14:27
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('layouts.app')
@section('title')
    <i class="fas fa-newspaper"></i>
    News erstellen
    <a href="{{ route('auth.news.index') }}" class="btn btn-secondary float-right">
        Zurück
    </a>
@endsection
@section('content')
    <form action="{{ route('auth.news.save') }}" method="POST" id="newsForm">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="title">Titel</label>
            <input type="text" class="form-control" id="title" name="title" autocomplete="off" required>
        </div>
        <div class="form-group">
            <label for="name">Kategorie</label>
            <select id="webNewsCategoryId" name="webNewsCategoryId" class="form-control">
                <option value=""></option>
                @foreach($categories as $category)
                    <option value="{{ $category->getId() }}">{{ $category->getName() }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="image">Bild</label>
            <input type="text" class="form-control" id="image" name="image" autocomplete="off" required>
        </div>
        <div class="form-group">
            <label for="shortDescription">Kurze Beschreibung</label>
            <textarea id="shortDescription" name="shortDescription" class="form-control" rows="3" required></textarea>
        </div>
        <div class="form-group">
            <label for="description">Beschreibung</label>
            <div id="editor">

            </div>
            <input type="hidden" class="form-control" id="description" name="description">
        </div>
        <div class="form-group">
            <label for="author">Erfasser</label>
            <input type="text" class="form-control" id="author" name="author" autocomplete="off" required>
        </div>
        <button type="submit" class="btn btn-primary">Erstellen</button>
    </form>
@endsection

