<?php
/**
 * BugReportController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 11.08.2021
 * Time: 10:00
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin;


use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Http\Request\Admin\BugReport\Save;
use App\Http\Request\AValidator;
use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Model\Business;
use App\Model\Character;
use App\Model\WebBugReport;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class BugReportController extends Controller
{

    /**
     * @return View
     */
    public function index(): View
    {
        $reports = (new WebBugReport())->getUnapprovedEntries();
        return view(
            'admin.bugReport.index',
            [
                'reports' => $reports,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @return View
     */
    public function show(Request $request, int $id): View
    {
        $report = null;
        $character = null;
        $entry = WebBugReport::find($id);
        if ($entry) {
            $report = WebBugReport::entryAsDto($entry);
            $characterEntry = $entry->character;
            if ($characterEntry) {
                $character = Character::entryAsDto($entry->character);
            }
        }
        return view(
            'admin.bugReport.show',
            [
                'report'    => $report,
                'character' => $character,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function save(Request $request, int $id): RedirectResponse
    {
        // Validiere die Anfrage
        $validator = (new Save($request, $id));
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            return redirect()->route('admin.bugReport.show', $id)
                ->with('messages', $validator->getErrorsAsMessageArray($validatedData))
                ->withInput()
                ->withErrors($validatedData->errors());
        }

        /**
         * @var $requestDto \App\Http\Request\Dto\Admin\BugReport\Save
         */
        $requestDto = $validator->toDto();

        \App\Libraries\Frontend\Admin\BugReport\Save::save($requestDto, false);

        $messages[] = (new Message())
            ->setMessage("Der Eintrag wurde aktualisiert")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.bugReport.index')->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function send(Request $request, int $id): RedirectResponse
    {
        // Validiere die Anfrage
        $validator = (new Save($request, $id));
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            return redirect()->route('admin.bugReport.show', $id)
                ->with('messages', $validator->getErrorsAsMessageArray($validatedData))
                ->withInput()
                ->withErrors($validatedData->errors());
        }

        /**
         * @var $requestDto \App\Http\Request\Dto\Admin\BugReport\Save
         */
        $requestDto = $validator->toDto();

        \App\Libraries\Frontend\Admin\BugReport\Save::save($requestDto, true);

        $messages[] = (new Message())
            ->setMessage("Der Eintrag wurde aktualisiert")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.bugReport.index')->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function delete(Request $request, int $id): RedirectResponse
    {
        $entry = WebBugReport::find($id);
        if (!($entry)) {
            return redirect()->route('admin.bugReport.index');
        }

        $dto = WebBugReport::entryAsDto($entry);
        (new WebBugReport())->deleteEntry($dto);

        Logging::delete(Name::BUG_REPORT, Auth::id(), $dto);

        $messages[] = (new Message())
            ->setMessage("Der Eintrag wurde gelöscht")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.bugReport.index')->with('messages', $messages);
    }
}
