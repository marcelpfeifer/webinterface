<?php
/**
 * Update
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 12.10.2021
 * Time: 15:29
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Admin\ShopUnregisteredVehicles;

use App\Http\Request\Dto\ADto;

class Update extends ADto
{
    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var string
     */
    private $model = '';

    /**
     * @var int
     */
    private $stock = 0;

    /**
     * @var string
     */
    private $positionX = '';

    /**
     * @var string
     */
    private $positionY = '';

    /**
     * @var string
     */
    private $positionZ = '';

    /**
     * @var string
     */
    private $rotationX = '';

    /**
     * @var string
     */
    private $rotationY = '';

    /**
     * @var string
     */
    private $rotationZ = '';

    /**
     * @var int
     */
    private $mapId = 0;

    /**
     * @var string
     */
    private $npcPositionX = '';

    /**
     * @var string
     */
    private $npcPositionY = '';

    /**
     * @var string
     */
    private $npcPositionZ = '';

    /**
     * @var string
     */
    private $npcRotation = '';

    /**
     * @var string
     */
    private $npcModel = '';

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Update
     */
    public function setId(int $id): Update
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getModel(): string
    {
        return $this->model;
    }

    /**
     * @param string $model
     * @return Update
     */
    public function setModel(string $model): Update
    {
        $this->model = $model;
        return $this;
    }

    /**
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     * @return Update
     */
    public function setStock(int $stock): Update
    {
        $this->stock = $stock;
        return $this;
    }

    /**
     * @return string
     */
    public function getPositionX(): string
    {
        return $this->positionX;
    }

    /**
     * @param string $positionX
     * @return Update
     */
    public function setPositionX(string $positionX): Update
    {
        $this->positionX = $positionX;
        return $this;
    }

    /**
     * @return string
     */
    public function getPositionY(): string
    {
        return $this->positionY;
    }

    /**
     * @param string $positionY
     * @return Update
     */
    public function setPositionY(string $positionY): Update
    {
        $this->positionY = $positionY;
        return $this;
    }

    /**
     * @return string
     */
    public function getPositionZ(): string
    {
        return $this->positionZ;
    }

    /**
     * @param string $positionZ
     * @return Update
     */
    public function setPositionZ(string $positionZ): Update
    {
        $this->positionZ = $positionZ;
        return $this;
    }

    /**
     * @return string
     */
    public function getRotationX(): string
    {
        return $this->rotationX;
    }

    /**
     * @param string $rotationX
     * @return Update
     */
    public function setRotationX(string $rotationX): Update
    {
        $this->rotationX = $rotationX;
        return $this;
    }

    /**
     * @return string
     */
    public function getRotationY(): string
    {
        return $this->rotationY;
    }

    /**
     * @param string $rotationY
     * @return Update
     */
    public function setRotationY(string $rotationY): Update
    {
        $this->rotationY = $rotationY;
        return $this;
    }

    /**
     * @return string
     */
    public function getRotationZ(): string
    {
        return $this->rotationZ;
    }

    /**
     * @param string $rotationZ
     * @return Update
     */
    public function setRotationZ(string $rotationZ): Update
    {
        $this->rotationZ = $rotationZ;
        return $this;
    }

    /**
     * @return int
     */
    public function getMapId(): int
    {
        return $this->mapId;
    }

    /**
     * @param int $mapId
     * @return Update
     */
    public function setMapId(int $mapId): Update
    {
        $this->mapId = $mapId;
        return $this;
    }

    /**
     * @return string
     */
    public function getNpcPositionX(): string
    {
        return $this->npcPositionX;
    }

    /**
     * @param string $npcPositionX
     * @return Update
     */
    public function setNpcPositionX(string $npcPositionX): Update
    {
        $this->npcPositionX = $npcPositionX;
        return $this;
    }

    /**
     * @return string
     */
    public function getNpcPositionY(): string
    {
        return $this->npcPositionY;
    }

    /**
     * @param string $npcPositionY
     * @return Update
     */
    public function setNpcPositionY(string $npcPositionY): Update
    {
        $this->npcPositionY = $npcPositionY;
        return $this;
    }

    /**
     * @return string
     */
    public function getNpcPositionZ(): string
    {
        return $this->npcPositionZ;
    }

    /**
     * @param string $npcPositionZ
     * @return Update
     */
    public function setNpcPositionZ(string $npcPositionZ): Update
    {
        $this->npcPositionZ = $npcPositionZ;
        return $this;
    }

    /**
     * @return string
     */
    public function getNpcRotation(): string
    {
        return $this->npcRotation;
    }

    /**
     * @param string $npcRotation
     * @return Update
     */
    public function setNpcRotation(string $npcRotation): Update
    {
        $this->npcRotation = $npcRotation;
        return $this;
    }

    /**
     * @return string
     */
    public function getNpcModel(): string
    {
        return $this->npcModel;
    }

    /**
     * @param string $npcModel
     * @return Update
     */
    public function setNpcModel(string $npcModel): Update
    {
        $this->npcModel = $npcModel;
        return $this;
    }
}
