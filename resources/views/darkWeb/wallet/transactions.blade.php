<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 16.08.2020
 * Time: 16:02
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */


/**
 * @var $transactions \App\Model\Dto\Transactions\TransactionsWithCharacters[]
 */
?>
@extends('darkWeb.layout.app')
@section('url')
    {{  md5('https://eternityV.de/wallet/transactions') }}
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <table class="table table-responsive table-dark">
                <thead>
                <tr>
                    <th class="darkWebColor" scope="col">Sender</th>
                    <th class="darkWebColor" scope="col">Empfänger</th>
                    <th class="darkWebColor" scope="col">Menge</th>
                </tr>
                </thead>
                <tbody>
                @foreach($transactions as $transaction)
                    <tr>
                        <td class="darkWebColor">{{ $transaction->getTx()->getBitcoinHashIllegal() }}</td>
                        <td class="darkWebColor">{{ $transaction->getRx()->getBitcoinHashIllegal() }}</td>
                        <td class="darkWebColor">{{ $transaction->getTransaction()->getAmount() }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
