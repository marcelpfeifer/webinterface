<?php
/**
 * Messages.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 01.11.2019
 * Time: 13:23
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use App\Dto\Messages\Frontend\Message;
use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_NUMBER_FROM = 'numberFrom';

    /**
     * @var string
     */
    const COLUMN_NUMBER_TO = 'numberTo';

    /**
     * @var string
     */
    const COLUMN_MESSAGE = 'messages';

    /**
     * @var string
     */
    const COLUMN_DATE = 'date';

    /**
     * @var string
     */
    const COLUMN_CREATED_AT = 'createdAt';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'messages';

    /**
     * @param Dto\Messages $dto
     */
    public function insertEntry(Dto\Messages $dto)
    {
        $this->insert([
            self::COLUMN_ID          => $dto->getId(),
            self::COLUMN_NUMBER_FROM => $dto->getNumberFrom(),
            self::COLUMN_NUMBER_TO   => $dto->getNumberTo(),
            self::COLUMN_MESSAGE     => $dto->getMessage(),
            self::COLUMN_DATE        => $dto->getDate(),
            self::COLUMN_CREATED_AT  => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\Messages $dto
     */
    public function updateEntry(Dto\Messages $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update([
            self::COLUMN_NUMBER_FROM => $dto->getNumberFrom(),
            self::COLUMN_NUMBER_TO   => $dto->getNumberTo(),
            self::COLUMN_MESSAGE     => $dto->getMessage(),
            self::COLUMN_DATE        => $dto->getDate(),
        ]);
    }

    /**
     * @param Dto\Messages $dto
     */
    public function deleteEntry(Dto\Messages $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param Messages $entry
     * @return Dto\Messages
     */
    public static function entryAsDto(Messages $entry): Dto\Messages
    {
        return (new Dto\Messages())
            ->setId($entry->{self::COLUMN_ID})
            ->setNumberFrom($entry->{self::COLUMN_NUMBER_FROM})
            ->setNumberTo($entry->{self::COLUMN_NUMBER_TO})
            ->setMessage($entry->{self::COLUMN_MESSAGE})
            ->setDate($entry->{self::COLUMN_DATE});
    }


    public function getMessagesOfNumber(int $characterId, int $number)
    {
        $result = $this
            ->where(self::COLUMN_NUMBER_FROM, $number)
            ->orWhere(self::COLUMN_NUMBER_TO, $number)
            ->get();

        return $result;
    }

}
