<?php
/**
 * ImageName
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 28.03.2020
 * Time: 14:26
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Account;

use App\Model\Dto\Account;

class ImageName
{

    /**
     * @param \Illuminate\Http\UploadedFile|null $file
     * @param Account $dto
     * @return Account
     */
    public static function upload(?\Illuminate\Http\UploadedFile $file, Account $dto): Account
    {
        if ($file) {
            $path = public_path('img/user/logo/');
            $imageName = $dto->getId() . '.jpg';
            $fullPath = $path . $imageName;

            if (!is_dir($path)) {
                mkdir($path, 0755, true);
            }
            if (file_exists($fullPath)) {
                unlink($fullPath);
            }
            rename($file->getPathname(), $fullPath);
            $dto->setImageName($imageName);
        }
        return $dto;
    }
}
