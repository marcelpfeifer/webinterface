<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateWebAdminWhitelistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Model\WebAdminWhitelist::truncate();
        Schema::table(
            'webAdminWhitelist',
            function (Blueprint $table) {
                $table->timestamp('retryDate')->nullable()->after('status');
            }
        );
        \Illuminate\Support\Facades\DB::statement(
            "ALTER TABLE webAdminWhitelist MODIFY status ENUM('APPROVED','DENIED') NOT NULL"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'webAdminWhitelist',
            function (Blueprint $table) {
                $table->dropColumn(['retryDate']);
            }
        );
        \Illuminate\Support\Facades\DB::statement(
            "ALTER TABLE webAdminWhitelist MODIFY status ENUM('APPROVED','DENIED','PENDING','FINISHED') NOT NULL"
        );
    }
}
