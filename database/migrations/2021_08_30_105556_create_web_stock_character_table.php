<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebStockCharacterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'webStockCharacter',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('webStockId')->nullable()->index();
                $table->foreign('webStockId')->references('id')->on('webStock')->onDelete('set null');
                $table->unsignedInteger('characterId')->index();
                $table->integer('amount');
                $table->integer('totalPrice');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webStockCharacter');
    }
}
