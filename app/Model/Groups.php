<?php
/**
 * Groups.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 01.11.2019
 * Time: 13:23
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Groups extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    const COLUMN_TYPE = 'type';

    /**
     * @var string
     */
    const COLUMN_MONEY = 'money';

    /**
     * @var string
     */
    const COLUMN_MESSAGE = 'message';

    /**
     * @var string
     */
    const COLUMN_CREATED_AT = 'createdAt';

    /**
     * @var string
     */
    const COLUMN_UPDATED_AT = 'updatedAt';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'groups';

    /**
     * @param Dto\Groups $dto
     */
    public function insertEntry(Dto\Groups $dto)
    {
        $this->insert([
            self::COLUMN_ID         => $dto->getId(),
            self::COLUMN_NAME       => $dto->getName(),
            self::COLUMN_TYPE       => $dto->getType(),
            self::COLUMN_MONEY      => $dto->getMoney(),
            self::COLUMN_MESSAGE    => $dto->getMessages(),
            self::COLUMN_CREATED_AT => Carbon::now(),
            self::COLUMN_UPDATED_AT => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\Groups $dto
     */
    public function updateEntry(Dto\Groups $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update([
            self::COLUMN_ID         => $dto->getId(),
            self::COLUMN_NAME       => $dto->getName(),
            self::COLUMN_TYPE       => $dto->getType(),
            self::COLUMN_MONEY      => $dto->getMoney(),
            self::COLUMN_MESSAGE    => $dto->getMessages(),
            self::COLUMN_UPDATED_AT => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\Groups $dto
     */
    public function deleteEntry(Dto\Groups $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param Groups $entry
     * @return Dto\Groups
     */
    public static function entryAsDto(Groups $entry): Dto\Groups
    {
        return (new Dto\Groups())
            ->setId($entry->{self::COLUMN_ID})
            ->setName($entry->{self::COLUMN_NAME})
            ->setType($entry->{self::COLUMN_TYPE})
            ->setMoney($entry->{self::COLUMN_MONEY})
            ->setMessages($entry->{self::COLUMN_MESSAGE});
    }
}
