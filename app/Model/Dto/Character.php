<?php
/**
 * Character
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 01.11.2019
 * Time: 13:40
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

use App\Libraries\Logging\ILogging;
use App\Libraries\Logging\Traits\Variables;
use Carbon\Carbon;

/**
 * Class Character
 * @package App\Model\Dto
 */
class Character implements ILogging
{
    use Variables;

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $email = '';

    /**
     * @var string
     */
    private $hash = '';

    /**
     * @var string
     */
    private $dob = '';

    /**
     * @var int
     */
    private $playingTime = 0;

    /**
     * @var string
     */
    private $face = '';

    /**
     * @var string
     */
    private $inventory = '';

    /**
     * @var string
     */
    private $lastPosition = '';

    /**
     * @var int
     */
    private $health = 0;

    /**
     * @var int
     */
    private $armour = 0;

    /**
     * @var int
     */
    private $cash = 0;

    /**
     * @var int
     */
    private $bank = 0;

    /**
     * @var int
     */
    private $hunger = 0;

    /**
     * @var int
     */
    private $thirst = 0;

    /**
     * @var int
     */
    private $dead = 0;

    /**
     * @var int
     */
    private $houseId = 0;

    /**
     * @var int
     */
    private $drivingLicenseA = 0;

    /**
     * @var int
     */
    private $drivingLicenseB = 0;

    /**
     * @var int
     */
    private $drivingLicenseCE = 0;

    /**
     * @var int
     */
    private $maxGarage = 0;

    /**
     * @var Carbon
     */
    private $lastLogin = null;

    /**
     * @var int
     */
    private $copRank = 0;

    /**
     * @var int
     */
    private $medRank = 0;

    /**
     * @var int
     */
    private $fibRank = 0;

    /**
     * @var int
     */
    private $bwfiRank = 0;

    /**
     * @var int
     */
    private $group = 0;

    /**
     * @var int
     */
    private $groupRank = 0;

    /**
     * @var int
     */
    private $dojRank = 0;

    /**
     * @var int
     */
    private $lawyerRank = 0;

    /**
     * @var int
     */
    private $taxiRank = 0;

    /**
     * @var int
     */
    private $dimension = 0;

    /**
     * @var int
     */
    private $serviceNumber = 0;

    /**
     * @var string
     */
    private $serviceName = '';

    /**
     * @var int
     */
    private $businessId = 0;

    /**
     * @var int
     */
    private $businessRank = 0;

    /**
     * @var float
     */
    private $bitcoin = 0.00;

    /**
     * @var float
     */
    private $bitcoinIllegal = 0.00;

    /**
     * @var string|null
     */
    private $bitcoinHashLegal = null;

    /**
     * @var string|null
     */
    private $bitcoinHashIllegal = null;

    /**
     * @var int
     */
    private $socialPoints = 0;

    /**
     * @var int
     */
    private $isDuty = 0;

    /**
     * @var int
     */
    private $gunLicense = 0;

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @var null|Carbon
     */
    private $updatedAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Character
     */
    public function setId(?int $id): Character
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Character
     */
    public function setName(string $name): Character
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Character
     */
    public function setEmail(string $email): Character
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     * @return Character
     */
    public function setHash(string $hash): Character
    {
        $this->hash = $hash;
        return $this;
    }

    /**
     * @return string
     */
    public function getDob(): string
    {
        return $this->dob;
    }

    /**
     * @param string $dob
     * @return Character
     */
    public function setDob(string $dob): Character
    {
        $this->dob = $dob;
        return $this;
    }

    /**
     * @return int
     */
    public function getPlayingTime(): int
    {
        return $this->playingTime;
    }

    /**
     * @param int $playingTime
     * @return Character
     */
    public function setPlayingTime(int $playingTime): Character
    {
        $this->playingTime = $playingTime;
        return $this;
    }

    /**
     * @return string
     */
    public function getFace(): string
    {
        return $this->face;
    }

    /**
     * @param string $face
     * @return Character
     */
    public function setFace(string $face): Character
    {
        $this->face = $face;
        return $this;
    }

    /**
     * @return string
     */
    public function getInventory(): string
    {
        return $this->inventory;
    }

    /**
     * @param string $inventory
     * @return Character
     */
    public function setInventory(string $inventory): Character
    {
        $this->inventory = $inventory;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastPosition(): string
    {
        return $this->lastPosition;
    }

    /**
     * @param string $lastPosition
     * @return Character
     */
    public function setLastPosition(string $lastPosition): Character
    {
        $this->lastPosition = $lastPosition;
        return $this;
    }

    /**
     * @return int
     */
    public function getHealth(): int
    {
        return $this->health;
    }

    /**
     * @param int $health
     * @return Character
     */
    public function setHealth(int $health): Character
    {
        $this->health = $health;
        return $this;
    }

    /**
     * @return int
     */
    public function getArmour(): int
    {
        return $this->armour;
    }

    /**
     * @param int $armour
     * @return Character
     */
    public function setArmour(int $armour): Character
    {
        $this->armour = $armour;
        return $this;
    }

    /**
     * @return int
     */
    public function getCash(): int
    {
        return $this->cash;
    }

    /**
     * @param int $cash
     * @return Character
     */
    public function setCash(int $cash): Character
    {
        $this->cash = $cash;
        return $this;
    }

    /**
     * @return int
     */
    public function getBank(): int
    {
        return $this->bank;
    }

    /**
     * @param int $bank
     * @return Character
     */
    public function setBank(int $bank): Character
    {
        $this->bank = $bank;
        return $this;
    }

    /**
     * @return int
     */
    public function getHunger(): int
    {
        return $this->hunger;
    }

    /**
     * @param int $hunger
     * @return Character
     */
    public function setHunger(int $hunger): Character
    {
        $this->hunger = $hunger;
        return $this;
    }

    /**
     * @return int
     */
    public function getThirst(): int
    {
        return $this->thirst;
    }

    /**
     * @param int $thirst
     * @return Character
     */
    public function setThirst(int $thirst): Character
    {
        $this->thirst = $thirst;
        return $this;
    }

    /**
     * @return int
     */
    public function getDead(): int
    {
        return $this->dead;
    }

    /**
     * @param int $dead
     * @return Character
     */
    public function setDead(int $dead): Character
    {
        $this->dead = $dead;
        return $this;
    }

    /**
     * @return int
     */
    public function getHouseId(): int
    {
        return $this->houseId;
    }

    /**
     * @param int $houseId
     * @return Character
     */
    public function setHouseId(int $houseId): Character
    {
        $this->houseId = $houseId;
        return $this;
    }

    /**
     * @return int
     */
    public function getDrivingLicenseA(): int
    {
        return $this->drivingLicenseA;
    }

    /**
     * @param int $drivingLicenseA
     * @return Character
     */
    public function setDrivingLicenseA(int $drivingLicenseA): Character
    {
        $this->drivingLicenseA = $drivingLicenseA;
        return $this;
    }

    /**
     * @return int
     */
    public function getDrivingLicenseB(): int
    {
        return $this->drivingLicenseB;
    }

    /**
     * @param int $drivingLicenseB
     * @return Character
     */
    public function setDrivingLicenseB(int $drivingLicenseB): Character
    {
        $this->drivingLicenseB = $drivingLicenseB;
        return $this;
    }

    /**
     * @return int
     */
    public function getDrivingLicenseCE(): int
    {
        return $this->drivingLicenseCE;
    }

    /**
     * @param int $drivingLicenseCE
     * @return Character
     */
    public function setDrivingLicenseCE(int $drivingLicenseCE): Character
    {
        $this->drivingLicenseCE = $drivingLicenseCE;
        return $this;
    }

    /**
     * @return int
     */
    public function getMaxGarage(): int
    {
        return $this->maxGarage;
    }

    /**
     * @param int $maxGarage
     * @return Character
     */
    public function setMaxGarage(int $maxGarage): Character
    {
        $this->maxGarage = $maxGarage;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getLastLogin(): Carbon
    {
        return $this->lastLogin;
    }

    /**
     * @param Carbon $lastLogin
     * @return Character
     */
    public function setLastLogin(Carbon $lastLogin): Character
    {
        $this->lastLogin = $lastLogin;
        return $this;
    }

    /**
     * @return int
     */
    public function getCopRank(): int
    {
        return $this->copRank;
    }

    /**
     * @param int $copRank
     * @return Character
     */
    public function setCopRank(int $copRank): Character
    {
        $this->copRank = $copRank;
        return $this;
    }

    /**
     * @return int
     */
    public function getMedRank(): int
    {
        return $this->medRank;
    }

    /**
     * @param int $medRank
     * @return Character
     */
    public function setMedRank(int $medRank): Character
    {
        $this->medRank = $medRank;
        return $this;
    }

    /**
     * @return int
     */
    public function getFibRank(): int
    {
        return $this->fibRank;
    }

    /**
     * @param int $fibRank
     * @return Character
     */
    public function setFibRank(int $fibRank): Character
    {
        $this->fibRank = $fibRank;
        return $this;
    }

    /**
     * @return int
     */
    public function getBwfiRank(): int
    {
        return $this->bwfiRank;
    }

    /**
     * @param int $bwfiRank
     * @return Character
     */
    public function setBwfiRank(int $bwfiRank): Character
    {
        $this->bwfiRank = $bwfiRank;
        return $this;
    }

    /**
     * @return int
     */
    public function getTaxiRank(): int
    {
        return $this->taxiRank;
    }

    /**
     * @param int $taxiRank
     * @return Character
     */
    public function setTaxiRank(int $taxiRank): Character
    {
        $this->taxiRank = $taxiRank;
        return $this;
    }

    /**
     * @return int
     */
    public function getGroup(): int
    {
        return $this->group;
    }

    /**
     * @param int $group
     * @return Character
     */
    public function setGroup(int $group): Character
    {
        $this->group = $group;
        return $this;
    }

    /**
     * @return int
     */
    public function getGroupRank(): int
    {
        return $this->groupRank;
    }

    /**
     * @param int $groupRank
     * @return Character
     */
    public function setGroupRank(int $groupRank): Character
    {
        $this->groupRank = $groupRank;
        return $this;
    }

    /**
     * @return int
     */
    public function getDojRank(): int
    {
        return $this->dojRank;
    }

    /**
     * @param int $dojRank
     * @return Character
     */
    public function setDojRank(int $dojRank): Character
    {
        $this->dojRank = $dojRank;
        return $this;
    }

    /**
     * @return int
     */
    public function getLawyerRank(): int
    {
        return $this->lawyerRank;
    }

    /**
     * @param int $lawyerRank
     * @return Character
     */
    public function setLawyerRank(int $lawyerRank): Character
    {
        $this->lawyerRank = $lawyerRank;
        return $this;
    }

    /**
     * @return int
     */
    public function getDimension(): int
    {
        return $this->dimension;
    }

    /**
     * @param int $dimension
     * @return Character
     */
    public function setDimension(int $dimension): Character
    {
        $this->dimension = $dimension;
        return $this;
    }

    /**
     * @return int
     */
    public function getServiceNumber(): int
    {
        return $this->serviceNumber;
    }

    /**
     * @param int $serviceNumber
     * @return Character
     */
    public function setServiceNumber(int $serviceNumber): Character
    {
        $this->serviceNumber = $serviceNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getServiceName(): string
    {
        return $this->serviceName;
    }

    /**
     * @param string $serviceName
     * @return Character
     */
    public function setServiceName(string $serviceName): Character
    {
        $this->serviceName = $serviceName;
        return $this;
    }

    /**
     * @return int
     */
    public function getBusinessId(): int
    {
        return $this->businessId;
    }

    /**
     * @param int $businessId
     * @return Character
     */
    public function setBusinessId(int $businessId): Character
    {
        $this->businessId = $businessId;
        return $this;
    }

    /**
     * @return int
     */
    public function getBusinessRank(): int
    {
        return $this->businessRank;
    }

    /**
     * @param int $businessRank
     * @return Character
     */
    public function setBusinessRank(int $businessRank): Character
    {
        $this->businessRank = $businessRank;
        return $this;
    }

    /**
     * @return float
     */
    public function getBitcoin(): float
    {
        return $this->bitcoin;
    }

    /**
     * @param float $bitcoin
     * @return Character
     */
    public function setBitcoin(float $bitcoin): Character
    {
        $this->bitcoin = $bitcoin;
        return $this;
    }

    /**
     * @return float
     */
    public function getBitcoinIllegal(): float
    {
        return $this->bitcoinIllegal;
    }

    /**
     * @param float $bitcoinIllegal
     * @return Character
     */
    public function setBitcoinIllegal(float $bitcoinIllegal): Character
    {
        $this->bitcoinIllegal = $bitcoinIllegal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBitcoinHashLegal(): ?string
    {
        return $this->bitcoinHashLegal;
    }

    /**
     * @param string|null $bitcoinHashLegal
     * @return Character
     */
    public function setBitcoinHashLegal(?string $bitcoinHashLegal): Character
    {
        $this->bitcoinHashLegal = $bitcoinHashLegal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBitcoinHashIllegal(): ?string
    {
        return $this->bitcoinHashIllegal;
    }

    /**
     * @param string|null $bitcoinHashIllegal
     * @return Character
     */
    public function setBitcoinHashIllegal(?string $bitcoinHashIllegal): Character
    {
        $this->bitcoinHashIllegal = $bitcoinHashIllegal;
        return $this;
    }

    /**
     * @return int
     */
    public function getSocialPoints(): int
    {
        return $this->socialPoints;
    }

    /**
     * @param int $socialPoints
     * @return Character
     */
    public function setSocialPoints(int $socialPoints): Character
    {
        $this->socialPoints = $socialPoints;
        return $this;
    }

    /**
     * @return int
     */
    public function getIsDuty(): int
    {
        return $this->isDuty;
    }

    /**
     * @param int $isDuty
     * @return Character
     */
    public function setIsDuty(int $isDuty): Character
    {
        $this->isDuty = $isDuty;
        return $this;
    }

    /**
     * @return int
     */
    public function getGunLicense(): int
    {
        return $this->gunLicense;
    }

    /**
     * @param int $gunLicense
     * @return Character
     */
    public function setGunLicense(int $gunLicense): Character
    {
        $this->gunLicense = $gunLicense;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return Character
     */
    public function setCreatedAt(?Carbon $createdAt): Character
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon|null $updatedAt
     * @return Character
     */
    public function setUpdatedAt(?Carbon $updatedAt): Character
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
