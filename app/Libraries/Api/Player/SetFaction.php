<?php
/**
 * SetFaction
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 13.04.2020
 * Time: 19:41
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Api\Player;


use App\Libraries\Api\AClient;
use App\Model\WebConfig;

class SetFaction extends AClient
{
    /**
     * @var string
     */
    const URL = 'player/setFaction';

    /**
     * @param int $pid
     * @param string $faction
     * @return bool
     */
    public static function call(int $pid, string $faction): bool
    {
        $response = self::sendRequest('POST', WebConfig::getEntryByKey('api_url')->getValue() . self::URL, [
            'query' => [
                'pid'       => $pid,
                'faction' => $faction,
            ]
        ]);
        return $response && $response->getStatusCode() == 200;
    }
}
