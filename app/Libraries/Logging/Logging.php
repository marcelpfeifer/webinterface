<?php
/**
 * WebAdminLogging
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 10.04.2020
 * Time: 09:15
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Logging;

use App\Model\Dto\WebAdminLogging;
use App\Model\Dto\WebAdminLoggingValue;
use App\Model\Enum\Logging\Action;
use Carbon\Carbon;

class Logging
{
    /**
     * @param string $name
     * @param int $accountId
     * @param ILogging $dto
     * @param int|null $loggingId
     * @return int|null
     */
    public static function insert(string $name, int $accountId, ILogging $dto, int $loggingId = null): ?int
    {
        $action = Action::INSERT;
        $loggingValues = [];
        foreach ($dto->getVariables() as $column => $value) {
            if ($value === null) {
                continue;
            }

            $loggingValues[] = (new WebAdminLoggingValue())
                ->setTableName($dto->getClassName())
                ->setColumnName($column)
                ->setOldValue('')
                ->setNewValue(self::trimValueLength($value ?? ''));
        }

        return self::log($name, $action, $accountId, $loggingValues, $loggingId);
    }

    /**
     * @param string $name
     * @param int $accountId
     * @param ILogging $oldDto
     * @param ILogging $newDto
     * @param int|null $loggingId
     * @return int|null
     */
    public static function update(
        string $name,
        int $accountId,
        ILogging $oldDto,
        ILogging $newDto,
        int $loggingId = null
    ): ?int {
        $action = Action::UPDATE;
        $loggingValues = [];

        $oldValues = $oldDto->getVariables();
        $newValues = $newDto->getVariables();

        $loggingValues[] = (new WebAdminLoggingValue())
            ->setTableName($newDto->getClassName())
            ->setColumnName('id')
            ->setOldValue($oldValues['id'] ?? '')
            ->setNewValue($newValues['id'] ?? '');

        foreach (array_diff_assoc($oldValues, $newValues) as $column => $value) {
            $oldValue = $oldValues[$column];
            $newColumn = $newValues[$column];
            if ($oldValue === null && $newColumn === null) {
                continue;
            }
            if ($column == 'id') {
                continue;
            }

            $loggingValues[] = (new WebAdminLoggingValue())
                ->setTableName($newDto->getClassName())
                ->setColumnName($column)
                ->setOldValue(self::trimValueLength($oldValue ?? ''))
                ->setNewValue(self::trimValueLength($newColumn ?? ''));
        }
        return self::log($name, $action, $accountId, $loggingValues, $loggingId);
    }

    /**
     * @param string $name
     * @param int $accountId
     * @param ILogging $dto
     * @param int|null $loggingId
     * @return int|null
     */
    public static function delete(string $name, int $accountId, ILogging $dto, int $loggingId = null): ?int
    {
        $action = Action::DELETE;
        $loggingValues = [];
        foreach ($dto->getVariables() as $column => $value) {
            if ($value === null) {
                continue;
            }

            $loggingValues[] = (new WebAdminLoggingValue())
                ->setTableName($dto->getClassName())
                ->setColumnName($column)
                ->setOldValue(self::trimValueLength($value ?? ''))
                ->setNewValue('');
        }

        return self::log($name, $action, $accountId, $loggingValues, $loggingId);
    }

    /**
     * @param string $name
     * @param string $action
     * @param int $accountId
     * @param WebAdminLoggingValue[] $loggingValues
     * @param int|null $loggingId
     * @return int|null
     */
    private static function log(
        string $name,
        string $action,
        int $accountId,
        array $loggingValues,
        int $loggingId = null
    ): ?int {
        if (count($loggingValues) === 0) {
            return null;
        }

        if ($loggingId === null) {
            $loggingDto = (new WebAdminLogging())
                ->setName($name)
                ->setAction($action)
                ->setDateTime(Carbon::now())
                ->setAccountId($accountId);
            $loggingId = (new \App\Model\WebAdminLogging())->insertEntry($loggingDto);
        }

        $loggingValueDb = new \App\Model\WebAdminLoggingValue();
        foreach ($loggingValues as $loggingValue) {
            $loggingValue->setLoggingId($loggingId);
            $loggingValueDb->insertEntry($loggingValue);
        }

        return $loggingId;
    }

    /**
     * @param string $value
     * @param int $length
     * @return string
     */
    private static function trimValueLength(string $value, int $length = 200): string
    {
        $value = strlen($value) > 200 ? substr($value, 0, $length) . "..." : $value;
        return $value;
    }
}
