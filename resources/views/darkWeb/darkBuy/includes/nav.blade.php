<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 09.08.2020
 * Time: 10:35
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
<div class="row">
    <div class="col-12">
        <a class="navbar-brand darkWebColor" href="{{ route('darkWeb.darkBuy.index', $hash)}}">DarkBuy</a>
    </div>
    <div class="col-12">
        <a class="nav-link darkWebColor" href="{{ route('darkWeb.darkBuy.index', $hash)}}">- Startseite</a>
    </div>
    @foreach($categories as $category)
    <div class="col-12">
        <a class="nav-link darkWebColor"
           href="{{ route('darkWeb.happyBuy.category',[
                                            'category'=> $category->getId(),
                                            'hash' => $hash
                                        ])}}">
           - {{ $category->getName() }}
        </a>
    </div>
    @endforeach
    <div class="col-12">
        <a class="nav-link darkWebColor" href="{{ route('darkWeb.darkBuy.offers', $hash) }}">- Meine Angebote</a>
    </div>
    <div class="col-12">
        <a class="nav-link darkWebColor" href="{{ route('darkWeb.darkBuy.bought', $hash) }}">- Gekauft</a>
    </div>
    <div class="col-12">
        <a class="darkWebColor nav-link" href="{{ route('darkWeb.darkBuy.create', $hash) }}">
            - Angebote erstellen
        </a>
    </div>
    <hr class="w-100 bg-white"/>
</div>
