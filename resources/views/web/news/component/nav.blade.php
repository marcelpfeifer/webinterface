<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.08.2021
 * Time: 16:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
<div class="row justify-content-center">
    <div class="col-6">
        <h1 class="text-center border-bottom font-weight-bold"> News</h1>
    </div>
</div>
<div class="row">
    <div class="col-2 border-left border-right text-center">
        <a class="text-dark {{ is_null($categoryId) ? 'font-weight-bold': '' }}"
           href="{{ route('web.news.index', $hash) }}">Startseite</a>
    </div>
    @foreach($categories as $category)
        <div class="col-2 border-left border-right text-center">
            <a class="text-dark text-small {{ ($categoryId === $category->getId()) ? 'font-weight-bold': '' }}"
               href="{{ route('web.news.category', [$category->getId(), $hash]) }}">
                {{ $category->getName() }}
            </a>
        </div>
    @endforeach
</div>
<hr>

