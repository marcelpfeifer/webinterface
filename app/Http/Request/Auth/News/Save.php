<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.08.2021
 * Time: 14:54
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Auth\News;

use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class Save extends AValidator implements IValidator
{
    /**
     * @return string[]
     */
    public function getRules(): array
    {
        return [
            'webNewsCategoryId' => [
                'int',
                'nullable'
            ],
            'title'             => [
                'required',
                'string',
            ],
            'image'             => [
                'string',
            ],
            'shortDescription'  => [
                'required',
                'string',
            ],
            'description'       => [
                'required',
                'string',
            ],
            'author'            => [
                'required',
                'string',
            ],
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'webNewsCategoryId.required' => 'Die Kategorie muss übergeben werden!',
            'webNewsCategoryId.int'      => 'Der Name muss übergeben werden!',
            'title.required'             => 'Der Titel muss übergeben werden!',
            'title.string'               => 'Der Titel darf nur Zeichen enthalten!',
            'image.string'               => 'Das Bild darf nur Zeichen enthalten!',
            'shortDescription.required'  => 'Die Kurzbeschreibung muss übergeben werden!',
            'shortDescription.string'    => 'Die Kurzbeschreibung darf nur Zeichen enthalten!',
            'description.required'       => 'Die Beschreibung muss übergeben werden!',
            'description.string'         => 'Die Beschreibung darf nur Zeichen enthalten!',
            'author.required'            => 'Der Erfasser muss übergeben werden!',
            'author.string'              => 'Der Erfasser darf nur Zeichen enthalten!',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Auth\News\Save())
            ->setId($this->id)
            ->setWebNewsCategoryId($this->request->input('webNewsCategoryId'))
            ->setTitle($this->request->input('title'))
            ->setImage($this->request->input('image'))
            ->setShortDescription($this->request->input('shortDescription'))
            ->setDescription($this->request->input('description'))
            ->setAuthor($this->request->input('author'));
    }
}
