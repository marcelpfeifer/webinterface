<?php
/**
 * StockController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 29.08.2021
 * Time: 11:03
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Http\Request\Admin\Stock\Save;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class StockController extends Controller
{

    /**
     * @return View
     */
    public function index(): View
    {
        $entries = [];
        foreach (\App\Model\WebStock::all() as $category) {
            $entries[] = \App\Model\WebStock::entryAsDto($category);
        }
        return \view(
            'admin.stock.index',
            [
                'entries' => $entries
            ]
        );
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return \view('admin.stock.create');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return View
     */
    public function edit(Request $request, int $id): View
    {
        $entry = \App\Model\WebStock::find($id);
        if (!$entry) {
            return \view('404');
        }

        return \view(
            'admin.stock.edit',
            [
                'dto' => \App\Model\WebStock::entryAsDto($entry),
            ]
        );
    }

    /**
     * @param Request $request
     * @param int|null $id
     * @return RedirectResponse
     */
    public function save(Request $request, int $id = null): RedirectResponse
    {
        // Validiere die Anfrage
        $validator = new Save($request, $id);
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            if ($id) {
                return redirect()->route('admin.stock.edit', $id)
                    ->with('messages', $validator->getErrorsAsMessageArray($validatedData))
                    ->withInput()
                    ->withErrors($validatedData->errors());
            } else {
                return redirect()->route('admin.stock.create')
                    ->with('messages', $validator->getErrorsAsMessageArray($validatedData))
                    ->withInput()
                    ->withErrors($validatedData->errors());
            }
        }

        \App\Http\Model\Admin\Stock\Save::save($validator->toDto());

        $messages[] = (new Message())
            ->setMessage("Wurde erfolgreich gespeichert")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.stock.index')->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int|null $id
     * @return RedirectResponse
     */
    public function saveEdit(Request $request, int $id): RedirectResponse
    {
        return $this->save($request, $id);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function delete(Request $request, int $id): RedirectResponse
    {
        $dto = (new \App\Model\Dto\WebStock())
            ->setId($id);
        (new \App\Model\WebStock())->deleteEntry($dto);
        $messages[] = (new Message())
            ->setMessage("Wurde gelöscht")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.stock.index')->with('messages', $messages);
    }
}
