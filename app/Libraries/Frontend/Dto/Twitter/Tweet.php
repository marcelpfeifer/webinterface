<?php
/**
 * Tweet
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 16.04.2020
 * Time: 15:32
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Dto\Twitter;

use App\Model\Dto\WebSocialPost;

class Tweet
{
    /**
     * @var WebSocialPost
     */
    private $tweet;

    /**
     * @var WebSocialPost|null
     */
    private $retweet;

    /**
     * @return WebSocialPost
     */
    public function getTweet(): WebSocialPost
    {
        return $this->tweet;
    }

    /**
     * @param WebSocialPost $tweet
     * @return Tweet
     */
    public function setTweet(WebSocialPost $tweet): Tweet
    {
        $this->tweet = $tweet;
        return $this;
    }

    /**
     * @return WebSocialPost|null
     */
    public function getRetweet(): ?WebSocialPost
    {
        return $this->retweet;
    }

    /**
     * @param WebSocialPost|null $retweet
     * @return Tweet
     */
    public function setRetweet(?WebSocialPost $retweet): Tweet
    {
        $this->retweet = $retweet;
        return $this;
    }
}
