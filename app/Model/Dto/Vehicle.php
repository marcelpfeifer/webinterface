<?php
/**
 * Vehicle
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 29.03.2020
 * Time: 10:38
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

use App\Libraries\Logging\ILogging;
use App\Libraries\Logging\Traits\Variables;
use Carbon\Carbon;

/**
 * Class Vehicle
 * @package App\Model\Dto
 */
class Vehicle implements ILogging
{
    use Variables;

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var int
     */
    private $guid = 0;

    /**
     * @var string
     */
    private $model = '';

    /**
     * @var string
     */
    private $customization = '';

    /**
     * @var string
     */
    private $numberPlate = '';

    /**
     * @var bool
     */
    private $parked = true;

    /**
     * @var string
     */
    private $faction = '';

    /**
     * @var bool
     */
    private $impounded = false;

    /**
     * @var bool
     */
    private $destroyed = false;

    /**
     * @var float
     */
    private $engineDistance = 0.00;

    /**
     * @var int
     */
    private $gId = -1;

    /**
     * @var string
     */
    private $inventory = '';

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @var null|Carbon
     */
    private $updatedAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Vehicle
     */
    public function setId(?int $id): Vehicle
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getGuid(): int
    {
        return $this->guid;
    }

    /**
     * @param int $guid
     * @return Vehicle
     */
    public function setGuid(int $guid): Vehicle
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return string
     */
    public function getModel(): string
    {
        return $this->model;
    }

    /**
     * @param string $model
     * @return Vehicle
     */
    public function setModel(string $model): Vehicle
    {
        $this->model = $model;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomization(): string
    {
        return $this->customization;
    }

    /**
     * @param string $customization
     * @return Vehicle
     */
    public function setCustomization(string $customization): Vehicle
    {
        $this->customization = $customization;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumberPlate(): string
    {
        return $this->numberPlate;
    }

    /**
     * @param string $numberPlate
     * @return Vehicle
     */
    public function setNumberPlate(string $numberPlate): Vehicle
    {
        $this->numberPlate = $numberPlate;
        return $this;
    }

    /**
     * @return bool
     */
    public function isParked(): bool
    {
        return $this->parked;
    }

    /**
     * @param bool $parked
     * @return Vehicle
     */
    public function setParked(bool $parked): Vehicle
    {
        $this->parked = $parked;
        return $this;
    }

    /**
     * @return string
     */
    public function getFaction(): string
    {
        return $this->faction;
    }

    /**
     * @param string $faction
     * @return Vehicle
     */
    public function setFaction(string $faction): Vehicle
    {
        $this->faction = $faction;
        return $this;
    }

    /**
     * @return bool
     */
    public function isImpounded(): bool
    {
        return $this->impounded;
    }

    /**
     * @param bool $impounded
     * @return Vehicle
     */
    public function setImpounded(bool $impounded): Vehicle
    {
        $this->impounded = $impounded;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDestroyed(): bool
    {
        return $this->destroyed;
    }

    /**
     * @param bool $destroyed
     * @return Vehicle
     */
    public function setDestroyed(bool $destroyed): Vehicle
    {
        $this->destroyed = $destroyed;
        return $this;
    }

    /**
     * @return float
     */
    public function getEngineDistance(): float
    {
        return $this->engineDistance;
    }

    /**
     * @param float $engineDistance
     * @return Vehicle
     */
    public function setEngineDistance(float $engineDistance): Vehicle
    {
        $this->engineDistance = $engineDistance;
        return $this;
    }

    /**
     * @return int
     */
    public function getGId(): int
    {
        return $this->gId;
    }

    /**
     * @param int $gId
     * @return Vehicle
     */
    public function setGId(int $gId): Vehicle
    {
        $this->gId = $gId;
        return $this;
    }

    /**
     * @return string
     */
    public function getInventory(): string
    {
        return $this->inventory;
    }

    /**
     * @param string $inventory
     * @return Vehicle
     */
    public function setInventory(string $inventory): Vehicle
    {
        $this->inventory = $inventory;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return Vehicle
     */
    public function setCreatedAt(?Carbon $createdAt): Vehicle
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon|null $updatedAt
     * @return Vehicle
     */
    public function setUpdatedAt(?Carbon $updatedAt): Vehicle
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
