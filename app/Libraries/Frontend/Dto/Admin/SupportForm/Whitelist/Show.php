<?php
/**
 * Show
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 13.11.2021
 * Time: 15:46
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Dto\Admin\SupportForm\Whitelist;

use App\Model\Dto\Account;
use App\Model\Dto\WebAdminWhitelist;
use App\Model\Dto\WebAdminWhitelistAnswer;
use App\Model\Dto\WebAdminWhitelistQuestion;

class Show
{
    /**
     * @var WebAdminWhitelistQuestion[]
     */
    private $questions = [];

    /**
     * @var WebAdminWhitelistAnswer[]
     */
    private $answers = [];

    /**
     * @var WebAdminWhitelist
     */
    private $entry;

    /**
     * @var Account
     */
    private $user;

    /**
     * @return WebAdminWhitelistQuestion[]
     */
    public function getQuestions(): array
    {
        return $this->questions;
    }

    /**
     * @param WebAdminWhitelistQuestion[] $questions
     * @return Show
     */
    public function setQuestions(array $questions): Show
    {
        $this->questions = $questions;
        return $this;
    }

    /**
     * @return WebAdminWhitelistAnswer[]
     */
    public function getAnswers(): array
    {
        return $this->answers;
    }

    /**
     * @param WebAdminWhitelistAnswer[] $answers
     * @return Show
     */
    public function setAnswers(array $answers): Show
    {
        $this->answers = $answers;
        return $this;
    }

    /**
     * @return WebAdminWhitelist
     */
    public function getEntry(): WebAdminWhitelist
    {
        return $this->entry;
    }

    /**
     * @param WebAdminWhitelist $entry
     * @return Show
     */
    public function setEntry(WebAdminWhitelist $entry): Show
    {
        $this->entry = $entry;
        return $this;
    }

    /**
     * @return Account
     */
    public function getUser(): Account
    {
        return $this->user;
    }

    /**
     * @param Account $user
     * @return Show
     */
    public function setUser(Account $user): Show
    {
        $this->user = $user;
        return $this;
    }
}
