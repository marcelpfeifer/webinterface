<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebAdminLoggingApiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webAdminLoggingApi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('accountId');
            $table->string('url');
            $table->text('options');
            $table->timestamp('dateTime')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webAdminLoggingApi');
    }
}
