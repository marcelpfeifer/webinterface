<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 12.08.2021
 * Time: 12:49
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

/**
 * @var $address \App\Model\Dto\WebMailAddress\WithWebMailAddressToAccount
 */
?>
@extends('layouts.app')
@section('title')
    <i class="fas fa-envelope"></i>
    Postfach
@endsection
@section('content')
    <div class="row">
        <div class="col-6">
            <a href="{{ route('auth.mailbox.create') }}" class="btn btn-primary">
                Neue Email anlegen
            </a>
        </div>
        <div class="col-6">
            <a href="{{ route('auth.mailbox.add') }}" class="btn btn-primary float-right">
                Bestehende Email hinzufügen
            </a>
        </div>
    </div>
    <table class="table table-striped mt-2">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Adresse</th>
            <th scope="col">Passwort</th>
            <th scope="col"></th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($addresses as $address)

            <tr>
                <td>{{ $address->getWebMailAddress()->getId() }}</td>
                <td>{{ $address->getWebMailAddress()->getEmail() }}</td>
                <td>
                    <input type='password' class="password "
                           value="{{ $address->getWebMailAddress()->getPassword() }}"/>
                    Password anzeigen
                    <input type='checkbox' class='check form-check-inline'/>
                </td>
                <td>
                    @if($address->getWebMailAddressToAccount()->isOwner())
                        <a href="{{ route('auth.mailbox.edit', $address->getWebMailAddress()->getId()) }}"
                           class="text-primary">
                            Bearbeiten
                        </a>
                    @endif
                </td>
                <td>
                    <a href="{{ route('auth.mailbox.mail.index', $address->getWebMailAddress()->getId()) }}"
                       class="text-primary">
                        Zum Postfach
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
