@extends('admin.layout.app')
@section('title')
    <i class="fab fa-wpforms"></i>
    Eintrag erstellen
@endsection
@section('content')
    <form action="{{ route('admin.supportForm.ban.store') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="userName">Name IC</label>
            <input type="text" class="form-control" id="userName" name="userName">
        </div>
        <div class="form-group">
            <label for="ipAddress">IP-Adresse</label>
            <input type="text" class="form-control" id="ipAddress" name="ipAddress">
        </div>
        <div class="form-group">
            <label for="socialClubName">SocialClub Name</label>
            <input type="text" class="form-control" id="socialClubName" name="socialClubName">
        </div>
        <div class="form-group">
            <label for="reason">Fallgrund</label>
            <textarea class="form-control" id="reason" name="reason"></textarea>
        </div>
        <div class="form-group">
            <label for="length">Banndauer</label>
            <input type="text" class="form-control" id="length" name="length">
        </div>
        <br/>
        <button type="submit" class="btn btn-dark">Erstellen</button>
    </form>
@endsection

