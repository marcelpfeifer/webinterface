<?php
/**
 * GetVehicles
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 18.09.2020
 * Time: 17:20
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Api\Server;

use App\Libraries\Api\AClient;
use App\Model\WebConfig;

class GetVehicles extends AClient
{

    /**
     * @var string
     */
    const URL = 'server/getVehicles';

    /**
     * @return array
     */
    public static function get(): array
    {
        $result = [];
        $response = self::sendRequest('GET', WebConfig::getEntryByKey('api_url')->getValue() . self::URL, []);
        if ($response && $response->getStatusCode() == 200) {
            $content = $response->getBody()->getContents();
            $entries = json_decode($content);
            foreach ($entries as $model => $price) {
                $result[$model] = $price;
            }
        } else {
            $result = [
                'guardian'  => 200,
                'ratloader' => 400,
                'dloader'   => 500,
                'sadler'    => 600,
            ];
        }
        return $result;
    }
}
