<?php
/**
 * Customization
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.01.2021
 * Time: 15:47
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Vehicle\Dto;


class Customization
{

    /**
     * @var int
     */
    private $engine = 0;

    /**
     * @var int
     */
    private $brakes = 0;

    /**
     * @var int
     */
    private $horns = 0;

    /**
     * @var int
     */
    private $suspension = 0;

    /**
     * @var int
     */
    private $armor = 0;

    /**
     * @var int
     */
    private $turbo = 0;

    /**
     * @var int
     */
    private $customTireSmoke = 0;

    /**
     * @var int
     */
    private $xenon = 0;

    /**
     * @return int
     */
    public function getEngine(): int
    {
        return $this->engine;
    }

    /**
     * @param int $engine
     * @return Customization
     */
    public function setEngine(int $engine): Customization
    {
        $this->engine = $engine;
        return $this;
    }

    /**
     * @return int
     */
    public function getBrakes(): int
    {
        return $this->brakes;
    }

    /**
     * @param int $brakes
     * @return Customization
     */
    public function setBrakes(int $brakes): Customization
    {
        $this->brakes = $brakes;
        return $this;
    }

    /**
     * @return int
     */
    public function getHorns(): int
    {
        return $this->horns;
    }

    /**
     * @param int $horns
     * @return Customization
     */
    public function setHorns(int $horns): Customization
    {
        $this->horns = $horns;
        return $this;
    }

    /**
     * @return int
     */
    public function getSuspension(): int
    {
        return $this->suspension;
    }

    /**
     * @param int $suspension
     * @return Customization
     */
    public function setSuspension(int $suspension): Customization
    {
        $this->suspension = $suspension;
        return $this;
    }

    /**
     * @return int
     */
    public function getArmor(): int
    {
        return $this->armor;
    }

    /**
     * @param int $armor
     * @return Customization
     */
    public function setArmor(int $armor): Customization
    {
        $this->armor = $armor;
        return $this;
    }

    /**
     * @return int
     */
    public function getTurbo(): int
    {
        return $this->turbo;
    }

    /**
     * @param int $turbo
     * @return Customization
     */
    public function setTurbo(int $turbo): Customization
    {
        $this->turbo = $turbo;
        return $this;
    }

    /**
     * @return int
     */
    public function getCustomTireSmoke(): int
    {
        return $this->customTireSmoke;
    }

    /**
     * @param int $customTireSmoke
     * @return Customization
     */
    public function setCustomTireSmoke(int $customTireSmoke): Customization
    {
        $this->customTireSmoke = $customTireSmoke;
        return $this;
    }

    /**
     * @return int
     */
    public function getXenon(): int
    {
        return $this->xenon;
    }

    /**
     * @param int $xenon
     * @return Customization
     */
    public function setXenon(int $xenon): Customization
    {
        $this->xenon = $xenon;
        return $this;
    }
}
