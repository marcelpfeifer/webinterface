<?php
/**
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 28.03.2020
 * Time: 15:16
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('admin.layout.app')
@section('title')
    Fahrzeugübersicht
    <a class="btn btn-dark btn-sm float-right"
       href="{{ route('admin.player.edit', $characterId) }}"
       role="button"
    >
        Zurück
    </a>
@endsection
@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Model</th>
            <th scope="col">Nummernschild</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($vehicles as $vehicle)
            <tr>
                <th scope="row">{{ $vehicle->getId() }}</th>
                <td>{{ $vehicle->getModel() }}</td>
                <td>{{ $vehicle->getNumberPlate() }}</td>
                <td>
                    <a class="text-secondary" href="{{ route('admin.player.vehicle.edit', $vehicle->getId()) }}">
                        Bearbeiten
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
