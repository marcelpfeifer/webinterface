<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.08.2021
 * Time: 13:53
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Model\Admin\WhitelistQuestion;

use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Model\Dto\WebAdminWhitelistQuestion;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class Save
{

    /**
     * @param \App\Http\Request\Dto\Admin\WhitelistQuestion\Save $requestDto
     */
    public static function save(\App\Http\Request\Dto\Admin\WhitelistQuestion\Save $requestDto)
    {
        $dto = (new WebAdminWhitelistQuestion())
            ->setId($requestDto->getId())
            ->setQuestion($requestDto->getQuestion())
            ->setAnswer($requestDto->getAnswer())
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());

        $model = new \App\Model\WebAdminWhitelistQuestion();
        if ($dto->getId()) {
            $oldDto = $model::entryAsDto($model::find($dto->getId()));
            $model->updateEntry($dto);
            Logging::update(Name::WHITELIST_QUESTION, Auth::id(), $oldDto, $dto);
        } else {
            $model->insertEntry($dto);
            Logging::insert(Name::WHITELIST_QUESTION, Auth::id(), $dto);
        }
    }
}
