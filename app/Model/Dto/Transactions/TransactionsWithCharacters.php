<?php
/**
 * TransactionsWithCharacters
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 23.08.2020
 * Time: 13:37
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto\Transactions;

use App\Model\Dto\Character;
use App\Model\Dto\Transactions;

class TransactionsWithCharacters
{

    /**
     * @var Transactions
     */
    private $transaction;

    /**
     * @var Character
     */
    private $tx;

    /**
     * @var Character
     */
    private $rx;

    /**
     * @return Transactions
     */
    public function getTransaction(): Transactions
    {
        return $this->transaction;
    }

    /**
     * @param Transactions $transaction
     * @return TransactionsWithCharacters
     */
    public function setTransaction(Transactions $transaction): TransactionsWithCharacters
    {
        $this->transaction = $transaction;
        return $this;
    }

    /**
     * @return Character
     */
    public function getTx(): Character
    {
        return $this->tx;
    }

    /**
     * @param Character $tx
     * @return TransactionsWithCharacters
     */
    public function setTx(Character $tx): TransactionsWithCharacters
    {
        $this->tx = $tx;
        return $this;
    }

    /**
     * @return Character
     */
    public function getRx(): Character
    {
        return $this->rx;
    }

    /**
     * @param Character $rx
     * @return TransactionsWithCharacters
     */
    public function setRx(Character $rx): TransactionsWithCharacters
    {
        $this->rx = $rx;
        return $this;
    }
}
