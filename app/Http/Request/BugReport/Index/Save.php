<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.08.2021
 * Time: 17:04
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\BugReport\Index;


use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;
use App\Libraries\Common\Enum\Db\Length;

class Save extends AValidator implements IValidator
{

    /**
     * @return string[]
     */
    public function getRules(): array
    {
        return [
            'title'       => [
                'required',
                'string',
                'max:' . Length::VARCHAR,
            ],
            'description' => [
                'required',
                'string',
                'max:' . Length::TEXT,
            ],
            'evidence'    => [
                'string',
                'nullable',
                'max:' . Length::VARCHAR,
            ],
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'title.required'       => 'Der Titel muss übergeben werden!',
            'title.string'         => 'Der Titel darf nur aus Zeichen bestehen!',
            'title.max'            => "Der Titel darf nicht so viele Zeichen enthalten! Maximale Zeichen: " . Length::VARCHAR,
            'description.required' => 'Die Beschreibung muss übergeben werden!',
            'description.string'   => 'Die Beschreibung darf nur aus Zeichen bestehen!',
            'description.max'      => "Die Beschreibung darf nicht so viele Zeichen enthalten! Maximale Zeichen: " . Length::TEXT,
            'evidence.string'      => 'Die Nachweise dürfen nur Zeichen enthalten!',
            'evidence.max'         => "Die Nachweise dürfen nicht so viele Zeichen enthalten! Maximale Zeichen: " . Length::VARCHAR,
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\BugReport\Index\Save())
            ->setTitle($this->request->input('title'))
            ->setDescription($this->request->input('description'))
            ->setEvidence($this->request->input('evidence', '') ?? '');
    }
}
