<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 11.08.2021
 * Time: 12:01
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Admin\BugReport;


use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;
use App\Libraries\Common\Enum\Db\Length;

class Save extends AValidator implements IValidator
{
    /**
     * @return string[]
     */
    public function getRules(): array
    {
        return [
            'title'       => [
                'required',
                'string',
                'max:' . Length::VARCHAR,
            ],
            'description' => [
                'required',
                'string',
                'max:' . Length::TEXT,
            ],
            'evidence'    => [
                'string',
                'nullable',
                'max:' . Length::VARCHAR,
            ],
            'position'    => [
                'string',
                'max:' . Length::VARCHAR,
            ],
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'title.required'       => 'Der Titel muss übergeben werden!',
            'title.string'         => 'Der Titel darf nur aus Zeichen bestehen!',
            'title.max'            => "Der Titel darf nicht so viele Zeichen enthalten! Maximale Zeichen: " . Length::VARCHAR,
            'description.required' => 'Die Beschreibung muss übergeben werden!',
            'description.string'   => 'Die Beschreibung darf nur aus Zeichen bestehen!',
            'description.max'      => "Die Beschreibung darf nicht so viele Zeichen enthalten! Maximale Zeichen: " . Length::TEXT,
            'evidence.string'      => 'Die Nachweise dürfen nur Zeichen enthalten!',
            'evidence.max'         => "Die Nachweise dürfen nicht so viele Zeichen enthalten! Maximale Zeichen: " . Length::VARCHAR,
            'position.string'      => 'Die Position darf nur Zeichen enthalten!',
            'position.max'         => "Die Position darf nicht so viele Zeichen enthalten! Maximale Zeichen: " . Length::VARCHAR,
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Admin\BugReport\Save())
            ->setId($this->id)
            ->setTitle($this->request->input('title'))
            ->setDescription($this->request->input('description'))
            ->setEvidence($this->request->input('evidence', '') ?? '')
            ->setPosition($this->request->input('position', '') ?? '');
    }
}
