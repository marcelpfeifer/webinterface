<?php
/**
 * VanishController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 08.04.2020
 * Time: 17:43
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Api\Player;

use App\Dto\Message;
use App\Http\Controllers\Api\AController;
use App\Http\Controllers\Api\IController;
use App\Http\Controllers\Controller;
use App\Http\Request\Api\Player\Vanish\Index;
use App\Http\Request\Api\Player\Vanish\Save;
use App\Http\Request\Dto\ADto;
use App\Libraries\Api\Player\Online;
use App\Libraries\Api\Player\Vanish;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class VanishController extends AController implements IController
{
    /**
     * @param Request $request
     * @return View
     */
    public function index(Request $request): View
    {
        $validator = new Index($request);
        return $this->processIndex($validator, 'api.player.vanish.index');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function save(Request $request): RedirectResponse
    {
        $validator = new Save($request);
        return $this->processSave($validator, 'api.player.vanish.index');
    }

    /**
     * @param \App\Http\Request\Dto\Api\Player\Vanish\Index $requestDto
     * @return array
     */
    public function getIndexData(ADto $requestDto): array
    {
        return [
            'players'       => Online::getPlayers(),
            'currentPlayer' => $requestDto->getCurrentPlayer(),
        ];
    }

    /**
     * @param \App\Http\Request\Dto\Api\Player\Vanish\Save $requestDto
     * @return bool
     */
    public function execApiCall(ADto $requestDto): bool
    {
        return Vanish::vanishPlayer($requestDto->getPlayerId(), $requestDto->isActivate());
    }

    /**
     * @param \App\Http\Request\Dto\Api\Player\Vanish\Save $requestDto
     * @return array
     */
    public function getRedirectData(ADto $requestDto): array
    {
        return [
            'currentPlayer' => $requestDto->getPlayerId()
        ];
    }
}
