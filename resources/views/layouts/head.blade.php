<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css"
          href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css"/>
    <link rel="shortcut icon" type="image/x-icon" href="{{ url('/img/favicon64.ico') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'EternityV') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://kit.fontawesome.com/fe55b35558.js" defer></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js" defer></script>
    <script>
        window.addEventListener("load", function () {
            window.cookieconsent.initialise({
                "palette": {
                    "popup": {
                        "background": "#212529"
                    },
                    "button": {
                        "background": "#fff",
                        "text": "#212529"
                    }
                },
                "theme": "classic",
                "position": "bottom",
                "content": {
                    "message": "Diese Seite verwendet Cookies, um Ihnen den bestmöglichen Service zu gewährleisten. Wenn Sie auf dieser Seite weitersurfen, stimmen Sie der Cookie-Nutzung zu.",
                    "dismiss": "Ok",
                    "link": "Mehr Infos",
                    "href": "{{ route('dataProtection') }}"
                }
            })
        });
    </script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
</head>
