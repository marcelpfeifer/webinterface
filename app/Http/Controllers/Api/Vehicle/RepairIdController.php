<?php
/**
 * RepairIdController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 13.04.2020
 * Time: 22:43
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Api\Vehicle;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Libraries\Api\Vehicle\RepairId;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class RepairIdController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        return view('api.vehicle.repairId.index');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function save(Request $request): RedirectResponse
    {
        $vehId = $request->input('vehId');

        if (RepairId::call($vehId)) {
            $messages[] = (new Message())
                ->setMessage('Erfolgreich ausgeführt')
                ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        } else {
            $messages[] = (new Message())
                ->setMessage('Anfrage fehlgeschlagen')
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
        }

        return redirect()->route('api.vehicle.repairId.index')->with('messages', $messages);
    }
}
