<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 26.10.2020
 * Time: 19:47
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

/**
 * @var $mail \App\Model\Dto\WebMail
 */
?>
@extends('layouts.app')
@section('title')
    <i class="fas fa-envelope"></i>
    @if($dto)
        Postfach: {{ $dto->getEmail() }}
    @endif
    <a class="btn btn-secondary btn-sm float-right"
       href="{{ route('auth.mailbox.mail.index', $webMailAddressId) }}"
       role="button"
    >
        Zurück
    </a>
@endsection
@section('content')
    <div class="row">
        <div class="col-2">
            <div class="row">
                <a href="{{ route('auth.mailbox.mail.index', $webMailAddressId) }}"
                   class="col-12 border p-2 bg-primary text-white">
                    Eingang
                </a>
                <a href="{{ route('auth.mailbox.mail.outgoing', $webMailAddressId) }}" class="col-12 border p-2">
                    Ausgang
                </a>
            </div>
        </div>
        <div class="col-10">
            <form action="{{ route('auth.mailbox.mail.save', $webMailAddressId) }}" method="POST" id="mailCreate"
                  enctype='multipart/form-data'>
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="to">An</label>
                    <input type="text" class="form-control" id="to" name="to" placeholder="max.mustermann@eternity.de"
                           required>
                </div>
                <div class="form-group">
                    <label for="title">Betreff</label>
                    <input type="text" class="form-control" id="title" name="title"
                           value="{{ ($mail) ? 'FWD: '. $mail->getTitle() : ''}}" required>
                </div>
                <div class="form-row">
                    <div class="col-12">
                        <label for="message">Text</label>
                        <div id="editor">
                            {!!  ($mail) ? '<br/><blockquote>'. $mail->getMessage() . '</blockquote>': '' !!}
                        </div>
                        <input type="hidden" id="message" name="message" required>
                    </div>
                </div>
                <br/>
                <br/>
                <br/>
                <br/>
                <div class="form-row">
                    <div class="col-12">
                        <label for="file">Anhang</label>
                        <input type="file" class="form-control-file" name="file[]" id="file" accept=".jpg,.png,.pdf"
                               multiple>
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-primary">Verschicken</button>
            </form>
        </div>
    </div>
@endsection
