<?php
/**
 * Store
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.10.2021
 * Time: 09:54
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Admin\SupportForm\Whitelist;


use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Model\Account;
use App\Model\Dto\WebAdminWhitelistAnswer;
use App\Model\WebAdminWhitelist;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Update
{

    /**
     * @param \App\Http\Request\Dto\Admin\SupportForm\WhiteList\Update $saveDto
     * @return bool
     */
    public static function save(\App\Http\Request\Dto\Admin\SupportForm\WhiteList\Update $saveDto)
    {
        $entry = WebAdminWhitelist::find($saveDto->getId());
        $dto = WebAdminWhitelist::entryAsDto($entry);
        $oldDto = clone $dto;

        $dto = (new \App\Model\Dto\WebAdminWhitelist())
            ->setId($saveDto->getId())
            ->setMessage($saveDto->getMessage())
            ->setUserName($saveDto->getUsername())
            ->setStatus($saveDto->getStatus())
            ->setRetryDate($saveDto->getRetryDate())
            ->setUserId(Auth::user()->{Account::COLUMN_ID});

        $loggingId = Logging::update(Name::SUPPORT_FORM_WHITELIST, Auth::id(), $oldDto, $dto);

        (new WebAdminWhitelist())->updateEntry($dto);;

        foreach ($saveDto->getAnswer() as $questionId => $accuracy) {
            $model = new \App\Model\WebAdminWhitelistAnswer();
            $dto = (new WebAdminWhitelistAnswer())
                ->setWebAdminWhitelistId($saveDto->getId())
                ->setWebAdminWhitelistQuestionId($questionId)
                ->setAccuracy($accuracy);
            $entry = $model->getEntryByWhiteListIdAndQuestionId($dto);
            if ($entry) {
                $oldDto = clone $entry;
                $entry->setAccuracy($accuracy);
                $model->updateEntry($entry);
                $loggingId = Logging::update(Name::SUPPORT_FORM_WHITELIST, Auth::id(), $oldDto, $entry);
            } else {
                $model->insertEntry($dto);
                Logging::insert(Name::SUPPORT_FORM_WHITELIST, Auth::id(), $dto, $loggingId);
            }
        }
        return true;
    }
}
