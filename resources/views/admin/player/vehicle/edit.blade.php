<?php
/**
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 28.03.2020
 * Time: 15:16
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

/**
 * @var $vehicle \App\Model\Dto\Vehicle
 */
?>

@extends('admin.layout.app')
@section('title')
    Fahrzeug bearbeiten
    <a class="btn btn-dark btn-sm float-right"
       href="{{ route('admin.player.vehicle.index', $vehicle->getGuid()) }}"
       role="button"
    >
        Zurück
    </a>
@endsection
@section('content')

    @if($vehicle)
        <form method="post" action="{{ route('admin.player.vehicle.save', $vehicle->getId()) }}">
            {{ csrf_field() }}
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="parked"
                       name="parked" {{ ($vehicle->isParked()) ? 'checked' : '' }}>
                <label class="form-check-label" for="parked">Geparked ?</label>
            </div>
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="impounded"
                       name="impounded" {{ ($vehicle->isImpounded()) ? 'checked' : '' }}>
                <label class="form-check-label" for="impounded">Beschlagnahmt ?</label>
            </div>
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="destroyed"
                       name="destroyed" {{ ($vehicle->isDestroyed()) ? 'checked' : '' }}>
                <label class="form-check-label" for="destroyed">Zerstört ?</label>
            </div>
            <div class="form-group">
                <label for="model">Model</label>
                <input type="text" class="form-control" id="model" name="model" value="{{ $vehicle->getModel() }}">
            </div>
            <div class="form-group">
                <label for="numberplate">Nummernschild</label>
                <input type="text" class="form-control" id="numberplate" name="numberplate"
                       value="{{ $vehicle->getNumberPlate() }}">
            </div>
            <div class="form-group">
                <label for="faction">Faktion</label>
                <input type="text" class="form-control" id="faction" name="faction"
                       value="{{ $vehicle->getFaction() }}">
            </div>
            <div class="form-group">
                <label for="gid">Garagen ID</label>
                <input type="number" class="form-control" id="gid" name="gid"
                       value="{{ $vehicle->getGId() }}">
            </div>
            <div class="form-group">
                <label for="engineDistance">Engine Distanz</label>
                <input type="number" class="form-control" id="engineDistance" name="engineDistance"
                       value="{{ $vehicle->getEngineDistance() }}">
            </div>
            <button type="submit" class="btn btn-primary">Speichern</button>
        </form>
        <form class="mt-1" action="{{ route('admin.player.vehicle.delete', $vehicle->getId())}}" method="POST">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="submit" class="btn btn-danger" value="Löschen"/>
        </form>
        <br/>
        <h4> Tuning</h4>
        <form method="post" action="{{ route('admin.player.vehicle.tune', $vehicle->getId()) }}">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="engine">Engine</label>
                <select name="engine" class="form-control" id="engine">
                    @for($i = 0; $i < 5; $i++)
                        <option
                            value="{{ $i }}" {{ ($i === $customization->getEngine()) ? 'selected':'' }}>{{ $i }}</option>
                    @endfor
                </select>
            </div>
            <div class="form-group">
                <label for="brakes">Brakes</label>
                <select name="brakes" class="form-control" id="brakes">
                    @for($i = 0; $i < 4; $i++)
                        <option value="{{ $i }}" {{ ($i === $customization->getBrakes()) ? 'selected':'' }}>{{ $i }}</option>
                    @endfor
                </select>
            </div>
            <div class="form-group">
                <label for="horns">Horns</label>
                <select name="horns" class="form-control" id="horns">
                    @for($i = 0; $i < 52; $i++)
                        <option value="{{ $i }}" {{ ($i === $customization->getHorns()) ? 'selected':'' }}>{{ $i }}</option>
                    @endfor
                </select>
            </div>
            <div class="form-group">
                <label for="suspension">Suspension</label>
                <select name="suspension" class="form-control" id="suspension">
                    @for($i = 0; $i < 5; $i++)
                        <option value="{{ $i }}" {{ ($i === $customization->getSuspension()) ? 'selected':'' }}>{{ $i }}</option>
                    @endfor
                </select>
            </div>
            <div class="form-group">
                <label for="armor">Armor</label>
                <select name="armor" class="form-control" id="armor">
                    @for($i = 0; $i < 6; $i++)
                        <option value="{{ $i }}" {{ ($i === $customization->getArmor()) ? 'selected':'' }}>{{ $i }}</option>
                    @endfor
                </select>
            </div>
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="turbo"
                       name="turbo" {{ ($customization->getTurbo()) ? 'checked' : '' }}>
                <label class="form-check-label" for="turbo">Turbo</label>
            </div>
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="customTireSmoke"
                       name="customTireSmoke" {{ ($customization->getCustomTireSmoke()) ? 'checked' : '' }}>
                <label class="form-check-label" for="customTireSmoke">Custom tire smok</label>
            </div>
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="xenon"
                       name="xenon" {{ ($customization->getXenon()) ? 'checked' : '' }}>
                <label class="form-check-label" for="xenon">Xenon</label>
            </div>
            <button type="submit" class="btn btn-primary">Speichern</button>
        </form>
    @else
        Fahrzeug wurde nicht gefunden!
    @endif
@endsection
