<?php
/**
 * WebStockChange
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 28.08.2021
 * Time: 16:44
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;


use Carbon\Carbon;

class WebStockChange
{
    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var int
     */
    private $webStockId = 0;

    /**
     * @var float
     */
    private $price = 0.00;

    /**
     * @var float
     */
    private $percentage = 0.00;

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @var null|Carbon
     */
    private $updatedAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebStockChange
     */
    public function setId(?int $id): WebStockChange
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getWebStockId(): int
    {
        return $this->webStockId;
    }

    /**
     * @param int $webStockId
     * @return WebStockChange
     */
    public function setWebStockId(int $webStockId): WebStockChange
    {
        $this->webStockId = $webStockId;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return WebStockChange
     */
    public function setPrice(float $price): WebStockChange
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return float
     */
    public function getPercentage(): float
    {
        return $this->percentage;
    }

    /**
     * @param float $percentage
     * @return WebStockChange
     */
    public function setPercentage(float $percentage): WebStockChange
    {
        $this->percentage = $percentage;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return WebStockChange
     */
    public function setCreatedAt(?Carbon $createdAt): WebStockChange
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon|null $updatedAt
     * @return WebStockChange
     */
    public function setUpdatedAt(?Carbon $updatedAt): WebStockChange
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
