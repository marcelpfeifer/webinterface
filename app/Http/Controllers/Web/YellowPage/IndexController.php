<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 12.12.2020
 * Time: 22:36
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Web\YellowPage;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Http\Request\Web\YellowPage\Save;
use App\Libraries\Api\Player\Online;
use App\Libraries\Api\Player\StartCall;
use App\Model\Character;
use App\Model\Dto\WebYellowPage;
use Illuminate\Http\Request;

class IndexController extends Controller
{

    /**
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, string $hash): \Illuminate\View\View
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return view('404');
        }
        $characterDto = Character::entryAsDto($character);

        $onlineCharacters = Online::getPlayers();
        $entries = (new \App\Model\WebYellowPage())->getOnlineEntries(array_keys($onlineCharacters));

        return view(
            'web.yellowPage.index',
            [
                'hash'      => $hash,
                'character' => $characterDto,
                'entries'   => $entries,
            ]
        );
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request, string $hash): \Illuminate\Http\RedirectResponse
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('web.index', $hash);
        }
        // Validiere die Anfrage
        $validator = (new Save($request));
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            $messages = [];
            foreach ($validatedData->errors()->all() as $error) {
                $messages[] = (new Message())
                    ->setMessage($error)
                    ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
            }
            return redirect()->route('web.yellowPage.index', $hash)
                ->with('messages', $messages)
                ->withInput()
                ->withErrors($validatedData->errors());
        }

        $characterDto = Character::entryAsDto($character);
        (new \App\Model\WebYellowPage())->deleteEntryByCharacterId($characterDto->getId());
        $dto = (new WebYellowPage())
            ->setCharacterId($characterDto->getId())
            ->setName($characterDto->getName())
            ->setMessage($validator->toDto()->getMessage());
        (new \App\Model\WebYellowPage())->insertEntry($dto);

        $messages[] = (new Message())
            ->setMessage("Nachricht wurde aktualisiert!")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('web.yellowPage.index', $hash)->with('messages', $messages);;
    }


    /**
     * @param Request $request
     * @param int $id
     * @param string $hash
     * @return \Illuminate\Http\RedirectResponse
     */
    public function call(Request $request, int $id, string $hash): \Illuminate\Http\RedirectResponse
    {
        $messages = [];
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('web.index', $hash);
        }

        $target = Character::find($id);
        if (!($target)) {
            return redirect()->route('web.index', $hash);
        }

        $targetDto = Character::entryAsDto($target);
        $characterDto = Character::entryAsDto($character);

        $worked = StartCall::call($characterDto->getId(), $targetDto->getId(), false);
        if (!$worked) {
            $messages[] = (new Message())
                ->setMessage("Konnte nicht angerufen werden!")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
        }
        return redirect()->route('web.yellowPage.index', $hash)->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request, string $hash): \Illuminate\Http\RedirectResponse
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('web.index', $hash);
        }
        $characterDto = Character::entryAsDto($character);
        (new \App\Model\WebYellowPage())->deleteEntryByCharacterId($characterDto->getId());

        $messages[] = (new Message())
            ->setMessage("Nachricht wurde gelöscht!")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('web.yellowPage.index', $hash)->with('messages', $messages);;
    }

}
