@extends('layouts.app')
@section('title')
    404 not found
@endsection
@section('content')
    <b> Leider konnte die aktuelle Seite nicht gefunden werden!</b>
    <br/>
    <br/>
    <img width="100%" height="100%" src="{{ url('img/sadDog.jpg') }}">
@endsection
