<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 13.04.2020
 * Time: 22:37
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('api.layout.app')
@section('title')
    <i class="fas fa-car"></i>
    Parkt ein Fahrzeug ein
@endsection
@section('content')
    <form method="post" action="{{ route('api.vehicle.park.save') }}">
        {{ csrf_field() }}

        <div class="form-group">
            <label for="vehId">Fahrzeug ID</label>
            <input type="number" class="form-control" id="vehId" name="vehId">
        </div>
        <button type="submit" class="btn btn-danger">Bestätigen</button>
    </form>
    <br/>
@endsection
