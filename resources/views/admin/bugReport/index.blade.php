<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 11.08.2021
 * Time: 09:56
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-bug"></i>
    Bug-Report Übersicht
@endsection
@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Titel</th>
            <th scope="col">Datum</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($reports as $report)
            <tr>
                <th scope="row">{{ $report->getId() }}</th>
                <td>{{ $report->getTitle() }}</td>
                <td>{{ $report->getCreatedAt()->format('Y-m-d H:i:s') }}</td>
                <td>
                    <a href="{{ route('admin.bugReport.show', $report->getId()) }}" class="btn btn-primary">
                        Anzeigen
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
