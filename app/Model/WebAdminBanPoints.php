<?php
/**
 * WebAdminBanPoints
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 05.09.2020
 * Time: 13:53
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class WebAdminBanPoints
 * @package App\Model
 */
class WebAdminBanPoints extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    const COLUMN_POINTS = 'points';

    /**
     * @var string
     */
    const COLUMN_LENGTH = 'length';

    /**
     * @var string
     */
    const COLUMN_ADDITIONAL = 'additional';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webAdminBanPoints';

    /**
     * @param Dto\WebAdminBanPoints $dto
     * @return int
     */
    public function insertEntry(Dto\WebAdminBanPoints $dto): int
    {
        return $this->insertGetId(
            [
                self::COLUMN_NAME       => $dto->getName(),
                self::COLUMN_POINTS     => $dto->getPoints(),
                self::COLUMN_LENGTH     => $dto->getLength(),
                self::COLUMN_ADDITIONAL => $dto->getAdditional(),
                self::CREATED_AT        => Carbon::now(),
                self::UPDATED_AT        => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebAdminBanPoints $dto
     */
    public function updateEntry(Dto\WebAdminBanPoints $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_NAME       => $dto->getName(),
                self::COLUMN_POINTS     => $dto->getPoints(),
                self::COLUMN_LENGTH     => $dto->getLength(),
                self::COLUMN_ADDITIONAL => $dto->getAdditional(),
                self::UPDATED_AT        => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebAdminBanPoints $dto
     */
    public function deleteEntry(\App\Model\Dto\WebAdminBanPoints $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebAdminBanPoints $entry
     * @return Dto\WebAdminBanPoints
     */
    public static function entryAsDto(WebAdminBanPoints $entry): Dto\WebAdminBanPoints
    {
        return (new Dto\WebAdminBanPoints())
            ->setId($entry->{self::COLUMN_ID})
            ->setName($entry->{self::COLUMN_NAME})
            ->setPoints($entry->{self::COLUMN_POINTS})
            ->setLength($entry->{self::COLUMN_LENGTH})
            ->setAdditional($entry->{self::COLUMN_ADDITIONAL})
            ->setCreatedAt($entry->{self::CREATED_AT})
            ->setUpdatedAt($entry->{self::UPDATED_AT});
    }
}
