<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 28.04.2020
 * Time: 17:18
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

/**
 * @var $viewDto \App\Libraries\Frontend\Dto\Admin\SupportForm\Whitelist\Show
 */
?>
@extends('admin.layout.app')
@section('title')
    <i class="fab fa-wpforms"></i>
    Eintrag {{ $viewDto->getEntry()->getId() }}
    <a class="btn btn-dark btn-sm float-right"
       href="{{ route('admin.supportForm.whitelist.index') }}"
       role="button">
        Zurück
    </a>
@endsection
@section('content')
    <form action="{{ route('admin.supportForm.whitelist.update', $viewDto->getEntry()->getId()) }}" method="POST">
        @method('patch')
        {{ csrf_field() }}
        <div class="form-group">
            <label for="username">Name IC</label>
            <input type="text" class="form-control" id="username" name="username"
                   value="{{ $viewDto->getEntry()->getUserName() }}">
        </div>
        <div class="form-group">
            <label for="status">Status</label>
            <select name="status" id="status" class="form-control">
                @foreach(\App\Model\Enum\WebAdminWhitelist\Status::getConstants() as $status)
                    <option
                        value="{{ $status }}" {{ $status === $viewDto->getEntry()->getStatus() ? 'selected': '' }}>{{ $status }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="retryDate">Erneuter Versuch</label>
            <input type="date" class="form-control" id="retryDate"
                   value="{{ $viewDto->getEntry()->getRetryDate() ? $viewDto->getEntry()->getRetryDate()->format('Y-m-d'):null }}"
                   name="retryDate"
                   min="{{ \Carbon\Carbon::now()->format('Y-m-d') }}">
        </div>
        <div class="form-group">
            <label for="message">Nachricht</label>
            <textarea rows="10" class="form-control" id="message" name="message"
                      required>{{ $viewDto->getEntry()->getMessage() }}</textarea>
        </div>
        <br/>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Fragebogen</th>
                <th scope="col"></th>
                <th scope="col"></th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($viewDto->getQuestions() as $question)
                @php
                    $answer = null;
                    $answers = $viewDto->getAnswers();
                    if(isset($answers[$question->getId()])){
                        $answer = $answers[$question->getId()];
                    }
                @endphp
                <tr>
                    <th scope="row">{{ $question->getQuestion() }}</th>
                    <td class="bg-success">Richtig</td>
                    <td class="bg-warning">Unsicher</td>
                    <td class="bg-danger">Falsch</td>
                </tr>
                <tr>
                    <td>{{ $question->getAnswer() }}</td>
                    <td class="bg-success">
                        <input class="form-control" type="radio"
                               value="{{ \App\Model\Enum\WebAdminWhitelistAnswer\Accuracy::RIGHT }}"
                               name="answer[{{ $question->getId() }}]"
                               id="answer[{{ $question->getId() }}]" {{ $answer && $answer->getAccuracy() === \App\Model\Enum\WebAdminWhitelistAnswer\Accuracy::RIGHT ? 'checked': ''}}>
                    </td>
                    <td class="bg-warning">
                        <input class="form-control" type="radio"
                               value="{{ \App\Model\Enum\WebAdminWhitelistAnswer\Accuracy::UNSURE }}"
                               name="answer[{{ $question->getId() }}]"
                               id="answer[{{ $question->getId() }}]" {{ $answer &&  $answer->getAccuracy() === \App\Model\Enum\WebAdminWhitelistAnswer\Accuracy::UNSURE ? 'checked': ''}}>
                    </td>
                    <td class="bg-danger">
                        <input class="form-control" type="radio"
                               value="{{ \App\Model\Enum\WebAdminWhitelistAnswer\Accuracy::WRONG }}"
                               name="answer[{{ $question->getId() }}]"
                               id="answer[{{ $question->getId() }}]" {{ $answer &&  $answer->getAccuracy() === \App\Model\Enum\WebAdminWhitelistAnswer\Accuracy::WRONG ? 'checked': ''}}>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <button type="submit" class="btn btn-dark">Aktualisieren</button>
    </form>

@endsection
