<?php
/**
 * Total
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 02.09.2021
 * Time: 17:46
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Dto\Web\Stock\Depot;


class Total
{

    /**
     * @var string
     */
    private $total = '';

    /**
     * @var string
     */
    private $gained = '';

    /**
     * @var string
     */
    private $gainedPercent = '';

    /**
     * @return string
     */
    public function getTotal(): string
    {
        return $this->total;
    }

    /**
     * @param string $total
     * @return Total
     */
    public function setTotal(string $total): Total
    {
        $this->total = $total;
        return $this;
    }

    /**
     * @return string
     */
    public function getGained(): string
    {
        return $this->gained;
    }

    /**
     * @param string $gained
     * @return Total
     */
    public function setGained(string $gained): Total
    {
        $this->gained = $gained;
        return $this;
    }

    /**
     * @return string
     */
    public function getGainedPercent(): string
    {
        return $this->gainedPercent;
    }

    /**
     * @param string $gainedPercent
     * @return Total
     */
    public function setGainedPercent(string $gainedPercent): Total
    {
        $this->gainedPercent = $gainedPercent;
        return $this;
    }
}
