<?php
/**
 * Store
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.10.2021
 * Time: 09:54
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Admin\SupportForm\Whitelist;

use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Model\Account;
use App\Model\Dto\WebAdminWhitelistAnswer;
use App\Model\WebAdminWhitelist;
use Illuminate\Support\Facades\Auth;

class Store
{

    /**
     * @param \App\Http\Request\Dto\Admin\SupportForm\WhiteList\Store $saveDto
     * @return bool
     */
    public static function save(\App\Http\Request\Dto\Admin\SupportForm\WhiteList\Store $saveDto): bool
    {
        $dto = (new \App\Model\Dto\WebAdminWhitelist())
            ->setMessage($saveDto->getMessage())
            ->setUserName($saveDto->getUsername())
            ->setStatus($saveDto->getStatus())
            ->setRetryDate($saveDto->getRetryDate())
            ->setUserId(Auth::user()->{Account::COLUMN_ID});

        $loggingId = Logging::insert(Name::SUPPORT_FORM_WHITELIST, Auth::id(), $dto);

        $WebAdminWhitelistId = (new WebAdminWhitelist())->insertEntry($dto);
        foreach ($saveDto->getAnswer() as $questionId => $accuracy) {
            $dto = (new WebAdminWhitelistAnswer())
                ->setWebAdminWhitelistId($WebAdminWhitelistId)
                ->setWebAdminWhitelistQuestionId($questionId)
                ->setAccuracy($accuracy);
            (new \App\Model\WebAdminWhitelistAnswer())->insertEntry($dto);
            Logging::insert(Name::SUPPORT_FORM_WHITELIST, Auth::id(), $dto, $loggingId);
        }
        return true;
    }
}
