<?php
/**
 * WebDarkWeb
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 07.08.2020
 * Time: 20:39
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class WebDarkWeb extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_CHARACTER_ID = 'characterId';


    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webDarkWeb';


    /**
     * @param int $characterId
     * @return Dto\WebDarkWeb|null
     */
    public function getEntryByCharacterId(int $characterId): ?Dto\WebDarkWeb
    {
        $entry = $this->where(self::COLUMN_CHARACTER_ID, $characterId)->first();
        return ($entry) ? self::entryAsDto($entry) : null;
    }

    /**
     * @param Dto\WebDarkWeb $dto
     */
    public function insertEntry(Dto\WebDarkWeb $dto)
    {
        $this->insert(
            [
                self::COLUMN_ID           => $dto->getId(),
                self::COLUMN_CHARACTER_ID => $dto->getCharacterId(),
                self::COLUMN_NAME         => $dto->getName(),
                self::CREATED_AT          => Carbon::now(),
                self::UPDATED_AT          => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebDarkWeb $dto
     */
    public function updateEntry(Dto\WebDarkWeb $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_ID           => $dto->getId(),
                self::COLUMN_CHARACTER_ID => $dto->getCharacterId(),
                self::COLUMN_NAME         => $dto->getName(),
                self::UPDATED_AT          => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebDarkWeb $dto
     */
    public function deleteEntry(\App\Model\Dto\WebDarkWeb $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebDarkWeb $entry
     * @return Dto\WebDarkWeb
     */
    public static function entryAsDto(WebDarkWeb $entry): Dto\WebDarkWeb
    {
        return (new Dto\WebDarkWeb())
            ->setId($entry->{self::COLUMN_ID})
            ->setCharacterId($entry->{self::COLUMN_CHARACTER_ID})
            ->setName($entry->{self::COLUMN_NAME});
    }
}
