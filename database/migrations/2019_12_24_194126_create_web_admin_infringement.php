<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebAdminInfringement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('WebAdminInfringement', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('userId');
            $table->string('userName');
            $table->string('videoLink');
            $table->text('reason');
            $table->string('reportedBy');
            $table->text('description');
            $table->text('action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('WebAdminInfringement');
    }
}
