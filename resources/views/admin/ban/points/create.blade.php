<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 20.09.2020
 * Time: 10:34
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-ban"></i>
    Eintrag erstellen
    <a class="btn btn-dark btn-sm float-right"
       href="{{ route('admin.ban.points.index') }}"
       role="button"
    >
        Zurück
    </a>
@endsection
@section('content')
    <form action="{{ route('admin.ban.points.save') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" required>
        </div>
        <div class="form-group">
            <label for="points">Punkte</label>
            <input type="number" class="form-control" id="points" name="points"
                   required>
        </div>
        <div class="form-group">
            <label for="length">Länge (in Stunden)</label>
            <input type="number" class="form-control" id="length" name="length" value="0" required>
            <p class="text-small"> 0 = PERMA</p>
        </div>
        <div class="form-group">
            <label for="additional">Zusatz (innerhalb eines Monates)</label>
            <input type="number" class="form-control" id="additional" name="additional" value="0" required>
            <p class="text-small">
                Führt zu Perma falls diese Strafe öfters als dieser Wert innerhalb eines Monates überschritten wird
            </p>
        </div>
        <br/>
        <button type="submit" class="btn btn-dark">Erstellen</button>
    </form>
@endsection

