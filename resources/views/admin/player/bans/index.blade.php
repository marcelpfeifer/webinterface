<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 05.05.2020
 * Time: 17:21
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('admin.layout.app')
@section('title')
    Strikes
    <a class="btn btn-dark btn-sm float-right ml-1"
       href="{{ route('admin.player.edit', $accountId) }}"
       role="button"
    >
        Zurück
    </a>
    <a class="btn btn-dark btn-sm float-right ml-1"
       href="{{ route('admin.player.bans.serverBans', $accountId) }}"
       role="button"
    >
        Server Bans
    </a>
@endsection
@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Punkte</th>
            <th scope="col">Grund</th>
            <th scope="col">Datum</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($bans as $ban)
            <tr>
                <th scope="row">{{ $ban->getId() }}</th>
                <td>{{ $ban->getPoints() }}</td>
                <td>{{ $ban->getReason() }}</td>
                <td>{{ $ban->getCreatedAt()->format('Y-m-d H:i:s') }}</td>
                <td>
                    <form action="{{ route('admin.player.bans.delete', $ban->getId()) }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE"/>
                        <button class="btn btn-danger" type="submit">Löschen</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <h4> Erstellen </h4>
    <form method="GET" action="{{ route('admin.player.bans.create') }}">
        <input type="hidden" name="accountId" value="{{ $accountId }}">
        <div class="form-group">
            <label for="reasonId">Grund</label>
            <select class="form-control" id="reasonId" name="reasonId" required>
                @foreach($reasons as $reason)
                    <option value="{{ $reason->id }}"> {{ $reason->name }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-dark">Erstellen</button>
    </form>
@endsection

