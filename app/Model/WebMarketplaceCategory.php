<?php
/**
 * WebMarketplaceCategory
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 02.05.2020
 * Time: 17:56
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class WebMarketplaceCategory extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webMarketplaceCategory';

    /**
     * @param Dto\WebMarketplaceCategory $dto
     */
    public function insertEntry(Dto\WebMarketplaceCategory $dto)
    {
        $this->insert([
            self::COLUMN_ID   => $dto->getId(),
            self::COLUMN_NAME => $dto->getName(),
            self::CREATED_AT  => Carbon::now(),
            self::UPDATED_AT  => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\WebMarketplaceCategory $dto
     */
    public function updateEntry(Dto\WebMarketplaceCategory $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update([
            self::COLUMN_ID   => $dto->getId(),
            self::COLUMN_NAME => $dto->getName(),
            self::UPDATED_AT  => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\WebMarketplaceCategory $dto
     */
    public function deleteEntry(\App\Model\Dto\WebMarketplaceCategory $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebMarketplaceCategory $entry
     * @return Dto\WebMarketplaceCategory
     */
    public static function entryAsDto(WebMarketplaceCategory $entry): Dto\WebMarketplaceCategory
    {
        return (new Dto\WebMarketplaceCategory())
            ->setId($entry->{self::COLUMN_ID})
            ->setName($entry->{self::COLUMN_NAME});
    }
}
