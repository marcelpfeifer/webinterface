<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 06.09.2020
 * Time: 11:37
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-ban"></i>
    Ban-Management
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <i class="fas fa-link"></i>
            <a href="{{ route('admin.ban.points.index') }}">Strikepunktemanagement</a>
        </div>
        <div class="col-12">
            <i class="fas fa-link"></i>
            <a href="{{ route('admin.ban.reason.index') }}">Begründungsmanagement </a>
        </div>
    </div>
@endsection
