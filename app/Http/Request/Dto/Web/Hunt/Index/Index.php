<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 16.09.2021
 * Time: 18:52
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Web\Hunt\Index;


use App\Http\Request\Dto\ADto;

class Index extends ADto
{

    /**
     * @var string
     */
    private $orderBy = '';

    /**
     * @return string
     */
    public function getOrderBy(): string
    {
        return $this->orderBy;
    }

    /**
     * @param string $orderBy
     * @return Index
     */
    public function setOrderBy(string $orderBy): Index
    {
        $this->orderBy = $orderBy;
        return $this;
    }
}
