<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 12.08.2021
 * Time: 12:49
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('layouts.app')
@section('title')
    <i class="fas fa-envelope"></i>
    Neue Email-Adresse hinzufügen
    <a class="btn btn-secondary btn-sm float-right"
       href="{{ route('auth.mailbox.index') }}"
       role="button"
    >
        Zurück
    </a>
@endsection
@section('content')
    <form action="{{ route('auth.mailbox.addSave') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" id="email" name="email" autocomplete="off" placeholder="max.mustermann@eternity.de" required>
        </div>
        <div class="form-group">
            <label for="password">Passwort</label>
            <input type="password" class="form-control" id="password" name="password" autocomplete="off" required>
        </div>
        <button type="submit" class="btn btn-primary">Hinzufügen</button>
    </form>
@endsection
