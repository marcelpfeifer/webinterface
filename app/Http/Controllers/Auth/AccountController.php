<?php
/**
 * AccountController.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 29.08.2019
 * Time: 20:06
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Auth;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Libraries\Frontend\Auth\Account\Index;
use App\Model\Account;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;
use Illuminate\Validation\Rule;

class AccountController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        if (!($account = Auth::user())) {
            $messages[] = (new Message())
                ->setMessage("Bitte anmelden")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::NOTICE);
            return redirect()->route('login')->with('messages', $messages);
        }

        $viewData = Index::getViewData();
        return view('auth.dashboard',$viewData);
    }

    /**
     * @return View
     */
    public function edit()
    {
        if (!($account = Auth::user())) {
            $messages[] = (new Message())
                ->setMessage("Bitte anmelden")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::NOTICE);
            return redirect()->route('login')->with('messages', $messages);
        }
        $accountDto = Account::entryAsDto($account);

        return \view('auth.account.edit', [
            'account' => $accountDto,
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function save(Request $request): RedirectResponse
    {
        $messages = [];
        if (!($account = Auth::user())) {
            $messages[] = (new Message())
                ->setMessage("Bitte anmelden")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::NOTICE);
            return redirect()->route('login')->with('messages', $messages);
        }

        // Validiere die Anfrage
        $validator = $this->validateRequest($request, $account);
        if ($validator->fails()) {
            $messages = [];
            foreach ($validator->errors()->all() as $error) {
                $messages[] = (new Message())
                    ->setMessage($error)
                    ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
            }
            return redirect()->route('auth.account.edit')
                ->with('messages', $messages)
                ->withInput()
                ->withErrors($validator->errors());
        }

        // Hole die Spieler Daten aus der Datenbank
        $accountDto = Account::entryAsDto($account);

        // Passwort darf nur aktualisiert werden wenn es auch übergeben wird ansonsten wird das alte behalten
        if ($password = $request->input('password')) {
            $accountDto->setPassword(\hash('sha256', $password));
        }

        // Lädt das Bild hoch
        $file = $request->file('image');
        $accountDto = Account\ImageName::upload($file, $accountDto);

        // Aktualisiere das DTO
        (new Account())->updateEntry($accountDto);

        $messages[] = (new Message())
            ->setMessage("Account wurde erfolgreich aktualisiert")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('auth.account.edit')->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param Account $account
     * @return \Illuminate\Contracts\Validation\Validator
     */
    private function validateRequest(Request $request, Account $account)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'password' => [
                    'nullable',
                    'string',
                    'min:8',
                    'confirmed'
                ],
                'image'    => [
                    'nullable',
                    'image',
                    'mimes:jpeg',
                    'max:500',
                    Rule::dimensions()
                        ->maxWidth(500)
                        ->maxHeight(500)
                        ->minHeight(500)
                        ->minWidth(500)
                ]
            ],
            [
                'password.required'  => 'Das Passwort muss angegeben werden',
                'password.string'    => 'Das Passwort muss ein String sein',
                'password.min'       => 'Das Passwort muss mindestens 8 Zeichen lang sein',
                'password.confirmed' => 'Die Passwörter müssen übereinstimmen',
                'image.image'        => 'Es muss ein Bild hochgeladen werden',
                'image.mimes'        => 'Für das Bild sind nur jpeg erlaubt',
                'image.max'          => 'Das Bild darf nicht größer als 500kb sein',
                'image.dimensions'   => 'Das Bild muss 500x500 Pixel groß sein',
            ]);

        return $validator;
    }
}
