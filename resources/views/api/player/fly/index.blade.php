<?php
/**
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 08.04.2020
 * Time: 17:53
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('api.layout.app')
@section('title')
    <i class="fab fa-fly"></i>
    Aktiviert fliegen für den Spieler
@endsection
@section('content')
    <form method="post" action="{{ route('api.player.fly.save') }}">
        {{ csrf_field() }}

        @include('api.includes.select', [
          'name' => 'pid',
          'label' => 'Spieler',
          'entries' => $players,
          'currentSelect' => $currentPlayer,
        ])

        <div class="form-check">
            <input type="checkbox" class="form-check-input" id="active" name="active">
            <label class="form-check-label" for="active">Aktivieren?</label>
        </div>
        <div class="form-check">
            <input type="checkbox" class="form-check-input" id="stealth" name="stealth">
            <label class="form-check-label" for="stealth">Unsichtbar danach?</label>
        </div>

        <button type="submit" class="btn btn-danger">Bestätigen</button>
    </form>
    <br/>
@endsection
