<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 12.08.2021
 * Time: 13:10
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Model\Auth\Mail\Mailbox;


use App\Http\Model\RandomString;
use App\Model\Dto\WebMailAddress;
use App\Model\Dto\WebMailAddressToAccount;
use Illuminate\Support\Facades\Auth;

class Save
{
    /**
     * @param \App\Http\Request\Dto\Auth\Mail\Mailbox\Save $requestDto
     */
    public static function save(\App\Http\Request\Dto\Auth\Mail\Mailbox\Save $requestDto, string $accountId)
    {
        $dto = (new WebMailAddress())
            ->setEmail($requestDto->getEmail())
            ->setPassword(RandomString::generate());
        $id = (new \App\Model\WebMailAddress())->insertEntry($dto);

        $mailAddressToAccountDto = (new WebMailAddressToAccount())
            ->setAccountId($accountId)
            ->setWebMailAddressId($id)
            ->setOwner(true);
        (new \App\Model\WebMailAddressToAccount())->insertEntry($mailAddressToAccountDto);
    }
}
