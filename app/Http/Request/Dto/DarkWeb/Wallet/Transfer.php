<?php
/**
 * Transfer
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 22.08.2020
 * Time: 21:24
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\DarkWeb\Wallet;

use App\Http\Request\Dto\ADto;

class Transfer extends ADto
{

    /**
     * @var string
     */
    private $hash = '';

    /**
     * @var int
     */
    private $amount = 0;

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     * @return Transfer
     */
    public function setHash(string $hash): Transfer
    {
        $this->hash = $hash;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     * @return Transfer
     */
    public function setAmount(int $amount): Transfer
    {
        $this->amount = $amount;
        return $this;
    }
}
