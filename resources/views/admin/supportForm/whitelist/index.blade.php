<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 28.04.2020
 * Time: 17:18
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

/**
 * @var $viewDto \App\Libraries\Frontend\Dto\Admin\SupportForm\Whitelist\Index
 */
?>
@extends('admin.layout.app')
@section('title')
    <i class="fab fa-wpforms"></i>
    Whitelist
    <a class="btn btn-dark btn-sm float-right"
       href="{{ route('admin.supportForm.whitelist.create') }}"
       role="button"
    >
        Eintrag erstellen
    </a>
@endsection
@section('content')
    <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name IC</th>
                <th scope="col">Status</th>
                <th scope="col">Wiederversuchsdatum</th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($viewDto->getResults() as $result)
                @php
                    $class = '';
                    switch ($result->getEntry()->getStatus()){
                        case \App\Model\Enum\WebAdminWhitelist\Status::APPROVED:
                        $class = 'bg-success';
                        break;
                        case \App\Model\Enum\WebAdminWhitelist\Status::DENIED:
                        $class = 'bg-danger';
                        break;
                    }

                @endphp
                <tr>
                    <th scope="row">{{ $result->getEntry()->getId() }}</th>
                    <td>{{ $result->getEntry()->getUsername() }}</td>
                    <td>{{ $result->getEntry()->getStatus() }}</td>
                    <td>{{$result->getEntry()->getRetryDate() ? $result->getEntry()->getRetryDate()->format('Y-m-d H:i:s') : null}}</td>
                    <td>
                        <a class="btn btn-primary"
                           href="{{ route('admin.supportForm.whitelist.show', $result->getEntry()->getId()) }}">Anzeigen</a>
                    </td>
                    <td>
                        <form action="{{ route('admin.supportForm.whitelist.destroy', $result->getEntry()->getId())}}"
                              method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="submit" class="btn btn-danger" value="Löschen"/>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
