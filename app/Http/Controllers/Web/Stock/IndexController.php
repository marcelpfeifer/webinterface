<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 29.08.2021
 * Time: 13:38
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Web\Stock;

use App\Dto\Message;
use App\Http\Controllers\HashController;
use App\Http\Request\AValidator;
use App\Http\Request\Web\Stock\Buy;
use App\Http\Request\Web\Stock\Sell;
use App\Libraries\Frontend\Web\Stock\Depot;
use App\Libraries\Frontend\Web\Stock\StockData;
use App\Model\Character;
use App\Model\Dto\WebMarketplaceBuy;
use App\Model\WebStock;
use App\Model\WebStockCharacter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class IndexController extends HashController
{

    /**
     * @param Request $request
     * @param string $hash
     * @return View
     */
    public function index(Request $request, string $hash): View
    {
        $stocks = [];
        foreach (WebStock::all() as $entry) {
            $stocks[] = WebStock::entryAsDto($entry);
        }
        return view(
            'web.stock.index',
            [
                'stocks' => $stocks,
                'hash'   => $hash,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function getStockData(Request $request, int $id): JsonResponse
    {
        $viewData = StockData::getViewData($id);
        return response()->json($viewData, 200);
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return View
     */
    public function show(Request $request, int $id, string $hash): View
    {
        $entry = WebStock::find($id);
        if (!$entry) {
            return \view('404');
        }
        $stock = WebStock::entryAsDto($entry);
        return view(
            'web.stock.show',
            [
                'stock' => $stock,
                'hash'  => $hash,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @param string $hash
     * @return View
     */
    public function buy(Request $request, int $id, string $hash): View
    {
        $entry = WebStock::find($id);
        if (!$entry) {
            return \view('404');
        }
        $stock = WebStock::entryAsDto($entry);
        return view(
            'web.stock.buy',
            [
                'stock' => $stock,
                'hash'  => $hash,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @param string $hash
     * @return RedirectResponse
     */
    public function buyPost(Request $request, int $id, string $hash): RedirectResponse
    {
        $character = $this->getCharacterByHash($hash);
        if (!($character)) {
            return redirect()->route('web.stock.index', $hash);
        }
        $validator = (new Buy($request, $id))
            ->setCharacterDto($character);
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            return redirect()->route('web.stock.buy', [$id, $hash])
                ->with('messages', $validator->getErrorsAsMessageArray($validatedData))
                ->withInput()
                ->withErrors($validatedData->errors());
        }

        $messages = \App\Http\Model\Web\Stock\Buy::save($validator->toDto());

        return redirect()->route('web.stock.depot', $hash)->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @param string $hash
     * @return View
     */
    public function sell(Request $request, int $id, string $hash): View
    {
        $character = $this->getCharacterByHash($hash);
        if (!($character)) {
            return \view('404');
        }
        $entry = WebStockCharacter::find($id);
        if (!$entry) {
            return \view('404');
        }
        $stock = WebStockCharacter::entryAsDto($entry);

        if ($stock->getCharacterId() !== $character->getId()) {
            return \view('404');
        }

        return view(
            'web.stock.sell',
            [
                'stock' => $stock,
                'hash'  => $hash,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @param string $hash
     * @return RedirectResponse
     */
    public function sellPost(Request $request, int $id, string $hash): RedirectResponse
    {
        $character = $this->getCharacterByHash($hash);
        if (!($character)) {
            return redirect()->route('web.stock.index', $hash);
        }
        $validator = (new Sell($request, $id))
            ->setCharacterDto($character);
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            return redirect()->route('web.stock.sell', [$id, $hash])
                ->with('messages', $validator->getErrorsAsMessageArray($validatedData))
                ->withInput()
                ->withErrors($validatedData->errors());
        }
        $messages = \App\Http\Model\Web\Stock\Sell::save($validator->toDto());

        return redirect()->route('web.stock.depot', $hash)->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return View
     */
    public function depot(Request $request, string $hash): View
    {
        $character = $this->getCharacterByHash($hash);
        if (!$character) {
            return \view('404');
        }
        $viewData = Depot::getViewData($character->getId(), $hash);
        return view('web.stock.depot', $viewData);
    }
}


