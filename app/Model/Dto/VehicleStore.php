<?php
/**
 * VehicleStore
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 29.04.2020
 * Time: 16:44
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

use App\Libraries\Logging\ILogging;
use App\Libraries\Logging\Traits\Variables;

class VehicleStore implements ILogging
{
    use Variables;

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var int
     */
    private $stock = 0;

    /**
     * @var string
     */
    private $posX = '';

    /**
     * @var string
     */
    private $posY = '';

    /**
     * @var string
     */
    private $posZ ='';

    /**
     * @var string
     */
    private $faction = '';

    /**
     * @var string
     */
    private $rotX ='';

    /**
     * @var string
     */
    private $rotY ='';

    /**
     * @var string
     */
    private $rotZ = '';

    /**
     * @var string
     */
    private $model = '';

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return VehicleStore
     */
    public function setId(?int $id): VehicleStore
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     * @return VehicleStore
     */
    public function setStock(int $stock): VehicleStore
    {
        $this->stock = $stock;
        return $this;
    }

    /**
     * @return string
     */
    public function getPosX(): string
    {
        return $this->posX;
    }

    /**
     * @param string $posX
     * @return VehicleStore
     */
    public function setPosX(string $posX): VehicleStore
    {
        $this->posX = $posX;
        return $this;
    }

    /**
     * @return string
     */
    public function getPosY(): string
    {
        return $this->posY;
    }

    /**
     * @param string $posY
     * @return VehicleStore
     */
    public function setPosY(string $posY): VehicleStore
    {
        $this->posY = $posY;
        return $this;
    }

    /**
     * @return string
     */
    public function getPosZ(): string
    {
        return $this->posZ;
    }

    /**
     * @param string $posZ
     * @return VehicleStore
     */
    public function setPosZ(string $posZ): VehicleStore
    {
        $this->posZ = $posZ;
        return $this;
    }

    /**
     * @return string
     */
    public function getFaction(): string
    {
        return $this->faction;
    }

    /**
     * @param string $faction
     * @return VehicleStore
     */
    public function setFaction(string $faction): VehicleStore
    {
        $this->faction = $faction;
        return $this;
    }

    /**
     * @return string
     */
    public function getRotX(): string
    {
        return $this->rotX;
    }

    /**
     * @param string $rotX
     * @return VehicleStore
     */
    public function setRotX(string $rotX): VehicleStore
    {
        $this->rotX = $rotX;
        return $this;
    }

    /**
     * @return string
     */
    public function getRotY(): string
    {
        return $this->rotY;
    }

    /**
     * @param string $rotY
     * @return VehicleStore
     */
    public function setRotY(string $rotY): VehicleStore
    {
        $this->rotY = $rotY;
        return $this;
    }

    /**
     * @return string
     */
    public function getRotZ(): string
    {
        return $this->rotZ;
    }

    /**
     * @param string $rotZ
     * @return VehicleStore
     */
    public function setRotZ(string $rotZ): VehicleStore
    {
        $this->rotZ = $rotZ;
        return $this;
    }
    /**
     * @return string
     */
    public function getModel(): string
    {
        return $this->model;
    }

    /**
     * @param string $model
     * @return VehicleStore
     */
    public function setModel(string $model): VehicleStore
    {
        $this->model = $model;
        return $this;
    }
}
