<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateWebMailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Illuminate\Support\Facades\DB::table('webMailAttachment')->truncate();
        \Illuminate\Support\Facades\DB::table('webMail')->delete();
        Schema::table(
            'webMail',
            function (Blueprint $table) {
                $table->unsignedBigInteger('from')->change();
                $table->unsignedBigInteger('to')->nullable()->change();
                $table->foreign('from')->references('id')->on('webMailAddress')->onDelete('cascade');
                $table->foreign('to')->references('id')->on('webMailAddress')->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'webMail',
            function (Blueprint $table) {
                $table->unsignedInteger('from')->change();
                $table->string('to')->nullable()->change();
            }
        );
    }
}
