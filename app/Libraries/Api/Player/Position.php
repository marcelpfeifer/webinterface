<?php
/**
 * Position
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.08.2021
 * Time: 17:24
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Api\Player;


use App\Libraries\Api\AClient;
use App\Model\WebConfig;

class Position extends AClient
{

    /**
     * @var string
     */
    const URL = 'player/position';

    /**
     * @param int $pid
     * @return string
     */
    public static function call(int $pid): string
    {
        $response = self::sendRequest(
            'GET',
            WebConfig::getEntryByKey('api_url')->getValue() . self::URL,
            [
                'query' => [
                    'pid' => $pid,
                ]
            ],
            false
        );
        if ($response && $response->getStatusCode() == 200) {
            $result = $response->getBody()->getContents();
        } else {
            $result = '{
                x => 0,
                y => 0,
                z => 0,
            }';
        }

        return $result;
    }
}
