<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 12.08.2021
 * Time: 12:49
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

/**
 * @var $address \App\Model\Dto\WebMailAddress\WithWebMailAddressToAccount
 */
?>

@extends('web.layout.app')
@section('url')
    https://eternityV.de/mail/
@endsection
@section('content')
    <div class="container bg-white text-dark pb-2">
        <div class="row bg-dark p-2">
            <div class="col text-white">
                <i class="fas fa-envelope"></i>
                Postfach
            </div>
            <div class="col justify-content-end row">
                <div class="col-3">
                    <a class="btn btn-secondary btn-sm float-right"
                       href="{{ route('web.index', $hash) }}"
                       role="button"
                    >
                        Zurück
                    </a>
                </div>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-6">
                <a href="{{ route('web.mailbox.create', $hash) }}" class="btn btn-primary">
                    Neue Email anlegen
                </a>
            </div>
            <div class="col-6">
                <a href="{{ route('web.mailbox.add', $hash) }}" class="btn btn-primary float-right">
                    Bestehende Email hinzufügen
                </a>
            </div>
        </div>
        <table class="table table-striped mt-2">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Adresse</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($addresses as $address)

                <tr>
                    <td>{{ $address->getWebMailAddress()->getId() }}</td>
                    <td>{{  $address->getWebMailAddress()->getEmail() }}</td>
                    <td>
                        <a href="{{ route('web.mailbox.mail.index', [ $address->getWebMailAddress()->getId(),$hash]) }}"
                           class="text-primary">
                            Zum Postfach
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
