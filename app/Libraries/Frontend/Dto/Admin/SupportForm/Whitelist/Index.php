<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 13.10.2021
 * Time: 14:23
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Dto\Admin\SupportForm\Whitelist;

use App\Libraries\Frontend\Dto\Admin\SupportForm\Whitelist\Index\Result;

class Index
{

    /**
     * @var Result[]
     */
    private $results = [];

    /**
     * @return Result[]
     */
    public function getResults(): array
    {
        return $this->results;
    }

    /**
     * @param Result[] $results
     * @return Index
     */
    public function setResults(array $results): Index
    {
        $this->results = $results;
        return $this;
    }
}
