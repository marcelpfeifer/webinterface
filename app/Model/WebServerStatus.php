<?php
/**
 * WebServerStatus
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 11.08.2021
 * Time: 16:23
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class WebServerStatus extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_RAM_MAX = 'ramMax';

    /**
     * @var string
     */
    const COLUMN_RAM_FREE = 'ramFree';

    /**
     * @var string
     */
    const COLUMN_CPU = 'cpu';

    /**
     * @var string
     */
    const COLUMN_ALTV = 'altv';

    /**
     * @var string
     */
    const COLUMN_ALTV_TEST = 'altvTest';

    /**
     * @var string
     */
    const COLUMN_TS = 'ts';

    /**
     * @var string
     */
    const COLUMN_FORUM = 'forum';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webServerStatus';

    /**
     * @param Dto\WebServerStatus $dto
     */
    public function insertEntry(Dto\WebServerStatus $dto)
    {
        $this->insert(
            [
                self::COLUMN_ID        => $dto->getId(),
                self::COLUMN_RAM_MAX   => $dto->getRamMax(),
                self::COLUMN_RAM_FREE  => $dto->getRamFree(),
                self::COLUMN_CPU       => $dto->getCpu(),
                self::COLUMN_ALTV      => $dto->isAltv(),
                self::COLUMN_ALTV_TEST => $dto->isAltvTest(),
                self::COLUMN_TS        => $dto->isTs(),
                self::COLUMN_FORUM     => $dto->isForum(),
                self::CREATED_AT       => Carbon::now(),
                self::UPDATED_AT       => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebServerStatus $dto
     */
    public function updateEntry(Dto\WebServerStatus $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_ID       => $dto->getId(),
                self::COLUMN_RAM_MAX  => $dto->getRamMax(),
                self::COLUMN_RAM_FREE => $dto->getRamFree(),
                self::COLUMN_CPU      => $dto->getCpu(),
                self::COLUMN_ALTV      => $dto->isAltv(),
                self::COLUMN_ALTV_TEST => $dto->isAltvTest(),
                self::COLUMN_TS        => $dto->isTs(),
                self::COLUMN_FORUM     => $dto->isForum(),
                self::UPDATED_AT      => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebServerStatus $dto
     */
    public function deleteEntry(Dto\WebServerStatus $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @return bool
     */
    public function deleteOldEntries(): bool
    {
        return $this
            ->where(self::CREATED_AT, '<=', Carbon::now()->subDays(14))
            ->delete();
    }

    /**
     * @param WebServerStatus $entry
     * @return Dto\WebServerStatus
     */
    public static function entryAsDto(WebServerStatus $entry): Dto\WebServerStatus
    {
        return (new Dto\WebServerStatus())
            ->setId($entry->{self::COLUMN_ID})
            ->setRamMax($entry->{self::COLUMN_RAM_MAX})
            ->setRamFree($entry->{self::COLUMN_RAM_FREE})
            ->setCpu($entry->{self::COLUMN_CPU})
            ->setAltv($entry->{self::COLUMN_ALTV})
            ->setAltvTest($entry->{self::COLUMN_ALTV_TEST})
            ->setTs($entry->{self::COLUMN_TS})
            ->setForum($entry->{self::COLUMN_FORUM})
            ->setCreatedAt($entry->{self::CREATED_AT})
            ->setUpdatedAt($entry->{self::UPDATED_AT});
    }
}
