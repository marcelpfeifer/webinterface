<?php
/**
 * Housing
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 12.05.2020
 * Time: 19:21
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

class Housing
{

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var int
     */
    private $houseId = 0;

    /**
     * @var int
     */
    private $guidId = 0;

    /**
     * @var string
     */
    private $inventory = '';

    /**
     * @var string
     */
    private $pin = '';

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Housing
     */
    public function setId(?int $id): Housing
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getHouseId(): int
    {
        return $this->houseId;
    }

    /**
     * @param int $houseId
     * @return Housing
     */
    public function setHouseId(int $houseId): Housing
    {
        $this->houseId = $houseId;
        return $this;
    }

    /**
     * @return int
     */
    public function getGuidId(): int
    {
        return $this->guidId;
    }

    /**
     * @param int $guidId
     * @return Housing
     */
    public function setGuidId(int $guidId): Housing
    {
        $this->guidId = $guidId;
        return $this;
    }

    /**
     * @return string
     */
    public function getInventory(): string
    {
        return $this->inventory;
    }

    /**
     * @param string $inventory
     * @return Housing
     */
    public function setInventory(string $inventory): Housing
    {
        $this->inventory = $inventory;
        return $this;
    }

    /**
     * @return string
     */
    public function getPin(): string
    {
        return $this->pin;
    }

    /**
     * @param string $pin
     * @return Housing
     */
    public function setPin(string $pin): Housing
    {
        $this->pin = $pin;
        return $this;
    }
}
