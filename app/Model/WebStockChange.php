<?php
/**
 * webStockChange
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 28.08.2021
 * Time: 16:44
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class WebStockChange extends Model
{
    /**
     * @var string
     */
    public const COLUMN_ID = 'id';

    /**
     * @var string
     */
    public const COLUMN_WEB_STOCK_ID = 'webStockId';

    /**
     * @var string
     */
    public const COLUMN_PRICE = 'price';

    /**
     * @var string
     */
    public const COLUMN_PERCENTAGE = 'percentage';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webStockChange';

    /**
     * @param int $webStockId
     * @return Dto\WebStockChange|null
     */
    public function getLatestEntry(int $webStockId): ?Dto\WebStockChange
    {
        $entry = $this->where(self::COLUMN_WEB_STOCK_ID, $webStockId)
            ->orderBy(self::CREATED_AT, 'DESC')
            ->first();
        return $entry ? self::entryAsDto($entry) : null;
    }

    /**
     * @param int $webStockId
     * @return Dto\WebStockChange[]
     */
    public function getEntries(int $webStockId): array
    {
        $entries = $this->where(self::COLUMN_WEB_STOCK_ID, $webStockId)
            ->where(self::CREATED_AT, '>=', Carbon::now()->subDays(5))
            ->get();
        $result = [];
        foreach ($entries as $entry) {
            $result[] = self::entryAsDto($entry);
        }
        return $result;
    }

    /**
     * @param Dto\WebStockChange $dto
     */
    public function insertEntry(Dto\WebStockChange $dto)
    {
        $this->insert(
            [
                self::COLUMN_WEB_STOCK_ID => $dto->getWebStockId(),
                self::COLUMN_PRICE        => $dto->getPrice(),
                self::COLUMN_PERCENTAGE   => $dto->getPercentage(),
                self::CREATED_AT          => Carbon::now(),
                self::UPDATED_AT          => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebStockChange $dto
     */
    public function updateEntry(Dto\WebStockChange $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_WEB_STOCK_ID => $dto->getWebStockId(),
                self::COLUMN_PRICE        => $dto->getPrice(),
                self::COLUMN_PERCENTAGE   => $dto->getPercentage(),
                self::UPDATED_AT          => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebStockChange $dto
     */
    public function deleteEntry(\App\Model\Dto\WebStockChange $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebStockChange $entry
     * @return Dto\WebStockChange
     */
    public static function entryAsDto(WebStockChange $entry): Dto\WebStockChange
    {
        return (new Dto\WebStockChange())
            ->setId($entry->{self::COLUMN_ID})
            ->setWebStockId($entry->{self::COLUMN_WEB_STOCK_ID})
            ->setPrice($entry->{self::COLUMN_PRICE})
            ->setPercentage($entry->{self::COLUMN_PERCENTAGE})
            ->setCreatedAt($entry->{self::CREATED_AT})
            ->setUpdatedAt($entry->{self::UPDATED_AT});
    }
}
