<?php
/**
 * WebAdminTechnicalFaq.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 04.09.2019
 * Time: 21:29
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

use App\Libraries\Logging\ILogging;
use App\Libraries\Logging\Traits\Variables;
use Carbon\Carbon;

class WebAdminTechnicalFaq implements ILogging
{

    use Variables;

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var null|int
     */
    private $userId = null;

    /**
     * @var string
     */
    private $explanation = '';

    /**
     * @var string
     */
    private $errorCode = '';

    /**
     * @var string
     */
    private $troubleShooting = '';

    /**
     * @var string
     */
    private $troubleShooting2 = '';

    /**
     * @var bool
     */
    private $fixed = false;

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @var null|Carbon
     */
    private $updatedAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebAdminTechnicalFaq
     */
    public function setId(?int $id): WebAdminTechnicalFaq
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @param int|null $userId
     * @return WebAdminTechnicalFaq
     */
    public function setUserId(?int $userId): WebAdminTechnicalFaq
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getExplanation(): string
    {
        return $this->explanation;
    }

    /**
     * @param string $explanation
     * @return WebAdminTechnicalFaq
     */
    public function setExplanation(string $explanation): WebAdminTechnicalFaq
    {
        $this->explanation = $explanation;
        return $this;
    }

    /**
     * @return string
     */
    public function getErrorCode(): string
    {
        return $this->errorCode;
    }

    /**
     * @param string $errorCode
     * @return WebAdminTechnicalFaq
     */
    public function setErrorCode(string $errorCode): WebAdminTechnicalFaq
    {
        $this->errorCode = $errorCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getTroubleShooting(): string
    {
        return $this->troubleShooting;
    }

    /**
     * @param string $troubleShooting
     * @return WebAdminTechnicalFaq
     */
    public function setTroubleShooting(string $troubleShooting): WebAdminTechnicalFaq
    {
        $this->troubleShooting = $troubleShooting;
        return $this;
    }

    /**
     * @return string
     */
    public function getTroubleShooting2(): string
    {
        return $this->troubleShooting2;
    }

    /**
     * @param string $troubleShooting2
     * @return WebAdminTechnicalFaq
     */
    public function setTroubleShooting2(string $troubleShooting2): WebAdminTechnicalFaq
    {
        $this->troubleShooting2 = $troubleShooting2;
        return $this;
    }

    /**
     * @return bool
     */
    public function isFixed(): bool
    {
        return $this->fixed;
    }

    /**
     * @param bool $fixed
     * @return WebAdminTechnicalFaq
     */
    public function setFixed(bool $fixed): WebAdminTechnicalFaq
    {
        $this->fixed = $fixed;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return WebAdminTechnicalFaq
     */
    public function setCreatedAt(?Carbon $createdAt): WebAdminTechnicalFaq
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon|null $updatedAt
     * @return WebAdminTechnicalFaq
     */
    public function setUpdatedAt(?Carbon $updatedAt): WebAdminTechnicalFaq
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
