<?php
/**
 * VehicleKeys
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 30.04.2020
 * Time: 15:55
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

class VehicleKeys
{

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var int
     */
    private $guid = 0;

    /**
     * @var int
     */
    private $carId = 0;

    /**
     * @var int
     */
    private $second = 0;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return VehicleKeys
     */
    public function setId(?int $id): VehicleKeys
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getGuid(): int
    {
        return $this->guid;
    }

    /**
     * @param int $guid
     * @return VehicleKeys
     */
    public function setGuid(int $guid): VehicleKeys
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return int
     */
    public function getCarId(): int
    {
        return $this->carId;
    }

    /**
     * @param int $carId
     * @return VehicleKeys
     */
    public function setCarId(int $carId): VehicleKeys
    {
        $this->carId = $carId;
        return $this;
    }

    /**
     * @return int
     */
    public function getSecond(): int
    {
        return $this->second;
    }

    /**
     * @param int $second
     * @return VehicleKeys
     */
    public function setSecond(int $second): VehicleKeys
    {
        $this->second = $second;
        return $this;
    }
}
