<?php
/**
 * WebAdminMail
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 17.10.2020
 * Time: 19:57
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

use Carbon\Carbon;

class WebMail
{
    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var int
     */
    private $from = 0;

    /**
     * @var int|null
     */
    private $to = null;

    /**
     * @var string
     */
    private $sender = '';

    /**
     * @var string
     */
    private $receiver = '';

    /**
     * @var string
     */
    private $title = '';

    /**
     * @var string
     */
    private $message = '';

    /**
     * @var bool
     */
    private $seen = false;

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @var null|Carbon
     */
    private $updatedAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebMail
     */
    public function setId(?int $id): WebMail
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getFrom(): int
    {
        return $this->from;
    }

    /**
     * @param int $from
     * @return WebMail
     */
    public function setFrom(int $from): WebMail
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getTo(): ?int
    {
        return $this->to;
    }

    /**
     * @param int|null $to
     * @return WebMail
     */
    public function setTo(?int $to): WebMail
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @return string
     */
    public function getSender(): string
    {
        return $this->sender;
    }

    /**
     * @param string $sender
     * @return WebMail
     */
    public function setSender(string $sender): WebMail
    {
        $this->sender = $sender;
        return $this;
    }

    /**
     * @return string
     */
    public function getReceiver(): string
    {
        return $this->receiver;
    }

    /**
     * @param string $receiver
     * @return WebMail
     */
    public function setReceiver(string $receiver): WebMail
    {
        $this->receiver = $receiver;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return WebMail
     */
    public function setTitle(string $title): WebMail
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return WebMail
     */
    public function setMessage(string $message): WebMail
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSeen(): bool
    {
        return $this->seen;
    }

    /**
     * @param bool $seen
     * @return WebMail
     */
    public function setSeen(bool $seen): WebMail
    {
        $this->seen = $seen;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return WebMail
     */
    public function setCreatedAt(?Carbon $createdAt): WebMail
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon|null $updatedAt
     * @return WebMail
     */
    public function setUpdatedAt(?Carbon $updatedAt): WebMail
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
