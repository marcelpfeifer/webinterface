<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 24.09.2021
 * Time: 17:28
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Dto\Admin\ServerLog;


use App\Libraries\Frontend\Dto\Admin\ServerLog\Index\Entry;

class Index
{

    /**
     * @var Entry[]
     */
    private $entries = [];

    /**
     * @return Entry[]
     */
    public function getEntries(): array
    {
        return $this->entries;
    }

    /**
     * @param Entry[] $entries
     * @return Index
     */
    public function setEntries(array $entries): Index
    {
        $this->entries = $entries;
        return $this;
    }

}
