<?php
/**
 * WebYellowPage
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 12.12.2020
 * Time: 22:28
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;


use Carbon\Carbon;

class WebYellowPage
{
    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var int
     */
    private $characterId = 0;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $message = '';

    /**
     * @var Carbon|null
     */
    private $createAt = null;

    /**
     * @var Carbon|null
     */
    private $updatedAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebYellowPage
     */
    public function setId(?int $id): WebYellowPage
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCharacterId(): int
    {
        return $this->characterId;
    }

    /**
     * @param int $characterId
     * @return WebYellowPage
     */
    public function setCharacterId(int $characterId): WebYellowPage
    {
        $this->characterId = $characterId;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return WebYellowPage
     */
    public function setName(string $name): WebYellowPage
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return WebYellowPage
     */
    public function setMessage(string $message): WebYellowPage
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreateAt(): ?Carbon
    {
        return $this->createAt;
    }

    /**
     * @param Carbon|null $createAt
     * @return WebYellowPage
     */
    public function setCreateAt(?Carbon $createAt): WebYellowPage
    {
        $this->createAt = $createAt;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon|null $updatedAt
     * @return WebYellowPage
     */
    public function setUpdatedAt(?Carbon $updatedAt): WebYellowPage
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
