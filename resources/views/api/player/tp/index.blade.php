<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 04.04.2020
 * Time: 19:04
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('api.layout.app')
@section('title')
    <i class="fas fa-map-signs"></i>
    Teleportiert einen Spieler zum einem anderen Spieler
@endsection
@section('content')
    <form method="post" action="{{ route('api.player.tp.save') }}">
        {{ csrf_field() }}

        @include('api.includes.select', [
         'name' => 'playerFrom',
         'label' => 'Von',
         'entries' => $players,
         'currentSelect' => $currentPlayer,
        ])

        @include('api.includes.select', [
       'name' => 'playerTo',
       'label' => 'Zu',
       'entries' => $players,
       'currentSelect' => null,
        ])

        <button type="submit" class="btn btn-danger">Teleportieren</button>
    </form>
    <br/>
@endsection
