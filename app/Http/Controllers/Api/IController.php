<?php
/**
 * IController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.10.2021
 * Time: 17:39
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Api;


use App\Http\Request\Dto\ADto;

interface IController
{
    /**
     * @param ADto $requestDto
     * @return array
     */
    public function getIndexData(ADto $requestDto): array;

    /**
     * @param ADto $requestDto
     * @return bool
     */
    public function execApiCall(ADto $requestDto): bool;

    /**
     * @param ADto $requestDto
     * @return array
     */
    public function getRedirectData(ADto $requestDto): array;
}
