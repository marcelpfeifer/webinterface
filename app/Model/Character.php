<?php
/**
 * Character.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 01.11.2019
 * Time: 13:23
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class Character extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    const COLUMN_HASH = 'hash';

    /**
     * @var string
     */
    const COLUMN_DOB = 'dob';

    /**
     * @var string
     */
    const COLUMN_PLAYING_TIME = 'playingtime';

    /**
     * @var string
     */
    const COLUMN_FACE = 'face';

    /**
     * @var string
     */
    const COLUMN_INVENTORY = 'inventory';

    /**
     * @var string
     */
    const COLUMN_LAST_POSITION = 'lastposition';

    /**
     * @var string
     */
    const COLUMN_HEALTH = 'health';

    /**
     * @var string
     */
    const COLUMN_ARMOUR = 'armour';

    /**
     * @var string
     */
    const COLUMN_CASH = 'cash';

    /**
     * @var string
     */
    const COLUMN_BANK = 'bank';

    /**
     * @var string
     */
    const COLUMN_HUNGER = 'hunger';

    /**
     * @var string
     */
    const COLUMN_THIRST = 'thirst';

    /**
     * @var string
     */
    const COLUMN_DEAD = 'dead';

    /**
     * @var string
     */
    const COLUMN_HOUSE_ID = 'houseId';

    /**
     * @var string
     */
    const COLUMN_DRIVING_LICENSE_A = 'drivingLicenseA';

    /**
     * @var string
     */
    const COLUMN_DRIVING_LICENSE_B = 'drivingLicenseB';

    /**
     * @var string
     */
    const COLUMN_DRIVING_LICENSE_CE = 'drivingLicenseCE';

    /**
     * @var string
     */
    const COLUMN_MAX_GARAGE = 'maxGarage';

    /**
     * @var string
     */
    const COLUMN_LAST_LOGIN = 'lastLogin';

    /**
     * @var string
     */
    const COLUMN_COP_RANK = 'copRank';

    /**
     * @var string
     */
    const COLUMN_MED_RANK = 'medRank';

    /**
     * @var string
     */
    const COLUMN_FIB_RANK = 'fibRank';

    /**
     * @var string
     */
    const COLUMN_BWFI_RANK = 'bwfiRank';

    /**
     * @var string
     */
    const COLUMN_GROUP = 'group';

    /**
     * @var string
     */
    const COLUMN_GROUP_RANK = 'groupRank';

    /**
     * @var string
     */
    const COLUMN_DOJ_RANK = 'dojRank';

    /**
     * @var string
     */
    const COLUMN_LAWYER_RANK = 'lawyerRank';

    /**
     * @var string
     */
    const COLUMN_TAXI_RANK = 'taxiRank';

    /**
     * @var string
     */
    const COLUMN_DIMENSION = 'dimension';

    /**
     * @var string
     */
    const COLUMN_SERVICE_NUMBER = 'serviceNumber';

    /**
     * @var string
     */
    const COLUMN_SERVICE_NAME = 'serviceName';

    /**
     * @var string
     */
    const COLUMN_BUSINESS_ID = 'businessId';

    /**
     * @var string
     */
    const COLUMN_BUSINESS_RANK = 'businessRank';

    /**
     * @var string
     */
    const COLUMN_BITCOIN = 'bitcoin';

    /**
     * @var string
     */
    const COLUMN_BITCOIN_ILLEGAL = 'bitcoinIllegal';

    /**
     * @var string
     */
    const COLUMN_BITCOIN_HASH_LEGAL = 'bitcoinHashLegal';

    /**
     * @var string
     */
    const COLUMN_BITCOIN_HASH_ILLEGAL = 'bitcoinHashIllegal';

    /**
     * @var string
     */
    const COLUMN_SOCIAL_POINTS = 'socialPoints';

    /**
     * @var string
     */
    const COLUMN_IS_DUTY = 'isDuty';

    /**
     * @var string
     */
    const COLUMN_GUN_LICENSE = 'gunLicense';

    /**
     * @var string
     */
    const COLUMN_CREATED_AT = 'createdAt';

    /**
     * @var string
     */
    const COLUMN_UPDATED_AT = 'updatedAt';


    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $casts = [
        self::COLUMN_LAST_LOGIN => 'date:Y-m-d',
    ];

    /**
     * @var array
     */
    protected $dates = [
        self::COLUMN_LAST_LOGIN,
        self::COLUMN_CREATED_AT,
        self::COLUMN_UPDATED_AT,
    ];

    /**
     * @var string
     */
    protected $table = 'character';

    /**
     * @param int $id
     * @return int
     */
    public static function getPhoneNumberById(int $id)
    {
        return $id + 555230;
    }

    /**
     * @param string $hash
     * @return Character|null
     */
    public function getEntryByHash(string $hash)
    {
        return $this
            ->where(self::COLUMN_HASH, $hash)
            ->first();
    }


    /**
     * @param string $hash
     * @return Dto\Character|null
     */
    public function getEntryByIllegalBitcoinHash(string $hash)
    {
        $entry = $this
            ->where(self::COLUMN_BITCOIN_HASH_ILLEGAL, $hash)
            ->first();
        return $entry ? self::entryAsDto($entry) : null;
    }

    /**
     * @param int $id
     * @return Dto\Character|null
     */
    public function getEntryById(int $id)
    {
        $entry = $this
            ->where(self::COLUMN_ID, $id)
            ->first();
        return ($entry && $entry->{self::COLUMN_NAME}) ? self::entryAsDto($entry) : null;
    }

    /**
     * @param string $search
     * @return LengthAwarePaginator
     */
    public function getCharacters(?string $search = ''): LengthAwarePaginator
    {
        $query = $this;

        if ($search) {
            $query = $query->where(self::COLUMN_NAME, 'LIKE', "%$search%");
        }

        return $query
            ->whereNotNull(self::COLUMN_NAME)
            ->paginate(15);
    }


    /**
     * @param int $groupId
     * @return Dto\Character[]
     */
    public function getAllUsersOfGroupByGroupId(int $groupId): array
    {
        $chars = $this
            ->where(self::COLUMN_GROUP, '=', $groupId)
            ->get();
        $result = [];
        foreach ($chars as $char) {
            $result[] = self::entryAsDto($char);
        }
        return $result;
    }

    /**
     * @param int $businessId
     * @return Dto\Character[]
     */
    public function getAllUsersOfGroupByBusinessId(int $businessId): array
    {
        $chars = $this
            ->where(self::COLUMN_BUSINESS_ID, '=', $businessId)
            ->get();
        $result = [];
        foreach ($chars as $char) {
            $result[] = self::entryAsDto($char);
        }
        return $result;
    }

    /**
     * @param Dto\Character $dto
     * @param string $serviceNumber
     * @return bool
     */
    public function doesServiceNumberExits(Dto\Character $dto, string $serviceNumber): bool
    {
        if ($dto->getDojRank()) {
            return $this
                ->where(self::COLUMN_DOJ_RANK, '!=', 0)
                ->where(self::COLUMN_SERVICE_NUMBER, '=', $serviceNumber)
                ->exists();
        }
        if ($dto->getCopRank()) {
            return $this
                ->where(self::COLUMN_COP_RANK, '!=', 0)
                ->where(self::COLUMN_SERVICE_NUMBER, '=', $serviceNumber)
                ->exists();
        }
        if ($dto->getMedRank()) {
            return $this
                ->where(self::COLUMN_MED_RANK, '!=', 0)
                ->where(self::COLUMN_SERVICE_NUMBER, '=', $serviceNumber)
                ->exists();
        }
        if ($dto->getLawyerRank()) {
            return $this
                ->where(self::COLUMN_LAWYER_RANK, '!=', 0)
                ->where(self::COLUMN_SERVICE_NUMBER, '=', $serviceNumber)
                ->exists();
        }
        if ($dto->getFibRank()) {
            return $this
                ->where(self::COLUMN_FIB_RANK, '!=', 0)
                ->where(self::COLUMN_SERVICE_NUMBER, '=', $serviceNumber)
                ->exists();
        }
        if ($dto->getBwfiRank()) {
            return $this
                ->where(self::COLUMN_BWFI_RANK, '!=', 0)
                ->where(self::COLUMN_SERVICE_NUMBER, '=', $serviceNumber)
                ->exists();
        }
        return true;
    }

    /**
     * @param Dto\Character $dto
     * @param string $serviceName
     * @return bool
     */
    public function doesServiceNameExits(Dto\Character $dto, string $serviceName): bool
    {
        if ($dto->getDojRank()) {
            return $this
                ->where(self::COLUMN_DOJ_RANK, '!=', 0)
                ->where(self::COLUMN_SERVICE_NAME, '=', $serviceName)
                ->exists();
        }
        if ($dto->getCopRank()) {
            return $this
                ->where(self::COLUMN_COP_RANK, '!=', 0)
                ->where(self::COLUMN_SERVICE_NAME, '=', $serviceName)
                ->exists();
        }
        if ($dto->getMedRank()) {
            return $this
                ->where(self::COLUMN_MED_RANK, '!=', 0)
                ->where(self::COLUMN_SERVICE_NAME, '=', $serviceName)
                ->exists();
        }
        if ($dto->getLawyerRank()) {
            return $this
                ->where(self::COLUMN_LAWYER_RANK, '!=', 0)
                ->where(self::COLUMN_SERVICE_NAME, '=', $serviceName)
                ->exists();
        }
        if ($dto->getFibRank()) {
            return $this
                ->where(self::COLUMN_FIB_RANK, '!=', 0)
                ->where(self::COLUMN_SERVICE_NAME, '=', $serviceName)
                ->exists();
        }
        if ($dto->getBwfiRank()) {
            return $this
                ->where(self::COLUMN_BWFI_RANK, '!=', 0)
                ->where(self::COLUMN_SERVICE_NAME, '=', $serviceName)
                ->exists();
        }
        return true;
    }

    /**
     * @param Dto\Character $dto
     */
    public function insertEntry(Dto\Character $dto)
    {
        $this->insert(
            [
                self::COLUMN_ID                   => $dto->getId(),
                self::COLUMN_NAME                 => $dto->getName(),
                self::COLUMN_HASH                 => $dto->getHash(),
                self::COLUMN_DOB                  => $dto->getDob(),
                self::COLUMN_PLAYING_TIME         => $dto->getPlayingTime(),
                self::COLUMN_FACE                 => $dto->getFace(),
                self::COLUMN_INVENTORY            => $dto->getInventory(),
                self::COLUMN_LAST_POSITION        => $dto->getLastPosition(),
                self::COLUMN_HEALTH               => $dto->getHealth(),
                self::COLUMN_ARMOUR               => $dto->getArmour(),
                self::COLUMN_CASH                 => $dto->getCash(),
                self::COLUMN_BANK                 => $dto->getBank(),
                self::COLUMN_HUNGER               => $dto->getHunger(),
                self::COLUMN_THIRST               => $dto->getThirst(),
                self::COLUMN_DEAD                 => $dto->getDead(),
                self::COLUMN_HOUSE_ID             => $dto->getHouseId(),
                self::COLUMN_DRIVING_LICENSE_A    => $dto->getDrivingLicenseA(),
                self::COLUMN_DRIVING_LICENSE_B    => $dto->getDrivingLicenseB(),
                self::COLUMN_DRIVING_LICENSE_CE   => $dto->getDrivingLicenseCE(),
                self::COLUMN_MAX_GARAGE           => $dto->getMaxGarage(),
                self::COLUMN_LAST_LOGIN           => $dto->getLastLogin(),
                self::COLUMN_COP_RANK             => $dto->getCopRank(),
                self::COLUMN_MED_RANK             => $dto->getMedRank(),
                self::COLUMN_FIB_RANK             => $dto->getFibRank(),
                self::COLUMN_BWFI_RANK            => $dto->getBwfiRank(),
                self::COLUMN_TAXI_RANK            => $dto->getTaxiRank(),
                self::COLUMN_GROUP                => $dto->getGroup(),
                self::COLUMN_GROUP_RANK           => $dto->getGroupRank(),
                self::COLUMN_DOJ_RANK             => $dto->getDojRank(),
                self::COLUMN_LAWYER_RANK          => $dto->getLawyerRank(),
                self::COLUMN_DIMENSION            => $dto->getDimension(),
                self::COLUMN_SERVICE_NAME         => $dto->getServiceName(),
                self::COLUMN_SERVICE_NUMBER       => $dto->getServiceNumber(),
                self::COLUMN_BUSINESS_ID          => $dto->getBusinessId(),
                self::COLUMN_BUSINESS_RANK        => $dto->getBusinessRank(),
                self::COLUMN_BITCOIN              => $dto->getBitcoin(),
                self::COLUMN_BITCOIN_ILLEGAL      => $dto->getBitcoinIllegal(),
                self::COLUMN_BITCOIN_HASH_LEGAL   => $dto->getBitcoinHashLegal(),
                self::COLUMN_BITCOIN_HASH_ILLEGAL => $dto->getBitcoinHashIllegal(),
                self::COLUMN_SOCIAL_POINTS        => $dto->getSocialPoints(),
                self::COLUMN_IS_DUTY              => $dto->getIsDuty(),
                self::COLUMN_GUN_LICENSE          => $dto->getGunLicense(),
                self::COLUMN_CREATED_AT           => Carbon::now(),
                self::COLUMN_UPDATED_AT           => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\Character $dto
     */
    public function updateEntry(Dto\Character $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_NAME                 => $dto->getName(),
                self::COLUMN_HASH                 => $dto->getHash(),
                self::COLUMN_DOB                  => $dto->getDob(),
                self::COLUMN_PLAYING_TIME         => $dto->getPlayingTime(),
                self::COLUMN_FACE                 => $dto->getFace(),
                self::COLUMN_INVENTORY            => $dto->getInventory(),
                self::COLUMN_LAST_POSITION        => $dto->getLastPosition(),
                self::COLUMN_HEALTH               => $dto->getHealth(),
                self::COLUMN_ARMOUR               => $dto->getArmour(),
                self::COLUMN_CASH                 => $dto->getCash(),
                self::COLUMN_BANK                 => $dto->getBank(),
                self::COLUMN_HUNGER               => $dto->getHunger(),
                self::COLUMN_THIRST               => $dto->getThirst(),
                self::COLUMN_DEAD                 => $dto->getDead(),
                self::COLUMN_HOUSE_ID             => $dto->getHouseId(),
                self::COLUMN_DRIVING_LICENSE_A    => $dto->getDrivingLicenseA(),
                self::COLUMN_DRIVING_LICENSE_B    => $dto->getDrivingLicenseB(),
                self::COLUMN_DRIVING_LICENSE_CE   => $dto->getDrivingLicenseCE(),
                self::COLUMN_MAX_GARAGE           => $dto->getMaxGarage(),
                self::COLUMN_LAST_LOGIN           => $dto->getLastLogin(),
                self::COLUMN_COP_RANK             => $dto->getCopRank(),
                self::COLUMN_MED_RANK             => $dto->getMedRank(),
                self::COLUMN_FIB_RANK             => $dto->getFibRank(),
                self::COLUMN_BWFI_RANK            => $dto->getBwfiRank(),
                self::COLUMN_TAXI_RANK            => $dto->getTaxiRank(),
                self::COLUMN_GROUP                => $dto->getGroup(),
                self::COLUMN_GROUP_RANK           => $dto->getGroupRank(),
                self::COLUMN_DOJ_RANK             => $dto->getDojRank(),
                self::COLUMN_LAWYER_RANK          => $dto->getLawyerRank(),
                self::COLUMN_DIMENSION            => $dto->getDimension(),
                self::COLUMN_SERVICE_NAME         => $dto->getServiceName(),
                self::COLUMN_SERVICE_NUMBER       => $dto->getServiceNumber(),
                self::COLUMN_BUSINESS_ID          => $dto->getBusinessId(),
                self::COLUMN_BUSINESS_RANK        => $dto->getBusinessRank(),
                self::COLUMN_BITCOIN              => $dto->getBitcoin(),
                self::COLUMN_BITCOIN_ILLEGAL      => $dto->getBitcoinIllegal(),
                self::COLUMN_BITCOIN_HASH_LEGAL   => $dto->getBitcoinHashLegal(),
                self::COLUMN_BITCOIN_HASH_ILLEGAL => $dto->getBitcoinHashIllegal(),
                self::COLUMN_SOCIAL_POINTS        => $dto->getSocialPoints(),
                self::COLUMN_IS_DUTY              => $dto->getIsDuty(),
                self::COLUMN_GUN_LICENSE          => $dto->getGunLicense(),
                self::COLUMN_UPDATED_AT           => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\Character $dto
     */
    public function deleteEntry(Dto\Character $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param Dto\Groups $dto
     */
    public function deleteGroupRank(\App\Model\Dto\Groups $dto)
    {
        $this->where(self::COLUMN_GROUP, $dto->getId())->update(
            [
                self::COLUMN_GROUP      => 0,
                self::COLUMN_GROUP_RANK => 0,
            ]
        );
    }

    /**
     * @param Dto\Business $dto
     */
    public function deleteBusinessRank(\App\Model\Dto\Business $dto)
    {
        $this->where(self::COLUMN_BUSINESS_ID, $dto->getId())->update(
            [
                self::COLUMN_BUSINESS_RANK => 0,
                self::COLUMN_BUSINESS_ID   => 0,
            ]
        );
    }

    /**
     * @param Character $entry
     * @return Dto\Character
     */
    public static function entryAsDto(Character $entry): Dto\Character
    {
        return (new Dto\Character())
            ->setId($entry->{self::COLUMN_ID})
            ->setName($entry->{self::COLUMN_NAME})
            ->setHash($entry->{self::COLUMN_HASH})
            ->setDob($entry->{self::COLUMN_DOB})
            ->setPlayingTime($entry->{self::COLUMN_PLAYING_TIME})
            ->setFace($entry->{self::COLUMN_FACE})
            ->setInventory($entry->{self::COLUMN_INVENTORY})
            ->setLastPosition($entry->{self::COLUMN_LAST_POSITION})
            ->setHealth($entry->{self::COLUMN_HEALTH})
            ->setArmour($entry->{self::COLUMN_ARMOUR})
            ->setCash($entry->{self::COLUMN_CASH})
            ->setBank($entry->{self::COLUMN_BANK})
            ->setHunger($entry->{self::COLUMN_HUNGER})
            ->setThirst($entry->{self::COLUMN_THIRST})
            ->setDead($entry->{self::COLUMN_DEAD})
            ->setHouseId($entry->{self::COLUMN_HOUSE_ID})
            ->setDrivingLicenseA($entry->{self::COLUMN_DRIVING_LICENSE_A})
            ->setDrivingLicenseB($entry->{self::COLUMN_DRIVING_LICENSE_B})
            ->setDrivingLicenseCE($entry->{self::COLUMN_DRIVING_LICENSE_CE})
            ->setMaxGarage($entry->{self::COLUMN_MAX_GARAGE})
            ->setLastLogin($entry->{self::COLUMN_LAST_LOGIN})
            ->setCopRank($entry->{self::COLUMN_COP_RANK})
            ->setMedRank($entry->{self::COLUMN_MED_RANK})
            ->setFibRank($entry->{self::COLUMN_FIB_RANK})
            ->setBwfiRank($entry->{self::COLUMN_BWFI_RANK})
            ->setTaxiRank($entry->{self::COLUMN_TAXI_RANK})
            ->setGroup($entry->{self::COLUMN_GROUP})
            ->setGroupRank($entry->{self::COLUMN_GROUP_RANK})
            ->setDojRank($entry->{self::COLUMN_DOJ_RANK})
            ->setLawyerRank($entry->{self::COLUMN_LAWYER_RANK})
            ->setDimension($entry->{self::COLUMN_DIMENSION})
            ->setServiceName($entry->{self::COLUMN_SERVICE_NAME})
            ->setServiceNumber($entry->{self::COLUMN_SERVICE_NUMBER})
            ->setBusinessId($entry->{self::COLUMN_BUSINESS_ID})
            ->setBusinessRank($entry->{self::COLUMN_BUSINESS_RANK})
            ->setBitcoin($entry->{self::COLUMN_BITCOIN})
            ->setBitcoinIllegal($entry->{self::COLUMN_BITCOIN_ILLEGAL})
            ->setBitcoinHashLegal($entry->{self::COLUMN_BITCOIN_HASH_LEGAL})
            ->setBitcoinHashIllegal($entry->{self::COLUMN_BITCOIN_HASH_ILLEGAL})
            ->setSocialPoints($entry->{self::COLUMN_SOCIAL_POINTS})
            ->setIsDuty($entry->{self::COLUMN_IS_DUTY})
            ->setGunLicense($entry->{self::COLUMN_GUN_LICENSE})
            ->setCreatedAt($entry->{self::COLUMN_CREATED_AT})
            ->setUpdatedAt($entry->{self::COLUMN_UPDATED_AT});
    }
}
