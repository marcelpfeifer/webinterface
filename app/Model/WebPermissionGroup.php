<?php
/**
 * WebPermission
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 24.07.2020
 * Time: 17:55
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class WebPermissionGroup extends Model
{
    /**
     * @var string
     */
    public const COLUMN_ID = 'id';

    /**
     * @var string
     */
    public const COLUMN_NAME = 'name';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webPermissionGroup';

    /**
     * @param Dto\WebPermissionGroup $dto
     */
    public function insertEntry(Dto\WebPermissionGroup $dto)
    {
        $this->insert(
            [
                self::COLUMN_ID   => $dto->getId(),
                self::COLUMN_NAME => $dto->getName(),
                self::CREATED_AT  => Carbon::now(),
                self::UPDATED_AT  => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebPermissionGroup $dto
     */
    public function updateEntry(Dto\WebPermissionGroup $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_ID   => $dto->getId(),
                self::COLUMN_NAME => $dto->getName(),
                self::UPDATED_AT  => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebPermissionGroup $dto
     */
    public function deleteEntry(\App\Model\Dto\WebPermissionGroup $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebPermissionGroup $entry
     * @return Dto\WebPermissionGroup
     */
    public static function entryAsDto(WebPermissionGroup $entry): Dto\WebPermissionGroup
    {
        return (new Dto\WebPermissionGroup())
            ->setId($entry->{self::COLUMN_ID})
            ->setName($entry->{self::COLUMN_NAME});
    }
}
