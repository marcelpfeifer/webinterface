<?php
/**
 * Copyright https://github.com/gabrielecirulli/2048
 * Date: 26.07.2020
 * Time: 15:42
 */

?>
@extends('darkWeb.layout.app')
@section('url')
    {{  md5('https://eternityV.de/2048') }}
@endsection
@section('head')
    <link href="{{ asset('2048/style/main.css') }}" rel="stylesheet" type="text/css">
    <script src="{{ asset('2048/js/bind_polyfill.js') }}"></script>
    <script src="{{ asset('2048/js/classlist_polyfill.js') }}"></script>
    <script src="{{ asset('2048/js/animframe_polyfill.js') }}"></script>
    <script src="{{ asset('2048/js/keyboard_input_manager.js') }}"></script>
    <script src="{{ asset('2048/js/html_actuator.js') }}"></script>
    <script src="{{ asset('2048/js/grid.js') }}"></script>
    <script src="{{ asset('2048/js/tile.js') }}"></script>
    <script src="{{ asset('2048/js/local_storage_manager.js') }}"></script>
    <script src="{{ asset('2048/js/game_manager.js') }}"></script>
    <script src="{{ asset('2048/js/application.js') }}"></script>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="container">
                <div class="heading">
                    <h1 class="title">2048</h1>
                    <div class="scores-container">
                        <div class="score-container">0</div>
                        <div class="best-container">0</div>
                    </div>
                </div>

                <div class="above-game">
                    <p class="game-intro">Join the numbers and get to the <strong>2048 tile!</strong></p>
                    <a class="restart-button">New Game</a>
                </div>

                <div class="game-container">
                    <div class="game-message">
                        <p></p>
                        <div class="lower">
                            <a class="keep-playing-button">Keep going</a>
                            <a class="retry-button">Try again</a>
                        </div>
                    </div>

                    <div class="grid-container">
                        <div class="grid-row">
                            <div class="grid-cell"></div>
                            <div class="grid-cell"></div>
                            <div class="grid-cell"></div>
                            <div class="grid-cell"></div>
                        </div>
                        <div class="grid-row">
                            <div class="grid-cell"></div>
                            <div class="grid-cell"></div>
                            <div class="grid-cell"></div>
                            <div class="grid-cell"></div>
                        </div>
                        <div class="grid-row">
                            <div class="grid-cell"></div>
                            <div class="grid-cell"></div>
                            <div class="grid-cell"></div>
                            <div class="grid-cell"></div>
                        </div>
                        <div class="grid-row">
                            <div class="grid-cell"></div>
                            <div class="grid-cell"></div>
                            <div class="grid-cell"></div>
                            <div class="grid-cell"></div>
                        </div>
                    </div>

                    <div class="tile-container">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        Credit to https://github.com/gabrielecirulli/2048
    </div>
@endsection
