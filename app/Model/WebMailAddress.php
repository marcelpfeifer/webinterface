<?php
/**
 * WebMailAddress
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 12.08.2021
 * Time: 11:36
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class WebMailAddress extends Model
{
    /**
     * @var string
     */
    public const COLUMN_ID = 'id';

    /**
     * @var string
     */
    public const COLUMN_EMAIL = 'email';

    /**
     * @var string
     */
    public const COLUMN_PASSWORD = 'password';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webMailAddress';

    /**
     * @return HasMany
     */
    public function accounts(): HasMany
    {
        return $this->hasMany(
            WebMailAddressToAccount::class,
            WebMailAddressToAccount::COLUMN_WEB_MAIL_ADDRESS_ID,
            self::COLUMN_ID
        );
    }

    /**
     * @param string $email
     * @param string $password
     * @return Dto\WebMailAddress|null
     */
    public function getEntryByEmailAndPassword(string $email, string $password): ?Dto\WebMailAddress
    {
        $entry = $this
            ->where(self::COLUMN_EMAIL, $email)
            ->where(self::COLUMN_PASSWORD, $password)
            ->first();
        return $entry ? self::entryAsDto($entry) : null;
    }

    /**
     * @param string $email
     * @return Dto\WebMailAddress|null
     */
    public function getEntryByMail(string $email): ?Dto\WebMailAddress
    {
        $entry = $this
            ->where(self::COLUMN_EMAIL, $email)
            ->first();
        return $entry ? self::entryAsDto($entry) : null;
    }

    /**
     * @param int $accountId
     * @return \App\Model\Dto\WebMailAddress\WithWebMailAddressToAccount[]
     */
    public function getEntriesForAccount(int $accountId): array
    {
        $result = [];
        $entries = $this
            ->with('accounts')
            ->whereHas(
                'accounts',
                function ($query) use ($accountId) {
                    return $query->where(WebMailAddressToAccount::COLUMN_ACCOUNT_ID, '=', $accountId);
                }
            )
            ->get();

        foreach ($entries as $entry) {
            $webMailAddressToAccount = null;
            foreach ($entry->accounts as $account) {
                $dto = WebMailAddressToAccount::entryAsDto($account);
                if($dto->getAccountId() === $accountId){
                    $webMailAddressToAccount = $dto;
                }
            }
            $result[] = (new \App\Model\Dto\WebMailAddress\WithWebMailAddressToAccount())
                ->setWebMailAddress(self::entryAsDto($entry))
                ->setWebMailAddressToAccount($webMailAddressToAccount);
        }
        return $result;
    }

    /**
     * @param Dto\WebMailAddress $dto
     * @return int
     */
    public function insertEntry(Dto\WebMailAddress $dto): int
    {
        return $this->insertGetId(
            [
                self::COLUMN_EMAIL    => $dto->getEmail(),
                self::COLUMN_PASSWORD => $dto->getPassword(),
                self::CREATED_AT      => Carbon::now(),
                self::UPDATED_AT      => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebMailAddress $dto
     */
    public function updateEntry(Dto\WebMailAddress $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_EMAIL    => $dto->getEmail(),
                self::COLUMN_PASSWORD => $dto->getPassword(),
                self::UPDATED_AT      => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebMailAddress $dto
     */
    public function deleteEntry(\App\Model\Dto\WebMailAddress $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebMailAddress $entry
     * @return Dto\WebMailAddress
     */
    public static function entryAsDto(WebMailAddress $entry): Dto\WebMailAddress
    {
        return (new Dto\WebMailAddress())
            ->setId($entry->{self::COLUMN_ID})
            ->setEmail($entry->{self::COLUMN_EMAIL})
            ->setPassword($entry->{self::COLUMN_PASSWORD});
    }
}
