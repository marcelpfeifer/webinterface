<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 30.04.2020
 * Time: 19:13
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
    <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('darkWeb.layout.head')
<body class="darkWeb bg-dark">
<header class="bg-dark fixed-top border-bottom">
    <div class="row p-4 w-100 ">
        <div class="col-3 darkWebColor btn">
            Dark-Browser v0.1
        </div>
        <div class="col-6 bg-secondary rounded darkWebColor pt-2">
            <a class="text-decoration-none" href="{{ route('darkWeb.index', $hash) }}">
                <img  style="margin-top: -7px" src="{{ url('img/onion.png') }}" height="25px" width="25px">
            </a>
            <span class="darkWebColor">|</span>
            <i class="fas fa-lock"></i>
            <span style="letter-spacing: 1px">@yield('url')</span>
        </div>
        <div class="col-3">

        </div>
    </div>
</header>
<div class="container">
    <div class="content min-vh-100" style="margin-top: 100px">
        @include('layouts.messages')
        @yield('content')
    </div>
</div>
</body>
</html>

