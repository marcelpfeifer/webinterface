<?php
/**
 * Math
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 30.08.2021
 * Time: 16:04
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Common;


class Math
{
    /**
     * @param float $lastPrice
     * @param float $newPrice
     * @return float|int
     */
    public static function getPercentageChange(float $lastPrice, float $newPrice)
    {
        $percentageChange = 0;
        if ($newPrice > $lastPrice) {
            $percentageChange = ($newPrice - $lastPrice) / $lastPrice * 100;
        } elseif ($newPrice < $lastPrice) {
            $percentageChange = -(($lastPrice - $newPrice) / $lastPrice * 100);
        }
        return $percentageChange;
    }
}
