<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertPermissionData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND                                   => 'Zugriff aufs Backend',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER                            => 'Spieler bearbeiten',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_VEHICLE               => 'Spieler Fahrzeuge bearbeiten',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_BANS                  => 'Spieler Bans bearbeiten',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PRISON                => 'Spieler aus dem Gefängnis holen',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_DIMENSION             => 'Spieler Dimension zurücksetzen',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PLAYER_POSITION_RESET => 'Spieler Position zurücksetzen',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PLAYER_RESET          => 'Spieler zurücksetzen',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PLAYER_NAME           => 'Spieler Namen bearbeiten',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PLAYER_PASSWORD       => 'Spieler Passwort bearbeiten',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PLAYER_IMAGE          => 'Spieler Bild bearbeiten',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PLAYER_MONEY          => 'Spieler Geld bearbeiten',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_WHITELIST             => 'Spieler Whitelist bearbeiten',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_FACTION               => 'Spieler Fraktion bearbeiten',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PERMISSION            => 'Spieler Rechte bearbeiten',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_GROUP                 => 'Spieler Gruppe bearbeiten',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_BUSINESS              => 'Spieler Firma bearbeiten',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_CODE_GENERATOR                    => 'Zugriff auf den Codegenerator',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_GROUP                             => 'Zugriff auf die Gruppenverwaltung',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_BUSINESS                          => 'Zugriff auf die Firmenverwaltung',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_VEHICLE_SHOP                      => 'Zugriff auf den Fahrzeugshop',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_HAPPY_BUY_CATEGORY                => 'Zugriff auf die Happy Buy Kategorien',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_ITEM_LIST                         => 'Zugriff auf die Item Liste',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_SETTINGS                          => 'Zugriff auf die Einstellungen',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PERMISSIONS                       => 'Zugriff auf die Rechte',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_SUPPORT_FORM_WHITELIST            => 'Zugriff auf das Whitelist Formular',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_SUPPORT_FORM_WHITELIST_APPROVE    => 'Darf Whitelist Eintrag annehmen',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_SUPPORT_FORM_TECHNICAL_FAQ        => 'Zugriff auf das Technische FAQ Formular',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_SUPPORT_FORM_REFUND               => 'Zugriff auf das Erstattungs Formular',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_SUPPORT_FORM_INFRINGEMENT         => 'Zugriff auf das Regelverstoß Formular',
            \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_SUPPORT_FORM_BANS                 => 'Zugriff auf das Ban Formular',
            \App\Libraries\Permission\Enum\Permission::ACCESS_API                                       => 'Zugriff auf die API',
            \App\Libraries\Permission\Enum\Permission::ACCESS_API_TP                                    => 'Zugriff auf die API - Teleportieren',
            \App\Libraries\Permission\Enum\Permission::ACCESS_API_INVISIBLE                             => 'Zugriff auf die API - Unsichtbar',
            \App\Libraries\Permission\Enum\Permission::ACCESS_API_FLY                                   => 'Zugriff auf die API - Fliegen',
            \App\Libraries\Permission\Enum\Permission::ACCESS_API_REVIVE                                => 'Zugriff auf die API - Wiederbeleben',
            \App\Libraries\Permission\Enum\Permission::ACCESS_API_GIVE_ITEM                             => 'Zugriff auf die API - Item geben',
            \App\Libraries\Permission\Enum\Permission::ACCESS_API_GIVE_MONEY                            => 'Zugriff auf die API - Geld geben',
            \App\Libraries\Permission\Enum\Permission::ACCESS_API_SET_MONEY                             => 'Zugriff auf die API - Geld setzen',
            \App\Libraries\Permission\Enum\Permission::ACCESS_API_REMOVE_MONEY                          => 'Zugriff auf die API - Geld entfernen',
            \App\Libraries\Permission\Enum\Permission::ACCESS_API_DIMENSION                             => 'Zugriff auf die API - Dimension',
            \App\Libraries\Permission\Enum\Permission::ACCESS_API_GOD_MODE                              => 'Zugriff auf die API - Godmode',
            \App\Libraries\Permission\Enum\Permission::ACCESS_API_FACTION                               => 'Zugriff auf die API - Fraktion',
            \App\Libraries\Permission\Enum\Permission::ACCESS_API_MODEL                                 => 'Zugriff auf die API - Model',
            \App\Libraries\Permission\Enum\Permission::ACCESS_API_KICK                                  => 'Zugriff auf die API - Kick',
            \App\Libraries\Permission\Enum\Permission::ACCESS_API_SPECTATE                              => 'Zugriff auf die API - Beobachten',
            \App\Libraries\Permission\Enum\Permission::ACCESS_API_FREEZE                                => 'Zugriff auf die API - Einfrieren',
            \App\Libraries\Permission\Enum\Permission::ACCESS_API_KILL                                  => 'Zugriff auf die API - Killen',
            \App\Libraries\Permission\Enum\Permission::ACCESS_API_PRISON                                => 'Zugriff auf die API - Gefängnis',
            \App\Libraries\Permission\Enum\Permission::ACCESS_API_NAMES                                 => 'Zugriff auf die API - Namen anzeigen',
            \App\Libraries\Permission\Enum\Permission::ACCESS_API_REPAIR_ID                             => 'Zugriff auf die API - Auto reparieren per ID',
            \App\Libraries\Permission\Enum\Permission::ACCESS_API_REPAIR_DRIVER                         => 'Zugriff auf die API - Auto reparieren per Fahrer',
            \App\Libraries\Permission\Enum\Permission::ACCESS_API_PARK                                  => 'Zugriff auf die API - Auto einparken',
            \App\Libraries\Permission\Enum\Permission::ACCESS_API_MESSAGE                               => 'Zugriff auf die API - Server Nachricht',

        ];

        $groupDto = (new \App\Model\Dto\WebPermissionGroup())
            ->setId(1)
            ->setName('Admin');

        (new \App\Model\WebPermissionGroup())->insertEntry($groupDto);

        foreach ($data as $id => $name) {
            $dto = (new \App\Model\Dto\WebPermission())
                ->setId($id)
                ->setName($name);
            (new \App\Model\WebPermission())->insertEntry($dto);

            $dto = (new \App\Model\Dto\WebPermissionToWebPermission())
                ->setWebPermissionId($id)
                ->setWebPermissionGroupId(1);
            (new \App\Model\WebPermissionToWebPermission())->insertEntry($dto);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
