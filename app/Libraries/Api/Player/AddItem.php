<?php
/**
 * AddItem
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 11.04.2020
 * Time: 14:02
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Api\Player;

use App\Libraries\Api\AClient;
use App\Model\WebConfig;
use Illuminate\Support\Facades\Log;

class AddItem extends AClient
{
    /**
     * @var string
     */
    const URL = 'player/addItem';

    /**
     * @param int $pid
     * @param string $item
     * @param int $amount
     * @return bool
     */
    public static function call(int $pid, string $item, int $amount): bool
    {
        $response = self::sendRequest('POST', WebConfig::getEntryByKey('api_url')->getValue() . self::URL, [
            'query' => [
                'pid'    => $pid,
                'item'   => $item,
                'amount' => $amount,
            ]
        ]);

        return $response && $response->getStatusCode() == 200;
    }
}
