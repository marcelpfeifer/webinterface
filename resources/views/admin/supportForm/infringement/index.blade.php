@php
    /** @var $entry \App\Model\Dto\WebAdminInfringement */
@endphp
@extends('admin.layout.app')
@section('title')
    <i class="fab fa-wpforms"></i>
    Regelverstoß
    <a class="btn btn-dark btn-sm float-right"
       href="{{ route('admin.supportForm.infringement.create') }}"
       role="button"
    >
        Eintrag erstellen
    </a>
@endsection
@section('content')
    <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th scope="col">Fall</th>
                <th scope="col">Datum</th>
                <th scope="col">Name IC</th>
                <th scope="col">Fall Grund</th>
                <th scope="col">Gemeldet von ...</th>
                <th scope="col">Bearbeitet von...</th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($entries as $entry)
                <tr>
                    <th scope="row">{{ $entry['dto']->getId() }}</th>
                    <td>{{ $entry['dto']->getCreatedAt()->format('Y-m-d') }}</td>
                    <td>{{ $entry['dto']->getUserName() }}</td>
                    <td>{{ $entry['dto']->getReason() }}</td>
                    <td>{{ $entry['dto']->getReportedBy() }}</td>
                    <td>{{ $entry['user']->getUsername() }}</td>
                    <td>
                        <a class="btn btn-primary"
                           href="{{ route('admin.supportForm.infringement.show', $entry['dto']->getId()) }}">Anzeigen</a>
                    </td>
                    <td>
                        <form action="{{ route('admin.supportForm.infringement.destroy', $entry['dto']->getId()) }}"
                              method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="submit" class="btn btn-danger" value="Löschen"/>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
