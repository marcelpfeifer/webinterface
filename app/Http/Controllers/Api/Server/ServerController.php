<?php
/**
 * ServerController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.10.2021
 * Time: 13:48
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Api\Server;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Http\Request\Api\Server\Server\Save;
use App\Http\Request\Enum\Api\Server\Server\Action;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ServerController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        return view('api.server.server.index', []);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function save(Request $request): RedirectResponse
    {
        $validator = (new Save($request));
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            return redirect()->route('api.server.server.index')
                ->with('messages', $validator->getErrorsAsMessageArray($validatedData))
                ->withInput()
                ->withErrors($validatedData->errors());
        }

        /**
         * @var $requestDto \App\Http\Request\Dto\Api\Server\Server\Save
         */
        $requestDto = $validator->toDto();

        $messages = [];
        switch ($requestDto->getAction()) {
            case Action::START:
                if (\App\Libraries\Api\Server\Start::call($requestDto->getType())) {
                    $messages[] = (new Message())
                        ->setMessage('Erfolgreich ausgeführt')
                        ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
                } else {
                    $messages[] = (new Message())
                        ->setMessage('Anfrage fehlgeschlagen')
                        ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
                }
                break;
            case Action::STOP:
                if (\App\Libraries\Api\Server\Stop::call($requestDto->getType())) {
                    $messages[] = (new Message())
                        ->setMessage('Erfolgreich ausgeführt')
                        ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
                } else {
                    $messages[] = (new Message())
                        ->setMessage('Anfrage fehlgeschlagen')
                        ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
                }
                break;
            case Action::STATUS:
                if (\App\Libraries\Api\Server\Status::call($requestDto->getType())) {
                    $messages[] = (new Message())
                        ->setMessage('Server läuft')
                        ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
                } else {
                    $messages[] = (new Message())
                        ->setMessage('Server läuft aktuell nicht')
                        ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
                }
                break;
        }

        return redirect()->route('api.server.server.index')->with('messages', $messages);
    }
}
