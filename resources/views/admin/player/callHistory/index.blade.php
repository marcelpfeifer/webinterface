<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 24.11.2020
 * Time: 15:42
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
/**
 * @var $history \App\Model\Dto\CallHistory[]
 */
?>
@extends('admin.layout.app')
@section('title')
    Anruferliste
    <a class="btn btn-dark btn-sm float-right"
       href="{{ route('admin.player.edit', $characterId) }}"
       role="button"
    >
        Zurück
    </a>
@endsection
@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Von</th>
            <th scope="col">An</th>
            <th scope="col">Datum</th>
            <th scope="col">Unterdrückt</th>
        </tr>
        </thead>
        <tbody>
        @foreach($history as $callHistory)
            <tr>
                <th scope="row">{{ $callHistory->getId() }}</th>
                <td>{{ $callHistory->getNumberFrom() }}</td>
                <td>{{ $callHistory->getNumberTo() }}</td>
                <td>{{ $callHistory->getDate() }}</td>
                <td>
                    @if($callHistory->isSuppressed())
                        <i class="fas fa-check"></i>
                    @else
                        <i class="fas fa-times"></i>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
