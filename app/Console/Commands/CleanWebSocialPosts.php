<?php
/**
 * CleanWebSocialPosts
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 17.05.2020
 * Time: 14:41
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CleanWebSocialPosts extends Command
{
    /**
     * @var string
     */
    protected $signature = 'webSocialPosts:clean';

    /**
     * @var string
     */
    protected $description = 'Löscht alte eine in den WebSocialPosts';

    public function handle()
    {
        DB::table('webSocialPost')
            ->where('created_at', '<=', Carbon::now()->subDays(14))
            ->delete();

        $this->info('Erfolgreich die alten Posts gelöscht.');
    }
}
