<nav class="bg-danger shadow-sm p-2">
    <span class="btn text-white">
        <i class="fas fa-cloud"></i>
        API
    </span>
    <div class="float-right">
        @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND)
        <a class="btn btn-dark" href="{{ route('admin.index') }}">
            <div>
                <i class="far fa-window-maximize"></i> Zum Admin
            </div>
        </a>
        @endpermission
        <a class="btn btn-dark" href="{{ route('auth.account.index') }}">
            <div>
                <i class="far fa-window-maximize"></i> Zum Frontend
            </div>
        </a>
    </div>
</nav>
