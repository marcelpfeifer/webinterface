<?php
/**
 * Status
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 18.08.2021
 * Time: 14:32
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Api\Forum;

use App\Libraries\Api\AClient;

class Status extends AClient
{
    /**
     * @var string
     */
    const URL = 'https://eternityv.de/';

    /**
     * @param bool $logging
     * @return bool
     */
    public static function call(bool $logging = false): bool
    {
        $response = self::sendRequest(
            'GET',
            self::URL,
            [],
            $logging
        );

        if ($response && $response->getStatusCode() == 200) {
            return true;
        }
        return false;
    }

}
