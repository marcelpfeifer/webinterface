<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.01.2021
 * Time: 19:07
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
<div class="row bg-primary p-1">
    @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_VEHICLE)
    <div class="ml-1">
        <a href="{{ route('admin.player.vehicle.index', $character->getId()) }}" class="btn btn-dark">
            Fahrzeugverwaltung
        </a>
    </div>
    @endpermission
    @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_BANS)
    <div class="ml-1">
        <a href="{{ route('admin.player.bans.index', $character->getId()) }}" class="btn btn-dark">
            Strikes
        </a>
    </div>
    @endpermission
    @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_CASH_FLOW)
    <div class="ml-1">
        <a href="{{ route('admin.player.cashFlow.index', $character->getId()) }}" class="btn btn-dark">
            Cashflow
        </a>
    </div>
    @endpermission
    @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_CALL_HISTORY)
    <div class="ml-1">
        <a href="{{ route('admin.player.callHistory.index', $character->getId()) }}" class="btn btn-dark">
            Anruferliste
        </a>
    </div>
    @endpermission
</div>
