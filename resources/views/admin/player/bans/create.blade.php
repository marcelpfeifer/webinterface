<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 06.01.2021
 * Time: 14:24
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('admin.layout.app')
@section('title')
    Eintrag erstellen
    <a class="btn btn-dark btn-sm float-right"
       href="{{ route('admin.player.bans.index', $accountId) }}"
       role="button"
    >
        Zurück
    </a>
@endsection
@section('content')
    <form action="{{ route('admin.player.bans.save', $accountId) }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="points">Punkte</label>
            <input type="text" class="form-control" id="points" name="points" value="{{ $dto->getPoints() }}" required>
        </div>
        <div class="form-group">
            <label for="reason">Grund</label>
            <textarea rows="3" class="form-control" id="reason" name="reason"
                      required>{{ $dto->getDescription() }}</textarea>
        </div>
        <div class="form-group">
            <label for="userReason">Ban-Nachricht für den Spieler</label>
            <textarea rows="3" class="form-control" id="userReason" name="userReason" required></textarea>
        </div>
        <br/>
        <button type="submit" class="btn btn-dark">Erstellen</button>
    </form>
@endsection

