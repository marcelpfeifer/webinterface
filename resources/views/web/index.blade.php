<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 30.04.2020
 * Time: 19:12
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('web.layout.app')
@section('url')
    https://eternityV.de/
@endsection
@section('content')
    <div class="row border border-dark bg-dark m-2">
        <div class="col-12">
            <div class="row bg-primary shadow">
                <h2 class="col text-center m-1">
                    Herzlich Willkommen auf der Startseite vom Los Santos Web!
                </h2>
            </div>
            <hr/>
            <div class="row justify-content-center">
                <div class="col-6">
                    <div class="row">
                        <div class="col-1 d-flex align-items-center justify-content-center">
                            <img class="img-fluid" src="{{ url('img/web/social.png') }}">
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <a class="text-white" href="{{ route('web.social.index', $hash) }}">Social</a>
                                </div>
                                <div class="col-12 font-italic text-secondary">
                                    Finde heraus, was gerade in der Welt los ist. Schreibe Tweets und tausche dich mit
                                    Freunden aus...
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="row justify-content-center">
                <div class="col-6">
                    <div class="row">
                        <div class="col-1 d-flex align-items-center justify-content-center text-primary">
                            <i class="fas fa-shopping-cart fa-3x"></i>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <a class="text-white"
                                       href="{{ route('web.marketplace.index', $hash) }}">Marktplatz</a>
                                </div>
                                <div class="col-12 font-italic text-secondary">
                                    Kaufen und verkaufen Sie Elektronikartikel, Autos, Kleidung, Mode, Sammlerstücke,
                                    Sportartikel, Digitalkameras, Babyartikel, Gutscheine und vieles mehr bei ...
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>

            <div class="row justify-content-center">
                <div class="col-6">
                    <div class="row">
                        <div class="col-1 d-flex align-items-center justify-content-center text-danger">
                            <i class="fas fa-coins fa-3x"></i>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <a class="text-white" href="{{ route('web.stock.index', $hash) }}">Börse</a>
                                </div>
                                <div class="col-12 font-italic text-secondary">
                                    Börsenkurse, News und Know-how direkt von der Quelle: Aktien, ETFs, Fonds,
                                    Rohstoffe, Anleihen, Zertifikate.
                                    Für Watchlist und Portfolio.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>


            <div class="row justify-content-center">
                <div class="col-6">
                    <div class="row">
                        <div class="col-1 d-flex align-items-center justify-content-center">
                            <i class="fas text-warning fa-mail-bulk fa-3x"></i>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <a class="text-white" href="{{ route('web.mailbox.index', $hash) }}">Mail</a>
                                </div>
                                <div class="col-12 font-italic text-secondary">
                                    Sichere E-Mail-Adresse aus Los Santos ✓Verschlüsselte Datenübertragung ✓Großes
                                    Postfach ✓nach ls. Datenschutz...
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="row justify-content-center">
                <div class="col-6">
                    <div class="row">
                        <div class="col-1 d-flex align-items-center justify-content-center">
                            <i class="fas text-warning fa-book-open fa-3x"></i>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <a class="text-white" href="{{ route('web.yellowPage.index', $hash) }}">Gelbe
                                        Seiten</a>
                                </div>
                                <div class="col-12 font-italic text-secondary">
                                    Gelbe Seiten - Das Branchenbuch für Los Santos gibt Auskunft zu Telefonnummern,
                                    Adressen, Faxnummern und Firmen-Infos in den Einträgen der ...
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="row justify-content-center">
                <div class="col-6">
                    <div class="row">
                        <div class="col-1 d-flex align-items-center justify-content-center">
                            <i class="fas fa-newspaper fa-3x"></i>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <a class="text-white" href="{{ route('web.news.index', $hash) }}">News</a>
                                </div>
                                <div class="col-12 font-italic text-secondary">
                                    Die News - die erste Adresse für Nachrichten und Information: An 365 Tagen im Jahr,
                                    rund um die Uhr aktualisiert - die wichtigsten News des Tages.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>

            <div class="row justify-content-center">
                <div class="col-6">
                    <div class="row">
                        <div class="col-1 d-flex align-items-center justify-content-center">
                            <i class="fas text-danger fa-crosshairs fa-3x"></i>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <a class="text-white" href="{{ route('web.hunt.index', $hash) }}">
                                        Jagd Scoreboard
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>

            <div class="row justify-content-center">
                <div class="col-6">
                    <div class="row">
                        <div class="col-1 d-flex align-items-center justify-content-center">
                            <i class="fas text-primary fa-dungeon fa-3x"></i>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <a class="text-white" href="{{ route('web.police.index', $hash) }}">
                                        Polizei Akten Sytsem
                                    </a>
                                </div>
                                <div class="col-12 font-italic text-secondary">
                                    Hier können geteilte Profile und Vorfälle von der Polizei angeschaut werden...
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="row justify-content-center">
                <div class="col-6">
                    <div class="row">
                        <div class="col-1 d-flex align-items-center justify-content-center">
                            <i class="fas text-danger fa-clinic-medical fa-3x"></i>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <a class="text-white" href="{{ route('web.medic.index', $hash) }}">
                                        Medical Akten Sytsem
                                    </a>
                                </div>
                                <div class="col-12 font-italic text-secondary">
                                    Hier können geteilte Profile und Vorfälle vom Krankenhaus angeschaut werden...
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
        </div>
@endsection
