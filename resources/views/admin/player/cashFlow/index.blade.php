<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 19.09.2020
 * Time: 10:27
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

/**
 * @var $dto \App\Model\Dto\Cashflow\TotalAmount
 */
?>
@extends('admin.layout.app')
@section('title')
    Cashflow
    <a class="btn btn-dark btn-sm float-right"
       href="{{ route('admin.player.edit', $characterId) }}"
       role="button"
    >
        Zurück
    </a>
@endsection
@section('content')
    <p>Totalles Geld: {{ $dto->getTotalAmount() }}</p>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Menge</th>
            <th scope="col">Reason</th>
            <th scope="col">Source</th>
            <th scope="col">Typ</th>
            <th scope="col">Datum</th>
        </tr>
        </thead>
        <tbody>
        @foreach($dto->getCashFlows() as $cashFlow)
            <tr>
                <th scope="row">{{ $cashFlow->getId() }}</th>
                <td>{{ $cashFlow->getAmount() }}</td>
                <td>{{ $cashFlow->getReason() }}</td>
                <td>{{ $cashFlow->getSource() }}</td>
                <td>{{ $cashFlow->getType() }}</td>
                <td>{{ $cashFlow->getCreatedAt()->format('Y-m-d H:i:s') }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
