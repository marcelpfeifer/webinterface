<?php
/**
 * Directory
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 24.09.2021
 * Time: 18:58
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Common;


class Directory
{

    /**
     * @param string $path
     * @param int $mode
     * @param bool $recursive
     */
    public static function create(string $path, int $mode = 0755, bool $recursive = true)
    {
        if (!file_exists($path)) {
            mkdir($path, $mode, $recursive);
        }
    }
}
