<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 05.01.2021
 * Time: 03:05
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-clipboard"></i>
    Kill Log-Einträge
@endsection
@section('content')
    <table class="table table-responsive">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Opfer</th>
            <th scope="col">Mörder</th>
            <th scope="col">Opfer Position</th>
            <th scope="col">Mörder Position</th>
            <th scope="col">War in Auto?</th>
            <th scope="col">Waffenhash</th>
            <th scope="col">Datum</th>
        </tr>
        </thead>
        <tbody>
        @foreach($entries as $entry)
            <tr>
                <th scope="row">{{ $entry->{\App\Model\DeathLog::COLUMN_ID} }}</th>
                <td>{{ $entry->{\App\Model\DeathLog::COLUMN_VICTIM} }}</td>
                <td>{{ $entry->{\App\Model\DeathLog::COLUMN_KILLER} }}</td>
                <td>{{ $entry->{\App\Model\DeathLog::COLUMN_VICTIM_POSITION} }}</td>
                <td>{{ $entry->{\App\Model\DeathLog::COLUMN_KILLER_POSITION} }}</td>
                <td>{{ $entry->{\App\Model\DeathLog::COLUMN_IS_KILLER_IN_VEHICLE} }}</td>
                <td>{{ $entry->{\App\Model\DeathLog::COLUMN_WEAPON_HASH} }}</td>
                <td>{{ $entry->{\App\Model\DeathLog::COLUMN_CREATED_AT}->format('Y-m-d H:i:s') }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="col-12">
            {{ $entries->links() }}
        </div>
    </div>
@endsection
