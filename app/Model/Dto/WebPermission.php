<?php
/**
 * WebMarketplaceCategory
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 02.05.2020
 * Time: 17:56
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

/**
 * Class WebPermission
 * @package App\Model\Dto
 */
class WebPermission
{

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebPermission
     */
    public function setId(?int $id): WebPermission
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return WebPermission
     */
    public function setName(string $name): WebPermission
    {
        $this->name = $name;
        return $this;
    }
}
