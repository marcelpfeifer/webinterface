<?php
/**
 * WebAdminBanPoints
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 05.09.2020
 * Time: 13:55
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;


use App\Libraries\Logging\ILogging;
use App\Libraries\Logging\Traits\Variables;
use Carbon\Carbon;

class WebAdminBanPoints implements ILogging
{

    use Variables;

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var int
     */
    private $points = 0;

    /**
     * @var int
     */
    private $length = 0;

    /**
     * @var int
     */
    private $additional = 0;

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @var null|Carbon
     */
    private $updatedAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebAdminBanPoints
     */
    public function setId(?int $id): WebAdminBanPoints
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return WebAdminBanPoints
     */
    public function setName(string $name): WebAdminBanPoints
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getPoints(): int
    {
        return $this->points;
    }

    /**
     * @param int $points
     * @return WebAdminBanPoints
     */
    public function setPoints(int $points): WebAdminBanPoints
    {
        $this->points = $points;
        return $this;
    }

    /**
     * @return int
     */
    public function getLength(): int
    {
        return $this->length;
    }

    /**
     * @param int $length
     * @return WebAdminBanPoints
     */
    public function setLength(int $length): WebAdminBanPoints
    {
        $this->length = $length;
        return $this;
    }

    /**
     * @return int
     */
    public function getAdditional(): int
    {
        return $this->additional;
    }

    /**
     * @param int $additional
     * @return WebAdminBanPoints
     */
    public function setAdditional(int $additional): WebAdminBanPoints
    {
        $this->additional = $additional;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return WebAdminBanPoints
     */
    public function setCreatedAt(?Carbon $createdAt): WebAdminBanPoints
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon|null $updatedAt
     * @return WebAdminBanPoints
     */
    public function setUpdatedAt(?Carbon $updatedAt): WebAdminBanPoints
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
