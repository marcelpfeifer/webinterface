<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 11.08.2021
 * Time: 12:21
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
**Reporter:** {{ $reporter->getName() }} <br>  <br/>

**Beschreibung:** {{ $bugReport->getDescription() }} <br>  <br/>

**Beweis:** {{ $bugReport->getEvidence() }} <br>  <br/>

**Position:** {{ $bugReport->getPosition() }} <br>  <br/>

**Supporter:** {{ $supporter->getUsername() }} <br>  <br/>

