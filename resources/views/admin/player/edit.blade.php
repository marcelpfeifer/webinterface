<?php
/**
 * player.blade.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 22.08.2019
 * Time: 21:49
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

/**
 * @var $character \App\Model\Dto\Character
 * @var $account \App\Model\Account
 */
?>
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-user"></i>
    Spieler
    <a class="btn btn-dark btn-sm float-right"
       href="{{ route('admin.player.index') }}"
       role="button"
    >
        Zurück
    </a>
@endsection
@section('content')
    @if(!($account))
        Benutzer wurde nicht gefunden
    @else
        <!-- Menü -->
        @include('admin.player.edit.includes.menu')
        <br/>
        <h2> {{ $character->getName() }}</h2>
        <p> Erstellt am {{ $character->getCreatedAt()->format('Y-m-d H:i:s') }}</p>
        <br/>
        <!-- Aktionen -->
        @include('admin.player.edit.includes.actions')
        <br/>
        <form method="post" action="{{ route('admin.player.save', $character->getId()) }}" enctype="multipart/form-data">
            @csrf
            <!-- Allgemein -->
            @include('admin.player.edit.includes.common')

            <!-- Social Punkte -->
            @include('admin.player.edit.includes.socialPoints')

            <!-- Geld -->
            @include('admin.player.edit.includes.money')

            <!-- Whitelist -->
            @include('admin.player.edit.includes.whitelist')

            <!-- Fraktionen -->
            @include('admin.player.edit.includes.faction')

            <!-- Lizenzen -->
            @include('admin.player.edit.includes.license')

            <!-- Rechte -->
            @include('admin.player.edit.includes.permission')

            <!-- Gruppe -->
            @include('admin.player.edit.includes.group')

            <!-- Firma -->
            @include('admin.player.edit.includes.business')

            <div class="form-row">
                <button type="submit" class="btn btn-dark">Aktualisieren</button>
            </div>
        </form>
    @endif
@endsection
