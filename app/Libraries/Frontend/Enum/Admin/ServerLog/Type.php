<?php
/**
 * Type
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 24.09.2021
 * Time: 17:30
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Enum\Admin\ServerLog;


use App\Libraries\Enum\AEnum;

class Type extends AEnum
{

    /**
     * @var string
     */
    const ERROR = 'ERROR';

    /**
     * @var string
     */
    const WARNING = 'WARNING';
}
