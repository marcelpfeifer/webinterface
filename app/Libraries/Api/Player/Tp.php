<?php
/**
 * Tp
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 05.04.2020
 * Time: 12:13
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Api\Player;


use App\Libraries\Api\AClient;
use App\Model\WebConfig;
use Illuminate\Support\Facades\Log;

class Tp extends AClient
{
    /**
     * @var string
     */
    const URL = 'player/tp';

    /**
     * @param int $playerFrom
     * @param int $playerTo
     * @return bool
     */
    public static function teleportPlayer(int $playerFrom, int $playerTo): bool
    {
        $response = self::sendRequest('POST', WebConfig::getEntryByKey('api_url')->getValue() . self::URL, [
            'query' => [
                'playerFrom' => $playerFrom,
                'playerTo'   => $playerTo,
            ]
        ]);
        return $response && $response->getStatusCode() == 200;
    }
}
