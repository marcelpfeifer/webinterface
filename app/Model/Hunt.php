<?php
/**
 * Hunt
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 16.09.2021
 * Time: 16:37
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;


use App\Model\Dto\Hunt\Leaderboard;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Hunt extends Model
{
    /**
     * @var string
     */
    public const COLUMN_ID = 'id';

    /**
     * @var string
     */
    public const COLUMN_GUID = 'guid';

    /**
     * @var string
     */
    public const COLUMN_ANIMAL = 'animal';

    /**
     * @var string
     */
    public const COLUMN_WEIGHT = 'weight';

    /**
     * @var string
     */
    public const COLUMN_DISTANCE = 'distance';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'hunt';

    /**
     * @return BelongsTo
     */
    public function character(): BelongsTo
    {
        return $this->belongsTo(Character::class, self::COLUMN_GUID, Character::COLUMN_ID);
    }

    /**
     * @param string $orderBy
     * @return array
     */
    public function leaderboard(string $orderBy): array
    {
        $result = [];
        $entries = $this
            ->selectRaw(
                self::COLUMN_GUID . ', COUNT(' . self::COLUMN_ANIMAL . ') as animalCount, SUM(' . self::COLUMN_WEIGHT . ') as totalWeight, MAX(' . self::COLUMN_DISTANCE . ') as distance'
            )
            ->groupBy(self::COLUMN_GUID)
            ->orderBy($orderBy, 'DESC')
            ->get();
        foreach ($entries as $entry) {
            $character = $entry->character;
            $name = '';
            if ($character) {
                $name = Character::entryAsDto($character)->getName();
            }

            $result[] = (new Leaderboard())
                ->setName($name)
                ->setAnimalCount($entry->animalCount)
                ->setDistance($entry->distance)
                ->setTotalWeight($entry->totalWeight);
        }
        return $result;
    }
}
