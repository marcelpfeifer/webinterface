<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 30.04.2020
 * Time: 19:10
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\View\View;

class IndexController
{

    /**
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\View\View
     */
    public function index(Request $request, string $hash): View
    {
        return view('web.index', [
            'hash' => $hash,
        ]);
    }
}
