<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 29.04.2020
 * Time: 17:25
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-car"></i>
    Eintrag bearbeiten
@endsection
@section('content')
    <form action="{{ route('admin.vehicleShop.update', $dto->getId()) }}" method="POST">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="model">Model</label>
            <select class="form-control" name="model" id="model">
                @foreach(array_keys($vehicles) as $vehicle)
                    <option value="{{ $vehicle}}" {{ $vehicle == $dto->getModel() ? 'selected' : '' }}> {{ $vehicle }}</option>
                @endforeach
            </select>
        </div>
        @if(isset($vehicles[$dto->getModel()]))
        <div class="form-group">
            <label for="price">Preis</label>
            <input readonly type="number" class="form-control" id="price" name="price" value="{{ $vehicles[$dto->getModel()] }}">
        </div>
        @endif
        <div class="form-group">
            <label for="stock">Lager</label>
            <input type="number" class="form-control" id="stock" name="stock" value="{{ $dto->getStock() }}" required>
        </div>
        <div class="form-group">
            <label for="faction">Fraktion</label>
            <select class="form-control" id="faction" name="faction" required>
                <option value="civ" {{ ($dto->getFaction() == 'civ') ? 'selected' : '' }}>civ</option>
                <option value="cop" {{ ($dto->getFaction() == 'cop') ? 'selected' : '' }}>cop</option>
                <option value="med" {{ ($dto->getFaction() == 'med') ? 'selected' : '' }}>med</option>
                <option value="fib" {{ ($dto->getFaction() == 'fib') ? 'selected' : '' }}>fib</option>
                <option value="doj" {{ ($dto->getFaction() == 'doj') ? 'selected' : '' }}>doj</option>
                <option value="lawyer" {{ ($dto->getFaction() == 'lawyer') ? 'selected' : '' }}>lawyer</option>
                <option value="taxi" {{ ($dto->getFaction() == 'taxi') ? 'selected' : '' }}>taxi</option>
            </select>
        </div>
        <div class="form-group">
            <label for="posX">Position X</label>
            <input type="text" class="form-control" id="posX" name="posX" value="{{ $dto->getPosX() }}" required>
        </div>
        <div class="form-group">
            <label for="posY">Position Y</label>
            <input type="text" class="form-control" id="posY" name="posY" value="{{ $dto->getPosY() }}" required>
        </div>
        <div class="form-group">
            <label for="posZ">Position Z</label>
            <input type="text" class="form-control" id="posZ" name="posZ" value="{{ $dto->getPosZ() }}" required>
        </div>
        <div class="form-group">
            <label for="rotX">Rotation X</label>
            <input type="text" class="form-control" id="rotX" name="rotX" value="{{ $dto->getRotX() }}" required>
        </div>
        <div class="form-group">
            <label for="rotY">Rotation Y</label>
            <input type="text" class="form-control" id="rotY" name="rotY" value="{{ $dto->getRotY() }}" required>
        </div>
        <div class="form-group">
            <label for="rotZ">Rotation Z</label>
            <input type="text" class="form-control" id="rotZ" name="rotZ" value="{{ $dto->getRotZ() }}" required>
        </div>
        <br/>
        <button type="submit" class="btn btn-dark">Aktualisieren</button>
    </form>
@endsection
