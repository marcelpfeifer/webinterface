<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebStockChangeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'webStockChange',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('webStockId');
                $table->foreign('webStockId')->references('id')->on('webStock')->onDelete('cascade');
                $table->float('price');
                $table->float('percentage');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webStockChange');
    }
}
