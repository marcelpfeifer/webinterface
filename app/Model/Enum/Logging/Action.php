<?php
/**
 * Action
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 10.04.2020
 * Time: 09:44
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Enum\Logging;


class Action
{
    /**
     * @var string
     */
    const INSERT = 'INSERT';

    /**
     * @var string
     */
    const UPDATE = 'UPDATE';

    /**
     * @var string
     */
    const DELETE = 'DELETE';
}
