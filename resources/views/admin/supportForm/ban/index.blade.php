@php
    /** @var $entry \App\Model\Dto\WebAdminBan */
@endphp
@extends('admin.layout.app')
@section('title')
    <i class="fab fa-wpforms"></i>
    Bans
    <a class="btn btn-dark btn-sm float-right"
       href="{{ route('admin.supportForm.ban.create') }}"
       role="button"
    >
        Eintrag erstellen
    </a>
@endsection
@section('content')
    <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th scope="col">Nummer</th>
                <th scope="col">Name IC</th>
                <th scope="col">IP Adresse</th>
                <th scope="col">SocialClub Name</th>
                <th scope="col">Fallgrund</th>
                <th scope="col">Banndauer</th>
                <th scope="col">Datum</th>
                <th scope="col">Ausgesprochen von...</th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($entries as  $entry)
                <tr>
                    <th scope="row">{{ $entry['dto']->getId() }}</th>
                    <td>{{ $entry['dto']->getUserName() }}</td>
                    <td>{{ $entry['dto']->getIpAddress() }}</td>
                    <td>{{ $entry['dto']->getSocialClubName() }}</td>
                    <td>{{ $entry['dto']->getReason() }}</td>
                    <td>{{ $entry['dto']->getLength() }}</td>
                    <td>{{ $entry['dto']->getCreatedAt()->format('Y-m-d') }}</td>
                    <td>{{ $entry['user']->getUsername() }}</td>
                    <td>
                        <a class="btn btn-primary"
                           href="{{ route('admin.supportForm.ban.show', $entry['dto']->getId()) }}">Anzeigen</a>
                    </td>
                    <td>
                        <form action="{{ route('admin.supportForm.ban.destroy', $entry['dto']->getId()) }}"
                              method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="submit" class="btn btn-danger" value="Löschen"/>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
