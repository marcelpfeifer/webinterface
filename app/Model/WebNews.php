<?php
/**
 * WebNews
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.08.2021
 * Time: 12:03
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class WebNews extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_WEB_NEWS_CATEGORY_ID = 'webNewsCategoryId';

    /**
     * @var string
     */
    const COLUMN_TITLE = 'title';

    /**
     * @var string
     */
    const COLUMN_IMAGE = 'image';

    /**
     * @var string
     */
    const COLUMN_SHORT_DESCRIPTION = 'shortDescription';

    /**
     * @var string
     */
    const COLUMN_DESCRIPTION = 'description';

    /**
     * @var string
     */
    const COLUMN_AUTHOR = 'author';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webNews';

    /**
     * @param int|null $categoryId
     * @return array
     */
    public function getNews(int $categoryId = null)
    {
        $news = $this
            ->when(
                $categoryId,
                function ($query, $categoryId) {
                    return $query->where(self::COLUMN_WEB_NEWS_CATEGORY_ID, $categoryId);
                }
            )
            ->orderBy(self::CREATED_AT, 'DESC')
            ->get();
        $result = [];
        foreach ($news as $entry) {
            $result[] = self::entryAsDto($entry);
        }
        return $result;
    }

    /**
     * @param Dto\WebNews $dto
     */
    public function insertEntry(Dto\WebNews $dto)
    {
        $this->insert(
            [
                self::COLUMN_ID                   => $dto->getId(),
                self::COLUMN_WEB_NEWS_CATEGORY_ID => $dto->getWebNewsCategoryId(),
                self::COLUMN_TITLE                => $dto->getTitle(),
                self::COLUMN_IMAGE                => $dto->getImage(),
                self::COLUMN_SHORT_DESCRIPTION    => $dto->getShortDescription(),
                self::COLUMN_DESCRIPTION          => $dto->getDescription(),
                self::COLUMN_AUTHOR               => $dto->getAuthor(),
                self::CREATED_AT                  => Carbon::now(),
                self::UPDATED_AT                  => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebNews $dto
     */
    public function updateEntry(Dto\WebNews $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_ID                   => $dto->getId(),
                self::COLUMN_WEB_NEWS_CATEGORY_ID => $dto->getWebNewsCategoryId(),
                self::COLUMN_TITLE                => $dto->getTitle(),
                self::COLUMN_IMAGE                => $dto->getImage(),
                self::COLUMN_SHORT_DESCRIPTION    => $dto->getShortDescription(),
                self::COLUMN_DESCRIPTION          => $dto->getDescription(),
                self::COLUMN_AUTHOR               => $dto->getAuthor(),
                self::UPDATED_AT                  => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebNews $dto
     */
    public function deleteEntry(\App\Model\Dto\WebNews $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebNews $entry
     * @return Dto\WebNews
     */
    public static function entryAsDto(WebNews $entry): Dto\WebNews
    {
        return (new Dto\WebNews())
            ->setId($entry->{self::COLUMN_ID})
            ->setWebNewsCategoryId($entry->{self::COLUMN_WEB_NEWS_CATEGORY_ID})
            ->setTitle($entry->{self::COLUMN_TITLE})
            ->setImage($entry->{self::COLUMN_IMAGE})
            ->setShortDescription($entry->{self::COLUMN_SHORT_DESCRIPTION})
            ->setDescription($entry->{self::COLUMN_DESCRIPTION})
            ->setAuthor($entry->{self::COLUMN_AUTHOR})
            ->setCreatedAt($entry->{self::CREATED_AT})
            ->setUpdatedAt($entry->{self::UPDATED_AT});
    }
}
