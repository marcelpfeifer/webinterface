<?php
/**
 * SetDimension
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 12.04.2020
 * Time: 13:22
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Api\Player;

use App\Libraries\Api\AClient;
use App\Model\WebConfig;

class SetDimension extends AClient
{
    /**
     * @var string
     */
    const URL = 'player/setDimension';

    /**
     * @param int $pid
     * @param int $dimension
     * @return bool
     */
    public static function call(int $pid, int $dimension): bool
    {
        $response = self::sendRequest('POST', WebConfig::getEntryByKey('api_url')->getValue() . self::URL, [
            'query' => [
                'pid'       => $pid,
                'dimension' => $dimension,
            ]
        ]);
        return $response && $response->getStatusCode() == 200;
    }
}
