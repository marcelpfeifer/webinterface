<?php
/**
 * WebMarketplaceBuy
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 02.05.2020
 * Time: 17:56
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class WebMarketplaceBuy extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_CHARACTER_ID = 'characterId';

    /**
     * @var string
     */
    const COLUMN_CATEGORY_ID = 'categoryId';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    const COLUMN_IMAGE_URL = 'imageUrl';

    /**
     * @var string
     */
    const COLUMN_MESSAGE = 'message';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webMarketplaceBuy';

    /**
     * @return Dto\WebMarketplaceBuy[]
     */
    public static function getAllOffers(): array
    {
        $offers = [];
        $results = self::orderBy(self::CREATED_AT, 'DESC')->get();
        foreach ($results as $entry) {
            $offers[] = self::entryAsDto($entry);
        }
        return $offers;
    }

    /**
     * @param int $characterId
     * @return Dto\WebMarketplaceBuy[]
     */
    public static function getOffersByCharacterId(int $characterId): array
    {
        $offers = [];
        $results = self::where(self::COLUMN_CHARACTER_ID, $characterId)
            ->orderBy(self::CREATED_AT, 'DESC')
            ->get();
        foreach ($results as $entry) {
            $offers[] = self::entryAsDto($entry);
        }
        return $offers;
    }

    /**
     * @param int $categoryId
     * @return Dto\WebMarketplaceBuy[]
     */
    public static function getOffersByCategoryId(int $categoryId): array
    {
        $offers = [];
        $results = self::where(self::COLUMN_CATEGORY_ID, $categoryId)
            ->orderBy(self::CREATED_AT, 'DESC')
            ->get();
        foreach ($results as $entry) {
            $offers[] = self::entryAsDto($entry);
        }
        return $offers;
    }

    /**
     * @param Dto\WebMarketplaceBuy $dto
     */
    public function insertEntry(Dto\WebMarketplaceBuy $dto)
    {
        $this->insert([
            self::COLUMN_ID           => $dto->getId(),
            self::COLUMN_CHARACTER_ID => $dto->getCharacterId(),
            self::COLUMN_CATEGORY_ID  => $dto->getCategoryId(),
            self::COLUMN_NAME         => $dto->getName(),
            self::COLUMN_IMAGE_URL    => $dto->getImageUrl(),
            self::COLUMN_MESSAGE      => $dto->getMessage(),
            self::CREATED_AT          => Carbon::now(),
            self::UPDATED_AT          => Carbon::now(),
        ]);
    }


    /**
     * @param Dto\WebMarketplaceBuy $dto
     */
    public function updateEntry(Dto\WebMarketplaceBuy $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update([
            self::COLUMN_ID           => $dto->getId(),
            self::COLUMN_CHARACTER_ID => $dto->getCharacterId(),
            self::COLUMN_CATEGORY_ID  => $dto->getCategoryId(),
            self::COLUMN_NAME         => $dto->getName(),
            self::COLUMN_IMAGE_URL    => $dto->getImageUrl(),
            self::COLUMN_MESSAGE      => $dto->getMessage(),
            self::UPDATED_AT          => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\WebMarketplaceBuy $dto
     */
    public function deleteEntry(\App\Model\Dto\WebMarketplaceBuy $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebMarketplaceBuy $entry
     * @return Dto\WebMarketplaceBuy
     */
    public static function entryAsDto(WebMarketplaceBuy $entry): Dto\WebMarketplaceBuy
    {
        return (new Dto\WebMarketplaceBuy())
            ->setId($entry->{self::COLUMN_ID})
            ->setCharacterId($entry->{self::COLUMN_CHARACTER_ID})
            ->setCategoryId($entry->{self::COLUMN_CATEGORY_ID})
            ->setName($entry->{self::COLUMN_NAME})
            ->setImageUrl($entry->{self::COLUMN_IMAGE_URL})
            ->setMessage($entry->{self::COLUMN_MESSAGE})
            ->setCreateAt($entry->{self::CREATED_AT})
            ->setUpdatedAt($entry->{self::UPDATED_AT});
    }
}
