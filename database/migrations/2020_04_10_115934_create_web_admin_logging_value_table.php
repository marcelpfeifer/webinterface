<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebAdminLoggingValueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webAdminLoggingValue', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('loggingId');
            $table->foreign('loggingId')->references('id')->on('webAdminLogging');
            $table->string('tableName');
            $table->string('columnName');
            $table->string('oldValue');
            $table->string('newValue');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('web_admin_logging_value');
    }
}
