<?php
/**
 * SetModel
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 23.04.2020
 * Time: 16:02
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Api\Player;

use App\Libraries\Api\AClient;
use App\Model\WebConfig;

class SetModel extends AClient
{
    /**
     * @var string
     */
    const URL = 'player/setModel';

    /**
     * @param int $pid
     * @param string $model
     * @return bool
     */
    public static function call(int $pid, string $model): bool
    {
        $response = self::sendRequest('POST', WebConfig::getEntryByKey('api_url')->getValue() . self::URL, [
            'query' => [
                'pid'  => $pid,
                'hash' => $model,
            ]
        ]);
        return $response && $response->getStatusCode() == 200;
    }
}
