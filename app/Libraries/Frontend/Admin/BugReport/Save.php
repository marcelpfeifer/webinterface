<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 11.08.2021
 * Time: 12:08
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Admin\BugReport;


use App\Model\Account;
use App\Model\Character;
use App\Model\WebBugReport;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class Save
{

    /**
     * @param \App\Http\Request\Dto\Admin\BugReport\Save $requestDto
     */
    public static function save(\App\Http\Request\Dto\Admin\BugReport\Save $requestDto, bool $sendMail = false)
    {
        $entry = WebBugReport::find($requestDto->getId());
        if (!$entry) {
            return;
        }
        $dto = WebBugReport::entryAsDto($entry);
        $dto->setTitle($requestDto->getTitle())
            ->setDescription($requestDto->getDescription())
            ->setEvidence($requestDto->getEvidence())
            ->setPosition($requestDto->getPosition())
            ->setApproved($sendMail);

        if ($sendMail) {
            self::sendMail($dto);
        }

        (new WebBugReport())->updateEntry($dto);
    }

    /**
     * @param \App\Model\Dto\WebBugReport $dto
     */
    private static function sendMail(\App\Model\Dto\WebBugReport $dto)
    {
        $supporter = Account::entryAsDto(Account::find(Auth::id()));
        $reporter = Character::entryAsDto(Character::find($dto->getCharacterId()));
        Mail::to(\App\Model\WebConfig::getEntryByKey(\App\Model\Enum\WebConfig::GITLAB_MAIL_URL)->getValue())
            ->send(new \App\Libraries\Mail\BugReport\Mail($dto, $supporter, $reporter));
    }
}
