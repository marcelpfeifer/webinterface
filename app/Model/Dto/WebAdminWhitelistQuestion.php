<?php
/**
 * WebAdminWhitelistQuestion
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 13.11.2021
 * Time: 13:06
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;


use App\Libraries\Logging\ILogging;
use App\Libraries\Logging\Traits\Variables;
use Carbon\Carbon;

class WebAdminWhitelistQuestion implements ILogging
{
    use Variables;

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var string
     */
    private $question = '';

    /**
     * @var string
     */
    private $answer = '';

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @var null|Carbon
     */
    private $updatedAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebAdminWhitelistQuestion
     */
    public function setId(?int $id): WebAdminWhitelistQuestion
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getQuestion(): string
    {
        return $this->question;
    }

    /**
     * @param string $question
     * @return WebAdminWhitelistQuestion
     */
    public function setQuestion(string $question): WebAdminWhitelistQuestion
    {
        $this->question = $question;
        return $this;
    }

    /**
     * @return string
     */
    public function getAnswer(): string
    {
        return $this->answer;
    }

    /**
     * @param string $answer
     * @return WebAdminWhitelistQuestion
     */
    public function setAnswer(string $answer): WebAdminWhitelistQuestion
    {
        $this->answer = $answer;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return WebAdminWhitelistQuestion
     */
    public function setCreatedAt(?Carbon $createdAt): WebAdminWhitelistQuestion
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon|null $updatedAt
     * @return WebAdminWhitelistQuestion
     */
    public function setUpdatedAt(?Carbon $updatedAt): WebAdminWhitelistQuestion
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
