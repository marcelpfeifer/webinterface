<?php
/**
 * index.blade.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 22.08.2019
 * Time: 21:37
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-user"></i>
    Spielerübersicht
@endsection
@section('content')
    <form action="{{ route('admin.player.index') }}" method="GET">
        <div class="form-row">
            <div class="form-group col-10">
                <input type="text" class="form-control" id="search" name="search" placeholder="Suche"
                       value="{{ $search }}">
            </div>
            <div class="form-group col-2">
                <button type="submit" class="btn btn-dark">Suchen</button>
            </div>
        </div>
    </form>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Geburtstag</th>
            <th scope="col">Cash</th>
            <th scope="col">Bank</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($characters as $character)
            <tr>
                <th scope="row">{{ $character->id }}</th>
                <td>{{ $character->name }}</td>
                <td>{{ $character->dob }}</td>
                <td>{{ $character->cash }}</td>
                <td>{{ $character->bank }}</td>
                <td><a class="text-secondary"
                       href="{{ route('admin.player.edit', $character->id) }}">Bearbeiten</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="col-12">
            {{ $characters->links() }}
        </div>
    </div>
@endsection

