<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 13.04.2020
 * Time: 22:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('api.layout.app')
@section('title')
    <i class="fab fa-watchman-monitoring"></i>
    Beobachte einen Spieler ingame.
@endsection
@section('content')
    <form method="post" action="{{ route('api.player.spectate.save') }}">
        {{ csrf_field() }}
        @include('api.includes.select', [
          'name' => 'pid',
          'label' => 'Spieler der beobachten soll',
          'entries' => $players,
          'currentSelect' => $currentPlayer,
        ])

        @include('api.includes.select', [
         'name' => 'targetPid',
         'label' => 'Spieler der beobachtet werden soll',
         'entries' => $players,
         'currentSelect' => null,
       ])

        <div class="form-check">
            <input type="checkbox" class="form-check-input" id="active" name="active">
            <label class="form-check-label" for="active"> Aktivieren?</label>
        </div>
        <button type="submit" class="btn btn-danger">Bestätigen</button>
    </form>
    <br/>
@endsection
