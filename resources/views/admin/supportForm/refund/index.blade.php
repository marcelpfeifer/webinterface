<?php
/**
 * index.blade.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 04.09.2019
 * Time: 21:42
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@php
    /** @var $entry \App\Model\Dto\WebAdminRefund */
@endphp
@extends('admin.layout.app')
@section('title')
    <i class="fab fa-wpforms"></i>
    Erstattung
    <a class="btn btn-dark btn-sm float-right"
       href="{{ route('admin.supportForm.refund.create') }}"
       role="button"
    >
        Eintrag erstellen
    </a>
@endsection
@section('content')
    <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th scope="col">Fall</th>
                <th scope="col">Datum</th>
                <th scope="col">Name IC</th>
                <th scope="col">Gegenstands Typ</th>
                <th scope="col">Betrag</th>
                <th scope="col">Bearbeitet von...</th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($entries as $entry)
                <tr>
                    <th scope="row">{{ $entry['dto']->getId() }}</th>
                    <td>{{ $entry['dto']->getCreatedAt()->format('Y-m-d') }}</td>
                    <td>{{ $entry['dto']->getUserName() }}</td>
                    <td>{{ $entry['dto']->getItemType() }}</td>
                    <td>{{ $entry['dto']->getAmount() }}</td>
                    <td>{{ $entry['user']->getUsername() }}</td>
                    <td>
                        <a class="btn btn-primary"
                           href="{{ route('admin.supportForm.refund.show', $entry['dto']->getId()) }}">Anzeigen</a>
                    </td>
                    <td>
                        <form action="{{ route('admin.supportForm.refund.destroy', $entry['dto']->getId())}}"
                              method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="submit" class="btn btn-danger" value="Löschen"/>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
