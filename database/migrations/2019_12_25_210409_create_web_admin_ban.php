<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebAdminBan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('WebAdminBan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('userId');
            $table->string('userName');
            $table->string('ipAddress');
            $table->string('socialClubName');
            $table->text('reason');
            $table->string('length');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('WebAdminBan');
    }
}
