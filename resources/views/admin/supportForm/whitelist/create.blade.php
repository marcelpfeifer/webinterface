<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 28.04.2020
 * Time: 17:18
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

/**
 * @var $viewDto \App\Libraries\Frontend\Dto\Admin\SupportForm\Whitelist\Create
 */
?>
@extends('admin.layout.app')
@section('title')
    <i class="fab fa-wpforms"></i>
    Eintrag erstellen
@endsection
@section('content')
    <form action="{{ route('admin.supportForm.whitelist.store') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="username">Name IC</label>
            <input type="text" class="form-control" id="username" name="username">
        </div>
        <div class="form-group">
            <label for="status">Status</label>
            <select name="status" id="status" class="form-control">
                @foreach(\App\Model\Enum\WebAdminWhitelist\Status::getConstants() as $status)
                    <option value="{{ $status }}">{{ $status }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="retryDate">Erneuter Versuch</label>
            <input type="date" class="form-control" id="retryDate" name="retryDate"
                   min="{{ \Carbon\Carbon::now()->format('Y-m-d') }}">
        </div>
        <div class="form-group">
            <label for="message">Nachricht</label>
            <textarea rows="10" class="form-control" id="message" name="message" required></textarea>
        </div>
        <br/>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Fragebogen</th>
                <th scope="col"></th>
                <th scope="col"></th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($viewDto->getQuestions() as $question)
                <tr>
                    <th scope="row">{{ $question->getQuestion() }}</th>
                    <td class="bg-success">Richtig</td>
                    <td class="bg-warning">Unsicher</td>
                    <td class="bg-danger">Falsch</td>
                </tr>
                <tr>
                    <td>{{ $question->getAnswer() }}</td>
                    <td class="bg-success">
                        <input class="form-control" type="radio" value="{{ \App\Model\Enum\WebAdminWhitelistAnswer\Accuracy::RIGHT }}" name="answer[{{ $question->getId() }}]" id="answer[{{ $question->getId() }}]">
                    </td>
                    <td class="bg-warning">
                        <input class="form-control" type="radio" value="{{ \App\Model\Enum\WebAdminWhitelistAnswer\Accuracy::UNSURE }}" name="answer[{{ $question->getId() }}]" id="answer[{{ $question->getId() }}]">
                    </td>
                    <td class="bg-danger">
                        <input class="form-control" type="radio" value="{{ \App\Model\Enum\WebAdminWhitelistAnswer\Accuracy::WRONG }}" name="answer[{{ $question->getId() }}]" id="answer[{{ $question->getId() }}]">
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <button type="submit" class="btn btn-dark">Erstellen</button>
    </form>
@endsection
