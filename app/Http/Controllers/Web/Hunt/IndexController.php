<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 16.09.2021
 * Time: 16:42
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Web\Hunt;


use App\Http\Request\Web\Hunt\Index\Index;
use App\Model\Enum\Hunt\Leaderboard\OrderBy;
use App\Model\Hunt;
use Illuminate\Http\Request;
use Illuminate\View\View;

class IndexController
{

    /**
     * @param Request $request
     * @param string $hash
     * @return View
     */
    public function index(Request $request, string $hash): View
    {
        // Validiere die Anfrage
        $validator = (new Index($request));
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            return view(
                'web.hunt.index',
                [
                    'entries' => [],
                    'hash'    => $hash,
                    'orderBy' => OrderBy::ANIMAL_COUNT,
                ]
            )->with('messages', $validator->getErrorsAsMessageArray($validatedData));
        }

        /**
         * @var $requestDto \App\Http\Request\Dto\Web\Hunt\Index\Index
         */
        $requestDto = $validator->toDto();

        $entries = (new Hunt())->leaderboard($requestDto->getOrderBy());

        return view(
            'web.hunt.index',
            [
                'entries' => $entries,
                'hash'    => $hash,
                'orderBy' => $requestDto->getOrderBy(),
            ]
        );
    }
}
