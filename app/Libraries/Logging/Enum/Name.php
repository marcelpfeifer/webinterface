<?php
/**
 * Name
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 10.04.2020
 * Time: 09:18
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Logging\Enum;

class Name
{

    /**
     * @var string
     */
    const CHARACTER = 'CHARACTER';

    /**
     * @var string
     */
    const VEHICLE = 'VEHICLE';

    /**
     * @var string
     */
    const BANS = 'BANS';

    /**
     * @var string
     */
    const GROUP = 'GROUP';

    /**
     * @var string
     */
    const BUSINESS = 'BUSINESS';

    /**
     * @var string
     */
    const REGISTRATION_CODE = 'REGISTRATION_CODE';

    /**
     * @var string
     */
    const SUPPORT_FORM_BAN = 'SUPPORT_FORM_BAN';

    /**
     * @var string
     */
    const SUPPORT_FORM_INFRINGEMENT = 'SUPPORT_FORM_INFRINGEMENT';

    /**
     * @var string
     */
    const SUPPORT_FORM_REFUND = 'SUPPORT_FORM_REFUND';

    /**
     * @var string
     */
    const SUPPORT_FORM_FAQ = 'SUPPORT_FORM_FAQ';

    /**
     * @var string
     */
    const SUPPORT_FORM_WHITELIST = 'SUPPORT_FORM_WHITELIST';

    /**
     * @var string
     */
    const VEHICLE_STORE = 'VEHICLE_STORE';

    /**
     * @var string
     */
    const VEHICLE_STORE_UNREGISTERED = 'VEHICLE_STORE_UNREGISTERED';

    /**
     * @var string
     */
    const ITEM_STORE = 'ITEM_STORE';

    /**
     * @var string
     */
    const BUG_REPORT = 'BUG_REPORT';

    /**
     * @var string
     */
    const REPORT = 'REPORT';

    /**
     * @var string
     */
    const NEWS = 'NEWS';

    /**
     * @var string
     */
    const STOCK = 'STOCK';

    /**
     * @var string
     */
    const NEWS_CATEGORY = 'NEWS_CATEGORY';

    /**
     * @var string
     */
    const WHITELIST_QUESTION = 'WHITELIST_QUESTION';
}
