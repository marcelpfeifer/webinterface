<?php
/**
 * TotalAmount
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 19.09.2020
 * Time: 10:36
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto\Cashflow;

use App\Model\Dto\Cashflow;

class TotalAmount
{

    /**
     * @var int
     */
    private $totalAmount = 0;

    /**
     * @var Cashflow[]
     */
    private $cashFlows = [];

    /**
     * @return int
     */
    public function getTotalAmount(): int
    {
        return $this->totalAmount;
    }

    /**
     * @param int $totalAmount
     * @return TotalAmount
     */
    public function setTotalAmount(int $totalAmount): TotalAmount
    {
        $this->totalAmount = $totalAmount;
        return $this;
    }

    /**
     * @return Cashflow[]
     */
    public function getCashFlows(): array
    {
        return $this->cashFlows;
    }

    /**
     * @param Cashflow[] $cashFlows
     * @return TotalAmount
     */
    public function setCashFlows(array $cashFlows): TotalAmount
    {
        $this->cashFlows = $cashFlows;
        return $this;
    }
}
