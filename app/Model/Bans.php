<?php
/**
 * Bans
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 05.05.2020
 * Time: 17:02
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Bans extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_GUID = 'guid';

    /**
     * @var string
     */
    const COLUMN_TIME_BAN = 'timeban';

    /**
     * @var string
     */
    const COLUMN_REASON = 'reason';

    /**
     * @var string
     */
    const COLUMN_EXPIRE = 'expire';

    /**
     * @var string
     */
    const COLUMN_CREATED_AT = 'createdAt';

    /**
     * @var string
     */
    const COLUMN_UPDATED_AT = 'updatedAt';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $dates = [
        self::COLUMN_CREATED_AT,
        self::COLUMN_UPDATED_AT,
    ];

    /**
     * @var string
     */
    protected $table = 'bans';

    /**
     * @param int $id
     * @return Dto\Bans|null
     */
    public function getBanById(int $id): ?Dto\Bans
    {
        $entry = $this->where(self::COLUMN_ID, $id)->first();

        if ($entry) {
            return self::entryAsDto($entry);
        }
        return null;
    }

    /**
     * @param int $characterId
     * @return Dto\Bans[]
     */
    public function getBansByCharacterId(int $characterId): array
    {
        $result = [];
        $entries = $this->where(self::COLUMN_GUID, $characterId)->get();
        foreach ($entries as $entry) {
            $result[] = self::entryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param Dto\Bans $dto
     */
    public function insertEntry(Dto\Bans $dto)
    {
        $this->insert([
            self::COLUMN_ID         => $dto->getId(),
            self::COLUMN_GUID       => $dto->getGuid(),
            self::COLUMN_TIME_BAN   => $dto->getTimeBan(),
            self::COLUMN_REASON     => $dto->getReason(),
            self::COLUMN_EXPIRE     => $dto->getExpire(),
            self::COLUMN_CREATED_AT => Carbon::now()->setTimezone('Europe/Berlin'),
            self::COLUMN_UPDATED_AT => Carbon::now()->setTimezone('Europe/Berlin'),
        ]);
    }

    /**
     * @param Dto\Bans $dto
     */
    public function updateEntry(Dto\Bans $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update([
            self::COLUMN_ID         => $dto->getId(),
            self::COLUMN_GUID       => $dto->getGuid(),
            self::COLUMN_TIME_BAN   => $dto->getTimeBan(),
            self::COLUMN_REASON     => $dto->getReason(),
            self::COLUMN_EXPIRE     => $dto->getExpire(),
            self::COLUMN_UPDATED_AT => Carbon::now()->setTimezone('Europe/Berlin'),
        ]);
    }

    /**
     * @param Dto\Bans $dto
     */
    public function deleteEntry(Dto\Bans $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param Bans $entry
     * @return Dto\Bans
     */
    public static function entryAsDto(Bans $entry): Dto\Bans
    {
        return (new Dto\Bans())
            ->setId($entry->{self::COLUMN_ID})
            ->setGuid($entry->{self::COLUMN_GUID})
            ->setTimeBan($entry->{self::COLUMN_TIME_BAN})
            ->setReason($entry->{self::COLUMN_REASON})
            ->setExpire($entry->{self::COLUMN_EXPIRE});
    }
}
