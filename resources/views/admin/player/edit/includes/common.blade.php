<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.01.2021
 * Time: 19:08
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
<!-- Allgemein -->
<h4>Allgemein</h4>
@permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PLAYER_NAME)
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name"
               value="{{ $character->getName() }}">
    </div>
</div>
@endpermission
@permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PLAYER_PASSWORD)
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="password">Passwort</label>
        <input type="password" class="form-control" id="password" name="password">
    </div>
</div>
@endpermission
<br/>

@permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PLAYER_IMAGE)
<h4>Bild bearbeiten</h4>
<div class="form-row">
    <div class="form-group col-12 ml-3">
        <div class="row">
            @if($account->imageName)
                <img src="{{ url('img/user/logo/'. $account->id. '.jpg') }}" height="125px"
                     width="125px">
            @else
                <i class="fas fa-image fa-8x text-primary img-fluid"></i>
            @endif
        </div>
        <div class="row mt-1">
            <input type="file" name="image" value="" class="form-control-file">
        </div>
    </div>
</div>
<br/>
@endpermission
