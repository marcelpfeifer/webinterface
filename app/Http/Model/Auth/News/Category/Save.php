<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.08.2021
 * Time: 13:53
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Model\Auth\News\Category;


use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Model\Dto\WebNewsCategory;
use Illuminate\Support\Facades\Auth;

class Save
{

    /**
     * @param \App\Http\Request\Dto\Auth\News\Category\Save $requestDto
     */
    public static function save(\App\Http\Request\Dto\Auth\News\Category\Save $requestDto)
    {
        $dto = (new WebNewsCategory())
            ->setId($requestDto->getId())
            ->setName($requestDto->getName())
            ->setShow($requestDto->isShow());

        $model = new \App\Model\WebNewsCategory();
        if ($dto->getId()) {
            $oldDto = $model::entryAsDto($model::find($dto->getId()));
            $model->updateEntry($dto);
            Logging::update(Name::NEWS_CATEGORY, Auth::id(), $oldDto, $dto);
        } else {
            $model->insertEntry($dto);
            Logging::insert(Name::NEWS_CATEGORY, Auth::id(), $dto);
        }
    }
}
