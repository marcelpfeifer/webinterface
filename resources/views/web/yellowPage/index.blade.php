<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 12.12.2020
 * Time: 22:39
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('web.layout.app')
@section('url')
    https://eternityV.de/gelbe-seite
@endsection
@section('content')
    <div class="container bg-white text-dark">
        <div class="row p-2 mb-2 shadow bg-warning">
            <div class="col-12">
                Gelbe Seiten
            </div>
        </div>
        @include('layouts.messages')
        <div class="row mb-2">
            <div class="col-12">
                <form action="{{ route('web.yellowPage.save', $hash) }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="message">Nachricht</label>
                        <textarea name="message" class="form-control" rows="3"></textarea>
                    </div>
                    <button type="submit" class="btn btn-warning">Aktualisieren</button>
                </form>
                <br/>
                <form action="{{ route('web.yellowPage.delete',$hash) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="DELETE"/>
                    <button class="btn btn-danger" type="submit">Löschen</button>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-12 border">
            </div>
        </div>

        <div class="p-2 row">
            @foreach($entries as $entry)
                <div class="col-12 border border-dark bg-warning">
                    <div class="row">
                        <div class="col-12 p-2" style="min-height: 100px">
                            {{ $entry->getMessage() }}
                        </div>
                        <div class="col-3 bg-dark">
                            <a href="{{ route('web.yellowPage.call', ['id' => $entry->getCharacterId(),'hash' => $hash]) }}"
                               class="text-white">
                                <i class="fa fa-phone text-success" aria-hidden="true"></i>
                                {{ \App\Model\Character::getPhoneNumberById($entry->getCharacterId()) }}
                            </a>
                        </div>
                        <div class="col-9 text-right border-top border-dark">
                            {{ $entry->getName() }}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
