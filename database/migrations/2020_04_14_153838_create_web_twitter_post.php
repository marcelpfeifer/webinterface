<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebTwitterPost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webTwitterPost', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('characterId');
            $table->unsignedBigInteger('retweet')->nullable();
            $table->foreign('retweet')->references('id')->on('webTwitterPost');
            $table->string('name');
            $table->text('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webTwitterPost');
    }
}
