import $ from 'jquery';
global.$ = $;

import Quill from 'quill';
global.Quill = Quill;

window.axios = require('axios');

require('../../node_modules/chart.js/dist/chart.min');
import 'bootstrap'

// Javascript für die Mail
require('./mail/index');
require('./news/index');
require('./web/stock/stock');
