<?php
/**
 * Depot
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 30.08.2021
 * Time: 15:56
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Dto\Web\Stock;


use App\Libraries\Frontend\Dto\Web\Stock\Depot\Entry;
use App\Libraries\Frontend\Dto\Web\Stock\Depot\Total;

class Depot
{

    /**
     * @var Total
     */
    private $total;

    /**
     * @var Entry[]
     */
    private $entries = [];

    /**
     * @return Total
     */
    public function getTotal(): Total
    {
        return $this->total;
    }

    /**
     * @param Total $total
     * @return Depot
     */
    public function setTotal(Total $total): Depot
    {
        $this->total = $total;
        return $this;
    }

    /**
     * @return Entry[]
     */
    public function getEntries(): array
    {
        return $this->entries;
    }

    /**
     * @param Entry[] $entries
     * @return Depot
     */
    public function setEntries(array $entries): Depot
    {
        $this->entries = $entries;
        return $this;
    }
}
