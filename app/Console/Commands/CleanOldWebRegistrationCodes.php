<?php
/**
 * CleanOldWebRegistrationCodes
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 17.05.2020
 * Time: 14:34
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CleanOldWebRegistrationCodes extends Command
{
    /**
     * @var string
     */
    protected $signature = 'webRegistrationCodes:clean';

    /**
     * @var string
     */
    protected $description = 'Löscht alte eine in den WebRegistrationCodes';

    public function handle()
    {
        DB::table('webRegistrationCodes')
            ->where('created_at', '<=', Carbon::now()->subDay())
            ->delete();

        $this->info('Erfolgreich die alten Codes gelöscht.');
    }
}
