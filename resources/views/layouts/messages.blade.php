<?php

/**
 * @var $messages \App\Dto\Message[]
 */

?>

@if (session()->has('messages'))
    @foreach(session('messages') as $message)
        @switch($message->getStatus())
            @case(\App\Libraries\Frontend\Enum\Message::SUCCESS)
            <div class="alert alert-success" role="alert">
                {{ $message->getMessage() }}
            </div>
            @break
            @case(\App\Libraries\Frontend\Enum\Message::ERROR)
            <div class="alert alert-danger" role="alert">
                {{ $message->getMessage() }}
            </div>
            @break
            @case(\App\Libraries\Frontend\Enum\Message::NOTICE)
            <div class="alert alert-warning" role="alert">
                {{ $message->getMessage() }}
            </div>
            @break
        @endswitch
    @endforeach
@endif
