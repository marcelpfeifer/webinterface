<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 11.08.2021
 * Time: 10:26
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('admin.layout.app')
@section('title')
    <div class="float-left">
        <a class="btn btn-dark mr-2"
           href="{{ route('admin.bugReport.index') }}"
           role="button"
        >
            Zurück
        </a>
        <i class="fas fa-bug"></i>
        Eintrag bearbeiten
    </div>

    @if($report)
        <div class="float-right">
            <form action="{{ route('admin.bugReport.delete', $report->getId())}}" method="POST">
                <input type="hidden" name="_method" value="DELETE">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="submit" class="btn btn-danger" value="Löschen"/>
            </form>
        </div>
    @endif
@endsection
@section('content')
    @if(is_null($report) || is_null($character))
        Der Report wurde nicht gefunden
    @else
        <form method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="character">Name</label>
                <input class="form-control" type="text" id="character" name="character"
                       value="{{ $character->getName() }}" readonly>
            </div>
            <div class="form-group">
                <label for="title">Titel</label>
                <input class="form-control" type="text" id="title" name="title" value="{{ $report->getTitle() }}">
            </div>
            <div class="form-group">
                <label for="description">Beschreibung</label>
                <textarea class="form-control" name="description" id="description"
                          rows="3">{{ $report->getDescription() }}</textarea>
            </div>
            <div class="form-group">
                <label for="evidence">Beweise</label>
                <input class="form-control" type="text" id="evidence" name="evidence"
                       value="{{ $report->getEvidence() }}">
            </div>
            <div class="form-group">
                <label for="position">Position</label>
                <input class="form-control" type="text" id="position" name="position"
                       value="{{ $report->getPosition() }}">
            </div>

            <br/>
            <button type="submit" class="btn btn-dark"
                    onclick='this.form.action="{{  route('admin.bugReport.save', $report->getId()) }}";'>
                Aktualisieren
            </button>
            <button type="submit" class="btn btn-primary"
                    onclick='this.form.action="{{  route('admin.bugReport.send', $report->getId()) }}";'>
                Freigeben
            </button>
        </form>
    @endif
@endsection
