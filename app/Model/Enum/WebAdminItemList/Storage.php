<?php
/**
 * Storage
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 11.05.2020
 * Time: 22:05
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Enum\WebAdminItemList;

class Storage
{

    /**
     * @var string
     */
    const PERSON = 'PERSON';

    /**
     * @var string
     */
    const HOUSE = 'HOUSE';

    /**
     * @var string
     */
    const VEHICLE = 'VEHICLE';
}
