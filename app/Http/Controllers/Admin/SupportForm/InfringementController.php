<?php
/**
 * InfringementController
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 24.12.2019
 * Time: 20:35
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\SupportForm;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Model\Account;
use App\Model\WebAdminInfringement;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InfringementController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_SUPPORT_FORM_INFRINGEMENT);
    }

    /**
     * @return View
     */
    public function index(): View
    {
        $entries = WebAdminInfringement::all();

        $result = [];
        foreach ($entries as $entry) {
            $dto = WebAdminInfringement::entryAsDto($entry);
            $user = Account::entryAsDto($entry->account);
            $result[] = [
                'dto' => $dto,
                'user' => $user,
            ];
        }

        return view('admin.supportForm.infringement.index', [
            "entries" => $result
        ]);
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return view('admin.supportForm.infringement.create');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $dto = (new \App\Model\Dto\WebAdminInfringement())
            ->setUserName($request->input('userName'))
            ->setVideoLink($request->input('videoLink'))
            ->setReason($request->input('reason'))
            ->setReportedBy($request->input('reportedBy'))
            ->setDescription($request->input('description'))
            ->setAction($request->input('action'))
            ->setUserId(Auth::user()->{Account::COLUMN_ID});

        (new WebAdminInfringement())->insertEntry($dto);
        Logging::insert(Name::SUPPORT_FORM_INFRINGEMENT, Auth::id(), $dto);

        $messages[] = (new Message())
            ->setMessage("Eintrag wurde erstellt")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.supportForm.infringement.index')->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return View
     */
    public function show(Request $request, int $id): View
    {
        $entry = WebAdminInfringement::find($id);
        $dto = WebAdminInfringement::entryAsDto($entry);
        $user = Account::entryAsDto($entry->account);

        return view('admin.supportForm.infringement.show', [
            'entry' => $dto,
            'user'  => $user
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(Request $request, int $id): RedirectResponse
    {
        $entry = WebAdminInfringement::find($id);
        $dto = WebAdminInfringement::entryAsDto($entry);

        (new WebAdminInfringement())->deleteEntry($dto);
        Logging::delete(Name::SUPPORT_FORM_INFRINGEMENT, Auth::id(), $dto);
        $messages[] = (new Message())
            ->setMessage("Eintrag wurde gelöscht")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.supportForm.infringement.index')->with('messages', $messages);
    }
}
