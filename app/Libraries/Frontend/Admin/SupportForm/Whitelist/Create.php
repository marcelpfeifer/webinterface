<?php
/**
 * Create
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 13.11.2021
 * Time: 15:08
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Admin\SupportForm\Whitelist;


use App\Model\WebAdminWhitelistQuestion;

class Create
{
    /**
     * @return \App\Libraries\Frontend\Dto\Admin\SupportForm\Whitelist\Create
     */
    public static function getViewData(): \App\Libraries\Frontend\Dto\Admin\SupportForm\Whitelist\Create
    {
        $questions = [];
        foreach (WebAdminWhitelistQuestion::all() as $entry) {
            $questions[] = WebAdminWhitelistQuestion::entryAsDto($entry);
        }

        return (new \App\Libraries\Frontend\Dto\Admin\SupportForm\Whitelist\Create())
            ->setQuestions($questions);
    }
}
