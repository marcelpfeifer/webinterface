<?php
/**
 * StockGenerate
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 29.08.2021
 * Time: 13:41
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Console\Commands;

use App\Model\Dto\WebStockChange;
use App\Model\WebStock;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;

class StockGenerate extends Command
{
    /**
     * @var string
     */
    protected $signature = 'stock:generate';

    /**
     * @var string
     */
    protected $description = 'Generiert den aktuellen Stock';

    public function handle()
    {
        foreach (WebStock::all() as $stock) {
            $dto = WebStock::entryAsDto($stock);
            $dto = self::updateLatestPrice($dto);
            $this->generateStockChange($dto);
        }
    }

    /**
     * @param \App\Model\Dto\WebStock $dto
     * @return \App\Model\Dto\WebStock
     */
    private function updateLatestPrice(\App\Model\Dto\WebStock $dto): \App\Model\Dto\WebStock
    {
        $latestEntry = (new \App\Model\WebStockChange())->getLatestEntry($dto->getId());
        if ($latestEntry) {
            if (!($latestEntry->getCreatedAt()->isSameDay(Carbon::now()))) {
                $dto->setLastPrice($dto->getPrice())
                    ->setPercentageChange(0.00);
                (new WebStock())->updateEntry($dto);
            }
        }
        return $dto;
    }

    /**
     * @param \App\Model\Dto\WebStock $dto
     */
    private function generateStockChange(\App\Model\Dto\WebStock $dto)
    {
        $isThereAChange = mt_rand(0, 1);
        if (!$isThereAChange) {
            $this->addStockChange($dto->getId(), $dto->getPrice(), 0);
            return;
        }

        $newPrice = $this->generateRandomNumberFromMinAndMaxChange($dto);

        $percentageChange = $this->getPercentageChange($dto->getLastPrice(), $newPrice);
        $percentageChangeToLatest = $this->getPercentageChange($dto->getPrice(), $newPrice);

        $dto->setPercentageChange($percentageChange)
            ->setPrice($newPrice);
        (new WebStock())->updateEntry($dto);

        $this->addStockChange($dto->getId(), $newPrice, $percentageChangeToLatest);
    }

    /**
     * @param int $webStockId
     * @param float $newPrice
     * @param float $percentageChangeToLatest
     */
    private function addStockChange(int $webStockId, float $newPrice, float $percentageChangeToLatest)
    {
        $webStockChangeDto = (new WebStockChange())
            ->setWebStockId($webStockId)
            ->setPrice($newPrice)
            ->setPercentage($percentageChangeToLatest)
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());
        (new \App\Model\WebStockChange())->insertEntry($webStockChangeDto);
    }

    /**
     * @param float $lastPrice
     * @param float $newPrice
     * @return float|int
     */
    private function getPercentageChange(float $lastPrice, float $newPrice)
    {
        $percentageChange = 0;
        if ($newPrice > $lastPrice) {
            $percentageChange = ($newPrice - $lastPrice) / $lastPrice * 100;
        } elseif ($newPrice < $lastPrice) {
            $percentageChange = -(($lastPrice - $newPrice) / $lastPrice * 100);
        }
        return $percentageChange;
    }

    /**
     * @param \App\Model\Dto\WebStock $dto
     * @return float|int
     */
    private function generateRandomNumberFromMinAndMaxChange(\App\Model\Dto\WebStock $dto): float
    {
        $minChange = ($dto->getLastPrice() / 100) * $dto->getMinChange();
        $maxChange = ($dto->getLastPrice() / 100) * $dto->getMaxChange();

        $lowestPrice = $dto->getLastPrice() + $minChange;
        $highestPrice = $dto->getLastPrice() + $maxChange;

        $randomPrice = rand($lowestPrice * 100, $highestPrice * 100) / 100;
        $normalizePrice = ($dto->getPrice() + $randomPrice) / 2;
        return $normalizePrice;
    }
}
