<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.01.2021
 * Time: 19:08
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
<h4>Aktionen</h4>
@permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PRISON)
<div class="row ml-1">
    <a href="{{ route('admin.player.releaseFromPrison', $character->getId()) }}"
       class="btn btn-dark btn-lg active" role="button" aria-pressed="true">
        Aus dem Gefängnis holen
    </a>
</div>
<br/>
@endpermission
@permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_DIMENSION)
<div class="row ml-1">
    <a href="{{ route('admin.player.resetDimension', $character->getId()) }}"
       class="btn btn-dark btn-lg active" role="button" aria-pressed="true">
        Dimension zurücksetzen (Aktuelle Dimension: {{ $character->getDimension() }})
    </a>

</div>
<br/>
@endpermission
@permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_DELETE_PLAYER_LOCKER)
<div class="row ml-1">
    <a href="{{ route('admin.player.deleteFactionLocker', $character->getId()) }}"
       class="btn btn-dark btn-lg active" role="button" aria-pressed="true">
        Spieler Locker zurücksetzen
    </a>

</div>
<br/>
<div class="row ml-1">
    <a href="{{ route('admin.player.deleteFactionLockerWeapon', $character->getId()) }}"
       class="btn btn-dark btn-lg active" role="button" aria-pressed="true">
        Spieler Waffen Locker zurücksetzen
    </a>
</div>
<br/>
@endpermission

@permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PLAYER_INVENTORY_RESET)
<div class="row ml-1">
    <a href="{{ route('admin.player.inventoryReset', $character->getId()) }}"
       class="btn btn-dark btn-lg active" role="button" aria-pressed="true">
        Spieler Inventar zurücksetzen
    </a>

</div>
<br/>
@endpermission

@permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PLAYER_POSITION_RESET)
<div class="row ml-1">
    <a href="{{ route('admin.player.resetPosition', $character->getId()) }}"
       class="btn btn-dark btn-lg active" role="button" aria-pressed="true">
        Spieler Position zurücksetzen
    </a>
</div>
<br/>
@endpermission
@permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PLAYER_RESET)
<div class="row ml-1">
    <a href="{{ route('admin.player.reset', $character->getId()) }}"
       class="btn btn-danger btn-lg active" role="button" aria-pressed="true">
        Spieler zurücksetzen
    </a>
</div>
<br/>
@endpermission
