<?php
/**
 * WebMailAttachment
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 11.12.2020
 * Time: 21:49
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;


class WebMailAttachment
{

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var null|int
     */
    private $webMailId = null;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $path = '';

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebMailAttachment
     */
    public function setId(?int $id): WebMailAttachment
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getWebMailId(): ?int
    {
        return $this->webMailId;
    }

    /**
     * @param int|null $webMailId
     * @return WebMailAttachment
     */
    public function setWebMailId(?int $webMailId): WebMailAttachment
    {
        $this->webMailId = $webMailId;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return WebMailAttachment
     */
    public function setName(string $name): WebMailAttachment
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return WebMailAttachment
     */
    public function setPath(string $path): WebMailAttachment
    {
        $this->path = $path;
        return $this;
    }
}
