<?php
/**
 * WebStockCharacter
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 30.08.2021
 * Time: 13:07
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;


use Carbon\Carbon;

class WebStockCharacter
{
    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var int|null
     */
    private $webStockId = null;

    /**
     * @var int
     */
    private $characterId = 0;

    /**
     * @var int
     */
    private $amount = 0;

    /**
     * @var float
     */
    private $totalPrice = 0.00;

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @var null|Carbon
     */
    private $updatedAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebStockCharacter
     */
    public function setId(?int $id): WebStockCharacter
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getWebStockId(): ?int
    {
        return $this->webStockId;
    }

    /**
     * @param int|null $webStockId
     * @return WebStockCharacter
     */
    public function setWebStockId(?int $webStockId): WebStockCharacter
    {
        $this->webStockId = $webStockId;
        return $this;
    }

    /**
     * @return int
     */
    public function getCharacterId(): int
    {
        return $this->characterId;
    }

    /**
     * @param int $characterId
     * @return WebStockCharacter
     */
    public function setCharacterId(int $characterId): WebStockCharacter
    {
        $this->characterId = $characterId;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     * @return WebStockCharacter
     */
    public function setAmount(int $amount): WebStockCharacter
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalPrice(): float
    {
        return $this->totalPrice;
    }

    /**
     * @param float $totalPrice
     * @return WebStockCharacter
     */
    public function setTotalPrice(float $totalPrice): WebStockCharacter
    {
        $this->totalPrice = $totalPrice;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return WebStockCharacter
     */
    public function setCreatedAt(?Carbon $createdAt): WebStockCharacter
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon|null $updatedAt
     * @return WebStockCharacter
     */
    public function setUpdatedAt(?Carbon $updatedAt): WebStockCharacter
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
