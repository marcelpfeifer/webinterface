<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.head')
<body>
<div>
    @include('layouts.header')
    <main class="py-4">
        @include('layouts.wrapper')
    </main>
    @include('layouts.footer')
</div>
</body>
</html>
