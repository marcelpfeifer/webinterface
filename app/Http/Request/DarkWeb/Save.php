<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 07.08.2020
 * Time: 21:56
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\DarkWeb;

use App\Http\Request\AValidator;
use App\Http\Request\IValidator;

class Save extends AValidator implements IValidator
{

    /**
     * @return string[]
     */
    public function getRules(): array
    {
        return [
            'name' => [
                'required',
                'string',
                'unique:webDarkWeb',
                'max:255',
            ]
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'name.required' => 'Der Name muss übergeben werden!',
            'name.string'   => 'Der Name darf nur Zeichen enthalten!',
            'name.unique'   => 'Der Name ist schon vergeben!',
            'name.max'      => 'Der Name darf maximal 255 Zeichen lang sein!',
        ];
    }
}
