<?php
/**
 * Action
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.10.2021
 * Time: 13:50
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Enum\Api\Server\Server;

use App\Libraries\Enum\AEnum;

class Action extends AEnum
{

    /**
     * @var string
     */
    public const START = 'start';


    /**
     * @var string
     */
    public const STOP = 'stop';


    /**
     * @var string
     */
    public const STATUS = 'status';
}
