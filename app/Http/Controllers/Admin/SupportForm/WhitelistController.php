<?php
/**
 * WhitelistController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 28.04.2020
 * Time: 17:10
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\SupportForm;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Http\Request\Admin\SupportForm\Whitelist\Store;
use App\Http\Request\Admin\SupportForm\Whitelist\Update;
use App\Libraries\Frontend\Admin\SupportForm\Whitelist\Index;
use App\Libraries\Frontend\Admin\SupportForm\Whitelist\Create;
use App\Libraries\Frontend\Admin\SupportForm\Whitelist\Show;
use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Model\Account;
use App\Model\WebAdminWhitelist;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WhitelistController extends Controller
{

    public function __construct()
    {
        $this->middleware(
            'permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_SUPPORT_FORM_WHITELIST
        );
    }

    /**
     * @return View
     */
    public function index(): View
    {
        $viewDto = Index::getViewData();

        return view(
            'admin.supportForm.whitelist.index',
            [
                "viewDto" => $viewDto
            ]
        );
    }

    /**
     * @return View
     */
    public function create(): View
    {
        $viewDto = Create::getViewData();
        return view(
            'admin.supportForm.whitelist.create',
            [
                "viewDto" => $viewDto
            ]
        );
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        // Validiere die Anfrage
        $validator = (new Store($request));
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            return redirect()->route('admin.supportForm.whitelist.create')
                ->with('messages', $validator->getErrorsAsMessageArray($validatedData))
                ->withInput()
                ->withErrors($validatedData->errors());
        }

        /**
         * @var $requestDto \App\Http\Request\Dto\Admin\SupportForm\WhiteList\Store
         */
        $requestDto = $validator->toDto();

        $saved = \App\Libraries\Frontend\Admin\SupportForm\Whitelist\Store::save($requestDto);

        $messages[] = (new Message())
            ->setMessage("Eintrag wurde erstellt")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.supportForm.whitelist.index')->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return View
     */
    public function show(Request $request, int $id): View
    {
        $viewDto = Show::getViewData($id);
        return view(
            'admin.supportForm.whitelist.show',
            [
                'viewDto' => $viewDto,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        // Validiere die Anfrage
        $validator = (new Update($request, $id));
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            return redirect()->route('admin.supportForm.whitelist.show', $id)
                ->with('messages', $validator->getErrorsAsMessageArray($validatedData))
                ->withInput()
                ->withErrors($validatedData->errors());
        }

        /**
         * @var $requestDto \App\Http\Request\Dto\Admin\SupportForm\WhiteList\Update
         */
        $requestDto = $validator->toDto();

        $saved = \App\Libraries\Frontend\Admin\SupportForm\Whitelist\Update::save($requestDto);

        $messages[] = (new Message())
            ->setMessage("Eintrag wurde aktualisiert")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.supportForm.whitelist.index')->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(Request $request, int $id): RedirectResponse
    {
        $entry = WebAdminWhitelist::find($id);
        $dto = WebAdminWhitelist::entryAsDto($entry);
        (new WebAdminWhitelist())->deleteEntry($dto);
        Logging::delete(Name::SUPPORT_FORM_WHITELIST, Auth::id(), $dto);

        $messages[] = (new Message())
            ->setMessage("Eintrag wurde gelöscht")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.supportForm.whitelist.index')->with('messages', $messages);
    }
}
