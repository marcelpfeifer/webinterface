<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.08.2021
 * Time: 16:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
<div class="row justify-content-center">
    <div class="col-6">
        <h1 class="text-center border-bottom font-weight-bold"> Börse </h1>
    </div>
</div>
<div class="row">
    <div class="col-2 border-left border-right text-center">
        <a class="text-dark font-weight-bold" href="{{ route('web.stock.index', $hash) }}">
            Startseite
        </a>
    </div>
    <div class="col-2 border-left border-right text-center">
        <a class="text-dark font-weight-bold" href="{{ route('web.stock.depot', $hash) }}">
            Depot
        </a>
    </div>
</div>
<hr>

