<?php
/**
 * WithWebMailAddressToAccount
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 25.08.2021
 * Time: 13:12
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto\WebMailAddress;


use App\Model\Dto\WebMailAddress;
use App\Model\Dto\WebMailAddressToAccount;

class WithWebMailAddressToAccount
{

    /**
     * @var WebMailAddress
     */
    private $webMailAddress;

    /**
     * @var WebMailAddressToAccount
     */
    private $webMailAddressToAccount;

    /**
     * @return WebMailAddress
     */
    public function getWebMailAddress(): WebMailAddress
    {
        return $this->webMailAddress;
    }

    /**
     * @param WebMailAddress $webMailAddress
     * @return WithWebMailAddressToAccount
     */
    public function setWebMailAddress(WebMailAddress $webMailAddress): WithWebMailAddressToAccount
    {
        $this->webMailAddress = $webMailAddress;
        return $this;
    }

    /**
     * @return WebMailAddressToAccount
     */
    public function getWebMailAddressToAccount(): WebMailAddressToAccount
    {
        return $this->webMailAddressToAccount;
    }

    /**
     * @param WebMailAddressToAccount $webMailAddressToAccount
     * @return WithWebMailAddressToAccount
     */
    public function setWebMailAddressToAccount(WebMailAddressToAccount $webMailAddressToAccount
    ): WithWebMailAddressToAccount {
        $this->webMailAddressToAccount = $webMailAddressToAccount;
        return $this;
    }
}
