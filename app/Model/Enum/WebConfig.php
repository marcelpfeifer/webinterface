<?php
/**
 * WebConfig
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 02.04.2020
 * Time: 17:32
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Enum;

/**
 * Class WebConfig
 * @package App\Model\Enum
 */
class WebConfig
{
    /**
     * @var string
     */
    const API_URL = 'api_url';

    /**
     * @var string
     */
    const API_SYSTEM_URL = 'api_system_url';

    /**
     * @var string
     */
    const GITLAB_MAIL_URL = 'gitlab_mail_url';
}
