<?php
/**
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.01.2021
 * Time: 19:08
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_SOCIAL_POINTS)
<!-- Social Punkte -->
<h4>Social Points</h4>
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="socialPoints">Social Points</label>
        <input type="text" class="form-control" id="socialPoints" name="socialPoints"
               value="{{ $character->getSocialPoints() }}">
    </div>
</div>
@endpermission
