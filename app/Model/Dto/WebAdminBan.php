<?php
/**
 * WebAdminBan
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 25.12.2019
 * Time: 21:58
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

use App\Libraries\Logging\ILogging;
use App\Libraries\Logging\Traits\Variables;
use Carbon\Carbon;

class WebAdminBan implements ILogging
{
    use Variables;
    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var null|int
     */
    private $userId = null;

    /**
     * @var string
     */
    private $userName = '';

    /**
     * @var string
     */
    private $ipAddress = '';

    /**
     * @var string
     */
    private $socialClubName = '';

    /**
     * @var string
     */
    private $reason = '';

    /**
     * @var string
     */
    private $length = '';

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @var null|Carbon
     */
    private $updatedAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebAdminBan
     */
    public function setId(?int $id): WebAdminBan
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @param int|null $userId
     * @return WebAdminBan
     */
    public function setUserId(?int $userId): WebAdminBan
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @param string $userName
     * @return WebAdminBan
     */
    public function setUserName(string $userName): WebAdminBan
    {
        $this->userName = $userName;
        return $this;
    }

    /**
     * @return string
     */
    public function getIpAddress(): string
    {
        return $this->ipAddress;
    }

    /**
     * @param string $ipAddress
     * @return WebAdminBan
     */
    public function setIpAddress(string $ipAddress): WebAdminBan
    {
        $this->ipAddress = $ipAddress;
        return $this;
    }

    /**
     * @return string
     */
    public function getSocialClubName(): string
    {
        return $this->socialClubName;
    }

    /**
     * @param string $socialClubName
     * @return WebAdminBan
     */
    public function setSocialClubName(string $socialClubName): WebAdminBan
    {
        $this->socialClubName = $socialClubName;
        return $this;
    }

    /**
     * @return string
     */
    public function getReason(): string
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     * @return WebAdminBan
     */
    public function setReason(string $reason): WebAdminBan
    {
        $this->reason = $reason;
        return $this;
    }

    /**
     * @return string
     */
    public function getLength(): string
    {
        return $this->length;
    }

    /**
     * @param string $length
     * @return WebAdminBan
     */
    public function setLength(string $length): WebAdminBan
    {
        $this->length = $length;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return WebAdminBan
     */
    public function setCreatedAt(?Carbon $createdAt): WebAdminBan
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon|null $updatedAt
     * @return WebAdminBan
     */
    public function setUpdatedAt(?Carbon $updatedAt): WebAdminBan
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
