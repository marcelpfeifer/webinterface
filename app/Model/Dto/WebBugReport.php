<?php
/**
 * WebBugReport
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 03.08.2021
 * Time: 17:57
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;


use App\Http\Request\Dto\ADto;
use App\Libraries\Logging\ILogging;
use App\Libraries\Logging\Traits\Variables;
use Carbon\Carbon;

class WebBugReport extends ADto implements ILogging
{
    use Variables;

    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var int
     */
    private $characterId = 0;

    /**
     * @var string
     */
    private $title = '';

    /**
     * @var string
     */
    private $description = '';

    /**
     * @var string
     */
    private $evidence = '';

    /**
     * @var string
     */
    private $position = '';

    /**
     * @var bool
     */
    private $approved = false;

    /**
     * @var Carbon
     */
    private $createdAt;

    /**
     * @var Carbon
     */
    private $updatedAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return WebBugReport
     */
    public function setId(int $id): WebBugReport
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCharacterId(): int
    {
        return $this->characterId;
    }

    /**
     * @param int $characterId
     * @return WebBugReport
     */
    public function setCharacterId(int $characterId): WebBugReport
    {
        $this->characterId = $characterId;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return WebBugReport
     */
    public function setTitle(string $title): WebBugReport
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return WebBugReport
     */
    public function setDescription(string $description): WebBugReport
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getEvidence(): string
    {
        return $this->evidence;
    }

    /**
     * @param string $evidence
     * @return WebBugReport
     */
    public function setEvidence(string $evidence): WebBugReport
    {
        $this->evidence = $evidence;
        return $this;
    }

    /**
     * @return string
     */
    public function getPosition(): string
    {
        return $this->position;
    }

    /**
     * @param string $position
     * @return WebBugReport
     */
    public function setPosition(string $position): WebBugReport
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @return bool
     */
    public function isApproved(): bool
    {
        return $this->approved;
    }

    /**
     * @param bool $approved
     * @return WebBugReport
     */
    public function setApproved(bool $approved): WebBugReport
    {
        $this->approved = $approved;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon $createdAt
     * @return WebBugReport
     */
    public function setCreatedAt(Carbon $createdAt): WebBugReport
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getUpdatedAt(): Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon $updatedAt
     * @return WebBugReport
     */
    public function setUpdatedAt(Carbon $updatedAt): WebBugReport
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
