<?php
/**
 * WebNewsCategory
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.08.2021
 * Time: 12:03
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class WebNewsCategory extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    const COLUMN_SHOW = 'show';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webNewsCategory';

    /**
     * @return Dto\WebNewsCategory[]
     */
    public function getActiveCategories(): array
    {
        $categories = $this->where(self::COLUMN_SHOW, true)->get();
        $result = [];

        foreach ($categories as $category) {
            $dto = self::entryAsDto($category);
            $result[$dto->getId()] = $dto;
        }
        return $result;
    }

    /**
     * @param Dto\WebNewsCategory $dto
     */
    public function insertEntry(Dto\WebNewsCategory $dto)
    {
        $this->insert(
            [
                self::COLUMN_ID   => $dto->getId(),
                self::COLUMN_NAME => $dto->getName(),
                self::COLUMN_SHOW => $dto->isShow(),
                self::CREATED_AT  => Carbon::now(),
                self::UPDATED_AT  => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebNewsCategory $dto
     */
    public function updateEntry(Dto\WebNewsCategory $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_ID   => $dto->getId(),
                self::COLUMN_NAME => $dto->getName(),
                self::COLUMN_SHOW => $dto->isShow(),
                self::UPDATED_AT  => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebNewsCategory $dto
     */
    public function deleteEntry(\App\Model\Dto\WebNewsCategory $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebNewsCategory $entry
     * @return Dto\WebNewsCategory
     */
    public static function entryAsDto(WebNewsCategory $entry): Dto\WebNewsCategory
    {
        return (new Dto\WebNewsCategory())
            ->setId($entry->{self::COLUMN_ID})
            ->setName($entry->{self::COLUMN_NAME})
            ->setShow($entry->{self::COLUMN_SHOW});
    }
}
