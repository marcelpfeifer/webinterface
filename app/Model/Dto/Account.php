<?php
/**
 * Account.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 01.11.2019
 * Time: 13:23
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

use App\Libraries\Logging\ILogging;
use App\Libraries\Logging\Traits\Variables;
use Carbon\Carbon;

class Account implements ILogging
{

    use Variables;

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var string
     */
    private $username = '';

    /**
     * @var string
     */
    private $password = '';

    /**
     * @var int
     */
    private $adminLvl = 0;

    /**
     * @var bool
     */
    private $whitelisted = false;

    /**
     * @var null|int
     */
    private $groupId = null;

    /**
     * @var string
     */
    private $imageName = '';

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @var null|Carbon
     */
    private $updatedAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Account
     */
    public function setId(?int $id): Account
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return Account
     */
    public function setUsername(string $username): Account
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return Account
     */
    public function setPassword(string $password): Account
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return int
     */
    public function getAdminLvl(): int
    {
        return $this->adminLvl;
    }

    /**
     * @param int $adminLvl
     * @return Account
     */
    public function setAdminLvl(int $adminLvl): Account
    {
        $this->adminLvl = $adminLvl;
        return $this;
    }

    /**
     * @return bool
     */
    public function isWhitelisted(): bool
    {
        return $this->whitelisted;
    }

    /**
     * @param bool $whitelisted
     * @return Account
     */
    public function setWhitelisted(bool $whitelisted): Account
    {
        $this->whitelisted = $whitelisted;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getGroupId(): ?int
    {
        return $this->groupId;
    }

    /**
     * @param int|null $groupId
     * @return Account
     */
    public function setGroupId(?int $groupId): Account
    {
        $this->groupId = $groupId;
        return $this;
    }

    /**
     * @return string
     */
    public function getImageName(): string
    {
        return $this->imageName;
    }

    /**
     * @param string $imageName
     * @return Account
     */
    public function setImageName(string $imageName): Account
    {
        $this->imageName = $imageName;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return Account
     */
    public function setCreatedAt(?Carbon $createdAt): Account
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon|null $updatedAt
     * @return Account
     */
    public function setUpdatedAt(?Carbon $updatedAt): Account
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
