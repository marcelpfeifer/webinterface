<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.01.2021
 * Time: 19:12
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_PERMISSION)
<h4>Rechte</h4>
<div class="form-group">
    <label for="groupId">Gruppe</label>
    <select class="form-control select" id="groupId" name="groupId">
        <option value="0" {{ ($character->getGroup() == 0) ? 'selected' : '' }}>
            Keine Gruppe
        </option>
        @foreach($permissionGroups as $permissionGroup)
            <option
                value="{{ $permissionGroup->id }}" {{ ($account->groupId == $permissionGroup->id) ? 'selected' : '' }}> {{ $permissionGroup->name }}</option>
        @endforeach
    </select>
</div>
<br/>
@endpermission
