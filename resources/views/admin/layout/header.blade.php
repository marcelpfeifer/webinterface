<nav class="navbar bg-primary shadow-sm">

    <span class="btn text-white">
        <i class="fas fa-cogs"></i> Admin-Menü
    </span>
    <div class="float-right">
        @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API)
        <a class="btn btn-dark" href="{{ route('api.index') }}">
            <div>
                <i class="far fa-window-maximize"></i> Zur API
            </div>
        </a>
        @endpermission
        <a class="btn btn-dark" href="{{ route('auth.account.index') }}">
            <div>
                <i class="far fa-window-maximize"></i> Zum Frontend
            </div>
        </a>
    </div>
</nav>
