<?php
/**
 * Permission
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 25.07.2020
 * Time: 11:09
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Permission\Enum;

/**
 * Class Permission
 * @package App\Libraries\Permission\Enum
 */
class Permission
{
    /**
     * @var int
     */
    public const ACCESS_BACKEND = 10000;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_PLAYER = 11000;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_PLAYER_EDIT_VEHICLE = 11100;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_PLAYER_EDIT_CASH_FLOW = 11105;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_PLAYER_EDIT_CALL_HISTORY = 11106;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_PLAYER_EDIT_BANS = 11200;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_PLAYER_EDIT_PRISON = 11300;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_PLAYER_EDIT_DIMENSION = 11400;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_PLAYER_EDIT_DELETE_PLAYER_LOCKER = 11450;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_PLAYER_EDIT_PLAYER_POSITION_RESET = 11500;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_PLAYER_EDIT_PLAYER_RESET = 11600;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_PLAYER_EDIT_PLAYER_INVENTORY_RESET = 11650;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_PLAYER_EDIT_PLAYER_NAME = 11700;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_PLAYER_EDIT_SOCIAL_POINTS = 11750;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_PLAYER_EDIT_PLAYER_PASSWORD = 11800;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_PLAYER_EDIT_PLAYER_IMAGE = 11900;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_PLAYER_EDIT_PLAYER_MONEY = 11110;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_PLAYER_EDIT_WHITELIST = 11120;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_PLAYER_EDIT_FACTION = 11130;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_PLAYER_EDIT_PERMISSION = 11140;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_PLAYER_EDIT_GROUP = 11150;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_PLAYER_EDIT_BUSINESS = 11160;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_CODE_GENERATOR = 12000;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_GROUP = 13000;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_BUSINESS = 14000;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_VEHICLE_SHOP = 15000;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_ITEM_STORE = 15500;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_HAPPY_BUY_CATEGORY = 16000;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_ITEM_LIST = 17000;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_BAN = 17500;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_SETTINGS = 18000;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_PERMISSIONS = 19000;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_REPORT = 19500;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_LOGS = 19700;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_SUPPORT_FORM_WHITELIST = 20000;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_SUPPORT_FORM_WHITELIST_APPROVE = 20100;


    /**
     * @var int
     */
    public const ACCESS_BACKEND_SUPPORT_FORM_TECHNICAL_FAQ = 21000;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_SUPPORT_FORM_REFUND = 22000;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_SUPPORT_FORM_INFRINGEMENT = 23000;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_SUPPORT_FORM_BANS = 24000;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_BUG_REPORT = 25000;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_STOCK = 26000;

    /**
     * @var int
     */
    public const ACCESS_BACKEND_WHITELIST_QUESTION = 27000;

    /**
     * @var int
     */
    public const ACCESS_API = 1000000;

    /**
     * @var int
     */
    public const ACCESS_API_TP = 1100000;

    /**
     * @var int
     */
    public const ACCESS_API_INVISIBLE = 1200000;

    /**
     * @var int
     */
    public const ACCESS_API_FLY = 1300000;

    /**
     * @var int
     */
    public const ACCESS_API_REVIVE = 1400000;

    /**
     * @var int
     */
    public const ACCESS_API_GIVE_ITEM = 1500000;

    /**
     * @var int
     */
    public const ACCESS_API_GIVE_MONEY = 1600000;

    /**
     * @var int
     */
    public const ACCESS_API_SET_MONEY = 1700000;

    /**
     * @var int
     */
    public const ACCESS_API_REMOVE_MONEY = 1800000;

    /**
     * @var int
     */
    public const ACCESS_API_DIMENSION = 1900000;

    /**
     * @var int
     */
    public const ACCESS_API_GOD_MODE = 2000000;

    /**
     * @var int
     */
    public const ACCESS_API_FACTION = 2100000;

    /**
     * @var int
     */
    public const ACCESS_API_MODEL = 2200000;

    /**
     * @var int
     */
    public const ACCESS_API_KICK = 2300000;

    /**
     * @var int
     */
    public const ACCESS_API_SPECTATE = 2400000;

    /**
     * @var int
     */
    public const ACCESS_API_FREEZE = 2500000;

    /**
     * @var int
     */
    public const ACCESS_API_KILL = 2600000;

    /**
     * @var int
     */
    public const ACCESS_API_PRISON = 2700000;

    /**
     * @var int
     */
    public const ACCESS_API_NAMES = 2800000;

    /**
     * @var int
     */
    public const ACCESS_API_REPAIR_ID = 2900000;

    /**
     * @var int
     */
    public const ACCESS_API_REPAIR_DRIVER = 3000000;

    /**
     * @var int
     */
    public const ACCESS_API_PARK = 3100000;

    /**
     * @var int
     */
    public const ACCESS_API_MESSAGE = 3200000;

    /**
     * @var int
     */
    public const ACCESS_API_SERVER = 3300000;

    /**
     * @var int
     */
    public const ACCESS_NEWS = 300000000;

    /**
     * @var int
     */
    public const ACCESS_NEWS_CATEGORY = 310000000;

    /**
     * @var int
     */
    public const ACCESS_NEWS_EDIT = 320000000;

    /**
     * @var int
     */
    public const ACCESS_NEWS_EDIT_DELETE = 330000000;
}
