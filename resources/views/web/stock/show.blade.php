<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 31.08.2021
 * Time: 14:52
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('web.layout.app')
@section('url')
    https://eternityV.de/börse/anzeige
@endsection
@section('content')
    <div class="container transparentBg bg-white text-dark min-vh-100">
        @include('web.stock.component.nav',[])
        <div class="stock" id="{{ $stock->getId() }}" data-name="{{$stock->getName()}}"
             data-route="{{ route('web.stock.getStockData', $stock->getId()) }}">
            <div class="col-12 text-center">
                <div class="row">
                    <div class="col-12 col-lg-4 text-center m-2 font-weight-bold">
                        {{ $stock->getName() }}
                    </div>
                    <div class="col m-2">
                        {{ $stock->getPrice() }}$
                    </div>
                    <div class="col">
                        <p class="border shadow-sm rounded m-2 bg-secondary {{ ($stock->getPercentageChange() < 0) ? 'bg-danger' : '' }}
                        {{ ($stock->getPercentageChange() > 0) ? 'bg-success' : '' }}">
                            {{ $stock->getPercentageChange() }}%
                        </p>
                    </div>
                    <div class="col m-2 text-secondary
                                {{ ($stock->getPercentageChange() < 0) ? 'text-danger' : '' }}
                    {{ ($stock->getPercentageChange() > 0) ? 'text-success' : '' }}">
                        {{ ($stock->getPercentageChange() > 0) ? '+' : ''}}{{ number_format($stock->getPrice() -$stock->getLastPrice(), 2) }}
                        $ aktueller Tag
                    </div>
                </div>
            </div>
            <canvas id="stockCanvas{{$stock->getId()}}"></canvas>
            <div class="row mt-2">
                <div class="col-12">
                    <a href="{{ route('web.stock.buy',[$stock->getId(),$hash]) }}" class="btn btn-primary float-right">
                        Kaufen für {{ $stock->getPrice() }}$
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
