<?php
/**
 * WebAdminInfringement
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 24.12.2019
 * Time: 20:17
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class WebAdminInfringement
 * @package App\Model
 */
class WebAdminInfringement extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_USER_ID = 'userId';

    /**
     * @var string
     */
    const COLUMN_USER_NAME = 'userName';

    /**
     * @var string
     */
    const COLUMN_VIDEO_LINK = 'videoLink';

    /**
     * @var string
     */
    const COLUMN_REASON = 'reason';

    /**
     * @var string
     */
    const COLUMN_REPORTED_BY = 'reportedBy';

    /**
     * @var string
     */
    const COLUMN_DESCRIPTION = 'description';

    /**
     * @var string
     */
    const COLUMN_ACTION = 'action';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webAdminInfringement';

    /**
     * @return BelongsTo
     */
    public function account(): BelongsTo
    {
        return $this->belongsTo(Account::class, self::COLUMN_USER_ID, Account::COLUMN_ID);
    }

    /**
     * @param Dto\WebAdminInfringement $dto
     */
    public function insertEntry(Dto\WebAdminInfringement $dto)
    {
        $this->insert([
            self::COLUMN_ID          => $dto->getId(),
            self::COLUMN_USER_ID     => $dto->getUserId(),
            self::COLUMN_USER_NAME   => $dto->getUserName(),
            self::COLUMN_VIDEO_LINK  => $dto->getVideoLink(),
            self::COLUMN_REASON      => $dto->getReason(),
            self::COLUMN_REPORTED_BY => $dto->getReportedBy(),
            self::COLUMN_DESCRIPTION => $dto->getDescription(),
            self::COLUMN_ACTION      => $dto->getAction(),
            self::CREATED_AT         => Carbon::now(),
            self::UPDATED_AT         => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\WebAdminInfringement $dto
     */
    public function updateEntry(Dto\WebAdminInfringement $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update([
            self::COLUMN_USER_ID     => $dto->getUserId(),
            self::COLUMN_USER_NAME   => $dto->getUserName(),
            self::COLUMN_VIDEO_LINK  => $dto->getVideoLink(),
            self::COLUMN_REASON      => $dto->getReason(),
            self::COLUMN_REPORTED_BY => $dto->getReportedBy(),
            self::COLUMN_DESCRIPTION => $dto->getDescription(),
            self::COLUMN_ACTION      => $dto->getAction(),
            self::UPDATED_AT         => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\WebAdminInfringement $dto
     */
    public function deleteEntry(\App\Model\Dto\WebAdminInfringement $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebAdminInfringement $entry
     * @return Dto\WebAdminInfringement
     */
    public static function entryAsDto(WebAdminInfringement $entry): Dto\WebAdminInfringement
    {
        return (new Dto\WebAdminInfringement())
            ->setId($entry->{self::COLUMN_ID})
            ->setUserId($entry->{self::COLUMN_USER_ID})
            ->setUserName($entry->{self::COLUMN_USER_NAME})
            ->setVideoLink($entry->{self::COLUMN_VIDEO_LINK})
            ->setReason($entry->{self::COLUMN_REASON})
            ->setReportedBy($entry->{self::COLUMN_REPORTED_BY})
            ->setDescription($entry->{self::COLUMN_DESCRIPTION})
            ->setAction($entry->{self::COLUMN_ACTION})
            ->setCreatedAt($entry->{self::CREATED_AT})
            ->setUpdatedAt($entry->{self::UPDATED_AT});
    }
}
