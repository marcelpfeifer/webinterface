<?php
/**
 * Mode
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 14.04.2020
 * Time: 15:33
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Enum\Twitter;


class Mode
{
    /**
     * @var string
     */
    const ALL = 'all';

    /**
     * @var string
     */
    const FOLLOWED = 'followed';

    /**
     * @var string
     */
    const SELF = 'self';
}
