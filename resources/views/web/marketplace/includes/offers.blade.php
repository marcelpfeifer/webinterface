<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 07.05.2020
 * Time: 19:22
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
<div class="row min-vh-100">
    <div class="col-12">
        <br/>
        @if(count($offers) == 0)
            Keine Angebote
        @else
            <div class="row m-2 align-content-center">
                <div class="col-12">
                    <h2 class="text-center"> @yield('pageTitle') </h2>
                </div>
                <div class="col-12">
                    @foreach($offers as $offer)
                        <div class="row border border-dark rounded-bottom">
                            <div class="col-12 m-1">
                                <div class="row">
                                    <div class="col-3">
                                        <img class="img-fluid" src="{{ $offer->getImageUrl() }}">
                                    </div>
                                    <div class="col-9">
                                        <div class="row">
                                            <div class="col-12">
                                                {!! $offer->getMessage() !!}
                                            </div>
                                        </div>
                                        <div class="border m-1"></div>
                                        <div class="row">
                                            <div class="col-12 text-right">
                                                Anzeige erstellt von {{ $offer->getName() }} am
                                                {{ $offer->getCreateAt()->format('Y-m-d H:i:s') }}
                                            </div>
                                            @if($character->getId() == $offer->getCharacterId())
                                                <div class="col-12 text-right">
                                                    <form action="{{ route('web.marketplace.delete',
                                                        [ 'id' => $offer->getId(), 'hash' => $hash]) }}" method="POST">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="_method" value="DELETE"/>
                                                        <button class="btn btn-danger" type="submit">Löschen
                                                        </button>
                                                    </form>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <br/>
                    @endforeach
                </div>
            </div>
        @endif

    </div>
</div>
