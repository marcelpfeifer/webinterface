<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableNames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::rename('WebAdminBan','webAdminBan');
       Schema::rename('WebAdminInfringement','webAdminInfringement');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('webAdminBan','WebAdminBan');
        Schema::rename('webAdminInfringement','WebAdminInfringement');
    }
}
