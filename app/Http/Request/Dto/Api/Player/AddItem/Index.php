<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.10.2021
 * Time: 15:35
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Api\Player\AddItem;


use App\Http\Request\Dto\ADto;

class Index extends ADto
{
    /**
     * @var int
     */
    private $currentPlayer = 0;

    /**
     * @return int
     */
    public function getCurrentPlayer(): int
    {
        return $this->currentPlayer;
    }

    /**
     * @param int $currentPlayer
     * @return Index
     */
    public function setCurrentPlayer(int $currentPlayer): Index
    {
        $this->currentPlayer = $currentPlayer;
        return $this;
    }
}
