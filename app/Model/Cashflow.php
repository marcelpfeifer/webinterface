<?php
/**
 * Cashflow
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 13.08.2020
 * Time: 20:42
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use App\Model\Dto\Cashflow\TotalAmount;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Cashflow extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_GUID = 'guid';

    /**
     * @var string
     */
    const COLUMN_AMOUNT = 'amount';

    /**
     * @var string
     */
    const COLUMN_REASON = 'reason';

    /**
     * @var string
     */
    const COLUMN_SOURCE = 'source';

    /**
     * @var string
     */
    const COLUMN_TYPE = 'type';

    /**
     * @var string
     */
    const COLUMN_CREATED_AT = 'createdAt';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'cashflow';

    /**
     * @var string[]
     */
    protected $dates = [
        self::COLUMN_CREATED_AT,
    ];

    /**
     * @param Dto\Cashflow $dto
     */
    public function insertEntry(Dto\Cashflow $dto)
    {
        $this->insert(
            [
                self::COLUMN_GUID       => $dto->getGuid(),
                self::COLUMN_AMOUNT     => $dto->getAmount(),
                self::COLUMN_REASON     => $dto->getReason(),
                self::COLUMN_SOURCE     => $dto->getSource(),
                self::COLUMN_TYPE       => $dto->getType(),
                self::COLUMN_CREATED_AT => Carbon::now(),
            ]
        );
    }

    /**
     * @param int $guid
     * @return TotalAmount
     */
    public function getEntriesByGuid(int $guid)
    {
        $entries = $this
            ->where(self::COLUMN_GUID, '=', $guid)
            ->orderBy(self::COLUMN_CREATED_AT, 'desc')
            ->get();

        $cashFlows = [];
        $totalAmount = 0;
        foreach ($entries as $entry) {
            $dto = self::entryAsDto($entry);
            $totalAmount += $dto->getAmount();
            $cashFlows[] = $dto;
        }

        $dto = (new TotalAmount())
            ->setTotalAmount($totalAmount)
            ->setCashFlows($cashFlows);
        return $dto;
    }

    /**
     * @param Dto\Cashflow $dto
     */
    public function updateEntry(Dto\Cashflow $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_ID     => $dto->getId(),
                self::COLUMN_GUID   => $dto->getGuid(),
                self::COLUMN_AMOUNT => $dto->getAmount(),
                self::COLUMN_REASON => $dto->getReason(),
                self::COLUMN_SOURCE => $dto->getSource(),
                self::COLUMN_TYPE   => $dto->getType(),
            ]
        );
    }

    /**
     * @param Dto\Cashflow $dto
     */
    public function deleteEntry(Dto\Cashflow $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param Cashflow $entry
     * @return Dto\Cashflow
     */
    public static function entryAsDto(Cashflow $entry): Dto\Cashflow
    {
        return (new Dto\Cashflow())
            ->setId($entry->{self::COLUMN_ID})
            ->setGuid($entry->{self::COLUMN_GUID})
            ->setReason($entry->{self::COLUMN_REASON})
            ->setAmount($entry->{self::COLUMN_AMOUNT})
            ->setSource($entry->{self::COLUMN_SOURCE})
            ->setType($entry->{self::COLUMN_TYPE})
            ->setCreatedAt($entry->{self::COLUMN_CREATED_AT});
    }
}

