<?php
/**
 * RepairController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 13.04.2020
 * Time: 22:43
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Api\Vehicle;
use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Libraries\Api\Player\Online;
use App\Libraries\Api\Vehicle\Repair;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class RepairController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        $players = Online::getPlayers();
        return view('api.vehicle.repair.index', [
            'players' => $players,
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function save(Request $request): RedirectResponse
    {
        $pid = $request->input('pid');

        if (Repair::call($pid)) {
            $messages[] = (new Message())
                ->setMessage('Erfolgreich ausgeführt')
                ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        } else {
            $messages[] = (new Message())
                ->setMessage('Anfrage fehlgeschlagen')
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
        }

        return redirect()->route('api.vehicle.repair.index')->with('messages', $messages);
    }
}
