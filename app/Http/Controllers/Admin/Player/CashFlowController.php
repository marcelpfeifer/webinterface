<?php
/**
 * CashFlowController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 19.09.2020
 * Time: 10:06
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\Player;

use App\Http\Controllers\Controller;
use App\Model\Cashflow;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CashFlowController extends Controller
{

    /**
     * @param Request $request
     * @param int $charId
     * @return View
     */
    public function index(Request $request, int $charId): View
    {
        $dto = (new Cashflow())->getEntriesByGuid($charId);
        return view(
            'admin.player.cashFlow.index',
            [
                'dto'         => $dto,
                'characterId' => $charId,
            ]
        );
    }
}
