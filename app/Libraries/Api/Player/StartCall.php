<?php
/**
 * StartCall
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 13.08.2020
 * Time: 20:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Api\Player;

use App\Libraries\Api\AClient;
use App\Model\WebConfig;

class StartCall extends AClient
{
    /**
     * @var string
     */
    const URL = 'player/startCall';

    /**
     * @param int $pid
     * @param int $targetId
     * @param bool $anonymous
     * @return bool
     */
    public static function call(int $pid, int $targetId, bool $anonymous): bool
    {
        $response = self::sendRequest(
            'POST',
            WebConfig::getEntryByKey('api_url')->getValue() . self::URL,
            [
                'query' => [
                    'pid'       => $pid,
                    'targetId'  => $targetId,
                    'anonymous' => $anonymous,
                ]
            ],
            false
        );

        return $response && $response->getStatusCode() == 200;
    }
}
