<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 01.05.2020
 * Time: 12:36
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('web.layout.app')
@section('url')
    https://eternityV.de/marktplatz/erstellen
@endsection
@section('content')
    <script>
        $(document).ready(function () {
            var quill = new Quill('#editor', {
                theme: 'snow'
            });
            $('form').on('submit', function () {
                $('#message').val($('#editor').find('.ql-editor').html());
            });
        });
    </script>
    <div class="container bg-white text-dark">
        <div class="row">
            <div class="col-12">
                @include('web.marketplace.includes.nav')
                @include('layouts.messages')
                <br/>
                <h2> Neues Angebot erstellen</h2>
                <form method="post" class="form" action="{{ route('web.marketplace.save', $hash) }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="categoryId">Kategorie</label>
                        <select class="form-control" id="categoryId" name="categoryId">
                            <option value="">Keine Kategorie</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->getId() }}">{{ $category->getName() }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-row">
                        <div class="col-12">
                            <label for="imageUrl">Bild</label>
                            <input class="form-control" id="imageUrl" name="imageUrl" type="text">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-12">
                            <label for="message">Text</label>
                            <div id="editor" class="form-control">

                            </div>
                            <input type="hidden" class="form-control" id="message" name="message">
                        </div>
                    </div>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <div class="form-row justify-content-end">
                        <button type="submit" class="btn btn-primary">Erstellen</button>
                    </div>
                </form>
                <br/>
            </div>
        </div>
    </div>
@endsection
