<?php
/**
 * Vanish.php
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 05.04.2020
 * Time: 12:13
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Api\Player;

use App\Libraries\Api\AClient;
use App\Model\WebConfig;
use Illuminate\Support\Facades\Log;

class Fly extends AClient
{
    /**
     * @var string
     */
    const URL = 'player/fly';

    /**
     * @param int $pid
     * @param bool $value
     * @param bool $stealth
     * @return bool
     */
    public static function call(int $pid, bool $value, bool $stealth): bool
    {
        $response = self::sendRequest('POST', WebConfig::getEntryByKey('api_url')->getValue() . self::URL, [
            'query' => [
                'pid'   => $pid,
                'value' => $value,
                'stealth' => $stealth,
            ]
        ]);
        return $response && $response->getStatusCode() == 200;
    }
}
