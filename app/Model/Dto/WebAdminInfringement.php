<?php
/**
 * WebAdminInfringement
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 24.12.2019
 * Time: 20:29
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;


use App\Libraries\Logging\ILogging;
use App\Libraries\Logging\Traits\Variables;
use Carbon\Carbon;

class WebAdminInfringement implements ILogging
{

    use Variables;

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var null|int
     */
    private $userId = null;

    /**
     * @var string
     */
    private $userName = '';

    /**
     * @var string
     */
    private $videoLink = '';

    /**
     * @var string
     */
    private $reason = '';

    /**
     * @var string
     */
    private $reportedBy = '';

    /**
     * @var string
     */
    private $description = '';

    /**
     * @var string
     */
    private $action = '';

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @var null|Carbon
     */
    private $updatedAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebAdminInfringement
     */
    public function setId(?int $id): WebAdminInfringement
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @param int|null $userId
     * @return WebAdminInfringement
     */
    public function setUserId(?int $userId): WebAdminInfringement
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @param string $userName
     * @return WebAdminInfringement
     */
    public function setUserName(string $userName): WebAdminInfringement
    {
        $this->userName = $userName;
        return $this;
    }

    /**
     * @return string
     */
    public function getVideoLink(): string
    {
        return $this->videoLink;
    }

    /**
     * @param string $videoLink
     * @return WebAdminInfringement
     */
    public function setVideoLink(string $videoLink): WebAdminInfringement
    {
        $this->videoLink = $videoLink;
        return $this;
    }

    /**
     * @return string
     */
    public function getReason(): string
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     * @return WebAdminInfringement
     */
    public function setReason(string $reason): WebAdminInfringement
    {
        $this->reason = $reason;
        return $this;
    }

    /**
     * @return string
     */
    public function getReportedBy(): string
    {
        return $this->reportedBy;
    }

    /**
     * @param string $reportedBy
     * @return WebAdminInfringement
     */
    public function setReportedBy(string $reportedBy): WebAdminInfringement
    {
        $this->reportedBy = $reportedBy;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return WebAdminInfringement
     */
    public function setDescription(string $description): WebAdminInfringement
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return WebAdminInfringement
     */
    public function setAction(string $action): WebAdminInfringement
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return WebAdminInfringement
     */
    public function setCreatedAt(?Carbon $createdAt): WebAdminInfringement
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon|null $updatedAt
     * @return WebAdminInfringement
     */
    public function setUpdatedAt(?Carbon $updatedAt): WebAdminInfringement
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
