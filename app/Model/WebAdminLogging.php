<?php
/**
 * WebAdminLogging
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 10.04.2020
 * Time: 09:25
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class WebAdminLogging extends Model
{

    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_ACCOUNT_ID = 'accountId';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    const COLUMN_ACTION = 'action';

    /**
     * @var string
     */
    const COLUMN_DATE_TIME = 'dateTime';

    /**
     * @var string
     */
    protected $table = 'webAdminLogging';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $dates = [
        self::COLUMN_DATE_TIME
    ];

    /**
     * @return LengthAwarePaginator
     */
    public function getLogEntriesPagination(): LengthAwarePaginator
    {
        return $this->orderBy(self::COLUMN_DATE_TIME, 'DESC')->paginate(15);
    }

    /**
     * @param Dto\WebAdminLogging $dto
     */
    public function insertEntry(Dto\WebAdminLogging $dto)
    {
        return $this->insertGetId(
            [
                self::COLUMN_ACCOUNT_ID => $dto->getAccountId(),
                self::COLUMN_NAME       => $dto->getName(),
                self::COLUMN_ACTION     => $dto->getAction(),
                self::COLUMN_DATE_TIME  => $dto->getDateTime(),
            ]
        );
    }

    /**
     * @param Dto\WebAdminLogging $dto
     */
    public function updateEntry(Dto\WebAdminLogging $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_ACCOUNT_ID => $dto->getAccountId(),
                self::COLUMN_NAME       => $dto->getName(),
                self::COLUMN_ACTION     => $dto->getAction(),
                self::COLUMN_DATE_TIME  => $dto->getDateTime(),
            ]
        );
    }

    /**
     * @param Dto\WebAdminLogging $dto
     */
    public function deleteEntry(Dto\WebAdminLogging $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebAdminLogging $entry
     * @return Dto\WebAdminLogging
     */
    public static function entryAsDto(WebAdminLogging $entry): Dto\WebAdminLogging
    {
        return (new Dto\WebAdminLogging())
            ->setId($entry->{self::COLUMN_ID})
            ->setName($entry->{self::COLUMN_NAME})
            ->setAction($entry->{self::COLUMN_ACTION})
            ->setDateTime($entry->{self::COLUMN_DATE_TIME});
    }
}
