<?php
/**
 * WebServerStatus
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 11.08.2021
 * Time: 16:22
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;


use Carbon\Carbon;

class WebServerStatus
{

    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var int
     */
    private $ramMax = 0;

    /**
     * @var int
     */
    private $ramFree = 0;

    /**
     * @var float
     */
    private $cpu = 0.00;

    /**
     * @var boolean
     */
    private $altv = false;

    /**
     * @var boolean
     */
    private $altvTest = false;

    /**
     * @var boolean
     */
    private $ts = false;

    /**
     * @var boolean
     */
    private $forum = false;

    /**
     * @var Carbon
     */
    private $createdAt;

    /**
     * @var Carbon
     */
    private $updatedAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return WebServerStatus
     */
    public function setId(int $id): WebServerStatus
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getRamMax(): int
    {
        return $this->ramMax;
    }

    /**
     * @param int $ramMax
     * @return WebServerStatus
     */
    public function setRamMax(int $ramMax): WebServerStatus
    {
        $this->ramMax = $ramMax;
        return $this;
    }

    /**
     * @return int
     */
    public function getRamFree(): int
    {
        return $this->ramFree;
    }

    /**
     * @param int $ramFree
     * @return WebServerStatus
     */
    public function setRamFree(int $ramFree): WebServerStatus
    {
        $this->ramFree = $ramFree;
        return $this;
    }

    /**
     * @return float
     */
    public function getCpu(): float
    {
        return $this->cpu;
    }

    /**
     * @param float $cpu
     * @return WebServerStatus
     */
    public function setCpu(float $cpu): WebServerStatus
    {
        $this->cpu = $cpu;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon $createdAt
     * @return WebServerStatus
     */
    public function setCreatedAt(Carbon $createdAt): WebServerStatus
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getUpdatedAt(): Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon $updatedAt
     * @return WebServerStatus
     */
    public function setUpdatedAt(Carbon $updatedAt): WebServerStatus
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAltv(): bool
    {
        return $this->altv;
    }

    /**
     * @param bool $altv
     * @return WebServerStatus
     */
    public function setAltv(bool $altv): WebServerStatus
    {
        $this->altv = $altv;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAltvTest(): bool
    {
        return $this->altvTest;
    }

    /**
     * @param bool $altvTest
     * @return WebServerStatus
     */
    public function setAltvTest(bool $altvTest): WebServerStatus
    {
        $this->altvTest = $altvTest;
        return $this;
    }

    /**
     * @return bool
     */
    public function isTs(): bool
    {
        return $this->ts;
    }

    /**
     * @param bool $ts
     * @return WebServerStatus
     */
    public function setTs(bool $ts): WebServerStatus
    {
        $this->ts = $ts;
        return $this;
    }

    /**
     * @return bool
     */
    public function isForum(): bool
    {
        return $this->forum;
    }

    /**
     * @param bool $forum
     * @return WebServerStatus
     */
    public function setForum(bool $forum): WebServerStatus
    {
        $this->forum = $forum;
        return $this;
    }


}
