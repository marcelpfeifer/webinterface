<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 30.06.2021
 * Time: 17:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
<h4>Führerschein</h4>
<div class="form-group">
    <label for="drivingLicenseA">A</label>
    <select class="form-control" id="drivingLicenseA" name="drivingLicenseA">
        <option value="0" {{ ($character->getDrivingLicenseA() == 0) ? 'selected' : '' }}>
            Nein
        </option>
        <option value="1" {{ ($character->getDrivingLicenseA() == 1) ? 'selected' : '' }}>Ja</option>
    </select>
</div>
<br/>
<div class="form-group">
    <label for="drivingLicenseB">B</label>
    <select class="form-control" id="drivingLicenseB" name="drivingLicenseB">
        <option value="0" {{ ($character->getDrivingLicenseB() == 0) ? 'selected' : '' }}>
            Nein
        </option>
        <option value="1" {{ ($character->getDrivingLicenseB() == 1) ? 'selected' : '' }}>Ja</option>
    </select>
</div>
<br/>
<div class="form-group">
    <label for="drivingLicenseCE">CE</label>
    <select class="form-control" id="drivingLicenseCE" name="drivingLicenseCE">
        <option value="0" {{ ($character->getDrivingLicenseCE()== 0) ? 'selected' : '' }}>
            Nein
        </option>
        <option value="1" {{ ($character->getDrivingLicenseCE() == 1) ? 'selected' : '' }}>Ja</option>
    </select>
</div>
<br/>
<h4>Lizenzen</h4>
<div class="form-group">
    <label for="gunLicense">Waffen</label>
    <select class="form-control" id="gunLicense" name="gunLicense">
        <option value="0" {{ ($character->getGunLicense()== 0) ? 'selected' : '' }}>Nein</option>
        <option value="1" {{ ($character->getGunLicense() == 1) ? 'selected' : '' }}>Ja</option>
    </select>
</div>
<br/>
