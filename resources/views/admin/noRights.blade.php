<?php
/**
 * noRights.blade.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 22.08.2019
 * Time: 21:43
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Keine Berechtigungen!!!!</div>
                    <div class="card-body">
                        Du hast keine Berechtigung das hier aufzurufen!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection