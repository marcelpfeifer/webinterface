<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.01.2021
 * Time: 19:10
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>

@permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_PLAYER_EDIT_WHITELIST)
<h4>Whitelist</h4>
<div class="form-group">
    <label for="whitelist">Auf der Whitelist</label>
    <select class="form-control" id="whitelist" name="whitelist">
        <option value="0" {{ ($account->whitelisted == 0) ? 'selected' : '' }}>
            Nein
        </option>
        <option value="1" {{ ($account->whitelisted == 1) ? 'selected' : '' }}>Ja</option>
    </select>
</div>
<br/>
@endpermission
