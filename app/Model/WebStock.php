<?php
/**
 * WebStock
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 28.08.2021
 * Time: 16:44
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class WebStock extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    const COLUMN_PRICE = 'price';

    /**
     * @var string
     */
    const COLUMN_STOCK = 'stock';

    /**
     * @var string
     */
    const COLUMN_MIN_CHANGE = 'minChange';

    /**
     * @var string
     */
    const COLUMN_MAX_CHANGE = 'maxChange';

    /**
     * @var string
     */
    const COLUMN_LAST_PRICE = 'lastPrice';


    /**
     * @var string
     */
    const COLUMN_PERCENTAGE_CHANGE = 'percentageChange';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webStock';

    /**
     * @return HasMany
     */
    public function changes(): HasMany
    {
        return $this->hasMany(WebMailAttachment::class, WebMailAttachment::COLUMN_WEB_MAIL_ID, self::COLUMN_ID);
    }

    /**
     * @param Dto\WebStock $dto
     * @return int
     */
    public function insertEntry(Dto\WebStock $dto): int
    {
        return $this->insertGetId(
            [
                self::COLUMN_NAME              => $dto->getName(),
                self::COLUMN_PRICE             => $dto->getPrice(),
                self::COLUMN_STOCK             => $dto->getStock(),
                self::COLUMN_MIN_CHANGE        => $dto->getMinChange(),
                self::COLUMN_MAX_CHANGE        => $dto->getMaxChange(),
                self::COLUMN_LAST_PRICE        => $dto->getLastPrice(),
                self::COLUMN_PERCENTAGE_CHANGE => $dto->getPercentageChange(),
                self::CREATED_AT               => Carbon::now(),
                self::UPDATED_AT               => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebStock $dto
     */
    public function updateEntry(Dto\WebStock $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_NAME              => $dto->getName(),
                self::COLUMN_PRICE             => $dto->getPrice(),
                self::COLUMN_STOCK             => $dto->getStock(),
                self::COLUMN_MIN_CHANGE        => $dto->getMinChange(),
                self::COLUMN_MAX_CHANGE        => $dto->getMaxChange(),
                self::COLUMN_LAST_PRICE        => $dto->getLastPrice(),
                self::COLUMN_PERCENTAGE_CHANGE => $dto->getPercentageChange(),
                self::UPDATED_AT               => Carbon::now(),
            ]
        );
    }

    /**
     * @param int $id
     * @param int $amount
     */
    public function subStock(int $id, int $amount)
    {
        $this->where(self::COLUMN_ID, $id)->decrement(self::COLUMN_STOCK, $amount);
    }

    /**
     * @param int $id
     * @param int $amount
     */
    public function addStock(int $id, int $amount)
    {
        $this->where(self::COLUMN_ID, $id)->increment(self::COLUMN_STOCK, $amount);
    }
    /**
     * @param Dto\WebStock $dto
     */
    public function deleteEntry(\App\Model\Dto\WebStock $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebStock $entry
     * @return Dto\WebStock
     */
    public static function entryAsDto(WebStock $entry): Dto\WebStock
    {
        return (new Dto\WebStock())
            ->setId($entry->{self::COLUMN_ID})
            ->setName($entry->{self::COLUMN_NAME})
            ->setPrice($entry->{self::COLUMN_PRICE})
            ->setStock($entry->{self::COLUMN_STOCK})
            ->setMinChange($entry->{self::COLUMN_MIN_CHANGE})
            ->setMaxChange($entry->{self::COLUMN_MAX_CHANGE})
            ->setLastPrice($entry->{self::COLUMN_LAST_PRICE})
            ->setPercentageChange($entry->{self::COLUMN_PERCENTAGE_CHANGE})
            ->setCreatedAt($entry->{self::CREATED_AT})
            ->setUpdatedAt($entry->{self::UPDATED_AT});
    }
}
