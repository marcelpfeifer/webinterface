<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 05.01.2021
 * Time: 03:05
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-clipboard"></i>
    Log-Eintrag
    <a class="btn btn-dark btn-sm float-right"
       href="{{ route('admin.log.index') }}"
       role="button"
    >
        Zurück
    </a>
@endsection
@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Table</th>
            <th scope="col">Column</th>
            <th scope="col">Alter Wert</th>
            <th scope="col">Neuer Wert</th>
        </tr>
        </thead>
        <tbody>
        @foreach($entries as $entry)
            <tr>
                <th scope="row">{{ $entry->getId() }}</th>
                <td>{{ $entry->getTableName() }}</td>
                <td>{{ $entry->getColumnName() }}</td>
                <td>{{ $entry->getOldValue() }}</td>
                <td>{{ $entry->getNewValue() }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
