<?php
/**
 * MailboxController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 12.08.2021
 * Time: 11:54
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Auth\Mail;


use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Http\Model\RandomString;
use App\Http\Request\Auth\Mail\Mailbox\AddSave;
use App\Http\Request\Auth\Mail\Mailbox\Save;
use App\Http\Request\AValidator;
use App\Model\WebMailAddress;
use App\Model\WebMailAddressToAccount;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class MailboxController extends Controller
{

    /**
     * @return View
     */
    public function index(): View
    {
        $addresses = (new WebMailAddress())->getEntriesForAccount(Auth::id());
        return view(
            'auth.mailbox.index',
            [
                'addresses' => $addresses,
            ]
        );
    }

    /**
     * @return View
     */
    public function add(): View
    {
        return view('auth.mailbox.add');
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return view('auth.mailbox.create');
    }

    /**
     * @return View
     */
    public function edit(Request $request, int $webMailAddressId): View
    {
        $mailAddress = $this->getMailBoxEntry($webMailAddressId);
        if (!$mailAddress) {
            return view('404');
        }
        $characters = (new WebMailAddressToAccount())->getEntriesByAddressId($webMailAddressId);
        return view(
            'auth.mailbox.edit',
            [
                'webMailAddressId' => $webMailAddressId,
                'characters'         => $characters,
            ]
        );
    }

    /**
     * @return RedirectResponse
     */
    public function generatePassword(Request $request, int $webMailAddressId): RedirectResponse
    {
        $mailAddress = $this->getMailBoxEntry($webMailAddressId);
        if (!$mailAddress) {
            return redirect()->route('auth.mailbox.index');
        }
        $mailAddress->setPassword(RandomString::generate());
        (new WebMailAddress())->updateEntry($mailAddress);
        return redirect()->route('auth.mailbox.index');
    }

    /**
     * @return RedirectResponse
     */
    public function deleteAccount(Request $request, int $webMailAddressId, int $accountId): RedirectResponse
    {
        $mailAddress = $this->getMailBoxEntry($webMailAddressId);
        if (!$mailAddress) {
            return redirect()->route('auth.mailbox.index');
        }

        $dto = (new \App\Model\Dto\WebMailAddressToAccount())
            ->setWebMailAddressId($webMailAddressId)
            ->setAccountId($accountId);
        (new WebMailAddressToAccount())->deleteEntry($dto);
        return redirect()->route('auth.mailbox.edit', $webMailAddressId);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function save(Request $request): RedirectResponse
    {
        // Validiere die Anfrage
        $validator = (new Save($request));
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            return redirect()->route('auth.mailbox.create')
                ->with('messages', $validator->getErrorsAsMessageArray($validatedData))
                ->withInput()
                ->withErrors($validatedData->errors());
        }

        /**
         * @var $requestDto \App\Http\Request\Dto\Auth\Mail\Mailbox\Save
         */
        $requestDto = $validator->toDto();

        \App\Http\Model\Auth\Mail\Mailbox\Save::save($requestDto, Auth::id());

        $messages[] = (new Message())
            ->setMessage("Email Account wurde erfolgreich angelegt")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('auth.mailbox.index')->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function addSave(Request $request): RedirectResponse
    {
        // Validiere die Anfrage
        $validator = (new AddSave($request));
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            return redirect()->route('auth.mailbox.add')
                ->with('messages', $validator->getErrorsAsMessageArray($validatedData))
                ->withInput()
                ->withErrors($validatedData->errors());
        }

        /**
         * @var $requestDto \App\Http\Request\Dto\Auth\Mail\Mailbox\AddSave
         */
        $requestDto = $validator->toDto();

        $worked = \App\Http\Model\Auth\Mail\Mailbox\AddSave::save($requestDto, Auth::id());

        if ($worked) {
            $messages[] = (new Message())
                ->setMessage("Email Account wurde erfolgreich angelegt")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        } else {
            $messages[] = (new Message())
                ->setMessage("Email Account konnte nicht hinzugefügt werden")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
        }
        return redirect()->route('auth.mailbox.index')->with('messages', $messages);
    }

    /**
     * @param int $webMailAddressId
     * @return \App\Model\Dto\WebMailAddress|null
     */
    private function getMailBoxEntry(int $webMailAddressId)
    {
        $entry = (new WebMailAddressToAccount())->getEntryByAddressIdAndAccountId(
            $webMailAddressId,
            Auth::id()
        );
        return $entry ? WebMailAddress::entryAsDto($entry->webMailAddress) : null;
    }
}
