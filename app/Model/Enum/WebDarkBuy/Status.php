<?php
/**
 * Type
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 09.08.2020
 * Time: 09:47
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Enum\WebDarkBuy;

class Status
{

    /**
     * @var string
     */
    const OPEN = 'OPEN';

    /**
     * @var string
     */
    const BOUGHT = 'BOUGHT';
}
