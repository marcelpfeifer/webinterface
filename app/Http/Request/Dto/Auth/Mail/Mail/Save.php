<?php
/**
 * Auth
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 23.11.2020
 * Time: 09:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Auth\Mail\Mail;

use App\Http\Request\Dto\ADto;
use Illuminate\Http\UploadedFile;

class Save extends ADto
{

    /**
     * @var int
     */
    private $webMailAddressId = 0;

    /**
     * @var string
     */
    private $to = '';

    /**
     * @var string
     */
    private $title = '';

    /**
     * @var string
     */
    private $message = '';

    /**
     * @var UploadedFile[]
     */
    private $files = [];

    /**
     * @return int
     */
    public function getWebMailAddressId(): int
    {
        return $this->webMailAddressId;
    }

    /**
     * @param int $webMailAddressId
     * @return Save
     */
    public function setWebMailAddressId(int $webMailAddressId): Save
    {
        $this->webMailAddressId = $webMailAddressId;
        return $this;
    }

    /**
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * @param string $to
     * @return Save
     */
    public function setTo(string $to): Save
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Save
     */
    public function setTitle(string $title): Save
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return Save
     */
    public function setMessage(string $message): Save
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return UploadedFile[]
     */
    public function getFiles(): array
    {
        return $this->files;
    }

    /**
     * @param UploadedFile[] $files
     * @return Save
     */
    public function setFiles(array $files): Save
    {
        $this->files = $files;
        return $this;
    }
}
