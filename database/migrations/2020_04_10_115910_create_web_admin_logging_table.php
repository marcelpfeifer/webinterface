<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebAdminLoggingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webAdminLogging', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('accountId');
            $table->string('name');
            $table->enum('action', [
                'INSERT',
                'UPDATE',
                'DELETE'
            ]);
            $table->timestamp('dateTime')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('web_admin_logging');
    }
}
