<?php
/**
 * WebAdminLoggingApi
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 07.03.2021
 * Time: 13:27
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;


use Carbon\Carbon;

class WebAdminLoggingApi
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var int
     */
    private $accountId = 0;

    /**
     * @var string
     */
    private $url = '';

    /**
     * @var string
     */
    private $options = '';

    /**
     * @var Carbon
     */
    private $dateTime;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebAdminLoggingApi
     */
    public function setId(?int $id): WebAdminLoggingApi
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getAccountId(): int
    {
        return $this->accountId;
    }

    /**
     * @param int $accountId
     * @return WebAdminLoggingApi
     */
    public function setAccountId(int $accountId): WebAdminLoggingApi
    {
        $this->accountId = $accountId;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return WebAdminLoggingApi
     */
    public function setUrl(string $url): WebAdminLoggingApi
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getOptions(): string
    {
        return $this->options;
    }

    /**
     * @param string $options
     * @return WebAdminLoggingApi
     */
    public function setOptions(string $options): WebAdminLoggingApi
    {
        $this->options = $options;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getDateTime(): Carbon
    {
        return $this->dateTime;
    }

    /**
     * @param Carbon $dateTime
     * @return WebAdminLoggingApi
     */
    public function setDateTime(Carbon $dateTime): WebAdminLoggingApi
    {
        $this->dateTime = $dateTime;
        return $this;
    }
}
