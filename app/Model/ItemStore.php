<?php
/**
 * ItemStore
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 19.09.2020
 * Time: 09:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ItemStore extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_ITEM = 'item';

    /**
     * @var string
     */
    const COLUMN_PRICE = 'price';

    /**
     * @var string
     */
    const COLUMN_STOCK = 'stock';

    /**
     * @var string
     */
    const COLUMN_SHOP = 'shop';

    /**
     * @var string
     */
    const COLUMN_CREATED_AT = 'createdAt';

    /**
     * @var string
     */
    const COLUMN_UPDATED_AT = 'updatedAt';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $dates = [
        self::COLUMN_CREATED_AT,
        self::COLUMN_UPDATED_AT,
    ];

    /**
     * @var string
     */
    protected $table = 'item_store';

    /**
     * @param Dto\ItemStore $dto
     */
    public function insertEntry(Dto\ItemStore $dto)
    {
        $this->insert(
            [
                self::COLUMN_ID         => $dto->getId(),
                self::COLUMN_ITEM       => $dto->getItem(),
                self::COLUMN_PRICE      => $dto->getPrice(),
                self::COLUMN_STOCK      => $dto->getStock(),
                self::COLUMN_SHOP       => $dto->getShop(),
                self::COLUMN_CREATED_AT => Carbon::now(),
                self::COLUMN_UPDATED_AT => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\ItemStore $dto
     */
    public function updateEntry(Dto\ItemStore $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_ITEM       => $dto->getItem(),
                self::COLUMN_PRICE      => $dto->getPrice(),
                self::COLUMN_STOCK      => $dto->getStock(),
                self::COLUMN_SHOP       => $dto->getShop(),
                self::COLUMN_UPDATED_AT => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\ItemStore $dto
     */
    public function deleteEntry(\App\Model\Dto\ItemStore $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param ItemStore $entry
     * @return Dto\ItemStore
     */
    public static function entryAsDto(ItemStore $entry): Dto\ItemStore
    {
        return (new Dto\ItemStore())
            ->setId($entry->{self::COLUMN_ID})
            ->setItem($entry->{self::COLUMN_ITEM})
            ->setPrice($entry->{self::COLUMN_PRICE})
            ->setStock($entry->{self::COLUMN_STOCK})
            ->setShop($entry->{self::COLUMN_SHOP});
    }
}
