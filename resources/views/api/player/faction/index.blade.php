<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 13.04.2020
 * Time: 19:42
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('api.layout.app')
@section('title')
    <i class="fas fa-ambulance"></i>
    Setzt die Fraktion für einen Spieler
@endsection
@section('content')
    <form method="post" action="{{ route('api.player.faction.save') }}">
        {{ csrf_field() }}

        @include('api.includes.select', [
          'name' => 'pid',
          'label' => 'Spieler',
          'entries' => $players,
          'currentSelect' => $currentPlayer,
        ])

        <div class="form-group">
            <label for="faction">Auf der Whitelist</label>
            <select class="form-control" id="faction" name="faction">
                @foreach(\App\Http\Request\Enum\Api\Player\Faction\Faction::getConstants() as $constant)
                    <option value="{{ $constant }}">
                        {{ $constant }}
                    </option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-danger">Bestätigen</button>
    </form>
    <br/>
@endsection

