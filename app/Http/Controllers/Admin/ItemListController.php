<?php
/**
 * ItemListController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 12.05.2020
 * Time: 19:38
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Vehicle;
use App\Model\WebAdminItemList;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ItemListController extends Controller
{

    /**
     * @return View
     */
    public function index(): View
    {
        $itemNames = (new WebAdminItemList())->getPossibleNames();
        $vehicleNames = (new Vehicle())->getPossibleNames();

        return view(
            'admin.itemList.index',
            [
                'itemNames'    => $itemNames,
                'vehicleNames' => $vehicleNames,
            ]
        );
    }

    /**
     * @param Request $request
     * @return View
     */
    public function searchItemList(Request $request): View
    {
        $items = [];
        $itemNames = (new WebAdminItemList())->getPossibleNames();
        $vehicleNames = (new Vehicle())->getPossibleNames();

        $name = $request->input('name');
        if ($name) {
            $items = (new WebAdminItemList())->getItemsByName($name);
        }
        return view(
            'admin.itemList.index',
            [
                'items'        => $items,
                'itemNames'    => $itemNames,
                'vehicleNames' => $vehicleNames,
            ]
        );
    }

    /**
     * @param Request $request
     * @return View
     */
    public function searchVehicles(Request $request): View
    {
        $vehicles = [];
        $itemNames = (new WebAdminItemList())->getPossibleNames();
        $vehicleNames = (new Vehicle())->getPossibleNames();

        $name = $request->input('name');
        if ($name) {
            $vehicles = (new Vehicle())->getVehiclesByModelWithCharacter($name);
        }
        return view(
            'admin.itemList.index',
            [
                'vehicles'     => $vehicles,
                'itemNames'    => $itemNames,
                'vehicleNames' => $vehicleNames,
            ]
        );
    }
}
