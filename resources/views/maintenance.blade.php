<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 19.09.2020
 * Time: 11:35
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
<html>
<head>
    <title>Site Maintenance</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<body>
<div class="jumbotron jumbotron-fluid h-100">
    <div class="container text-center">
        <h1 class="display-4">Maintenance</h1>
        <p>Die Seite sollte gleich wieder verfügbar sein!</p>
        <p>- Eprox</p>
    </div>
</div>
</body>
</html>

