<?php
/**
 * BanController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 05.05.2020
 * Time: 17:00
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\Player;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Http\Model\Admin\Player\Ban\Save;
use App\Libraries\Api\Player\Kick;
use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Model\Bans;
use App\Model\WebAdminBanAccount;
use App\Model\WebAdminBanReason;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class BanController extends Controller
{

    /**
     * @param Request $request
     * @param int $accountId
     * @return View
     */
    public function index(Request $request, int $accountId): View
    {
        $bans = (new WebAdminBanAccount())->getBansByAccountId($accountId);
        return view(
            'admin.player.bans.index',
            [
                'bans'      => $bans,
                'reasons'   => WebAdminBanReason::all(),
                'accountId' => $accountId,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $accountId
     * @return View
     */
    public function serverBans(Request $request, int $accountId): View
    {
        $bans = (new Bans())->getBansByCharacterId($accountId);

        return view(
            'admin.player.bans.bans',
            [
                'bans'      => $bans,
                'accountId' => $accountId,
            ]
        );
    }

    /**
     * @param Request $request
     * @return View
     */
    public function create(Request $request): View
    {
        $accountId = $request->input('accountId');
        $reasonId = $request->input('reasonId');

        $reason = WebAdminBanReason::find($reasonId);
        $dto = WebAdminBanReason::entryAsDto($reason);

        return view(
            'admin.player.bans.create',
            [
                'dto'       => $dto,
                'accountId' => $accountId,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $accountId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|View
     */
    public function save(Request $request, int $accountId)
    {
        $dto = (new \App\Model\Dto\WebAdminBanAccount())
            ->setAccountId($accountId)
            ->setPoints($request->input('points'))
            ->setReason($request->input('reason'));
        (new WebAdminBanAccount())->insertEntry($dto);

        Save::save($dto, $request->input('userReason'));

        // Schreibe Log Eintrag
        Logging::insert(Name::BANS, Auth::id(), $dto);

        $messages[] = (new Message())
            ->setMessage('Erfolgreich aktualisiert')
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.player.bans.index', $accountId)->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|View
     */
    public function delete(Request $request, int $id)
    {
        $entry = WebAdminBanAccount::find($id);
        if (!($entry)) {
            $messages[] = (new Message())
                ->setMessage("Der Eintrag wurde nicht gefunden")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
            return redirect()->route('admin.player.index')->with('messages', $messages);
        }

        $dto = WebAdminBanAccount::entryAsDto($entry);
        (new WebAdminBanAccount())->deleteEntry($dto);
        Logging::delete(Name::BANS, Auth::id(), $dto);

        $messages[] = (new Message())
            ->setMessage('Erfolgreich gelöscht')
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.player.bans.index', $dto->getAccountId())->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|View
     */
    public function deleteServerBans(Request $request, int $id)
    {
        $db = new Bans();
        $dto = $db->getBanById($id);
        $db->deleteEntry($dto);

        // Schreibe Log Eintrag
        Logging::delete(Name::BANS, Auth::id(), $dto);

        $messages[] = (new Message())
            ->setMessage('Erfolgreich gelöscht')
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.player.bans.serverBans', $dto->getGuid())->with('messages', $messages);
    }
}
