<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 29.04.2020
 * Time: 17:25
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-box"></i>
    Eintrag erstellen
@endsection
@section('content')
    <form action="{{ route('admin.itemStore.store') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="item">Item</label>
            <input type="text" class="form-control" id="item" name="item" required>
        </div>
        <div class="form-group">
            <label for="price">Preis</label>
            <input type="number" class="form-control" id="price" name="price" required>
        </div>
        <div class="form-group">
            <label for="stock">Lager</label>
            <input type="number" class="form-control" id="stock" name="stock" required>
        </div>
        <div class="form-group">
            <label for="shop">Shop</label>
            <input type="text" class="form-control" id="shop" name="shop" required>
        </div>
        <br/>
        <button type="submit" class="btn btn-dark">Erstellen</button>
    </form>
@endsection

