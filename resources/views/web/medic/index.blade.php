<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 27.10.2021
 * Time: 09:12
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('web.layout.app')
@section('url')
    https://eternityV.de/medic/
@endsection
@section('content')
    <div class="embed-responsive embed-responsive-1by1">
        <iframe class="embed-responsive-item" src="https://mas.eternityv.de/share"></iframe>
    </div>
@endsection
