<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 01.05.2020
 * Time: 09:48
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\DarkWeb\DarkBuy;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Libraries\Api\Player\StartCall;
use App\Libraries\Frontend\DarkWeb\DarkBuy;
use App\Model\Character;
use App\Model\Dto\WebDarkBuy;
use App\Model\Dto\WebMarketplaceBuy;
use App\Model\Enum\WebDarkBuy\Status;
use App\Model\WebDarkBuyCategory;
use App\Model\WebDarkWeb;
use App\Model\WebMarketplaceCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class IndexController extends Controller
{

    /**
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, string $hash)
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('darkWeb.index', $hash);
        }
        $characterDto = Character::entryAsDto($character);
        return view(
            'darkWeb.darkBuy.index',
            [
                'hash'       => $hash,
                'character'  => $characterDto,
                'offers'     => \App\Model\WebDarkBuy::getAllOffers(),
                'categories' => $this->getCategories()
            ]
        );
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function offers(Request $request, string $hash)
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('darkWeb.index', $hash);
        }

        $characterDto = Character::entryAsDto($character);
        return view(
            'darkWeb.darkBuy.offers',
            [
                'hash'       => $hash,
                'character'  => $characterDto,
                'offers'     => \App\Model\WebDarkBuy::getOffersByCharacterId($characterDto->getId()),
                'categories' => $this->getCategories()
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $category
     * @param string $hash
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function category(Request $request, int $category, string $hash)
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('darkWeb.index', $hash);
        }
        $category = WebDarkBuyCategory::find($category);
        if (!($category)) {
            return redirect()->route('darkWeb.index', $hash);
        }
        $characterDto = Character::entryAsDto($character);

        return view(
            'darkWeb.darkBuy.category',
            [
                'hash'        => $hash,
                'character'   => $characterDto,
                'offers'      => \App\Model\WebDarkBuy::getOffersByCategoryId($category->id),
                'categories'  => $this->getCategories(),
                'categoryDto' => WebDarkBuyCategory::entryAsDto($category),
            ]
        );
    }


    /**
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function bought(Request $request, string $hash)
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('darkWeb.index', $hash);
        }
        $characterDto = Character::entryAsDto($character);

        return view(
            'darkWeb.darkBuy.bought',
            [
                'hash'       => $hash,
                'character'  => $characterDto,
                'offers'     => \App\Model\WebDarkBuy::getBoughtOffers($characterDto->getId()),
                'categories' => $this->getCategories()

            ]
        );
    }


    /**
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request, string $hash)
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('darkWeb.index', $hash);
        }
        $characterDto = Character::entryAsDto($character);

        return view(
            'darkWeb.darkBuy.create',
            [
                'hash'       => $hash,
                'character'  => $characterDto,
                'categories' => $this->getCategories()

            ]
        );
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request, string $hash)
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('darkWeb.index', $hash);
        }
        // Validiere die Anfrage
        $validator = $this->validateRequest($request);
        if ($validator->fails()) {
            $messages = [];
            foreach ($validator->errors()->all() as $error) {
                $messages[] = (new Message())
                    ->setMessage($error)
                    ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
            }
            return redirect()->route('darkWeb.darkBuy.create', $hash)
                ->with('messages', $messages)
                ->withInput()
                ->withErrors($validator->errors());
        }
        $characterDto = Character::entryAsDto($character);
        $darkWebDto = (new WebDarkWeb())->getEntryByCharacterId($characterDto->getId());
        if (!($darkWebDto)) {
            return redirect()->route('darkWeb.darkBuy.create', $hash);
        }
        $imageUrl = $request->input('imageUrl');
        $message = $request->input('message');
        $price = $request->input('price');
        $categoryId = $request->input('categoryId');

        $dto = (new WebDarkBuy())
            ->setCharacterId($characterDto->getId())
            ->setStatus(Status::OPEN)
            ->setPrice($price)
            ->setCategoryId($categoryId ?: null)
            ->setName($darkWebDto->getName())
            ->setImageUrl($imageUrl)
            ->setMessage($message);
        (new \App\Model\WebDarkBuy())->insertEntry($dto);

        return redirect()->route('darkWeb.darkBuy.offers', $hash);
    }

    /**
     * @param Request $request
     * @param int $id
     * @param string $hash
     * @return \Illuminate\Http\RedirectResponse
     */
    public function pay(Request $request, int $id, string $hash)
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('darkWeb.index', $hash);
        }
        $characterDto = Character::entryAsDto($character);
        $entry = \App\Model\WebDarkBuy::find($id);
        if (!($entry)) {
            return redirect()->route('darkWeb.index', $hash);
        }
        $dto = \App\Model\WebDarkBuy::entryAsDto($entry);
        $payed = DarkBuy::pay($characterDto, $dto);

        if ($payed) {
            $dto = \App\Model\WebDarkBuy::entryAsDto($entry);
            $dto->setStatus(Status::BOUGHT)
                ->setBuyerId($characterDto->getId());
            (new \App\Model\WebDarkBuy())->updateEntry($dto);

            return redirect()->route('darkWeb.darkBuy.bought', $hash);
        } else {
            $messages[] = (new Message())
                ->setMessage("Konnte nicht gekauft werden!")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
            return redirect()->route('darkWeb.darkBuy.offers', $hash)->with('messages', $messages);
        }
    }

    /**
     * @param Request $request
     * @param int $id
     * @param string $hash
     * @return \Illuminate\Http\RedirectResponse
     */
    public function call(Request $request, int $id, string $hash)
    {
        $messages = [];
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('darkWeb.index', $hash);
        }
        $characterDto = Character::entryAsDto($character);
        $entry = \App\Model\WebDarkBuy::find($id);
        if (!($entry)) {
            return redirect()->route('darkWeb.index', $hash);
        }

        $dto = \App\Model\WebDarkBuy::entryAsDto($entry);

        $worked = StartCall::call($dto->getBuyerId(), $dto->getCharacterId(), true);
        if (!$worked) {
            $messages[] = (new Message())
                ->setMessage("Konnte nicht angerufen werden!")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
        }
        return redirect()->route('darkWeb.darkBuy.bought', $hash)->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @param string $hash
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request, int $id, string $hash)
    {
        $character = (new Character())->getEntryByHash($hash);
        if (!($character)) {
            return redirect()->route('darkWeb.index', $hash);
        }

        $entry = \App\Model\WebDarkBuy::find($id);
        if (!($entry)) {
            return redirect()->route('darkWeb.index', $hash);
        }
        $dto = \App\Model\WebDarkBuy::entryAsDto($entry);
        $characterDto = Character::entryAsDto($character);
        if ($dto->getCharacterId() !== $characterDto->getId()) {
            return redirect()->route('darkWeb.darkBuy.index', $hash);
        }

        (new \App\Model\WebDarkBuy())->deleteEntry($dto);

        return redirect()->route('darkWeb.darkBuy.index', $hash);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    private function validateRequest(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'imageUrl'   => [
                    'url',
                    'required'
                ],
                'categoryId' => [
                    'nullable',
                    'int',
                ],
                'price'      => [
                    'int',
                    'required'
                ],
                'message'    => [
                    'string',
                    'required'
                ]
            ],
            [
                'imageUrl.url'      => 'Die Bilderurl muss eine valide URL sein',
                'imageUrl.required' => 'Die Bilderurl muss angegeben werden',
                'categoryId.int'    => 'Die Kategorie muss eine Zahl sein',
                'price.required'    => 'Der Preis muss übergeben werden',
                'price.int'         => 'Der Preis muss eine Zahl sein',
                'message.string'    => 'Die Nachricht muss ein Text sein',
                'message.required'  => 'Die Nachricht muss ausgefüllt werden',

            ]
        );

        return $validator;
    }

    /**
     * @return \App\Model\Dto\WebDarkBuyCategory[]
     */
    private function getCategories()
    {
        $categories = [];
        foreach (\App\Model\WebDarkBuyCategory::all() as $entry) {
            $categories[] = \App\Model\WebDarkBuyCategory::entryAsDto($entry);
        }
        return $categories;
    }
}
