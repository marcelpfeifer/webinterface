<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            @include('layouts.messages')
            <div class="card bg-white text-secondary">
                <div class="card-header bg-dark text-white">  @yield('title') </div>
                <div class="card-body text-dark">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</div>
