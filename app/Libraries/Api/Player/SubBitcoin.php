<?php
/**
 * SubBitcoin
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 13.08.2020
 * Time: 20:18
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Api\Player;


use App\Libraries\Api\AClient;
use App\Model\WebConfig;

class SubBitcoin extends AClient
{
    /**
     * @var string
     */
    const URL = 'player/subBitcoin';

    /**
     * @param int $pid
     * @param int $value
     * @param bool $legal
     * @return bool
     */
    public static function call(int $pid, int $value, bool $legal): bool
    {
        $response = self::sendRequest(
            'POST',
            WebConfig::getEntryByKey('api_url')->getValue() . self::URL,
            [
                'query' => [
                    'pid'   => $pid,
                    'value' => $value,
                    'legal' => $legal,
                ]
            ]
        );

        return $response && $response->getStatusCode() == 200;
    }
}
