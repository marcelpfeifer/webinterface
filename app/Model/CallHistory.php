<?php
/**
 * CallHistory
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 24.11.2020
 * Time: 15:17
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CallHistory extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_NUMBER_FROM = 'numberFrom';

    /**
     * @var string
     */
    const COLUMN_NUMBER_TO = 'numberTo';

    /**
     * @var string
     */
    const COLUMN_DATE = 'date';

    /**
     * @var string
     */
    const COLUMN_SUPPRESSED = 'suppressed';

    /**
     * @var string
     */
    const COLUMN_CREATED_AT = 'createdAt';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'call_history';

    /**
     * @var string[]
     */
    protected $dates = [
        self::COLUMN_CREATED_AT,
    ];

    /**
     * @param Dto\CallHistory $dto
     */
    public function insertEntry(Dto\CallHistory $dto)
    {
        $this->insert(
            [
                self::COLUMN_NUMBER_FROM => $dto->getNumberFrom(),
                self::COLUMN_NUMBER_TO   => $dto->getNumberTo(),
                self::COLUMN_DATE        => $dto->getDate(),
                self::COLUMN_SUPPRESSED  => $dto->isSuppressed(),
                self::COLUMN_CREATED_AT  => Carbon::now(),
            ]
        );
    }

    /**
     * @param int $characterId
     * @return Dto\CallHistory[]
     */
    public function getCallHistoryByCharacterId(int $characterId)
    {
        $results = [];
        $number = Character::getPhoneNumberById($characterId);
        $entries = $this->where(self::COLUMN_NUMBER_FROM, '=', $number)
            ->orWhere(self::COLUMN_NUMBER_TO, '=', $number)
            ->orderBy(self::COLUMN_CREATED_AT, 'DESC')
            ->get();

        foreach ($entries as $entry) {
            $results[] = self::entryAsDto($entry);
        }

        return $results;
    }

    /**
     * @param Dto\CallHistory $dto
     */
    public function updateEntry(Dto\CallHistory $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_NUMBER_FROM => $dto->getNumberFrom(),
                self::COLUMN_NUMBER_TO   => $dto->getNumberTo(),
                self::COLUMN_DATE        => $dto->getDate(),
                self::COLUMN_SUPPRESSED  => $dto->isSuppressed(),
                self::COLUMN_CREATED_AT  => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\CallHistory $dto
     */
    public function deleteEntry(Dto\CallHistory $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param CallHistory $entry
     * @return Dto\CallHistory
     */
    public static function entryAsDto(CallHistory $entry): Dto\CallHistory
    {
        return (new Dto\CallHistory())
            ->setId($entry->{self::COLUMN_ID})
            ->setNumberFrom($entry->{self::COLUMN_NUMBER_FROM})
            ->setNumberTo($entry->{self::COLUMN_NUMBER_TO})
            ->setDate($entry->{self::COLUMN_DATE})
            ->setSuppressed($entry->{self::COLUMN_SUPPRESSED})
            ->setCreatedAt($entry->{self::COLUMN_CREATED_AT});
    }
}
