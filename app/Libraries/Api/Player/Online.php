<?php
/**
 * Online
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 05.04.2020
 * Time: 10:04
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Api\Player;

use App\Libraries\Api\AClient;
use App\Model\Character;
use App\Model\WebConfig;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class Online extends AClient
{

    /**
     * @var string
     */
    const URL = 'player/online';

    /**
     * @return array
     */
    public static function getPlayers(): array
    {
        $result = [];
        $response = self::sendRequest('GET', WebConfig::getEntryByKey('api_url')->getValue() . self::URL, [], false);
        if ($response && $response->getStatusCode() == 200) {
            $content = $response->getBody()->getContents();
            $entries = json_decode($content);
            foreach ($entries as $id => $name) {
                $result[$id] = $name;
            }
        } else {
            $result = [
                1 => 'No Server Response',
                4 => 'Test',
            ];
        }

        return $result;
    }
}
