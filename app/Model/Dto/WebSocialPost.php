<?php
/**
 * WebSocialPost
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 14.04.2020
 * Time: 17:52
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Dto;

use Carbon\Carbon;

class WebSocialPost
{

    /**
     * @var int|null
     */
    private $id;

    /**
     * @var int
     */
    private $characterId;

    /**
     * @var int|null
     */
    private $retweet;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $message = '';

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @var null|Carbon
     */
    private $updatedAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return WebSocialPost
     */
    public function setId(?int $id): WebSocialPost
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCharacterId(): int
    {
        return $this->characterId;
    }

    /**
     * @param int $characterId
     * @return WebSocialPost
     */
    public function setCharacterId(int $characterId): WebSocialPost
    {
        $this->characterId = $characterId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRetweet(): ?int
    {
        return $this->retweet;
    }

    /**
     * @param int|null $retweet
     * @return WebSocialPost
     */
    public function setRetweet(?int $retweet): WebSocialPost
    {
        $this->retweet = $retweet;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return WebSocialPost
     */
    public function setName(string $name): WebSocialPost
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return WebSocialPost
     */
    public function setMessage(string $message): WebSocialPost
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return WebSocialPost
     */
    public function setCreatedAt(?Carbon $createdAt): WebSocialPost
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon|null $updatedAt
     * @return WebSocialPost
     */
    public function setUpdatedAt(?Carbon $updatedAt): WebSocialPost
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}

