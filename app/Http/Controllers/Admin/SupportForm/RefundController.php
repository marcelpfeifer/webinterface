<?php
/**
 * RefundController
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 22.11.2019
 * Time: 20:41
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\SupportForm;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Model\Account;
use App\Model\WebAdminRefund;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RefundController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:' . \App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND_SUPPORT_FORM_REFUND);
    }

    /**
     * @return View
     */
    public function index(): View
    {
        $entries = WebAdminRefund::all();

        $result = [];
        foreach ($entries as $entry) {
            $dto = WebAdminRefund::entryAsDto($entry);
            $user = Account::entryAsDto($entry->account);
            $result[] = [
                'dto' => $dto,
                'user' => $user,
            ];
        }

        return view('admin.supportForm.refund.index', [
            "entries" => $result
        ]);
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return view('admin.supportForm.refund.create');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $dto = (new \App\Model\Dto\WebAdminRefund())
            ->setUserName($request->input('userName'))
            ->setItemType($request->input('itemType'))
            ->setAmount($request->input('amount'))
            ->setDescription($request->input('description'))
            ->setUserId(Auth::user()->{Account::COLUMN_ID});

        (new WebAdminRefund())->insertEntry($dto);
        Logging::insert(Name::SUPPORT_FORM_REFUND, Auth::id(), $dto);

        $messages[] = (new Message())
            ->setMessage("Eintrag wurde erstellt")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.supportForm.refund.index')->with('phone', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return View
     */
    public function show(Request $request, int $id): View
    {
        $entry = WebAdminRefund::find($id);
        $dto = WebAdminRefund::entryAsDto($entry);
        $user = Account::entryAsDto($entry->account);

        return view('admin.supportForm.refund.show', [
            'entry' => $dto,
            'user'  => $user
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(Request $request, int $id): RedirectResponse
    {
        $entry = WebAdminRefund::find($id);
        $dto = WebAdminRefund::entryAsDto($entry);

        (new WebAdminRefund())->deleteEntry($dto);
        Logging::delete(Name::SUPPORT_FORM_REFUND, Auth::id(), $dto);

        $messages[] = (new Message())
            ->setMessage("Eintrag wurde gelöscht")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.supportForm.refund.index')->with('phone', $messages);
    }
}
