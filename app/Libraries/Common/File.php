<?php
/**
 * File
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 07.10.2021
 * Time: 15:53
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Common;


class File
{

    /**
     * @param string $path
     * @param int $mode
     */
    public static function changePermission(string $path, int $mode = 0755): void
    {
        if (file_exists($path)) {
            chmod($path, $mode);
        }
    }
}
