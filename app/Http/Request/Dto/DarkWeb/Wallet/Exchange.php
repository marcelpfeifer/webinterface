<?php
/**
 * Exchange
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 22.08.2020
 * Time: 21:24
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\DarkWeb\Wallet;


use App\Http\Request\Dto\ADto;

class Exchange extends ADto
{

    /**
     * @var int
     */
    private $amount = 0;

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     * @return Exchange
     */
    public function setAmount(int $amount): Exchange
    {
        $this->amount = $amount;
        return $this;
    }
}
