<?php
/**
 * ShowNamesController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 26.04.2020
 * Time: 14:29
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Api\Player;

use App\Dto\Message;
use App\Http\Controllers\Api\AController;
use App\Http\Controllers\Api\IController;
use App\Http\Controllers\Controller;
use App\Http\Request\Api\Player\ShowNames\Index;
use App\Http\Request\Api\Player\ShowNames\Save;
use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Libraries\Api\Player\Online;
use App\Libraries\Api\Player\ShowNames;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ShowNamesController extends AController implements IController
{
    /**
     * @param Request $request
     * @return View
     */
    public function index(Request $request): View
    {
        $validator = new Index($request);
        return $this->processIndex($validator, 'api.player.showNames.index');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function save(Request $request): RedirectResponse
    {
        $validator = new Save($request);
        return $this->processSave($validator, 'api.player.showNames.index');
    }

    /**
     * @param \App\Http\Request\Dto\Api\Player\ShowNames\Index $requestDto
     * @return array
     */
    public function getIndexData(ADto $requestDto): array
    {
        return [
            'players'       => Online::getPlayers(),
            'currentPlayer' => $requestDto->getCurrentPlayer(),
        ];
    }

    /**
     * @param \App\Http\Request\Dto\Api\Player\ShowNames\Save $requestDto
     * @return bool
     */
    public function execApiCall(ADto $requestDto): bool
    {
        return ShowNames::call($requestDto->getPlayerId(), $requestDto->isActivate());
    }

    /**
     * @param \App\Http\Request\Dto\Api\Player\ShowNames\Save $requestDto
     * @return array
     */
    public function getRedirectData(ADto $requestDto): array
    {
        return [
            'currentPlayer' => $requestDto->getPlayerId()
        ];
    }
}
