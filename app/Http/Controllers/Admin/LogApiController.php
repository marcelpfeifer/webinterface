<?php
/**
 * LogApiController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 07.03.2021
 * Time: 13:35
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\WebAdminLoggingApi;
use Illuminate\View\View;

class LogApiController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        $entries = (new WebAdminLoggingApi())->getLogEntriesPagination();
        return \view(
            'admin.logApi.index',
            [
                'entries' => $entries
            ]
        );
    }
}
