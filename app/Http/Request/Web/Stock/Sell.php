<?php
/**
 * Buy
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 02.09.2021
 * Time: 14:15
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Web\Stock;


use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;
use App\Model\Dto\Character;
use App\Model\WebStock;
use App\Model\WebStockCharacter;
use Illuminate\Database\Eloquent\Model;

class Sell extends AValidator implements IValidator
{

    /**
     * @var Character
     */
    private $characterDto;

    /**
     * @var \App\Model\Dto\WebStockCharacter
     */
    private $dto;

    /**
     * @return Character
     */
    public function getCharacterDto(): Character
    {
        return $this->characterDto;
    }

    /**
     * @param Character $characterDto
     * @return Sell
     */
    public function setCharacterDto(Character $characterDto): Sell
    {
        $this->characterDto = $characterDto;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getRules(): array
    {
        return [
            'amount' => [
                'required',
                'int',
            ]
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'amount.required' => 'Die Anzahl muss übergeben werden!',
            'amount.int'      => 'Die Anzahl darf nur Zeichen enthalten!',
        ];
    }

    /**
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validate(): \Illuminate\Contracts\Validation\Validator
    {
        $validator = parent::validate();

        $validator->after(
            function ($validator) {
                $amount = $this->request->input('amount', 0);
                $this->dto = WebStockCharacter::entryAsDto(WebStockCharacter::find($this->id));
                if ($this->dto->getCharacterId() !== $this->characterDto->getId()) {
                    $validator->errors()->add('id', 'Du bist nicht der Inhaber dieser Aktien!!');
                }
                if ($this->dto->getAmount() < $amount) {
                    $validator->errors()->add('amount', 'Du hast nicht so viele Anteile von der Aktie!');
                }
            }
        );

        return $validator;
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Web\Stock\Sell())
            ->setId($this->id)
            ->setWebStockCharacter($this->dto)
            ->setCharacter($this->characterDto)
            ->setAmount($this->request->input('amount'));
    }
}
