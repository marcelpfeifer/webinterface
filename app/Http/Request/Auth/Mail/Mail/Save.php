<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 08.11.2020
 * Time: 10:56
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Auth\Mail\Mail;

use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class Save extends AValidator implements IValidator
{
    /**
     * @return string[]
     */
    public function getRules(): array
    {
        return [
            'to'      => [
                'required',
                'string',
            ],
            'title'   => [
                'required',
                'string',
            ],
            'message' => [
                'required',
                'string',
            ],
            'file.*'  => [
                'mimes:jpg,jpeg,png,pdf',
                'max:20000'
            ],
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'to.required'      => 'Der Empfämger muss übergeben werden!',
            'to.string'        => 'Der Empfämger darf nur aus Zeichen bestehen!',
            'title.required'   => 'Der Titel muss übergeben werden!',
            'title.string'     => 'Der Titel  darf nur aus Zeichen bestehen!',
            'message.required' => 'Die Nachricht muss übergeben werden!',
            'file.*.mimes'     => 'Es sind nur JPG,PNG und PDF`s erlaubt!',
            'file.*.max'       => 'Die Datei ist zu groß!',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Auth\Mail\Mail\Save())
            ->setWebMailAddressId($this->id)
            ->setTo($this->request->input('to'))
            ->setTitle($this->request->input('title'))
            ->setMessage($this->request->input('message'))
            ->setFiles($this->request->file('file', []));
    }
}
