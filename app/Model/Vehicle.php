<?php
/**
 * Vehicle.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 01.11.2019
 * Time: 13:23
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use App\Model\Dto\Vehicle\WithCharacter;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Vehicle extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_GUID = 'guid';

    /**
     * @var string
     */
    const COLUMN_MODEL = 'model';

    /**
     * @var string
     */
    const COLUMN_CUSTOMIZATION = 'customization';

    /**
     * @var string
     */
    const COLUMN_NUMBERPLATE = 'numberPlate';

    /**
     * @var string
     */
    const COLUMN_PARKED = 'parked';

    /**
     * @var string
     */
    const COLUMN_FACTION = 'faction';

    /**
     * @var string
     */
    const COLUMN_IMPOUNDED = 'impounded';

    /**
     * @var string
     */
    const COLUMN_DESTROYED = 'destroyed';

    /**
     * @var string
     */
    const COLUMN_G_ID = 'gId';

    /**
     * @var string
     */
    const COLUMN_ENGINE_DISTANCE = 'engineDistance';

    /**
     * @var string
     */
    const COLUMN_INVENTORY = 'inventory';

    /**
     * @var string
     */
    const COLUMN_CREATED_AT = 'createdAt';

    /**
     * @var string
     */
    const COLUMN_UPDATED_AT = 'updatedAt';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'vehicle';

    /**
     * @param int $id
     * @return Dto\Vehicle|null
     */
    public function getVehicleById(int $id): ?Dto\Vehicle
    {
        $entry = $this->where(self::COLUMN_ID, $id)->first();

        if ($entry) {
            return self::entryAsDto($entry);
        }
        return null;
    }

    /**
     * @param int $characterId
     * @return Dto\Vehicle[]
     */
    public function getVehiclesByCharacterId(int $characterId): array
    {
        $vehicles = [];
        $entries = $this->where(self::COLUMN_GUID, $characterId)->get();
        foreach ($entries as $entry) {
            $vehicles[] = self::entryAsDto($entry);
        }

        return $vehicles;
    }

    /**
     * @param string $model
     * @return Dto\Vehicle[]
     */
    public function getVehiclesByModelWithCharacter(string $model): array
    {
        $vehicles = [];
        $entries = $this->where(self::COLUMN_MODEL, $model)->get();
        foreach ($entries as $entry) {
            $character = $entry->character;
            $vehicles[] = (new WithCharacter())
                ->setCharacter($character ? Character::entryAsDto($entry->character) : null)
                ->setVehicle(self::entryAsDto($entry));
        }

        return $vehicles;
    }

    /**
     * @return mixed
     */
    public function getPossibleNames()
    {
        return $this
            ->select(self::COLUMN_MODEL)
            ->groupBy(self::COLUMN_MODEL)
            ->pluck(self::COLUMN_MODEL)
            ->toArray();
    }

    /**
     * @param Dto\Vehicle $dto
     */
    public function insertEntry(Dto\Vehicle $dto)
    {
        $this->insert(
            [
                self::COLUMN_ID              => $dto->getId(),
                self::COLUMN_GUID            => $dto->getGuid(),
                self::COLUMN_MODEL           => $dto->getModel(),
                self::COLUMN_CUSTOMIZATION   => $dto->getCustomization(),
                self::COLUMN_NUMBERPLATE     => $dto->getNumberPlate(),
                self::COLUMN_PARKED          => $dto->isParked(),
                self::COLUMN_FACTION         => $dto->getFaction(),
                self::COLUMN_IMPOUNDED       => $dto->isImpounded(),
                self::COLUMN_DESTROYED       => $dto->isDestroyed(),
                self::COLUMN_ENGINE_DISTANCE => $dto->getEngineDistance(),
                self::COLUMN_INVENTORY       => $dto->getInventory(),
                self::COLUMN_CREATED_AT      => Carbon::now(),
                self::COLUMN_UPDATED_AT      => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\Vehicle $dto
     */
    public function updateEntry(Dto\Vehicle $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_ID              => $dto->getId(),
                self::COLUMN_GUID            => $dto->getGuid(),
                self::COLUMN_MODEL           => $dto->getModel(),
                self::COLUMN_CUSTOMIZATION   => $dto->getCustomization(),
                self::COLUMN_NUMBERPLATE     => $dto->getNumberPlate(),
                self::COLUMN_PARKED          => $dto->isParked(),
                self::COLUMN_FACTION         => $dto->getFaction(),
                self::COLUMN_IMPOUNDED       => $dto->isImpounded(),
                self::COLUMN_DESTROYED       => $dto->isDestroyed(),
                self::COLUMN_ENGINE_DISTANCE => $dto->getEngineDistance(),
                self::COLUMN_INVENTORY       => $dto->getInventory(),
                self::COLUMN_UPDATED_AT      => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\Vehicle $dto
     */
    public function deleteEntry(Dto\Vehicle $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param int $guid
     */
    public function deleteEntryByGuid(int $guid)
    {
        $this->where(self::COLUMN_GUID, $guid)->delete();
    }

    /**
     * @param Vehicle $entry
     * @return Dto\Vehicle
     */
    public static function entryAsDto(Vehicle $entry): Dto\Vehicle
    {
        return (new Dto\Vehicle())
            ->setId($entry->{self::COLUMN_ID})
            ->setGuid($entry->{self::COLUMN_GUID})
            ->setModel($entry->{self::COLUMN_MODEL})
            ->setCustomization($entry->{self::COLUMN_CUSTOMIZATION})
            ->setNumberPlate($entry->{self::COLUMN_NUMBERPLATE})
            ->setParked($entry->{self::COLUMN_PARKED})
            ->setFaction($entry->{self::COLUMN_FACTION})
            ->setImpounded($entry->{self::COLUMN_IMPOUNDED})
            ->setDestroyed($entry->{self::COLUMN_DESTROYED})
            ->setGId($entry->{self::COLUMN_G_ID})
            ->setEngineDistance($entry->{self::COLUMN_ENGINE_DISTANCE})
            ->setInventory($entry->{self::COLUMN_INVENTORY});
    }

    /**
     * @return BelongsTo
     */
    public function character(): BelongsTo
    {
        return $this->belongsTo(Character::class, self::COLUMN_GUID, Character::COLUMN_ID);
    }
}
