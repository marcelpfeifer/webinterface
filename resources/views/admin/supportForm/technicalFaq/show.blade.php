<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 20.11.2019
 * Time: 20:56
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@php
    /** @var $entry \App\Model\Dto\WebAdminTechnicalFaq */
    /** @var $user \App\Model\Dto\Account */
@endphp
@extends('admin.layout.app')
@section('title')
    <i class="fab fa-wpforms"></i>
    Eintrag {{ $entry->getId() }}
    <a class="btn btn-dark btn-sm float-right"
       href="{{ route('admin.supportForm.technicalFaq.index') }}"
       role="button"
    > Zurück </a>
@endsection
@section('content')
    <dl class="row">
        <dt class="col-3">Fehlermeldung</dt>
        <dd class="col-9">{{ $entry->getExplanation() }}</dd>

        <dt class="col-3">Fehlercode</dt>
        <dd class="col-9">{{ $entry->getErrorCode() }}</dd>

        <dt class="col-3">Fehlerbehebung</dt>
        <dd class="col-9">{{ $entry->getTroubleShooting() }}</dd>

        <dt class="col-3">Fehlerbehebung 2</dt>
        <dd class="col-9">{{ $entry->getTroubleShooting2() }}</dd>

        <dt class="col-3">Gelöst</dt>
        <dd class="col-9">
            @if($entry->isFixed())
                <i class="fas fa-check"></i>
            @else
                <i class="fas fa-times"></i>
            @endif
        </dd>

        <dt class="col-3">Eingetragener Supporter</dt>
        <dd class="col-9">{{ $user->getUsername() }}</dd>

        <dt class="col-3">Datum</dt>
        <dd class="col-9">{{ $entry->getCreatedAt()->format('Y-m-d H:i:s') }}</dd>
    </dl>
@endsection
