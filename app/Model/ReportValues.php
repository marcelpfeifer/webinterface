<?php
/**
 * ReportValues
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 18.09.2020
 * Time: 17:34
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use App\Model\Dto\ReportValues\WithCharacter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ReportValues extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_CASE_ID = 'caseId';

    /**
     * @var string
     */
    const COLUMN_TARGET_ID = 'targetId';

    /**
     * @var string
     */
    const COLUMN_DISTANCE = 'distance';

    /**
     * @var string
     */
    const COLUMN_CREATED_AT = 'createdAt';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'report_values';

    /**
     * @var string[]
     */
    protected $dates = [
        self::COLUMN_CREATED_AT
    ];

    /**
     * @param int $caseId
     * @return Dto\ReportValues\WithCharacter[]
     */
    public function getEntriesByCaseId(int $caseId)
    {
        $result = [];
        $entries = $this->where(self::COLUMN_CASE_ID, $caseId)->get();

        foreach ($entries as $entry) {
            $result[] = (new WithCharacter())
                ->setCharacter($entry->character ? Character::entryAsDto($entry->character) : null)
                ->setReportValue(self::entryAsDto($entry));
        }

        return $result;
    }

    /**
     * @param ReportValues $entry
     * @return Dto\ReportValues
     */
    public static function entryAsDto(ReportValues $entry): Dto\ReportValues
    {
        return (new Dto\ReportValues())
            ->setId($entry->{self::COLUMN_ID})
            ->setCaseId($entry->{self::COLUMN_CASE_ID})
            ->setTargetId($entry->{self::COLUMN_TARGET_ID})
            ->setDistance($entry->{self::COLUMN_DISTANCE})
            ->setCreatedAt($entry->{self::COLUMN_CREATED_AT});
    }

    /**
     * @return BelongsTo
     */
    public function character(): BelongsTo
    {
        return $this->belongsTo(Character::class, self::COLUMN_TARGET_ID, Character::COLUMN_ID);
    }
}
