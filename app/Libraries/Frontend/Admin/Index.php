<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 11.08.2021
 * Time: 17:08
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Admin;


use App\Model\WebServerStatus;

class Index
{

    /**
     * @return array[]
     */
    public static function getViewData(): array
    {
        $labels = [];
        $ramMax = 0;
        $ram = [];
        $cpu = [];
        $altv = [];
        $altvTest = [];
        $ts = [];
        $forum = [];
        foreach (WebServerStatus::all() as $webServerStatus) {
            $dto = WebServerStatus::entryAsDto($webServerStatus);
            $labels[] = self::getLabel($dto);
            if ($ramMax < $dto->getRamMax()) {
                $ramMax = $dto->getRamMax();
            }
            $ram[] = $dto->getRamMax() - $dto->getRamFree();
            $cpu[] = $dto->getCpu();
            $altv[] = $dto->isAltv();
            $altvTest[] = $dto->isAltvTest();
            $ts[] = $dto->isTs();
            $forum[] = $dto->isForum();
        }

        return [
            'labels'   => json_encode($labels),
            'ramMax'   => $ramMax,
            'ram'      => json_encode($ram),
            'cpu'      => json_encode($cpu),
            'altv'     => json_encode($altv),
            'altvTest' => json_encode($altvTest),
            'ts'       => json_encode($ts),
            'forum'    => json_encode($forum),
        ];
    }

    /**
     * @var array
     */
    private static $selectedFormats = [];

    /**
     * @param \App\Model\Dto\WebServerStatus $dto
     * @return string
     */
    private static function getLabel(\App\Model\Dto\WebServerStatus $dto): string
    {
        $carbon = $dto->getCreatedAt()
            ->setTimezone('Europe/Berlin');
        $currentDay = $carbon->format('Y-m-d');
        if (in_array($currentDay, self::$selectedFormats)) {
            return $carbon->format('H:i');
        }

        self::$selectedFormats[] = $currentDay;
        return $carbon->format('Y-m-d H:i');
    }
}
