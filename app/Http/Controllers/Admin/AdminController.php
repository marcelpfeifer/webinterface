<?php
/**
 * AdminController.php
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 31.08.2019
 * Time: 20:31
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Libraries\Frontend\Admin\Index;
use Illuminate\View\View;

class AdminController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        $viewData = Index::getViewData();
        return view('admin.index', $viewData);
    }
}
