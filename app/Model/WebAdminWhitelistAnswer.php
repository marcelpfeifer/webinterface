<?php
/**
 * WebAdminWhitelistAnswer
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 13.11.2021
 * Time: 13:12
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class WebAdminWhitelistAnswer extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_WEB_ADMIN_WHITELIST_ID = 'webAdminWhitelistId';

    /**
     * @var string
     */
    const COLUMN_WEB_ADMIN_WHITELIST_QUESTION_ID = 'webAdminWhitelistQuestionId';

    /**
     * @var string
     */
    const COLUMN_ACCURACY = 'accuracy';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webAdminWhitelistAnswer';

    /**
     * @var array
     */
    protected $dates = [
        self::CREATED_AT,
        self::UPDATED_AT,
    ];

    /**
     * @return BelongsTo
     */
    public function question(): BelongsTo
    {
        return $this->belongsTo(
            WebAdminWhitelistQuestion::class,
            WebAdminWhitelistQuestion::COLUMN_ID,
            self::COLUMN_WEB_ADMIN_WHITELIST_QUESTION_ID
        );
    }

    /**
     * @return BelongsTo
     */
    public function whitelist(): BelongsTo
    {
        return $this->belongsTo(
            WebAdminWhitelist::class,
            WebAdminWhitelist::COLUMN_ID,
            self::COLUMN_WEB_ADMIN_WHITELIST_ID
        );
    }

    /**
     * @param Dto\WebAdminWhitelistAnswer $dto
     * @return Dto\WebAdminWhitelistAnswer|null
     */
    public function getEntryByWhiteListIdAndQuestionId(Dto\WebAdminWhitelistAnswer $dto): ?Dto\WebAdminWhitelistAnswer
    {
        $entry = $this->where(self::COLUMN_WEB_ADMIN_WHITELIST_ID, $dto->getWebAdminWhitelistId())
            ->where(self::COLUMN_WEB_ADMIN_WHITELIST_QUESTION_ID, $dto->getWebAdminWhitelistQuestionId())
            ->first();
        return $entry ? self::entryAsDto($entry) : null;
    }

    /**
     * @param Dto\WebAdminWhitelistAnswer $dto
     * @return bool
     */
    public function insertEntry(Dto\WebAdminWhitelistAnswer $dto): bool
    {
        return $this->insert(
            [
                self::COLUMN_ID                              => $dto->getId(),
                self::COLUMN_WEB_ADMIN_WHITELIST_ID          => $dto->getWebAdminWhitelistId(),
                self::COLUMN_WEB_ADMIN_WHITELIST_QUESTION_ID => $dto->getWebAdminWhitelistQuestionId(),
                self::COLUMN_ACCURACY                        => $dto->getAccuracy(),
                self::CREATED_AT                             => Carbon::now(),
                self::UPDATED_AT                             => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebAdminWhitelistAnswer $dto
     * @return bool
     */
    public function updateEntry(Dto\WebAdminWhitelistAnswer $dto): bool
    {
        return $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_WEB_ADMIN_WHITELIST_ID          => $dto->getWebAdminWhitelistId(),
                self::COLUMN_WEB_ADMIN_WHITELIST_QUESTION_ID => $dto->getWebAdminWhitelistQuestionId(),
                self::COLUMN_ACCURACY                        => $dto->getAccuracy(),
                self::UPDATED_AT                             => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebAdminWhitelistAnswer $dto
     */
    public function deleteEntry(\App\Model\Dto\WebAdminWhitelistAnswer $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebAdminWhitelistAnswer $entry
     * @return Dto\WebAdminWhitelistAnswer
     */
    public static function entryAsDto(WebAdminWhitelistAnswer $entry): Dto\WebAdminWhitelistAnswer
    {
        return (new Dto\WebAdminWhitelistAnswer())
            ->setId($entry->{self::COLUMN_ID})
            ->setWebAdminWhitelistId($entry->{self::COLUMN_WEB_ADMIN_WHITELIST_ID})
            ->setWebAdminWhitelistQuestionId($entry->{self::COLUMN_WEB_ADMIN_WHITELIST_QUESTION_ID})
            ->setAccuracy($entry->{self::COLUMN_ACCURACY})
            ->setCreatedAt($entry->{self::CREATED_AT})
            ->setUpdatedAt($entry->{self::UPDATED_AT});
    }
}
