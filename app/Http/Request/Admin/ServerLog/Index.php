<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 10.10.2021
 * Time: 14:51
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Admin\ServerLog;


use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;
use App\Libraries\Frontend\Enum\Admin\ServerLog\Type;
use Illuminate\Validation\Rule;

class Index extends AValidator implements IValidator
{
    /**
     * @return string[]
     */
    public function getRules(): array
    {
        return [
            'filter'   => [
                'nullable',
                'array',
            ],
            'filter.*' => [
                'string',
                Rule::in(Type::getConstants())
            ],
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Admin\ServerLog\Index())
            ->setFilter($this->request->input('filter', []));
    }
}
