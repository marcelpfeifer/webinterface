<?php
/**
 * WebDarkBuy
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 02.05.2020
 * Time: 17:56
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use App\Model\Enum\WebDarkBuy\Status;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class WebDarkBuy extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_CHARACTER_ID = 'characterId';

    /**
     * @var string
     */
    const COLUMN_CATEGORY_ID = 'categoryId';

    /**
     * @var string
     */
    const COLUMN_BUYER_ID = 'buyerId';

    /**
     * @var string
     */
    const COLUMN_STATUS = 'status';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    const COLUMN_PRICE = 'price';

    /**
     * @var string
     */
    const COLUMN_IMAGE_URL = 'imageUrl';

    /**
     * @var string
     */
    const COLUMN_MESSAGE = 'message';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webDarkBuy';

    /**
     * @return Dto\WebDarkBuy[]
     */
    public static function getAllOffers(): array
    {
        $offers = [];
        $results = self::where(self::COLUMN_STATUS, Status::OPEN)
            ->orderBy(self::CREATED_AT, 'DESC')
            ->get();
        foreach ($results as $entry) {
            $offers[] = self::entryAsDto($entry);
        }
        return $offers;
    }

    /**
     * @param int $characterId
     * @return Dto\WebDarkBuy[]
     */
    public static function getOffersByCharacterId(int $characterId): array
    {
        $offers = [];
        $results = self::where(self::COLUMN_CHARACTER_ID, $characterId)
            ->orderBy(self::CREATED_AT, 'DESC')
            ->get();
        foreach ($results as $entry) {
            $offers[] = self::entryAsDto($entry);
        }
        return $offers;
    }

    /**
     * @param int $buyerId
     * @return Dto\WebDarkBuy[]
     */
    public static function getBoughtOffers(int $buyerId): array
    {
        $offers = [];
        $results = self::where(self::COLUMN_BUYER_ID, $buyerId)
            ->orderBy(self::UPDATED_AT, 'DESC')
            ->get();
        foreach ($results as $entry) {
            $offers[] = self::entryAsDto($entry);
        }
        return $offers;
    }

    /**
     * @param int $categoryId
     * @return Dto\WebDarkBuy[]
     */
    public static function getOffersByCategoryId(int $categoryId): array
    {
        $offers = [];
        $results = self::where(self::COLUMN_CATEGORY_ID, $categoryId)
            ->where(self::COLUMN_STATUS, Status::OPEN)
            ->orderBy(self::CREATED_AT, 'DESC')
            ->get();
        foreach ($results as $entry) {
            $offers[] = self::entryAsDto($entry);
        }
        return $offers;
    }

    /**
     * @param Dto\WebDarkBuy $dto
     */
    public function insertEntry(Dto\WebDarkBuy $dto)
    {
        $this->insert(
            [
                self::COLUMN_ID           => $dto->getId(),
                self::COLUMN_CHARACTER_ID => $dto->getCharacterId(),
                self::COLUMN_CATEGORY_ID  => $dto->getCategoryId(),
                self::COLUMN_BUYER_ID     => $dto->getBuyerId(),
                self::COLUMN_STATUS       => $dto->getStatus(),
                self::COLUMN_NAME         => $dto->getName(),
                self::COLUMN_PRICE        => $dto->getPrice(),
                self::COLUMN_IMAGE_URL    => $dto->getImageUrl(),
                self::COLUMN_MESSAGE      => $dto->getMessage(),
                self::CREATED_AT          => Carbon::now(),
                self::UPDATED_AT          => Carbon::now(),
            ]
        );
    }


    /**
     * @param Dto\WebDarkBuy $dto
     */
    public function updateEntry(Dto\WebDarkBuy $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_ID           => $dto->getId(),
                self::COLUMN_CHARACTER_ID => $dto->getCharacterId(),
                self::COLUMN_CATEGORY_ID  => $dto->getCategoryId(),
                self::COLUMN_BUYER_ID     => $dto->getBuyerId(),
                self::COLUMN_STATUS       => $dto->getStatus(),
                self::COLUMN_NAME         => $dto->getName(),
                self::COLUMN_PRICE        => $dto->getPrice(),
                self::COLUMN_IMAGE_URL    => $dto->getImageUrl(),
                self::COLUMN_MESSAGE      => $dto->getMessage(),
                self::UPDATED_AT          => Carbon::now(),
            ]
        );
    }

    /**
     * @param Dto\WebDarkBuy $dto
     */
    public function deleteEntry(\App\Model\Dto\WebDarkBuy $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebDarkBuy $entry
     * @return Dto\WebDarkBuy
     */
    public static function entryAsDto(WebDarkBuy $entry): Dto\WebDarkBuy
    {
        return (new Dto\WebDarkBuy())
            ->setId($entry->{self::COLUMN_ID})
            ->setCharacterId($entry->{self::COLUMN_CHARACTER_ID})
            ->setCategoryId($entry->{self::COLUMN_CATEGORY_ID})
            ->setBuyerId($entry->{self::COLUMN_BUYER_ID})
            ->setStatus($entry->{self::COLUMN_STATUS})
            ->setName($entry->{self::COLUMN_NAME})
            ->setPrice($entry->{self::COLUMN_PRICE})
            ->setImageUrl($entry->{self::COLUMN_IMAGE_URL})
            ->setMessage($entry->{self::COLUMN_MESSAGE})
            ->setCreateAt($entry->{self::CREATED_AT})
            ->setUpdatedAt($entry->{self::UPDATED_AT});
    }
}
