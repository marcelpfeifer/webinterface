<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 29.08.2021
 * Time: 16:23
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>

@extends('web.layout.app')
@section('url')
    https://eternityV.de/börse/
@endsection
@section('content')
    <div class="container transparentBg bg-white text-dark min-vh-100">
        @include('web.stock.component.nav',[])
        <div class="accordion">
            @foreach($stocks as $stock)
                <div class="card stock" id="{{ $stock->getId() }}" data-name="{{$stock->getName()}}"
                     data-route="{{ route('web.stock.getStockData', $stock->getId()) }}">
                    <div class="card-header" id="heading{{ $stock->getId()}}">
                        <div class="col-12 text-center">
                            <div class="row">
                                <div class="col-1">
                                    <button class="btn btn-secondary" data-toggle="collapse"
                                            data-target="#collapse{{ $stock->getId()}}" aria-expanded="true"
                                            aria-controls="collapse{{ $stock->getId()}}">
                                        <i class="fas fa-compress-alt"></i>
                                    </button>
                                </div>
                                <div class="col-10 col-lg-4 text-center m-2 font-weight-bold">
                                    <a class="text-secondary" href="{{ route('web.stock.show', [$stock->getId(),$hash]) }}">{{ $stock->getName() }}</a>
                                </div>
                                <div class="col m-2">
                                    {{ $stock->getPrice() }}$
                                </div>
                                <div class="col">
                                    <p class="border shadow-sm rounded m-2 bg-secondary {{ ($stock->getPercentageChange() < 0) ? 'bg-danger' : '' }}
                                    {{ ($stock->getPercentageChange() > 0) ? 'bg-success' : '' }}">
                                        {{ $stock->getPercentageChange() }}%
                                    </p>
                                </div>
                                <div class="col m-2 text-secondary
                                {{ ($stock->getPercentageChange() < 0) ? 'text-danger' : '' }}
                                {{ ($stock->getPercentageChange() > 0) ? 'text-success' : '' }}">
                                    {{ ($stock->getPercentageChange() > 0) ? '+' : ''}}{{ number_format($stock->getPrice() -$stock->getLastPrice(), 2) }}
                                    $ aktueller Tag
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="collapse{{ $stock->getId()}}" class="collapse"
                         aria-labelledby="heading{{ $stock->getId()}}">
                        <div class="card-body">
                            <i id="stockSpinner{{$stock->getId()}}" class="fas fa-spinner fa-spin"></i>
                            <canvas style="display: none" id="stockCanvas{{$stock->getId()}}">
                            </canvas>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
