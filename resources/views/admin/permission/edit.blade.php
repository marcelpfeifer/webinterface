<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 25.07.2020
 * Time: 16:48
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-balance-scale-right"></i>
    Rechte für die Gruppe "{{ $group->getName() }}" bearbeiten
@endsection
@section('content')
    <form action="{{ route('admin.permission.save', $group->getId()) }}" method="POST">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}

        @foreach($permissions as $permission)
            <div class="form-check">
                <input class="form-check-input" name="{{ $permission->getId() }}" type="checkbox" id="{{ $permission->getId() }}" {{ (in_array($permission->getId(), $groupPermissions)) ? 'checked':'' }}>
                <label class="form-check-label" for="{{ $permission->getId() }}">
                    {{ $permission->getName() }}
                </label>
            </div>
        @endforeach

        <div class="form-group">
            <button type="submit" class="btn btn-dark form-control">Aktualisieren</button>
        </div>
    </form>
@endsection
