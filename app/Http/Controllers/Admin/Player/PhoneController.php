<?php
/**
 * PhoneControllerp
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 26.08.2019
 * Time: 16:50
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\Player;

use App\Http\Controllers\Controller;
use App\Model\Character;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;

class PhoneController extends Controller
{


    /**
     * @param Request $request
     * @param int $userId
     * @return View
     */
    public function index(Request $request, int $userId): View
    {
        $messages = (new \App\Model\Messages())->getMessagesOfNumber($userId, Character::getPhoneNumberById($userId));
        return view(
            'admin.player.phone.index',
            [
                'messages' => $messages
            ]
        );
    }
}
