<?php
/**
 * WebAdminBan
 * Copyright (C) Marcel Pfeifer 2019 <webmaster@m-pfeifer.de>
 * Date: 25.12.2019
 * Time: 21:59
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class WebAdminBan
 * @package App\Model
 */
class WebAdminBan extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_USER_ID = 'userId';

    /**
     * @var string
     */
    const COLUMN_USER_NAME = 'userName';

    /**
     * @var string
     */
    const COLUMN_IP_ADDRESS = 'ipAddress';

    /**
     * @var string
     */
    const COLUMN_SOCIAL_CLUB_NAME = 'socialClubName';

    /**
     * @var string
     */
    const COLUMN_REASON = 'reason';

    /**
     * @var string
     */
    const COLUMN_LENGTH = 'length';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'webAdminBan';

    /**
     * @return BelongsTo
     */
    public function account(): BelongsTo
    {
        return $this->belongsTo(Account::class, self::COLUMN_USER_ID, Account::COLUMN_ID);
    }

    /**
     * @param Dto\WebAdminBan $dto
     */
    public function insertEntry(Dto\WebAdminBan $dto)
    {
        $this->insert([
            self::COLUMN_ID               => $dto->getId(),
            self::COLUMN_USER_ID          => $dto->getUserId(),
            self::COLUMN_USER_NAME        => $dto->getUserName(),
            self::COLUMN_IP_ADDRESS       => $dto->getIpAddress(),
            self::COLUMN_SOCIAL_CLUB_NAME => $dto->getSocialClubName(),
            self::COLUMN_REASON           => $dto->getReason(),
            self::COLUMN_LENGTH           => $dto->getLength(),
            self::CREATED_AT              => Carbon::now(),
            self::UPDATED_AT              => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\WebAdminBan $dto
     */
    public function updateEntry(Dto\WebAdminBan $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update([
            self::COLUMN_USER_ID          => $dto->getUserId(),
            self::COLUMN_USER_NAME        => $dto->getUserName(),
            self::COLUMN_IP_ADDRESS       => $dto->getIpAddress(),
            self::COLUMN_SOCIAL_CLUB_NAME => $dto->getSocialClubName(),
            self::COLUMN_REASON           => $dto->getReason(),
            self::COLUMN_LENGTH           => $dto->getLength(),
            self::UPDATED_AT              => Carbon::now(),
        ]);
    }

    /**
     * @param Dto\WebAdminBan $dto
     */
    public function deleteEntry(\App\Model\Dto\WebAdminBan $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param WebAdminBan $entry
     * @return Dto\WebAdminBan
     */
    public static function entryAsDto(WebAdminBan $entry): Dto\WebAdminBan
    {
        return (new Dto\WebAdminBan())
            ->setId($entry->{self::COLUMN_ID})
            ->setUserId($entry->{self::COLUMN_USER_ID})
            ->setUserName($entry->{self::COLUMN_USER_NAME})
            ->setIpAddress($entry->{self::COLUMN_IP_ADDRESS})
            ->setSocialClubName($entry->{self::COLUMN_SOCIAL_CLUB_NAME})
            ->setReason($entry->{self::COLUMN_REASON})
            ->setLength($entry->{self::COLUMN_LENGTH})
            ->setCreatedAt($entry->{self::CREATED_AT})
            ->setUpdatedAt($entry->{self::UPDATED_AT});
    }
}
