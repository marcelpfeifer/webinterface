<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 13.04.2020
 * Time: 19:42
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('api.layout.app')
@section('title')
    <i class="fas fa-paw"></i>
    Setzt ein Spieler Model
@endsection
@section('content')
    <form method="post" action="{{ route('api.player.model.save') }}">
        {{ csrf_field() }}

        @include('api.includes.select', [
          'name' => 'pid',
          'label' => 'Spieler',
          'entries' => $players,
          'currentSelect' => $currentPlayer,
        ])

        <div class="form-group">
            <label for="model">Model</label>
            <select class="form-control" id="model" name="model">
                <option value="a_c_chimp">
                    Affe (Chimp)
                </option>
                <option value="a_c_rhesus">
                    Affe (Rhesus)
                </option>
                <option value="a_c_coyote">
                    Coyote
                </option>
                <option value="a_c_dolphin">
                    Delphine
                </option>
                <option value="a_c_fish">
                    Fisch
                </option>
                <option value="a_c_sharkhammer">
                    Hai (Hammer)
                </option>
                <option value="a_c_sharktiger">
                    Hai (Tiger)
                </option>
                <option value="a_c_hen">
                    Huhn
                </option>
                <option value="a_c_chop">
                    Hund (Chop)
                </option>
                <option value="a_c_husky">
                    Hund (Husky)
                </option>
                <option value="a_c_poodle">
                    Hund (Pudel)
                </option>
                <option value="a_c_pug">
                    Hund (Pug)
                </option>
                <option value="a_c_retriever">
                    Hund (Retriever)
                </option>
                <option value="a_c_rottweiler">
                    Hund (Rotweiler)
                </option>
                <option value="a_c_shepherd">
                    Hund (Shepherd)
                </option>
                <option value="a_c_westy">
                    Hund (Westy)
                </option>
                <option value="a_c_chickenhawk">
                    Hühnerfalke
                </option>
                <option value="a_c_cat_01">
                    Katze
                </option>
                <option value="a_c_mtlion">
                    Katze (Berg)
                </option>
                <option value="a_c_panther">
                    Katze (Panther)
                </option>
                <option value="a_c_rabbit_01">
                    Hase
                </option>
                <option value="a_c_cow">
                    Kuh
                </option>
                <option value="a_c_cormorant">
                    Kormoran
                </option>
                <option value="a_c_crow">
                    Krähe
                </option>
                <option value="a_c_pigeon">
                    Pigeon
                </option>
                <option value="a_c_rat">
                    Ratte
                </option>
                <option value="a_c_deer">
                    Reh
                </option>
                <option value="a_c_stingray">
                    Rochen
                </option>
                <option value="a_c_pig">
                    Schwein
                </option>
                <option value="a_c_seagull">
                    Seemöwe
                </option>
                <option value="a_c_humpback">
                    Wal (Buckel)
                </option>
                <option value="a_c_killerwhale">
                    Wal (Killer)
                </option>
                <option value="a_c_boar">
                    Wildschwein
                </option>

                <option value="u_m_y_corpse_01">
                    Skelett
                </option>
                <option value="reset">
                    Reset
                </option>
            </select>
        </div>
        <button type="submit" class="btn btn-danger">Bestätigen</button>
    </form>
    <br/>
@endsection

