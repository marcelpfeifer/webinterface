class News {

    /**
     * @public
     */
    create() {
        var quill = new Quill('#editor', {
            theme: 'snow'
        });

        var form = document.getElementById('newsForm');
        form.addEventListener('submit', function () {
            let value = document.getElementsByClassName('ql-editor')[0].innerHTML;
            let field = document.getElementById('description');
            field.value = value;
        });

    }

    /**
     * @public
     */
    edit() {
        var quill = new Quill('#editor', {
            theme: 'snow'
        });

        var form = document.getElementById('newsForm');
        form.addEventListener('submit', function () {
            let value = document.getElementsByClassName('ql-editor')[0].innerHTML;
            let field = document.getElementById('description');
            field.value = value;
        });

    }
}

/**
 * Rufe die Klasse auf
 */
$(document).ready(function () {
    if (window.location.href.indexOf("auth/news/create") > -1) {
        let news = new News();
        news.create();
    }
    if (window.location.href.indexOf("auth/news/edit") > -1) {
        let news = new News();
        news.edit();
    }

});
