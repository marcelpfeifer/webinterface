<?php
/**
 * ServerLogController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 24.09.2021
 * Time: 17:21
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Libraries\Frontend\Admin\ServerLog\Index;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\View\View;

class ServerLogController extends Controller
{

    /**
     * @return View
     */
    public function index(Request $request): View
    {
        // Validiere die Anfrage
        $validator = new \App\Http\Request\Admin\ServerLog\Index($request);
        $validatedData = $validator->validate();
        if ($validatedData->fails()) {
            return \view('admin.index')
                ->with('messages', $validator->getErrorsAsMessageArray($validatedData));
        }

        /**
         * @var $requestDto \App\Http\Request\Dto\Admin\ServerLog\Index
         */
        $requestDto = $validator->toDto();

        $viewData = Index::getViewData($requestDto->getFilter());

        return view(
            'admin.serverLog.index',
            [
                'viewData' => $viewData,
                'filter'   => $requestDto->getFilter(),
            ]
        );
    }

    /**
     * @return RedirectResponse
     */
    public function reload(): RedirectResponse
    {
        $messages = [];

        Artisan::call('serverLog:copy');

        $messages[] = (new Message())
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS)
            ->setMessage('Erfolgreich aktualisiert');
        return redirect()->route('admin.serverLog.index')->with('messages', $messages);
    }
}
