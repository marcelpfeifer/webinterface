<?php
/**
 * Update
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 12.10.2021
 * Time: 15:44
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Frontend\Admin\VehicleShopUnregistered;


use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Model\Dto\ShopUnregisteredVehicles;
use App\Model\VehicleStore;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class Update
{
    /**
     * @param \App\Http\Request\Dto\Admin\ShopUnregisteredVehicles\Update $requestDto
     * @return bool
     */
    public static function save(\App\Http\Request\Dto\Admin\ShopUnregisteredVehicles\Update $requestDto): bool
    {
        $entry = \App\Model\ShopUnregisteredVehicles::find($requestDto->getId());
        $oldDto = \App\Model\ShopUnregisteredVehicles::entryAsDto($entry);
        $dto = (new ShopUnregisteredVehicles())
            ->setId($requestDto->getId())
            ->setModel($requestDto->getModel())
            ->setStock($requestDto->getStock())
            ->setPosX($requestDto->getPositionX())
            ->setPosY($requestDto->getPositionY())
            ->setPosZ($requestDto->getPositionZ())
            ->setRotX($requestDto->getRotationX())
            ->setRotY($requestDto->getRotationY())
            ->setRotZ($requestDto->getRotationZ())
            ->setMapId($requestDto->getMapId())
            ->setNpcPosX($requestDto->getNpcPositionX())
            ->setNpcPosY($requestDto->getNpcPositionY())
            ->setNpcPosZ($requestDto->getNpcPositionZ())
            ->setNpcRot($requestDto->getNpcRotation())
            ->setNpcModel($requestDto->getNpcModel())
            ->setUpdatedAt(Carbon::now());

        Logging::update(Name::VEHICLE_STORE_UNREGISTERED, Auth::id(), $oldDto, $dto);

        return (new \App\Model\ShopUnregisteredVehicles())->updateEntry($dto);
    }
}
