<?php
/**
 * PointsController
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 06.09.2020
 * Time: 11:16
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\Ban;

use App\Dto\Message;
use App\Http\Controllers\Controller;
use App\Libraries\Logging\Enum\Name;
use App\Libraries\Logging\Logging;
use App\Model\WebAdminBanPoints;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class PointsController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        $entries = [];
        foreach (WebAdminBanPoints::all() as $entry) {
            $entries[] = WebAdminBanPoints::entryAsDto($entry);
        }
        return \view(
            'admin.ban.points.index',
            [
                'entries' => $entries,
            ]
        );
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return \view('admin.ban.points.create', []);
    }

    /**
     * @return View
     */
    public function edit(Request $request, int $id): View
    {
        $dto = null;
        $entry = WebAdminBanPoints::find($id);
        if ($entry) {
            $dto = WebAdminBanPoints::entryAsDto($entry);
        }
        return \view(
            'admin.ban.points.edit',
            [
                'dto' => $dto,
            ]
        );
    }

    /**
     * @return RedirectResponse
     */
    public function save(Request $request): RedirectResponse
    {
        $dto = (new \App\Model\Dto\WebAdminBanPoints())
            ->setName($request->input('name'))
            ->setPoints($request->input('points'))
            ->setLength($request->input('length'))
            ->setAdditional($request->input('additional'));

        Logging::insert(Name::BANS, Auth::id(), $dto);
        (new WebAdminBanPoints())->insertEntry($dto);

        $messages[] = (new Message())
            ->setMessage("Der Eintrag wurde angelegt")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.ban.points.index')->with('messages', $messages);
    }

    /**
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $entry = WebAdminBanPoints::find($id);
        if (!$entry) {
            $messages[] = (new Message())
                ->setMessage("Der Eintrag wurde nicht gefunden")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
            return redirect()->route('admin.ban.points.index')->with('messages', $messages);
        }

        $oldDto = WebAdminBanPoints::entryAsDto($entry);
        $dto = (clone $oldDto)->setName($request->input('name'))
            ->setPoints($request->input('points'))
            ->setLength($request->input('length'))
            ->setAdditional($request->input('additional'));

        Logging::update(Name::BANS, Auth::id(), $oldDto, $dto);
        (new WebAdminBanPoints())->updateEntry($dto);

        $messages[] = (new Message())
            ->setMessage("Der Eintrag wurde aktualisiert")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.ban.points.index')->with('messages', $messages);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function delete(Request $request, int $id): RedirectResponse
    {
        $entry = WebAdminBanPoints::find($id);
        if (!($entry)) {
            $messages[] = (new Message())
                ->setMessage("Der Eintrag wurde nicht gefunden")
                ->setStatus(\App\Libraries\Frontend\Enum\Message::ERROR);
            return redirect()->route('admin.ban.points.index');
        }

        $dto = WebAdminBanPoints::entryAsDto($entry);
        (new WebAdminBanPoints())->deleteEntry($dto);
        Logging::delete(Name::BANS, Auth::id(), $dto);

        $messages[] = (new Message())
            ->setMessage("Der Eintrag wurde gelöscht")
            ->setStatus(\App\Libraries\Frontend\Enum\Message::SUCCESS);
        return redirect()->route('admin.ban.points.index')->with('messages', $messages);
    }
}
