<div class="col-12 col-md-3">
    <div class="card bg-white border-danger text-secondary">
        <div class="card-header border-danger bg-danger text-white">
            <i class="fas fa-bars"></i>
            Navigation
        </div>
        <div class="card-body text-dark">
            <h4 class="font-italic border-bottom border-danger">Spieler</h4>
            <ul class="nav flex-column">
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API_TP)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('api.player.tp.index') }}">
                        <i class="fas text-danger fa-map-signs"></i>
                        Teleportieren
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API_INVISIBLE)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('api.player.vanish.index') }}">
                        <i class="fas text-danger fa-smog"></i>
                        Unsichtbar
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API_FLY)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('api.player.fly.index') }}">
                        <i class="fab text-danger fa-fly"></i>
                        Fliegen
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API_REVIVE)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('api.player.revive.index') }}">
                        <i class="fas text-danger fa-cross"></i>
                        Wiederbeleben
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API_GIVE_ITEM)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('api.player.addItem.index') }}">
                        <i class="fas text-danger fa-box"></i>
                        Item geben
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API_GIVE_MONEY)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('api.player.giveMoney.index') }}">
                        <i class="fas text-danger fa-money-bill"></i>
                        Geld geben
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API_REMOVE_MONEY)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('api.player.removeMoney.index') }}">
                        <i class="fas text-danger fa-money-bill"></i>
                        Geld entfernen
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API_SET_MONEY)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('api.player.setMoney.index') }}">
                        <i class="fas text-danger fa-money-bill"></i>
                        Geld setzen
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API_DIMENSION)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('api.player.dimension.index') }}">
                        <i class="fas text-danger fa-expand-arrows-alt"></i>
                        Dimension setzen
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API_GOD_MODE)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('api.player.godMode.index') }}">
                        <i class="fas text-danger fa-bible"></i>
                        Godmode setzen
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API_FACTION)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('api.player.faction.index') }}">
                        <i class="fas text-danger fa-ambulance"></i>
                        Fraktion geben
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API_MODEL)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('api.player.model.index') }}">
                        <i class="fas text-danger fa-paw"></i>
                        Model setzen
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API_KICK)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('api.player.kick.index') }}">
                        <i class="far text-danger fa-futbol"></i>
                        Kicken
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API_SPECTATE)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('api.player.spectate.index') }}">
                        <i class="fab text-danger fa-watchman-monitoring"></i>
                        Beobachten
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API_FREEZE)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('api.player.freeze.index') }}">
                        <i class="fas text-danger fa-icicles"></i>
                        Freeze
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API_KILL)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('api.player.kill.index') }}">
                        <i class="fas text-danger fa-skull-crossbones"></i>
                        Töten
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API_PRISON)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('api.player.unJail.index') }}">
                        <i class="fas text-danger fa-dungeon"></i>
                        Aus dem Gefängnis holen
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API_NAMES)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('api.player.showNames.index') }}">
                        <i class="fas text-danger fa-signature"></i>
                        Namen anzeigen
                    </a>
                </li>
                @endpermission
            </ul>

            <h4 class="font-italic border-bottom border-danger">Fahrzeug</h4>
            <ul class="nav flex-column">
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API_REPAIR_ID)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('api.vehicle.repairId.index') }}">
                        <i class="fas text-danger fa-car"></i>
                        Reparieren per ID
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API_REPAIR_DRIVER)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('api.vehicle.repair.index') }}">
                        <i class="fas text-danger fa-car"></i>
                        Reparieren als Fahrer
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API_PARK)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('api.vehicle.park.index') }}">
                        <i class="fas text-danger fa-car"></i>
                        Einparken
                    </a>
                </li>
                @endpermission
            </ul>

            <h4 class="font-italic border-bottom border-danger">Server</h4>
            <ul class="nav flex-column">
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API_MESSAGE)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('api.server.message.index') }}">
                        <i class="fas text-danger fa-bullhorn"></i>
                        Message
                    </a>
                </li>
                @endpermission
                @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_API_SERVER)
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="{{ route('api.server.server.index') }}">
                        <i class="fas text-danger fa-play-circle"></i>
                        Server
                    </a>
                </li>
                @endpermission
            </ul>

            @permission(\App\Libraries\Permission\Enum\Permission::ACCESS_BACKEND)
            <div class="row mt-1 border-top border-danger">
                <div class="col-12 mt-1 text-center">
                    <a class="btn text-center btn-dark" href="{{ route('admin.index') }}">Zum Admin</a>
                </div>
            </div>
            @endpermission
        </div>
    </div>
</div>
