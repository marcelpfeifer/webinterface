<?php
/**
 * WebMailAddressToAccount
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 12.08.2021
 * Time: 11:41
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class WebMailAddressToAccount extends Model
{

    /**
     * @var string
     */
    public const COLUMN_WEB_MAIL_ADDRESS_ID = 'webMailAddressId';

    /**
     * @var string
     */
    public const COLUMN_ACCOUNT_ID = 'accountId';

    /**
     * @var string
     */
    public const COLUMN_OWNER = 'owner';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'webMailAddressToAccount';

    /**
     * @return BelongsTo
     */
    public function webMailAddress(): BelongsTo
    {
        return $this->belongsTo(WebMailAddress::class, self::COLUMN_WEB_MAIL_ADDRESS_ID, WebMailAddress::COLUMN_ID);
    }

    /**
     * @return BelongsTo
     */
    public function account(): BelongsTo
    {
        return $this->belongsTo(Account::class, self::COLUMN_ACCOUNT_ID, Account::COLUMN_ID);
    }

    /**
     * @param int $addressId
     * @return \App\Model\Dto\Character[]
     */
    public function getEntriesByAddressId(int $addressId)
    {
        $entries = $this->where(self::COLUMN_WEB_MAIL_ADDRESS_ID, $addressId)->get();
        $result = [];

        foreach ($entries as $entry) {
            $result[] = Character::entryAsDto($entry->account->character);
        }
        return $result;
    }

    /**
     * @param int $addressId
     * @param int $accountId
     * @return mixed
     */
    public function getEntryByAddressIdAndAccountId(int $addressId, int $accountId)
    {
        return $this->where(self::COLUMN_WEB_MAIL_ADDRESS_ID, $addressId)
            ->where(self::COLUMN_ACCOUNT_ID, $accountId)
            ->first();
    }

    /**
     * @param Dto\WebMailAddressToAccount $dto
     */
    public function insertEntry(Dto\WebMailAddressToAccount $dto)
    {
        $this->insert(
            [
                self::COLUMN_WEB_MAIL_ADDRESS_ID => $dto->getWebMailAddressId(),
                self::COLUMN_ACCOUNT_ID          => $dto->getAccountId(),
                self::COLUMN_OWNER               => $dto->isOwner(),
            ]
        );
    }

    /**
     * @param Dto\WebMailAddressToAccount $dto
     * @return bool
     */
    public function deleteEntry(\App\Model\Dto\WebMailAddressToAccount $dto): bool
    {
        return $this->where(self::COLUMN_WEB_MAIL_ADDRESS_ID, $dto->getWebMailAddressId())
            ->where(self::COLUMN_ACCOUNT_ID, $dto->getAccountId())
            ->delete();
    }

    /**
     * @param WebMailAddressToAccount $entry
     * @return Dto\WebMailAddressToAccount
     */
    public static function entryAsDto(WebMailAddressToAccount $entry): Dto\WebMailAddressToAccount
    {
        return (new Dto\WebMailAddressToAccount())
            ->setWebMailAddressId($entry->{self::COLUMN_WEB_MAIL_ADDRESS_ID})
            ->setAccountId($entry->{self::COLUMN_ACCOUNT_ID})
            ->setOwner($entry->{self::COLUMN_OWNER});
    }
}
