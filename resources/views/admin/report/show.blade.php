<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 18.09.2020
 * Time: 17:46
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */
?>
@extends('admin.layout.app')
@section('title')
    <i class="fas fa-bug"></i>
    Reportübersicht
    <a class="btn btn-dark btn-sm float-right"
       href="{{ route('admin.report.index') }}"
       role="button"
    >
        Zurück
    </a>
    @if($report)
        <div class="float-right mr-2">
            <form action="{{ route('admin.report.delete', $report->getId())}}" method="POST">
                <input type="hidden" name="_method" value="DELETE">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="submit" class="btn btn-sm btn-danger" value="Löschen"/>
            </form>
        </div>
    @endif
@endsection
@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Spielername</th>
            <th scope="col">Distanz</th>
        </tr>
        </thead>
        <tbody>
        @foreach($reportValues as $reportValue)
            <tr>
                <th scope="row">{{ $reportValue->getReportValue()->getId() }}</th>
                <td>{{ $reportValue->getCharacter() ? $reportValue->getCharacter()->getName() : $reportValue->getReportValue()->getTargetId() }}</td>
                <td>{{ $reportValue->getReportValue()->getDistance() }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
