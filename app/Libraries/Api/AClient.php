<?php
/**
 * AClient
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 04.04.2020
 * Time: 19:13
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libraries\Api;

use App\Libraries\LoggingApi\LoggingApi;
use Illuminate\Support\Facades\Log;
use Psr\Http\Message\ResponseInterface;

abstract class AClient
{

    /**
     * @param string $method
     * @param string $url
     * @param array $options
     * @return ResponseInterface|null
     */
    public static function sendRequest(string $method, string $url, array $options, bool $logging = true): ?ResponseInterface
    {
        if($logging) {
            // Speichere die Anfrage
            LoggingApi::log($url, $options);
        }

        try {
            $client = new \GuzzleHttp\Client();
            $result = $client->request($method, $url, $options);
        } catch (\Throwable $exception) {
            Log::error(print_r($exception->getMessage(), true));
            return null;
        }

        return $result;
    }
}
