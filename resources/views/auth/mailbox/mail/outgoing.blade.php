<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 17.10.2020
 * Time: 20:05
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

/**
 * @var $mails \App\Model\Dto\WebMail[]
 */

?>
@extends('layouts.app')
@section('title')
    <i class="fas fa-envelope"></i>
    Postfachausgang: {{ $dto->getEmail() }}
    <a class="btn btn-secondary btn-sm float-right"
       href="{{ route('auth.mailbox.mail.create', $webMailAddressId) }}"
       role="button"
    >
        Neue Email
    </a>

@endsection
@section('content')
    <div class="row">
        <div class="col-2">
            <div class="row">
                <a href="{{ route('auth.mailbox.mail.index', $webMailAddressId) }}"
                   class="col-12 border p-2">
                    Eingang
                </a>
                <a href="{{ route('auth.mailbox.mail.outgoing', $webMailAddressId) }}" class="col-12 border p-2 bg-primary text-white">
                    Ausgang
                </a>
            </div>
        </div>
        <div class="col-10">
            <table class="table table-striped border">
                <thead>
                <tr>
                    <th scope="col">Betreff</th>
                    <th scope="col">An</th>
                    <th scope="col">Datum</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($mails as $mail)
                    <tr class="{{ $mail->isSeen() ? '' : 'font-weight-bold' }}">
                        <td>{{ $mail->getTitle() }}</td>
                        <td>{{ $mail->getReceiver() }}</td>
                        <td>{{ $mail->getCreatedAt()->format('Y-m-d H:i:s') }}</td>
                        <td>
                            <a href="{{ route('auth.mailbox.mail.show',[$webMailAddressId, $mail->getId()]) }}">
                                Anzeigen
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
