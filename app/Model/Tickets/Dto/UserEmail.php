<?php
/**
 * UserEmail
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 27.08.2020
 * Time: 19:30
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Model\Tickets\Dto;


class UserEmail
{
    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var int
     */
    private $userId = 0;

    /**
     * @var int
     */
    private $flags = 0;

    /**
     * @var string
     */
    private $address = '';

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return UserEmail
     */
    public function setId(int $id): UserEmail
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return UserEmail
     */
    public function setUserId(int $userId): UserEmail
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return int
     */
    public function getFlags(): int
    {
        return $this->flags;
    }

    /**
     * @param int $flags
     * @return UserEmail
     */
    public function setFlags(int $flags): UserEmail
    {
        $this->flags = $flags;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return UserEmail
     */
    public function setAddress(string $address): UserEmail
    {
        $this->address = $address;
        return $this;
    }
}
