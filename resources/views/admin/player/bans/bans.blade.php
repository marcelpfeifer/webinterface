<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 05.05.2020
 * Time: 17:21
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

?>
@extends('admin.layout.app')
@section('title')
    Bans
    <a class="btn btn-dark btn-sm float-right"
       href="{{ route('admin.player.bans.index', $accountId) }}"
       role="button"
    >
        Zurück
    </a>
@endsection
@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">TimeBan</th>
            <th scope="col">Ablaufdatum</th>
            <th scope="col">Grund</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($bans as $ban)
            <tr>
                <th scope="row">{{ $ban->getId() }}</th>
                <td>{{ $ban->getTimeBan() }}</td>
                <td>{{ $ban->getExpire() }}</td>
                <td>{{ $ban->getReason() }}</td>
                <td>
                    <form action="{{ route('admin.player.bans.deleteServerBans', $ban->getId()) }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE"/>
                        <button class="btn btn-danger" type="submit">Löschen</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

